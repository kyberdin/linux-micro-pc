<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.0.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="15" fill="11" visible="no" active="no"/>
<layer number="3" name="Route3" color="13" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="11" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="14" fill="11" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Bemassung" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="bot_pads" color="7" fill="5" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="S_DOKU" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="BIT_Library">
<packages>
<package name="QFN-48">
<description>QFN-48 package - Used primarily for TPS65217CRSLR power controller</description>
<smd name="PAD" x="-0.1143" y="0.03301875" dx="4.0386" dy="4.041140625" layer="1" rot="R90"/>
<smd name="48" x="-2.04340625" y="2.858240625" dx="0.20828125" dy="1.016" layer="1"/>
<smd name="47" x="-1.647165625" y="2.858240625" dx="0.20828125" dy="1.016" layer="1"/>
<smd name="46" x="-1.250671875" y="2.858240625" dx="0.20828125" dy="1.016" layer="1"/>
<smd name="45" x="-0.85443125" y="2.858240625" dx="0.20828125" dy="1.016" layer="1"/>
<smd name="44" x="-0.462765625" y="2.858240625" dx="0.20828125" dy="1.016" layer="1"/>
<smd name="43" x="-0.066525" y="2.858240625" dx="0.20828125" dy="1.016" layer="1"/>
<smd name="42" x="0.328953125" y="2.858240625" dx="0.20828125" dy="1.016" layer="1"/>
<smd name="41" x="0.72519375" y="2.858240625" dx="0.20828125" dy="1.016" layer="1"/>
<smd name="40" x="1.11813125" y="2.858240625" dx="0.20828125" dy="1.016" layer="1"/>
<smd name="39" x="1.514371875" y="2.858240625" dx="0.20828125" dy="1.016" layer="1"/>
<smd name="38" x="1.9168125" y="2.858240625" dx="0.20828125" dy="1.016" layer="1"/>
<smd name="37" x="2.317621875" y="2.858240625" dx="0.20828125" dy="1.016" layer="1"/>
<smd name="12" x="-2.963803125" y="-2.357478125" dx="0.20828125" dy="1.016" layer="1" rot="R90"/>
<smd name="11" x="-2.963803125" y="-1.9612375" dx="0.20828125" dy="1.016" layer="1" rot="R90"/>
<smd name="10" x="-2.963803125" y="-1.56474375" dx="0.20828125" dy="1.016" layer="1" rot="R90"/>
<smd name="9" x="-2.963803125" y="-1.168503125" dx="0.20828125" dy="1.016" layer="1" rot="R90"/>
<smd name="8" x="-2.963803125" y="-0.7768375" dx="0.20828125" dy="1.016" layer="1" rot="R90"/>
<smd name="7" x="-2.963803125" y="-0.380596875" dx="0.20828125" dy="1.016" layer="1" rot="R90"/>
<smd name="6" x="-2.963803125" y="0.01488125" dx="0.20828125" dy="1.016" layer="1" rot="R90"/>
<smd name="5" x="-2.963803125" y="0.411121875" dx="0.20828125" dy="1.016" layer="1" rot="R90"/>
<smd name="4" x="-2.963803125" y="0.804059375" dx="0.20828125" dy="1.016" layer="1" rot="R90"/>
<smd name="3" x="-2.963803125" y="1.2003" dx="0.20828125" dy="1.016" layer="1" rot="R90"/>
<smd name="2" x="-2.963803125" y="1.602740625" dx="0.20828125" dy="1.016" layer="1" rot="R90"/>
<smd name="1" x="-2.963803125" y="2.00355" dx="0.20828125" dy="1.016" layer="1" rot="R90"/>
<smd name="24" x="2.320775" y="-3.244753125" dx="0.20828125" dy="1.016" layer="1" rot="R180"/>
<smd name="23" x="1.924534375" y="-3.244753125" dx="0.20828125" dy="1.016" layer="1" rot="R180"/>
<smd name="22" x="1.528040625" y="-3.244753125" dx="0.20828125" dy="1.016" layer="1" rot="R180"/>
<smd name="21" x="1.1318" y="-3.244753125" dx="0.20828125" dy="1.016" layer="1" rot="R180"/>
<smd name="20" x="0.740134375" y="-3.244753125" dx="0.20828125" dy="1.016" layer="1" rot="R180"/>
<smd name="19" x="0.34389375" y="-3.244753125" dx="0.20828125" dy="1.016" layer="1" rot="R180"/>
<smd name="18" x="-0.051584375" y="-3.244753125" dx="0.20828125" dy="1.016" layer="1" rot="R180"/>
<smd name="17" x="-0.447825" y="-3.244753125" dx="0.20828125" dy="1.016" layer="1" rot="R180"/>
<smd name="16" x="-0.8407625" y="-3.244753125" dx="0.20828125" dy="1.016" layer="1" rot="R180"/>
<smd name="15" x="-1.237003125" y="-3.244753125" dx="0.20828125" dy="1.016" layer="1" rot="R180"/>
<smd name="14" x="-1.63944375" y="-3.244753125" dx="0.20828125" dy="1.016" layer="1" rot="R180"/>
<smd name="13" x="-2.040253125" y="-3.244753125" dx="0.20828125" dy="1.016" layer="1" rot="R180"/>
<smd name="36" x="3.17365625" y="2.00383125" dx="0.20828125" dy="1.016" layer="1" rot="R270"/>
<smd name="35" x="3.17365625" y="1.607590625" dx="0.20828125" dy="1.016" layer="1" rot="R270"/>
<smd name="34" x="3.17365625" y="1.211096875" dx="0.20828125" dy="1.016" layer="1" rot="R270"/>
<smd name="33" x="3.17365625" y="0.81485625" dx="0.20828125" dy="1.016" layer="1" rot="R270"/>
<smd name="32" x="3.17365625" y="0.423190625" dx="0.20828125" dy="1.016" layer="1" rot="R270"/>
<smd name="31" x="3.17365625" y="0.02695" dx="0.20828125" dy="1.016" layer="1" rot="R270"/>
<smd name="30" x="3.17365625" y="-0.368528125" dx="0.20828125" dy="1.016" layer="1" rot="R270"/>
<smd name="29" x="3.17365625" y="-0.76476875" dx="0.20828125" dy="1.016" layer="1" rot="R270"/>
<smd name="28" x="3.17365625" y="-1.15770625" dx="0.20828125" dy="1.016" layer="1" rot="R270"/>
<smd name="27" x="3.17365625" y="-1.553946875" dx="0.20828125" dy="1.016" layer="1" rot="R270"/>
<smd name="26" x="3.17365625" y="-1.9563875" dx="0.20828125" dy="1.016" layer="1" rot="R270"/>
<smd name="25" x="3.17365625" y="-2.357196875" dx="0.20828125" dy="1.016" layer="1" rot="R270"/>
<wire x1="-2.8575" y1="-2.5654" x2="-2.8575" y2="2.2098" width="0.0508" layer="51"/>
<wire x1="2.53888125" y1="2.8194" x2="-2.23631875" y2="2.8194" width="0.0508" layer="51"/>
<wire x1="-2.2479" y1="-3.175" x2="2.5273" y2="-3.175" width="0.0508" layer="51"/>
<wire x1="3.1369" y1="2.2247875" x2="3.1369" y2="-2.5504125" width="0.0508" layer="51"/>
<wire x1="-2.8575" y1="-3.175" x2="-2.8575" y2="-2.6035" width="0.0508" layer="21"/>
<wire x1="-2.8575" y1="-3.175" x2="-2.286" y2="-3.175" width="0.0508" layer="21"/>
<wire x1="-2.8575" y1="2.2606" x2="-2.8575" y2="2.8194" width="0.0508" layer="21"/>
<wire x1="-2.8575" y1="2.8194" x2="-2.286" y2="2.8194" width="0.0508" layer="21"/>
<wire x1="2.5654" y1="2.8194" x2="3.1369" y2="2.8194" width="0.0508" layer="21"/>
<wire x1="3.1369" y1="2.2606" x2="3.1369" y2="2.8194" width="0.0508" layer="21"/>
<wire x1="2.5654" y1="-3.175" x2="3.1369" y2="-3.175" width="0.0508" layer="21"/>
<wire x1="3.1369" y1="-3.175" x2="3.1369" y2="-2.6035" width="0.0508" layer="21"/>
<text x="-3.300553125" y="2.739340625" size="0.6096" layer="21" ratio="1">1</text>
<circle x="-2.539340625" y="2.521075" radius="0.15386875" width="0.127" layer="21"/>
<text x="3.089071875" y="2.791003125" size="0.508" layer="25" ratio="1">&gt;NAME</text>
</package>
<package name="DC-POWER-JACK-CUI">
<description>2.1X5.5MM</description>
<wire x1="7.14" y1="-2.01" x2="7.14" y2="-3.7" width="0.2032" layer="21"/>
<wire x1="7.2" y1="3.77" x2="7.2" y2="2.17" width="0.2032" layer="21"/>
<wire x1="-5.495" y1="-3.08" x2="1.78" y2="-3.08" width="0.2032" layer="51" style="shortdash"/>
<wire x1="-5.42" y1="3.26" x2="1.88" y2="3.26" width="0.2032" layer="51" style="shortdash"/>
<wire x1="-3.89" y1="-1.94" x2="0.78" y2="-1.94" width="0.2032" layer="51" style="shortdash"/>
<wire x1="-4.11" y1="1.96" x2="0.72" y2="1.93" width="0.2032" layer="51" style="shortdash"/>
<wire x1="-3.89" y1="-1.94" x2="-4.11" y2="1.96" width="0.2032" layer="51" curve="-166.57811"/>
<wire x1="5.6" y1="3.77" x2="7.2" y2="3.77" width="0.2032" layer="21"/>
<text x="-3.304" y="4.996" size="0.8128" layer="25" rot="R90">&gt;NAME</text>
<smd name="POS@0" x="-1.35" y="5.9" dx="2.8" dy="2.4" layer="1" rot="R270"/>
<wire x1="-6.35" y1="-4.5" x2="-6.35" y2="4.5" width="0.2032" layer="51"/>
<wire x1="8" y1="4.53" x2="-6.3" y2="4.53" width="0.2032" layer="51"/>
<wire x1="7.94" y1="-4.5" x2="-6.36" y2="-4.5" width="0.2032" layer="51"/>
<smd name="GND@1" x="4.75" y="-5.9" dx="2.8" dy="2.4" layer="1" rot="R270"/>
<smd name="GND@0" x="-1.35" y="-5.9" dx="2.8" dy="2.4" layer="1" rot="R270"/>
<smd name="POS@1" x="4.75" y="5.9" dx="2.8" dy="2.4" layer="1" rot="R270"/>
<wire x1="7.99" y1="-4.44" x2="7.99" y2="4.56" width="0.2032" layer="21"/>
<hole x="-1.35" y="0" drill="1.6"/>
<hole x="3.15" y="0" drill="1.6"/>
<wire x1="5.51" y1="-3.71" x2="7.14" y2="-3.7" width="0.2032" layer="21"/>
</package>
<package name="SMD-0805">
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
<text x="1.77165" y="-0.0254" size="0.4064" layer="25">&gt;NAME</text>
<wire x1="-1.64585" y1="0.88445" x2="1.64585" y2="0.88445" width="0.0508" layer="39"/>
<wire x1="-1.64585" y1="-0.89355" x2="1.64585" y2="-0.89355" width="0.0508" layer="39"/>
<wire x1="-1.64645" y1="-0.8902" x2="-1.64645" y2="0.88385" width="0.0508" layer="39"/>
<wire x1="1.6492" y1="-0.8902" x2="1.6492" y2="0.88385" width="0.0508" layer="39"/>
<wire x1="-1.6764" y1="0.93345" x2="-1.6764" y2="-0.9271" width="0.127" layer="21"/>
<wire x1="-1.6764" y1="-0.9271" x2="1.6764" y2="-0.9271" width="0.127" layer="21"/>
<wire x1="1.6764" y1="-0.9271" x2="1.6764" y2="0.93345" width="0.127" layer="21"/>
<wire x1="1.6764" y1="0.93345" x2="-1.6764" y2="0.93345" width="0.127" layer="21"/>
</package>
<package name="FUSE_PTGL">
<description>PTGL Thermistor Fuse (Through Hole) 10mmL x 6mmW x 20mmH, 14mmD</description>
<pad name="1" x="0" y="0" drill="0.8"/>
<pad name="2" x="9.9822" y="0" drill="0.8"/>
<wire x1="0" y1="1.27" x2="9.9695" y2="1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="9.9695" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.127" layer="21" curve="180"/>
<wire x1="9.9695" y1="-1.27" x2="9.9695" y2="1.27" width="0.127" layer="21" curve="180"/>
<text x="2.54" y="1.4605" size="0.508" layer="21">&gt;NAME</text>
<text x="2.4765" y="-2.032" size="0.508" layer="21">&gt;VALUE</text>
</package>
<package name="153CLV-1012_6.6X6.6">
<description>Aluminum Electrolytic Capacitor, 6.6mm x 6.6mm</description>
<wire x1="2.522" y1="-3.168" x2="-3.168" y2="-3.168" width="0.2032" layer="21"/>
<wire x1="-3.168" y1="-3.168" x2="-3.168" y2="3.453" width="0.2032" layer="51"/>
<wire x1="-3.168" y1="3.429" x2="2.522" y2="3.429" width="0.2032" layer="21"/>
<wire x1="3.429" y1="2.522" x2="3.429" y2="0.992" width="0.2032" layer="21"/>
<wire x1="3.422" y1="2.262" x2="3.422" y2="0.532" width="0.2032" layer="51"/>
<wire x1="3.422" y1="0.532" x2="3.422" y2="-2.268" width="0.2032" layer="51"/>
<wire x1="3.422" y1="-2.268" x2="2.522" y2="-3.168" width="0.2032" layer="21"/>
<wire x1="2.522" y1="3.429" x2="3.429" y2="2.522" width="0.2032" layer="21"/>
<wire x1="2.872" y1="-0.788" x2="2.872" y2="1.042" width="0.2032" layer="51" curve="36.869898"/>
<wire x1="-2.618" y1="-0.788" x2="-2.618" y2="1.042" width="0.2032" layer="51" curve="-36.869898"/>
<wire x1="-2.618" y1="-0.788" x2="2.872" y2="-0.788" width="0.2032" layer="21" curve="143.130102"/>
<wire x1="-2.618" y1="1.042" x2="2.872" y2="1.042" width="0.2032" layer="21" curve="-143.130102"/>
<smd name="-" x="-2.6895" y="0.1905" dx="3.81" dy="1.524" layer="1"/>
<text x="-3.1945" y="3.8155" size="0.508" layer="25">&gt;NAME</text>
<text x="-3.293" y="-3.9275" size="0.508" layer="27">&gt;VALUE</text>
<smd name="+" x="2.7715" y="0.1905" dx="3.81" dy="1.524" layer="1"/>
<wire x1="-1.143" y1="-2.4695" x2="-1.143" y2="2.7125" width="0.2032" layer="51"/>
<wire x1="4.3815" y1="1.2135" x2="4.3815" y2="2.5855" width="0.2032" layer="21"/>
<wire x1="5.073" y1="1.905" x2="3.701" y2="1.905" width="0.2032" layer="21"/>
</package>
<package name="153CLV-1012_4.3X4.3">
<description>Aluminum Electrolytic Capacitor, 4.3mm x 4.3mm</description>
<wire x1="1.125" y1="-2.0885" x2="-2.025" y2="-2.0885" width="0.2032" layer="21"/>
<wire x1="-2.025" y1="-2.0885" x2="-2.025" y2="2.1971" width="0.2032" layer="51"/>
<wire x1="-2.025" y1="2.1971" x2="1.125" y2="2.1971" width="0.2032" layer="21"/>
<wire x1="2.2606" y1="1.0615" x2="2.2606" y2="-1.1885" width="0.2032" layer="51"/>
<wire x1="2.2606" y1="-1.1885" x2="1.125" y2="-2.0885" width="0.2032" layer="21"/>
<wire x1="1.125" y1="2.1971" x2="2.2606" y2="1.0615" width="0.2032" layer="21"/>
<wire x1="1.475" y1="-0.534" x2="1.475" y2="0.8515" width="0.2032" layer="51" curve="36.869898"/>
<wire x1="-1.475" y1="-0.4705" x2="-1.475" y2="0.8515" width="0.2032" layer="51" curve="-36.869898"/>
<wire x1="-1.475" y1="-0.534" x2="1.475" y2="-0.534" width="0.2032" layer="21" curve="143.130102"/>
<wire x1="-1.475" y1="0.8515" x2="1.475" y2="0.8515" width="0.2032" layer="21" curve="-143.130102"/>
<smd name="-" x="-1.864" y="0" dx="2.54" dy="1.524" layer="1"/>
<text x="-2.0515" y="2.4185" size="0.508" layer="25">&gt;NAME</text>
<smd name="+" x="1.8825" y="0" dx="2.54" dy="1.524" layer="1"/>
<wire x1="-0.762" y1="-1.39" x2="-0.762" y2="1.6965" width="0.2032" layer="21"/>
<wire x1="2.6035" y1="1.15" x2="2.6035" y2="2.522" width="0.2032" layer="21"/>
<wire x1="3.295" y1="1.8415" x2="1.923" y2="1.8415" width="0.2032" layer="21"/>
</package>
<package name="153CLV-1012_5.3X5.3">
<wire x1="1.887" y1="-2.8505" x2="-2.533" y2="-2.8505" width="0.2032" layer="21"/>
<wire x1="-2.533" y1="-2.8505" x2="-2.533" y2="2.432734375" width="0.2032" layer="51"/>
<wire x1="-2.533" y1="2.432734375" x2="1.887" y2="2.432734375" width="0.2032" layer="21"/>
<wire x1="2.7432" y1="1.5695" x2="2.7432" y2="-1.9505" width="0.2032" layer="51"/>
<wire x1="2.7432" y1="-1.9505" x2="1.887" y2="-2.8505" width="0.2032" layer="21"/>
<wire x1="1.887" y1="2.432734375" x2="2.7432" y2="1.5695" width="0.2032" layer="21"/>
<smd name="-" x="-2.0545" y="-0.0635" dx="2.54" dy="1.524" layer="1"/>
<smd name="+" x="2.1365" y="0" dx="2.54" dy="1.524" layer="1"/>
<wire x1="3.3655" y1="1.023" x2="3.3655" y2="1.76" width="0.2032" layer="21"/>
<wire x1="3.7395" y1="1.397" x2="2.939" y2="1.397" width="0.2032" layer="21"/>
<rectangle x1="1.68783125" y1="-1.15696875" x2="2.33553125" y2="1.04266875" layer="51" rot="R90"/>
<rectangle x1="-2.12216875" y1="-1.15696875" x2="-1.47446875" y2="1.04266875" layer="51" rot="R90"/>
<text x="2.032" y="-3.302" size="0.4064" layer="25">&gt;NAME</text>
</package>
<package name="SOT23-5">
<wire x1="1.422" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="-0.81" x2="-1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<wire x1="1.422" y1="0.81" x2="1.422" y2="-0.81" width="0.1524" layer="21"/>
<text x="-1.905" y="-1.27" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="LED_5MM">
<wire x1="2.5654" y1="-1.8542" x2="2.5654" y2="1.9558" width="0.2032" layer="21"/>
<wire x1="2.5654" y1="-1.8542" x2="2.5654" y2="1.9558" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-1.1176" y1="0.0508" x2="0.0254" y2="1.1938" width="0.1524" layer="51" curve="-90"/>
<wire x1="0.0254" y1="-1.0922" x2="1.1684" y2="0.0508" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.6256" y1="0.0508" x2="0.0254" y2="1.7018" width="0.1524" layer="51" curve="-90"/>
<wire x1="0.0254" y1="-1.6002" x2="1.6764" y2="0.0508" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.1336" y1="0.0508" x2="0.0254" y2="2.2098" width="0.1524" layer="51" curve="-90"/>
<wire x1="0.0254" y1="-2.1082" x2="2.1844" y2="0.0508" width="0.1524" layer="51" curve="90"/>
<circle x="0.0254" y="0.0508" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.2446" y="0.0508" drill="0.8128" shape="octagon"/>
<pad name="C" x="1.2954" y="0.0508" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="3.6322" size="0.508" layer="21">&gt;NAME</text>
<text x="-2.1082" y="-4.064" size="0.508" layer="21">&gt;VALUE</text>
</package>
<package name="LED_ARRAY_9">
<description>Circular LED array</description>
<circle x="0" y="0" radius="12.192" width="0.4064" layer="51"/>
<wire x1="4.699" y1="1.905" x2="4.699" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="4.699" y1="1.905" x2="4.699" y2="-1.905" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="8.382" y1="0" x2="7.239" y2="-1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="7.239" y1="1.143" x2="6.096" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="8.89" y1="0" x2="7.239" y2="-1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="7.239" y1="1.651" x2="5.588" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="9.398" y1="0" x2="7.239" y2="-2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="7.239" y1="2.159" x2="5.08" y2="0" width="0.1524" layer="51" curve="90"/>
<circle x="7.239" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A8" x="8.509" y="0" drill="0.8128" shape="octagon" rot="R180"/>
<pad name="K8" x="5.969" y="0" drill="0.8128" shape="octagon" rot="R180"/>
<wire x1="-1.905" y1="-2.54" x2="1.905" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="1.905" y2="-2.54" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="0" y1="1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.143" y1="0" x2="0" y2="-1.143" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.651" y1="0" x2="0" y2="-1.651" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="-90"/>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A1" x="0" y="1.27" drill="0.8128" shape="octagon" rot="R270"/>
<pad name="K1" x="0" y="-1.27" drill="0.8128" shape="octagon" rot="R270"/>
<wire x1="1.905" y1="-4.699" x2="-1.905" y2="-4.699" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-4.699" x2="-1.905" y2="-4.699" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="0" y1="-8.382" x2="-1.143" y2="-7.239" width="0.1524" layer="51" curve="-90"/>
<wire x1="1.143" y1="-7.239" x2="0" y2="-6.096" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="-8.89" x2="-1.651" y2="-7.239" width="0.1524" layer="51" curve="-90"/>
<wire x1="1.651" y1="-7.239" x2="0" y2="-5.588" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="-9.398" x2="-2.159" y2="-7.239" width="0.1524" layer="51" curve="-90"/>
<wire x1="2.159" y1="-7.239" x2="0" y2="-5.08" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="-7.239" radius="2.54" width="0.1524" layer="21"/>
<pad name="A2" x="0" y="-8.509" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="K2" x="0" y="-5.969" drill="0.8128" shape="octagon" rot="R90"/>
<wire x1="-4.699" y1="-1.905" x2="-4.699" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-4.699" y1="-1.905" x2="-4.699" y2="1.905" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-8.382" y1="0" x2="-7.239" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="-7.239" y1="-1.143" x2="-6.096" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-8.89" y1="0" x2="-7.239" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="-7.239" y1="-1.651" x2="-5.588" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-9.398" y1="0" x2="-7.239" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="-7.239" y1="-2.159" x2="-5.08" y2="0" width="0.1524" layer="51" curve="90"/>
<circle x="-7.239" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A4" x="-8.509" y="0" drill="0.8128" shape="octagon"/>
<pad name="K4" x="-5.969" y="0" drill="0.8128" shape="octagon"/>
<wire x1="-1.905" y1="4.699" x2="1.905" y2="4.699" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="4.699" x2="1.905" y2="4.699" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="0" y1="8.382" x2="1.143" y2="7.239" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.143" y1="7.239" x2="0" y2="6.096" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="8.89" x2="1.651" y2="7.239" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.651" y1="7.239" x2="0" y2="5.588" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="9.398" x2="2.159" y2="7.239" width="0.1524" layer="51" curve="-90"/>
<wire x1="-2.159" y1="7.239" x2="0" y2="5.08" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="7.239" radius="2.54" width="0.1524" layer="21"/>
<pad name="A6" x="0" y="8.509" drill="0.8128" shape="octagon" rot="R270"/>
<pad name="K6" x="0" y="5.969" drill="0.8128" shape="octagon" rot="R270"/>
<wire x1="4.5770625" y1="-1.850296875" x2="1.882984375" y2="-4.544375" width="0.2032" layer="21"/>
<wire x1="4.5770625" y1="-1.850296875" x2="1.882984375" y2="-4.544375" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="5.834296875" y1="-5.801609375" x2="4.217853125" y2="-5.801609375" width="0.1524" layer="51" curve="-90"/>
<wire x1="5.834296875" y1="-4.1851625" x2="4.217853125" y2="-4.1851625" width="0.1524" layer="51" curve="90"/>
<wire x1="6.193509375" y1="-6.16081875" x2="3.858640625" y2="-6.16081875" width="0.1524" layer="51" curve="-90"/>
<wire x1="6.193509375" y1="-3.825953125" x2="3.858640625" y2="-3.825953125" width="0.1524" layer="51" curve="90"/>
<wire x1="6.55271875" y1="-6.52003125" x2="3.49943125" y2="-6.52003125" width="0.1524" layer="51" curve="-90"/>
<wire x1="6.55271875" y1="-3.46674375" x2="3.49943125" y2="-3.46674375" width="0.1524" layer="51" curve="90"/>
<circle x="5.026075" y="-4.9933875" radius="2.54" width="0.1524" layer="21"/>
<pad name="A9" x="5.9241" y="-5.8914125" drill="0.8128" shape="octagon" rot="R135"/>
<pad name="K9" x="4.12805" y="-4.0953625" drill="0.8128" shape="octagon" rot="R135"/>
<wire x1="1.948353125" y1="4.62779375" x2="4.64243125" y2="1.93371875" width="0.2032" layer="21"/>
<wire x1="1.948353125" y1="4.62779375" x2="4.64243125" y2="1.93371875" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="5.899665625" y1="5.88503125" x2="5.899665625" y2="4.268584375" width="0.1524" layer="51" curve="-90"/>
<wire x1="4.283221875" y1="5.88503125" x2="4.283221875" y2="4.268584375" width="0.1524" layer="51" curve="90"/>
<wire x1="6.258878125" y1="6.244240625" x2="6.258878125" y2="3.909375" width="0.1524" layer="51" curve="-90"/>
<wire x1="3.924009375" y1="6.244240625" x2="3.924009375" y2="3.909375" width="0.1524" layer="51" curve="90"/>
<wire x1="6.6180875" y1="6.60345" x2="6.6180875" y2="3.5501625" width="0.1524" layer="51" curve="-90"/>
<wire x1="3.5648" y1="6.60345" x2="3.5648" y2="3.5501625" width="0.1524" layer="51" curve="90"/>
<circle x="5.09144375" y="5.07680625" radius="2.54" width="0.1524" layer="21"/>
<pad name="A7" x="5.98946875" y="5.974834375" drill="0.8128" shape="octagon" rot="R225"/>
<pad name="K7" x="4.19341875" y="4.17878125" drill="0.8128" shape="octagon" rot="R225"/>
<wire x1="-4.65993125" y1="1.90609375" x2="-1.96585625" y2="4.60016875" width="0.2032" layer="21"/>
<wire x1="-4.65993125" y1="1.90609375" x2="-1.96585625" y2="4.60016875" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-5.91716875" y1="5.85740625" x2="-4.300721875" y2="5.85740625" width="0.1524" layer="51" curve="-90"/>
<wire x1="-5.91716875" y1="4.240959375" x2="-4.300721875" y2="4.240959375" width="0.1524" layer="51" curve="90"/>
<wire x1="-6.276378125" y1="6.216615625" x2="-3.9415125" y2="6.216615625" width="0.1524" layer="51" curve="-90"/>
<wire x1="-6.276378125" y1="3.88175" x2="-3.9415125" y2="3.88175" width="0.1524" layer="51" curve="90"/>
<wire x1="-6.6355875" y1="6.575828125" x2="-3.5823" y2="6.575828125" width="0.1524" layer="51" curve="-90"/>
<wire x1="-6.6355875" y1="3.522540625" x2="-3.5823" y2="3.522540625" width="0.1524" layer="51" curve="90"/>
<circle x="-5.10894375" y="5.049184375" radius="2.54" width="0.1524" layer="21"/>
<pad name="A5" x="-6.00696875" y="5.947209375" drill="0.8128" shape="octagon" rot="R45"/>
<pad name="K5" x="-4.21091875" y="4.15115625" drill="0.8128" shape="octagon" rot="R315"/>
<wire x1="-1.876053125" y1="-4.609196875" x2="-4.570128125" y2="-1.91511875" width="0.2032" layer="21"/>
<wire x1="-1.876053125" y1="-4.609196875" x2="-4.570128125" y2="-1.91511875" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-5.827365625" y1="-5.86643125" x2="-5.827365625" y2="-4.249984375" width="0.1524" layer="51" curve="-90"/>
<wire x1="-4.21091875" y1="-5.86643125" x2="-4.21091875" y2="-4.249984375" width="0.1524" layer="51" curve="90"/>
<wire x1="-6.186575" y1="-6.225640625" x2="-6.186575" y2="-3.890775" width="0.1524" layer="51" curve="-90"/>
<wire x1="-3.851709375" y1="-6.225640625" x2="-3.851709375" y2="-3.890775" width="0.1524" layer="51" curve="90"/>
<wire x1="-6.545784375" y1="-6.584853125" x2="-6.545784375" y2="-3.531565625" width="0.1524" layer="51" curve="-90"/>
<wire x1="-3.4925" y1="-6.584853125" x2="-3.4925" y2="-3.531565625" width="0.1524" layer="51" curve="90"/>
<circle x="-5.019140625" y="-5.058209375" radius="2.54" width="0.1524" layer="21"/>
<pad name="A3" x="-5.91716875" y="-5.956234375" drill="0.8128" shape="octagon" rot="R45"/>
<pad name="K3" x="-4.121115625" y="-4.160184375" drill="0.8128" shape="octagon" rot="R45"/>
<wire x1="-9.017" y1="0" x2="9.017" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="-9.017" x2="0" y2="9.017" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-6.35" x2="6.4008" y2="6.4008" width="0.127" layer="21"/>
<wire x1="6.35" y1="-6.35" x2="-6.4008" y2="6.4008" width="0.127" layer="21"/>
</package>
<package name="LED_0805_PIPE">
<description>LED (0805 pkg) with light pipe attachment</description>
<wire x1="-2.6" y1="2.59" x2="2.59" y2="2.59" width="0.1" layer="21"/>
<wire x1="2.59" y1="2.59" x2="2.58" y2="-2.64" width="0.1" layer="21"/>
<wire x1="2.58" y1="-2.64" x2="-2.61" y2="-2.64" width="0.1" layer="21"/>
<wire x1="-2.61" y1="-2.64" x2="-2.6" y2="2.59" width="0.1" layer="21"/>
<hole x="-1.89" y="1.89" drill="1"/>
<hole x="1.89" y="-1.89" drill="1"/>
<smd name="ANODE" x="-1.05" y="0" dx="1.19" dy="1.19" layer="1"/>
<smd name="CATHODE" x="1.05" y="0" dx="1.19" dy="1.19" layer="1"/>
</package>
<package name="LED_ARRAY_50DEG_1206">
<description>LED Array - 1206 SMT LEDs, 50deg view angle

&lt;p&gt; Light circles represent area of light on lens @ 50deg per LED, based on 0.35" distance (base of dome / top of package housing to lens, not board floor)</description>
<circle x="0" y="0" radius="11.43" width="0.127" layer="21"/>
<circle x="-3.4925" y="6.0452" radius="4.1402" width="0.0508" layer="51"/>
<circle x="3.4925" y="6.0452" radius="4.1402" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="4.1402" width="0.0508" layer="51"/>
<circle x="-3.4925" y="-6.0452" radius="4.1402" width="0.0508" layer="51"/>
<circle x="6.985" y="0" radius="4.1402" width="0.0508" layer="51"/>
<circle x="-6.985" y="0" radius="4.1402" width="0.0508" layer="51"/>
<circle x="3.4925" y="-6.0452" radius="4.1402" width="0.0508" layer="51"/>
<wire x1="-7.95" y1="0.787" x2="-6.02" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-7.95" y1="-0.787" x2="-6.02" y2="-0.787" width="0.1016" layer="51"/>
<smd name="A1" x="-8.385" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="C1" x="-5.585" y="0" dx="1.6" dy="1.8" layer="1"/>
<rectangle x1="-8.6868" y1="-0.8509" x2="-7.9367" y2="0.8491" layer="51"/>
<rectangle x1="-6.0333" y1="-0.8491" x2="-5.2832" y2="0.8509" layer="51"/>
<rectangle x1="-7.1849" y1="-0.4001" x2="-6.7851" y2="0.4001" layer="35"/>
<circle x="-6.985" y="0" radius="0.148103125" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="0.148103125" width="0.0508" layer="51"/>
<circle x="6.985" y="0" radius="0.148103125" width="0.0508" layer="51"/>
<circle x="3.4925" y="6.0452" radius="0.148103125" width="0.0508" layer="51"/>
<circle x="-3.4925" y="-6.0452" radius="0.148103125" width="0.0508" layer="51"/>
<circle x="3.4925" y="-6.0452" radius="0.148103125" width="0.0508" layer="51"/>
<circle x="-3.4925" y="6.0452" radius="0.148103125" width="0.0508" layer="51"/>
<wire x1="-6.6294" y1="0.5588" x2="-6.6294" y2="-0.5588" width="0.0508" layer="21"/>
<wire x1="0.787" y1="0.965" x2="0.787" y2="-0.965" width="0.1016" layer="51"/>
<wire x1="-0.787" y1="0.965" x2="-0.787" y2="-0.965" width="0.1016" layer="51"/>
<smd name="A2" x="0" y="1.4" dx="1.6" dy="1.8" layer="1" rot="R270"/>
<smd name="C2" x="0" y="-1.4" dx="1.6" dy="1.8" layer="1" rot="R270"/>
<rectangle x1="-0.37595" y1="0.47675" x2="0.37415" y2="2.17675" layer="51" rot="R270"/>
<rectangle x1="-0.37415" y1="-2.17675" x2="0.37595" y2="-0.47675" layer="51" rot="R270"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35" rot="R270"/>
<wire x1="0.5588" y1="-0.3556" x2="-0.5588" y2="-0.3556" width="0.0508" layer="21"/>
<wire x1="7.95" y1="-0.8505" x2="6.02" y2="-0.8505" width="0.1016" layer="51"/>
<wire x1="7.95" y1="0.7235" x2="6.02" y2="0.7235" width="0.1016" layer="51"/>
<smd name="A3" x="8.385" y="-0.0635" dx="1.6" dy="1.8" layer="1" rot="R180"/>
<smd name="C3" x="5.585" y="-0.0635" dx="1.6" dy="1.8" layer="1" rot="R180"/>
<rectangle x1="7.9367" y1="-0.9126" x2="8.6868" y2="0.7874" layer="51" rot="R180"/>
<rectangle x1="5.2832" y1="-0.9144" x2="6.0333" y2="0.7856" layer="51" rot="R180"/>
<rectangle x1="6.7851" y1="-0.4636" x2="7.1849" y2="0.3366" layer="35" rot="R180"/>
<wire x1="6.6294" y1="-0.6223" x2="6.6294" y2="0.4953" width="0.0508" layer="21"/>
<wire x1="-4.2795" y1="-6.9975" x2="-4.2795" y2="-5.0675" width="0.1016" layer="51"/>
<wire x1="-2.7055" y1="-6.9975" x2="-2.7055" y2="-5.0675" width="0.1016" layer="51"/>
<smd name="A4" x="-3.4925" y="-7.4325" dx="1.6" dy="1.8" layer="1" rot="R90"/>
<smd name="C4" x="-3.4925" y="-4.6325" dx="1.6" dy="1.8" layer="1" rot="R90"/>
<rectangle x1="-3.86665" y1="-8.20925" x2="-3.11655" y2="-6.50925" layer="51" rot="R90"/>
<rectangle x1="-3.86845" y1="-5.55575" x2="-3.11835" y2="-3.85575" layer="51" rot="R90"/>
<rectangle x1="-3.6924" y1="-6.4326" x2="-3.2926" y2="-5.6324" layer="35" rot="R90"/>
<wire x1="-4.0513" y1="-5.6769" x2="-2.9337" y2="-5.6769" width="0.0508" layer="21"/>
<wire x1="2.7055" y1="-6.9975" x2="2.7055" y2="-5.0675" width="0.1016" layer="51"/>
<wire x1="4.2795" y1="-6.9975" x2="4.2795" y2="-5.0675" width="0.1016" layer="51"/>
<smd name="A5" x="3.4925" y="-7.4325" dx="1.6" dy="1.8" layer="1" rot="R90"/>
<smd name="C5" x="3.4925" y="-4.6325" dx="1.6" dy="1.8" layer="1" rot="R90"/>
<rectangle x1="3.11835" y1="-8.20925" x2="3.86845" y2="-6.50925" layer="51" rot="R90"/>
<rectangle x1="3.11655" y1="-5.55575" x2="3.86665" y2="-3.85575" layer="51" rot="R90"/>
<rectangle x1="3.2926" y1="-6.4326" x2="3.6924" y2="-5.6324" layer="35" rot="R90"/>
<wire x1="2.9337" y1="-5.6769" x2="4.0513" y2="-5.6769" width="0.0508" layer="21"/>
<wire x1="-2.7055" y1="6.9975" x2="-2.7055" y2="5.0675" width="0.1016" layer="51"/>
<wire x1="-4.2795" y1="6.9975" x2="-4.2795" y2="5.0675" width="0.1016" layer="51"/>
<smd name="A6" x="-3.4925" y="7.4325" dx="1.6" dy="1.8" layer="1" rot="R270"/>
<smd name="C6" x="-3.4925" y="4.6325" dx="1.6" dy="1.8" layer="1" rot="R270"/>
<rectangle x1="-3.86845" y1="6.50925" x2="-3.11835" y2="8.20925" layer="51" rot="R270"/>
<rectangle x1="-3.86665" y1="3.85575" x2="-3.11655" y2="5.55575" layer="51" rot="R270"/>
<rectangle x1="-3.6924" y1="5.6324" x2="-3.2926" y2="6.4326" layer="35" rot="R270"/>
<wire x1="-2.9337" y1="5.6769" x2="-4.0513" y2="5.6769" width="0.0508" layer="21"/>
<wire x1="4.2795" y1="6.9975" x2="4.2795" y2="5.0675" width="0.1016" layer="51"/>
<wire x1="2.7055" y1="6.9975" x2="2.7055" y2="5.0675" width="0.1016" layer="51"/>
<smd name="A7" x="3.4925" y="7.4325" dx="1.6" dy="1.8" layer="1" rot="R270"/>
<smd name="C7" x="3.4925" y="4.6325" dx="1.6" dy="1.8" layer="1" rot="R270"/>
<rectangle x1="3.11655" y1="6.50925" x2="3.86665" y2="8.20925" layer="51" rot="R270"/>
<rectangle x1="3.11835" y1="3.85575" x2="3.86845" y2="5.55575" layer="51" rot="R270"/>
<rectangle x1="3.2926" y1="5.6324" x2="3.6924" y2="6.4326" layer="35" rot="R270"/>
<wire x1="4.0513" y1="5.6769" x2="2.9337" y2="5.6769" width="0.0508" layer="21"/>
</package>
<package name="LED_ARRAY_60DEG_4PLCC">
<description>LED Array, 4-PLCC SMD LEDs, 60deg View Angle

&lt;p&gt; Light circles represent area of light on lens @ 60deg per LED, based on 0.31" distance (base of dome / top of package housing to lens, not board floor)</description>
<circle x="0" y="0" radius="11.43" width="0.127" layer="21"/>
<circle x="0" y="0" radius="4.5466" width="0.0508" layer="51"/>
<circle x="-3.81" y="-5.334" radius="4.5466" width="0.0508" layer="51"/>
<circle x="0" y="6.5405" radius="4.5466" width="0.0508" layer="51"/>
<circle x="-6.223" y="1.905" radius="4.5466" width="0.0508" layer="51"/>
<circle x="6.223" y="1.905" radius="4.5466" width="0.0508" layer="51"/>
<circle x="3.81" y="-5.334" radius="4.5466" width="0.0508" layer="51"/>
<wire x1="-7.704403125" y1="0.20683125" x2="-4.707203125" y2="0.20683125" width="0.0508" layer="51"/>
<wire x1="-7.704403125" y1="0.20683125" x2="-7.704403125" y2="3.61043125" width="0.0508" layer="21"/>
<wire x1="-7.704403125" y1="3.61043125" x2="-4.707203125" y2="3.61043125" width="0.0508" layer="51"/>
<circle x="-6.205803125" y="1.90863125" radius="1.2065" width="0.0508" layer="51"/>
<smd name="A2" x="-5.456503125" y="3.52788125" dx="0.889" dy="2.032" layer="1" rot="R180"/>
<smd name="A1" x="-6.955103125" y="3.52788125" dx="0.889" dy="2.032" layer="1" rot="R180"/>
<smd name="A3" x="-6.955103125" y="0.28938125" dx="0.889" dy="2.032" layer="1" rot="R180"/>
<smd name="C" x="-5.456503125" y="0.28938125" dx="0.889" dy="2.032" layer="1" rot="R180"/>
<wire x1="-4.707203125" y1="0.20683125" x2="-4.707203125" y2="3.61043125" width="0.0508" layer="21"/>
<wire x1="-7.704403125" y1="2.51823125" x2="-4.707203125" y2="2.51823125" width="0.0508" layer="51"/>
<wire x1="-7.704403125" y1="1.29903125" x2="-4.707203125" y2="1.29903125" width="0.0508" layer="51"/>
<wire x1="-5.89008125" y1="1.59961875" x2="-4.8817" y2="1.59961875" width="0.4064" layer="21"/>
<wire x1="-1.7018" y1="1.4986" x2="-1.7018" y2="-1.4986" width="0.0508" layer="51"/>
<wire x1="-1.7018" y1="1.4986" x2="1.7018" y2="1.4986" width="0.0508" layer="21"/>
<wire x1="1.7018" y1="1.4986" x2="1.7018" y2="-1.4986" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="1.2065" width="0.0508" layer="51"/>
<smd name="A4" x="1.61925" y="-0.7493" dx="0.889" dy="2.032" layer="1" rot="R90"/>
<smd name="A5" x="1.61925" y="0.7493" dx="0.889" dy="2.032" layer="1" rot="R90"/>
<smd name="A6" x="-1.61925" y="0.7493" dx="0.889" dy="2.032" layer="1" rot="R90"/>
<smd name="C1" x="-1.61925" y="-0.7493" dx="0.889" dy="2.032" layer="1" rot="R90"/>
<wire x1="-1.7018" y1="-1.4986" x2="1.7018" y2="-1.4986" width="0.0508" layer="21"/>
<wire x1="0.6096" y1="1.4986" x2="0.6096" y2="-1.4986" width="0.0508" layer="51"/>
<wire x1="-0.6096" y1="1.4986" x2="-0.6096" y2="-1.4986" width="0.0508" layer="51"/>
<wire x1="-0.322578125" y1="-0.27305" x2="-0.322578125" y2="-1.28143125" width="0.4064" layer="21"/>
<wire x1="-2.10934375" y1="-6.82018125" x2="-2.10934375" y2="-3.82298125" width="0.0508" layer="51"/>
<wire x1="-2.10934375" y1="-6.82018125" x2="-5.51294375" y2="-6.82018125" width="0.0508" layer="21"/>
<wire x1="-5.51294375" y1="-6.82018125" x2="-5.51294375" y2="-3.82298125" width="0.0508" layer="51"/>
<circle x="-3.81114375" y="-5.32158125" radius="1.2065" width="0.0508" layer="51"/>
<smd name="A7" x="-5.43039375" y="-4.57228125" dx="0.889" dy="2.032" layer="1" rot="R270"/>
<smd name="A8" x="-5.43039375" y="-6.07088125" dx="0.889" dy="2.032" layer="1" rot="R270"/>
<smd name="A9" x="-2.19189375" y="-6.07088125" dx="0.889" dy="2.032" layer="1" rot="R270"/>
<smd name="C2" x="-2.19189375" y="-4.57228125" dx="0.889" dy="2.032" layer="1" rot="R270"/>
<wire x1="-2.10934375" y1="-3.82298125" x2="-5.51294375" y2="-3.82298125" width="0.0508" layer="21"/>
<wire x1="-4.42074375" y1="-6.82018125" x2="-4.42074375" y2="-3.82298125" width="0.0508" layer="51"/>
<wire x1="-3.20154375" y1="-6.82018125" x2="-3.20154375" y2="-3.82298125" width="0.0508" layer="51"/>
<wire x1="-3.499840625" y1="-4.99859375" x2="-3.499840625" y2="-3.9902125" width="0.4064" layer="21"/>
<wire x1="-1.696821875" y1="8.039175" x2="-1.696821875" y2="5.041975" width="0.0508" layer="51"/>
<wire x1="-1.696821875" y1="8.039175" x2="1.706778125" y2="8.039175" width="0.0508" layer="21"/>
<wire x1="1.706778125" y1="8.039175" x2="1.706778125" y2="5.041975" width="0.0508" layer="51"/>
<circle x="0.004978125" y="6.540575" radius="1.2065" width="0.0508" layer="51"/>
<smd name="A10" x="1.624228125" y="5.791275" dx="0.889" dy="2.032" layer="1" rot="R90"/>
<smd name="A11" x="1.624228125" y="7.289875" dx="0.889" dy="2.032" layer="1" rot="R90"/>
<smd name="A12" x="-1.614271875" y="7.289875" dx="0.889" dy="2.032" layer="1" rot="R90"/>
<smd name="C3" x="-1.614271875" y="5.791275" dx="0.889" dy="2.032" layer="1" rot="R90"/>
<wire x1="-1.696821875" y1="5.041975" x2="1.706778125" y2="5.041975" width="0.0508" layer="21"/>
<wire x1="0.614578125" y1="8.039175" x2="0.614578125" y2="5.041975" width="0.0508" layer="51"/>
<wire x1="-0.604621875" y1="8.039175" x2="-0.604621875" y2="5.041975" width="0.0508" layer="51"/>
<wire x1="-0.322653125" y1="6.272428125" x2="-0.322653125" y2="5.264046875" width="0.4064" layer="21"/>
<wire x1="5.31281875" y1="-3.626459375" x2="2.31561875" y2="-3.626459375" width="0.0508" layer="51"/>
<wire x1="5.31281875" y1="-3.626459375" x2="5.31281875" y2="-7.030059375" width="0.0508" layer="21"/>
<wire x1="5.31281875" y1="-7.030059375" x2="2.31561875" y2="-7.030059375" width="0.0508" layer="51"/>
<circle x="3.81421875" y="-5.328259375" radius="1.2065" width="0.0508" layer="51"/>
<smd name="A13" x="3.06491875" y="-6.947509375" dx="0.889" dy="2.032" layer="1"/>
<smd name="A14" x="4.56351875" y="-6.947509375" dx="0.889" dy="2.032" layer="1"/>
<smd name="A15" x="4.56351875" y="-3.709009375" dx="0.889" dy="2.032" layer="1"/>
<smd name="C4" x="3.06491875" y="-3.709009375" dx="0.889" dy="2.032" layer="1"/>
<wire x1="2.31561875" y1="-3.626459375" x2="2.31561875" y2="-7.030059375" width="0.0508" layer="21"/>
<wire x1="5.31281875" y1="-5.937859375" x2="2.31561875" y2="-5.937859375" width="0.0508" layer="51"/>
<wire x1="5.31281875" y1="-4.718659375" x2="2.31561875" y2="-4.718659375" width="0.0508" layer="51"/>
<wire x1="3.531209375" y1="-5.007203125" x2="2.522828125" y2="-5.007203125" width="0.4064" layer="21"/>
<wire x1="7.717384375" y1="3.613046875" x2="4.720184375" y2="3.613046875" width="0.0508" layer="51"/>
<wire x1="7.717384375" y1="3.613046875" x2="7.717384375" y2="0.209446875" width="0.0508" layer="21"/>
<wire x1="7.717384375" y1="0.209446875" x2="4.720184375" y2="0.209446875" width="0.0508" layer="51"/>
<circle x="6.218784375" y="1.911246875" radius="1.2065" width="0.0508" layer="51"/>
<smd name="A16" x="5.469484375" y="0.291996875" dx="0.889" dy="2.032" layer="1"/>
<smd name="A17" x="6.968084375" y="0.291996875" dx="0.889" dy="2.032" layer="1"/>
<smd name="A18" x="6.968084375" y="3.530496875" dx="0.889" dy="2.032" layer="1"/>
<smd name="C5" x="5.469484375" y="3.530496875" dx="0.889" dy="2.032" layer="1"/>
<wire x1="4.720184375" y1="3.613046875" x2="4.720184375" y2="0.209446875" width="0.0508" layer="21"/>
<wire x1="7.717384375" y1="1.301646875" x2="4.720184375" y2="1.301646875" width="0.0508" layer="51"/>
<wire x1="7.717384375" y1="2.520846875" x2="4.720184375" y2="2.520846875" width="0.0508" layer="51"/>
<wire x1="5.943703125" y1="2.2233625" x2="4.935321875" y2="2.2233625" width="0.4064" layer="21"/>
<wire x1="-6.491553125" y1="1.90863125" x2="-5.951803125" y2="1.90863125" width="0.0508" layer="51"/>
<wire x1="-6.205803125" y1="2.16263125" x2="-6.205803125" y2="1.65463125" width="0.0508" layer="51"/>
<wire x1="-4.06514375" y1="-5.32158125" x2="-3.55714375" y2="-5.32158125" width="0.0508" layer="51"/>
<wire x1="-3.81114375" y1="-5.60733125" x2="-3.81114375" y2="-5.06758125" width="0.0508" layer="51"/>
<wire x1="3.81421875" y1="-5.582259375" x2="3.81421875" y2="-5.074259375" width="0.0508" layer="51"/>
<wire x1="4.09996875" y1="-5.328259375" x2="3.56021875" y2="-5.328259375" width="0.0508" layer="51"/>
<wire x1="0.254" y1="0" x2="-0.254" y2="0" width="0.0508" layer="51"/>
<wire x1="0" y1="0.28575" x2="0" y2="-0.254" width="0.0508" layer="51"/>
<wire x1="0.258978125" y1="6.540575" x2="-0.249021875" y2="6.540575" width="0.0508" layer="51"/>
<wire x1="0.004978125" y1="6.826325" x2="0.004978125" y2="6.286575" width="0.0508" layer="51"/>
<wire x1="6.218784375" y1="1.657246875" x2="6.218784375" y2="2.165246875" width="0.0508" layer="51"/>
<wire x1="6.504534375" y1="1.911246875" x2="5.964784375" y2="1.911246875" width="0.0508" layer="51"/>
<circle x="-6.223" y="1.905" radius="0.3127" width="0.0508" layer="51"/>
<circle x="6.223" y="1.905" radius="0.3127" width="0.0508" layer="51"/>
<circle x="3.81" y="-5.334" radius="0.3127" width="0.0508" layer="51"/>
<circle x="-3.81" y="-5.334" radius="0.3127" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="0.3127" width="0.0508" layer="51"/>
<circle x="0" y="6.5405" radius="0.3127" width="0.0508" layer="51"/>
</package>
<package name="SMD-1008(2520)">
<smd name="1" x="-1.1744" y="-0.00635" dx="1.397" dy="2.54" layer="1"/>
<text x="2.2225" y="-0.00635" size="0.254" layer="25">&gt;NAME</text>
<rectangle x1="-0.5128" y1="-1.0508" x2="0.4874" y2="0.9492" layer="35"/>
<wire x1="-1.2954" y1="0.990346875" x2="1.203959375" y2="0.990346875" width="0.127" layer="51"/>
<wire x1="-1.2954" y1="-1.00965" x2="-1.2954" y2="0.990346875" width="0.127" layer="51"/>
<wire x1="1.2446" y1="-1.00965" x2="1.2446" y2="0.990346875" width="0.127" layer="51"/>
<wire x1="-1.2954" y1="-1.00965" x2="1.203959375" y2="-1.00965" width="0.127" layer="51"/>
<rectangle x1="-1.2954" y1="-1.00965" x2="-0.79553125" y2="0.99034375" layer="51"/>
<rectangle x1="0.74473125" y1="-1.00939375" x2="1.2446" y2="0.9906" layer="51" rot="R180"/>
<smd name="2" x="1.16875" y="-0.00635" dx="1.397" dy="2.54" layer="1"/>
<wire x1="-2.0955" y1="1.4732" x2="2.0955" y2="1.4732" width="0.127" layer="39"/>
<wire x1="2.0955" y1="1.4732" x2="2.0955" y2="-1.4859" width="0.127" layer="39"/>
<wire x1="2.0955" y1="-1.4859" x2="-2.0955" y2="-1.4859" width="0.127" layer="39"/>
<wire x1="-2.0955" y1="-1.4859" x2="-2.0955" y2="1.4732" width="0.127" layer="39"/>
<wire x1="-2.08915" y1="1.4732" x2="2.0955" y2="1.4732" width="0.127" layer="21"/>
<wire x1="2.0955" y1="1.4732" x2="2.0955" y2="-1.4859" width="0.127" layer="21"/>
<wire x1="2.0955" y1="-1.4859" x2="-2.0955" y2="-1.4859" width="0.127" layer="21"/>
<wire x1="-2.0955" y1="-1.4859" x2="-2.0955" y2="1.4732" width="0.127" layer="21"/>
</package>
<package name="HDR_1X1">
<description>Single Pin Header</description>
<wire x1="-0.6096" y1="1.2446" x2="0.6604" y2="1.2446" width="0.1524" layer="21"/>
<wire x1="0.6604" y1="1.2446" x2="1.2954" y2="0.6096" width="0.1524" layer="21"/>
<wire x1="-0.6096" y1="1.2446" x2="-1.2446" y2="0.6096" width="0.1524" layer="21"/>
<wire x1="1.2954" y1="-0.6604" x2="0.6604" y2="-1.2954" width="0.1524" layer="21"/>
<wire x1="-1.2446" y1="0.6096" x2="-1.2446" y2="-0.6604" width="0.1524" layer="21"/>
<wire x1="-1.2446" y1="-0.6604" x2="-0.6096" y2="-1.2954" width="0.1524" layer="21"/>
<wire x1="0.6604" y1="-1.2954" x2="-0.6096" y2="-1.2954" width="0.1524" layer="21"/>
<wire x1="1.2954" y1="0.6096" x2="1.2954" y2="-0.6604" width="0.1524" layer="21"/>
<pad name="1" x="0.0254" y="-0.0254" drill="1.016" shape="octagon"/>
<text x="-1.6256" y="-1.2446" size="0.6096" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-0.2286" y1="-0.2794" x2="0.2794" y2="0.2286" layer="51"/>
</package>
<package name="SW_K12A">
<description>C&amp;K K12A 0.1A 30V</description>
<pad name="1" x="-1.3" y="0" drill="0.9"/>
<pad name="2" x="1.3" y="0" drill="0.9"/>
<hole x="-3.75" y="-2.5" drill="1.1"/>
<hole x="3.75" y="2.5" drill="1.1"/>
<circle x="0" y="0" radius="6" width="0.127" layer="21"/>
</package>
<package name="SM08">
<smd name="A1" x="-3.1" y="1.9" dx="1.8" dy="1" layer="1" cream="no"/>
<smd name="A2" x="3.1" y="1.9" dx="1.8" dy="1" layer="1" cream="no"/>
<smd name="B2" x="3.1" y="-1.9" dx="1.8" dy="1" layer="1" cream="no"/>
<smd name="B1" x="-3.1" y="-1.9" dx="1.8" dy="1" layer="1" cream="no"/>
<text x="-3.2258" y="0.508" size="0.508" layer="21">&gt;NAME</text>
<text x="-3.2258" y="-0.889" size="0.508" layer="21">&gt;VALUE</text>
</package>
<package name="RST_TAC_SW-TH_RA">
<description>FSMRA2JH Tactile switch (through hole, right angle)</description>
<pad name="1" x="-2.2987" y="0.0635" drill="1.1"/>
<pad name="2" x="2.1971" y="0.0635" drill="1.1"/>
<wire x1="-3.556" y1="-2.4765" x2="3.556" y2="-2.4765" width="0.127" layer="21"/>
<wire x1="-1.651" y1="-3.7465" x2="-1.651" y2="-2.4892" width="0.127" layer="21"/>
<wire x1="1.8415" y1="-3.7465" x2="1.8415" y2="-2.5019" width="0.127" layer="21"/>
<wire x1="-1.651" y1="-3.7465" x2="1.8415" y2="-3.7465" width="0.127" layer="21"/>
<wire x1="1.8415" y1="-3.7465" x2="1.8542" y2="-3.7465" width="0.127" layer="21"/>
<wire x1="3.556" y1="-2.4765" x2="3.556" y2="1.524" width="0.127" layer="21"/>
<wire x1="3.556" y1="1.524" x2="3.556" y2="4.1275" width="0.127" layer="51"/>
<wire x1="-3.556" y1="-2.4765" x2="-3.556" y2="1.524" width="0.127" layer="21"/>
<wire x1="-3.556" y1="1.524" x2="-3.556" y2="4.1275" width="0.127" layer="51"/>
<wire x1="-3.556" y1="1.524" x2="-3.175" y2="1.524" width="0.127" layer="21"/>
<wire x1="-3.175" y1="1.524" x2="3.175" y2="1.524" width="0.127" layer="21"/>
<wire x1="3.175" y1="1.524" x2="3.556" y2="1.524" width="0.127" layer="21"/>
<wire x1="3.175" y1="4.1275" x2="3.175" y2="1.524" width="0.127" layer="51"/>
<wire x1="-3.175" y1="4.1275" x2="-3.175" y2="1.524" width="0.127" layer="51"/>
<wire x1="-3.556" y1="4.1275" x2="-3.175" y2="4.1275" width="0.127" layer="21"/>
<wire x1="3.556" y1="4.1275" x2="3.175" y2="4.1275" width="0.127" layer="21"/>
<hole x="3.4544" y="2.6035" drill="1.3"/>
<hole x="-3.556" y="2.6035" drill="1.3"/>
</package>
<package name="SW_KMR">
<smd name="4" x="-1.91135" y="0.75565" dx="0.899921875" dy="0.999996875" layer="1" rot="R270"/>
<smd name="3" x="2.0828" y="0.75565" dx="0.899921875" dy="0.999996875" layer="1" rot="R90"/>
<smd name="2" x="2.0828" y="-0.94615" dx="0.899921875" dy="0.999996875" layer="1" rot="R90"/>
<smd name="1" x="-1.91135" y="-0.94615" dx="0.899921875" dy="0.999996875" layer="1" rot="R90"/>
<wire x1="-1.89865" y1="-1.397" x2="-1.89865" y2="1.20141875" width="0.0508" layer="51"/>
<wire x1="2.047240625" y1="-1.397" x2="2.047240625" y2="1.19506875" width="0.0508" layer="51"/>
<wire x1="-1.27" y1="1.20294375" x2="1.4351" y2="1.20294375" width="0.0508" layer="21"/>
<wire x1="-1.27" y1="-1.397" x2="1.4351" y2="-1.397" width="0.0508" layer="21"/>
<wire x1="-1.89865" y1="-1.397" x2="-1.27" y2="-1.397" width="0.0508" layer="51"/>
<wire x1="2.0447" y1="-1.397" x2="1.4351" y2="-1.397" width="0.0508" layer="51"/>
<wire x1="1.4351" y1="1.20294375" x2="2.047240625" y2="1.20294375" width="0.0508" layer="51"/>
<wire x1="-1.89865" y1="1.20294375" x2="-1.27" y2="1.20294375" width="0.0508" layer="51"/>
<circle x="0.01905" y="-0.0381" radius="0.621259375" width="0.0508" layer="51"/>
<text x="-0.61595" y="1.524" size="0.508" layer="25">&gt;NAME</text>
<text x="-1.12395" y="-2.159" size="0.508" layer="27">&gt;VALUE</text>
</package>
<package name="SOIC-8">
<wire x1="2.4" y1="1.9" x2="2.4" y2="-1.4" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1.4" x2="2.4" y2="-1.9" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1.9" x2="-2.4" y2="-1.9" width="0.127" layer="51"/>
<wire x1="-2.4" y1="-1.9" x2="-2.4" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-2.4" y1="-1.4" x2="-2.4" y2="1.9" width="0.127" layer="21"/>
<wire x1="-2.4" y1="1.9" x2="2.4" y2="1.9" width="0.127" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="-2.4" y2="-1.4" width="0.127" layer="21"/>
<smd name="2" x="-0.635" y="-2.75" dx="0.6" dy="1.5" layer="1"/>
<smd name="7" x="-0.635" y="2.75" dx="0.6" dy="1.5" layer="1"/>
<smd name="1" x="-1.905" y="-2.75" dx="0.6" dy="1.5" layer="1"/>
<smd name="3" x="0.635" y="-2.75" dx="0.6" dy="1.5" layer="1"/>
<smd name="4" x="1.905" y="-2.75" dx="0.6" dy="1.5" layer="1"/>
<smd name="8" x="-1.905" y="2.75" dx="0.6" dy="1.5" layer="1"/>
<smd name="6" x="0.635" y="2.75" dx="0.6" dy="1.5" layer="1"/>
<smd name="5" x="1.905" y="2.75" dx="0.6" dy="1.5" layer="1"/>
<text x="-2.667" y="-1.905" size="0.6096" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-2.15" y1="-3.1" x2="-1.66" y2="-2" layer="51"/>
<rectangle x1="-0.88" y1="-3.1" x2="-0.39" y2="-2" layer="51"/>
<rectangle x1="0.39" y1="-3.1" x2="0.88" y2="-2" layer="51"/>
<rectangle x1="1.66" y1="-3.1" x2="2.15" y2="-2" layer="51"/>
<rectangle x1="1.66" y1="2" x2="2.15" y2="3.1" layer="51"/>
<rectangle x1="0.39" y1="2" x2="0.88" y2="3.1" layer="51"/>
<rectangle x1="-0.88" y1="2" x2="-0.39" y2="3.1" layer="51"/>
<rectangle x1="-2.15" y1="2" x2="-1.66" y2="3.1" layer="51"/>
<wire x1="-2.3495" y1="0.889" x2="-2.3495" y2="-0.381" width="0.127" layer="21" curve="-180"/>
</package>
<package name="XTAL_7XC-7A">
<smd name="2" x="1.85" y="0" dx="1.7" dy="2.4" layer="1"/>
<smd name="1" x="-1.85" y="0" dx="1.7" dy="2.4" layer="1"/>
<wire x1="-2.6" y1="1.6" x2="2.6" y2="1.6" width="0.127" layer="21"/>
<wire x1="2.6" y1="-1.6" x2="-2.6" y2="-1.6" width="0.127" layer="21"/>
<text x="-2.286" y="2.159" size="0.4064" layer="25">&gt;NAME</text>
</package>
<package name="XTAL_HC49UP">
<wire x1="-5.1091" y1="1.143" x2="-3.429" y2="2.0321" width="0.0508" layer="21" curve="-55.770993" cap="flat"/>
<wire x1="-5.715" y1="1.143" x2="-5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.032" x2="5.1091" y2="1.143" width="0.0508" layer="21" curve="-55.772485" cap="flat"/>
<wire x1="5.715" y1="1.143" x2="5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="-3.429" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.429" y1="-2.032" x2="-3.429" y2="-2.032" width="0.0508" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="3.429" y2="1.27" width="0.0508" layer="21"/>
<wire x1="5.461" y1="-2.413" x2="-5.461" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="6.477" y1="-0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.715" y1="-1.143" x2="5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-2.159" x2="5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="3.9826" y2="-1.143" width="0.0508" layer="21" curve="25.842828" cap="flat"/>
<wire x1="3.429" y1="1.27" x2="3.9826" y2="1.143" width="0.0508" layer="21" curve="-25.842828" cap="flat"/>
<wire x1="3.429" y1="-2.032" x2="5.109" y2="-1.1429" width="0.0508" layer="21" curve="55.771157" cap="flat"/>
<wire x1="3.9826" y1="-1.143" x2="3.9826" y2="1.143" width="0.0508" layer="51" curve="128.314524" cap="flat"/>
<wire x1="5.1091" y1="-1.143" x2="5.1091" y2="1.143" width="0.0508" layer="51" curve="68.456213" cap="flat"/>
<wire x1="-5.1091" y1="-1.143" x2="-3.429" y2="-2.032" width="0.0508" layer="21" curve="55.772485" cap="flat"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.9826" y2="1.143" width="0.0508" layer="51" curve="-128.314524" cap="flat"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.429" y2="-1.27" width="0.0508" layer="21" curve="25.842828" cap="flat"/>
<wire x1="-3.9826" y1="1.143" x2="-3.429" y2="1.27" width="0.0508" layer="21" curve="-25.842828" cap="flat"/>
<wire x1="-6.477" y1="-0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.1091" y1="-1.143" x2="-5.1091" y2="1.143" width="0.0508" layer="51" curve="-68.456213" cap="flat"/>
<wire x1="-5.715" y1="-1.143" x2="-5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-0.381" x2="-5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-2.159" x2="-5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.715" y1="-0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="2.032" x2="3.429" y2="2.032" width="0.0508" layer="21"/>
<wire x1="5.461" y1="2.413" x2="-5.461" y2="2.413" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.254" y1="0.635" x2="-0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.635" x2="0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.635" x2="0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.635" x2="-0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.016" y2="0" width="0.0508" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.016" y2="0" width="0.0508" layer="21"/>
<smd name="1" x="-4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<smd name="2" x="4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<text x="-6.35" y="3.429" size="0.508" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-4.191" size="0.508" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.604" y1="-3.048" x2="6.604" y2="3.048" layer="43"/>
</package>
<package name="XTAL_HC49US">
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.2032" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.2032" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.429" y2="-2.286" width="0.2032" layer="21" curve="-180"/>
<wire x1="-3.429" y1="2.286" x2="-3.429" y2="-2.286" width="0.2032" layer="21" curve="180"/>
<pad name="1" x="-2.54" y="0" drill="0.7" diameter="1.651" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651" rot="R90"/>
<text x="-5.08" y="-3.302" size="0.508" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.08" y="2.667" size="0.508" layer="25" ratio="10">&gt;NAME</text>
</package>
<package name="XTAL_HC49UV">
<wire x1="-2.921" y1="-2.286" x2="2.921" y2="-2.286" width="0.4064" layer="21"/>
<wire x1="-2.921" y1="2.286" x2="2.921" y2="2.286" width="0.4064" layer="21"/>
<wire x1="-2.921" y1="-1.778" x2="2.921" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.778" x2="-2.921" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.778" x2="2.921" y2="-1.778" width="0.1524" layer="21" curve="-180"/>
<wire x1="2.921" y1="2.286" x2="2.921" y2="-2.286" width="0.4064" layer="21" curve="-180"/>
<wire x1="-2.921" y1="2.286" x2="-2.921" y2="-2.286" width="0.4064" layer="21" curve="180"/>
<wire x1="-2.921" y1="1.778" x2="-2.921" y2="-1.778" width="0.1524" layer="21" curve="180"/>
<wire x1="-0.254" y1="0.889" x2="0.254" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.889" x2="0.254" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.889" x2="-0.254" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.889" x2="-0.254" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0.889" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.889" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="-5.08" y="2.921" size="0.508" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-3.556" size="0.508" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-2.794" x2="3.81" y2="2.794" layer="43"/>
<rectangle x1="-4.318" y1="-2.54" x2="-3.81" y2="2.54" layer="43"/>
<rectangle x1="-4.826" y1="-2.286" x2="-4.318" y2="2.286" layer="43"/>
<rectangle x1="-5.334" y1="-1.778" x2="-4.826" y2="1.778" layer="43"/>
<rectangle x1="-5.588" y1="-1.27" x2="-5.334" y2="1.016" layer="43"/>
<rectangle x1="3.81" y1="-2.54" x2="4.318" y2="2.54" layer="43"/>
<rectangle x1="4.318" y1="-2.286" x2="4.826" y2="2.286" layer="43"/>
<rectangle x1="4.826" y1="-1.778" x2="5.334" y2="1.778" layer="43"/>
<rectangle x1="5.334" y1="-1.016" x2="5.588" y2="1.016" layer="43"/>
</package>
<package name="XTAL_SMD_10.5X4.8">
<wire x1="-6.2" y1="1.5" x2="-6.2" y2="2.4" width="0.127" layer="21"/>
<wire x1="-6.2" y1="2.4" x2="6.2" y2="2.4" width="0.127" layer="21"/>
<wire x1="6.2" y1="2.4" x2="6.2" y2="1.5" width="0.127" layer="21"/>
<wire x1="-6.2" y1="-1.5" x2="-6.2" y2="-2.4" width="0.127" layer="21"/>
<wire x1="-6.2" y1="-2.4" x2="6.2" y2="-2.4" width="0.127" layer="21"/>
<wire x1="6.2" y1="-2.4" x2="6.2" y2="-1.5" width="0.127" layer="21"/>
<smd name="1" x="-4.3" y="0" dx="5.5" dy="1.5" layer="1"/>
<smd name="2" x="4.3" y="0" dx="5.5" dy="1.5" layer="1"/>
</package>
<package name="XTAL_TC26H">
<wire x1="-0.889" y1="-0.889" x2="0.889" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="0.762" y1="5.207" x2="1.016" y2="4.953" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="4.953" x2="-0.762" y2="5.207" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.762" y1="5.207" x2="0.762" y2="5.207" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.889" x2="0.889" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-0.508" x2="1.016" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.889" x2="-0.889" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-0.508" x2="-0.889" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-0.508" x2="-1.016" y2="4.953" width="0.1524" layer="21"/>
<wire x1="0.508" y1="-1.778" x2="0.508" y2="-1.397" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="-1.778" x2="-0.508" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="0.635" y1="-1.905" x2="1.27" y2="-2.54" width="0.4064" layer="51"/>
<wire x1="-0.635" y1="-1.905" x2="-1.27" y2="-2.54" width="0.4064" layer="51"/>
<wire x1="-0.508" y1="2.413" x2="-0.508" y2="2.032" width="0.1524" layer="21"/>
<wire x1="0.508" y1="2.032" x2="-0.508" y2="2.032" width="0.1524" layer="21"/>
<wire x1="0.508" y1="2.032" x2="0.508" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="2.413" x2="0.508" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="2.794" x2="0" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="1.651" x2="0" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0" y1="1.651" x2="0" y2="1.143" width="0.1524" layer="21"/>
<wire x1="0" y1="1.651" x2="0.508" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0" y1="2.794" x2="0" y2="3.302" width="0.1524" layer="21"/>
<wire x1="0" y1="2.794" x2="0.508" y2="2.794" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-0.508" x2="0.889" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.508" x2="-0.889" y2="-0.508" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="-2.54" drill="0.8128" diameter="1.6764"/>
<pad name="2" x="1.27" y="-2.54" drill="0.8128" diameter="1.6764"/>
<text x="-1.397" y="-0.508" size="0.508" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="2.667" y="-0.508" size="0.508" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3048" y1="-1.524" x2="0.7112" y2="-0.9398" layer="21"/>
<rectangle x1="-0.7112" y1="-1.524" x2="-0.3048" y2="-0.9398" layer="21"/>
<rectangle x1="-1.778" y1="-1.778" x2="1.778" y2="5.842" layer="43"/>
</package>
<package name="XTAL_TC38H">
<wire x1="-1.397" y1="-0.889" x2="1.397" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="7.366" x2="1.524" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.524" y1="7.112" x2="-1.27" y2="7.366" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="7.366" x2="1.27" y2="7.366" width="0.1524" layer="21"/>
<wire x1="1.397" y1="-0.889" x2="1.397" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.508" x2="1.397" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.508" x2="1.524" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="-0.889" x2="-1.397" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.508" x2="-1.397" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.508" x2="-1.524" y2="7.112" width="0.1524" layer="21"/>
<wire x1="1.397" y1="-0.508" x2="-1.397" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.5588" y1="-1.8288" x2="0.508" y2="-1.778" width="0.4064" layer="51"/>
<wire x1="0.508" y1="-1.778" x2="0.508" y2="-1.397" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="-1.778" x2="-0.508" y2="-1.524" width="0.4064" layer="21"/>
<wire x1="-0.5588" y1="-1.8288" x2="-0.508" y2="-1.778" width="0.4064" layer="51"/>
<wire x1="0.635" y1="-1.905" x2="1.27" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-1.905" x2="-1.27" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="3.048" x2="-0.762" y2="2.667" width="0.1524" layer="21"/>
<wire x1="0.762" y1="2.667" x2="-0.762" y2="2.667" width="0.1524" layer="21"/>
<wire x1="0.762" y1="2.667" x2="0.762" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="3.048" x2="0.762" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="3.429" x2="0" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="2.286" x2="0" y2="2.286" width="0.1524" layer="21"/>
<wire x1="0" y1="2.286" x2="0" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0" y1="2.286" x2="0.762" y2="2.286" width="0.1524" layer="21"/>
<wire x1="0" y1="3.429" x2="0" y2="3.937" width="0.1524" layer="21"/>
<wire x1="0" y1="3.429" x2="0.762" y2="3.429" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="-2.54" drill="0.8128" diameter="1.6764"/>
<pad name="2" x="1.27" y="-2.54" drill="0.8128" diameter="1.6764"/>
<text x="-1.905" y="-0.508" size="0.508" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="2.54" y="-0.508" size="0.508" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3048" y1="-1.524" x2="0.7112" y2="-0.9398" layer="21"/>
<rectangle x1="-0.7112" y1="-1.524" x2="-0.3048" y2="-0.9398" layer="21"/>
<rectangle x1="-1.778" y1="-1.524" x2="1.778" y2="7.874" layer="43"/>
</package>
<package name="XTAL_TH_11.5X5">
<pad name="1" x="-2.54" y="0" drill="0.5334"/>
<pad name="2" x="2.3368" y="0" drill="0.5334"/>
<wire x1="-3.3202875" y1="2.48970625" x2="3.231134375" y2="2.4892" width="0.127" layer="21"/>
<wire x1="-3.390140625" y1="-2.487675" x2="3.1790625" y2="-2.4892" width="0.127" layer="21"/>
<wire x1="-3.294634375" y1="2.491996875" x2="-3.290571875" y2="-2.495040625" width="0.127" layer="21" curve="180"/>
<wire x1="3.16255" y1="-2.488440625" x2="3.1412125" y2="2.48716875" width="0.127" layer="21" curve="180"/>
<text x="-3.69849375" y="2.79044375" size="0.6096" layer="25">&gt;NAME</text>
<text x="-3.7917125" y="-3.34848125" size="0.6096" layer="27">&gt;VALUE</text>
</package>
<package name="XTAL_CM2X0C">
<smd name="4" x="-3.14325" y="1.5875" dx="1.30048125" dy="1.905" layer="1" rot="R180"/>
<smd name="1" x="-3.14325" y="-1.5875" dx="1.30048125" dy="1.905" layer="1" rot="R180"/>
<smd name="2" x="2.31775" y="-1.55575" dx="1.30048125" dy="1.905" layer="1" rot="R180"/>
<smd name="3" x="2.31775" y="1.5875" dx="1.30048125" dy="1.905" layer="1" rot="R180"/>
<wire x1="-4.22275" y1="-1.27" x2="-4.22275" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.40233125" y1="-1.27" x2="3.40233125" y2="1.27" width="0.127" layer="21"/>
<wire x1="-3.96875" y1="1.27" x2="-2.31775" y2="1.27" width="0.127" layer="51"/>
<wire x1="1.55575" y1="1.27" x2="3.20675" y2="1.27" width="0.127" layer="51"/>
<wire x1="1.55575" y1="-1.27" x2="3.20675" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-3.96875" y1="-1.27" x2="-2.31775" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-4.22275" y1="-1.27" x2="-3.96875" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="1.4732" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.39725" y1="-1.27" x2="3.20675" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-4.22275" y1="1.27" x2="-3.96875" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="1.4732" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.39725" y1="1.27" x2="3.20675" y2="1.27" width="0.127" layer="21"/>
<circle x="2.54" y="0" radius="0.179603125" width="0.127" layer="21"/>
<text x="-4.5085" y="-1.3335" size="0.508" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="SC-70-5">
<wire x1="-1" y1="-0.625" x2="1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="0.280159375" y1="0.625" x2="1" y2="0.625" width="0.127" layer="51"/>
<smd name="1" x="-0.65" y="-0.825" dx="0.4" dy="0.75" layer="1" cream="no"/>
<smd name="2" x="0" y="-0.825" dx="0.4" dy="0.75" layer="1" cream="no"/>
<smd name="3" x="0.65" y="-0.825" dx="0.4" dy="0.75" layer="1" cream="no"/>
<smd name="4" x="0.65" y="0.825" dx="0.4" dy="0.75" layer="1" cream="no"/>
<smd name="5" x="-0.65" y="0.825" dx="0.4" dy="0.75" layer="1" cream="no"/>
<text x="-1.2192" y="-0.9144" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-0.85" y1="0.45" x2="-0.45" y2="1.2" layer="31"/>
<rectangle x1="0.45" y1="0.45" x2="0.85" y2="1.2" layer="31"/>
<rectangle x1="-0.8382" y1="-1.1684" x2="-0.508" y2="-0.4826" layer="31"/>
<rectangle x1="-0.1651" y1="-1.1684" x2="0.1651" y2="-0.4826" layer="31"/>
<rectangle x1="0.508" y1="-1.1684" x2="0.8382" y2="-0.4826" layer="31"/>
<wire x1="-1.016" y1="0.6096" x2="-1.016" y2="-0.6096" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.6096" x2="1.016" y2="-0.6096" width="0.127" layer="21"/>
<wire x1="-0.2794" y1="0.6248375" x2="0.2794" y2="0.6248375" width="0.127" layer="21"/>
<wire x1="-1.0127" y1="0.625" x2="-0.290315625" y2="0.625" width="0.127" layer="51"/>
</package>
<package name="SSOP-8">
<wire x1="1.13156875" y1="1.026159375" x2="-1.1684" y2="1.026159375" width="0.0508" layer="21"/>
<wire x1="-1.1684" y1="1.026159375" x2="-1.1684" y2="-0.9738375" width="0.0508" layer="51"/>
<wire x1="1.13156875" y1="-0.9738375" x2="-1.1684" y2="-0.9738375" width="0.0508" layer="21"/>
<wire x1="1.13156875" y1="1.026159375" x2="1.13156875" y2="-0.9738375" width="0.0508" layer="51"/>
<rectangle x1="1.1557" y1="0.665478125" x2="1.55575" y2="0.885440625" layer="51"/>
<smd name="8" x="1.480821875" y="0.776221875" dx="0.238759375" dy="0.5334" layer="1" rot="R90"/>
<smd name="7" x="1.480821875" y="0.276225" dx="0.238759375" dy="0.5334" layer="1" rot="R90"/>
<smd name="6" x="1.480821875" y="-0.223775" dx="0.238759375" dy="0.5334" layer="1" rot="R90"/>
<smd name="5" x="1.480821875" y="-0.723775" dx="0.238759375" dy="0.5334" layer="1" rot="R90"/>
<rectangle x1="1.158240625" y1="0.1676375" x2="1.558290625" y2="0.3876" layer="51"/>
<rectangle x1="1.16078125" y1="-0.337821875" x2="1.56083125" y2="-0.117859375" layer="51"/>
<rectangle x1="1.158240625" y1="-0.833121875" x2="1.558290625" y2="-0.613159375" layer="51"/>
<wire x1="-1.167128125" y1="-0.972821875" x2="1.132840625" y2="-0.972821875" width="0.0508" layer="21"/>
<rectangle x1="-1.591309375" y1="-0.832103125" x2="-1.191259375" y2="-0.612140625" layer="51" rot="R180"/>
<smd name="4" x="-1.51638125" y="-0.722884375" dx="0.238759375" dy="0.5334" layer="1" rot="R270"/>
<smd name="3" x="-1.51638125" y="-0.2228875" dx="0.238759375" dy="0.5334" layer="1" rot="R270"/>
<smd name="2" x="-1.51638125" y="0.2771125" dx="0.238759375" dy="0.5334" layer="1" rot="R270"/>
<smd name="1" x="-1.51638125" y="0.7771125" dx="0.238759375" dy="0.5334" layer="1" rot="R270"/>
<rectangle x1="-1.59385" y1="-0.3342625" x2="-1.1938" y2="-0.1143" layer="51" rot="R180"/>
<rectangle x1="-1.596390625" y1="0.171196875" x2="-1.196340625" y2="0.391159375" layer="51" rot="R180"/>
<rectangle x1="-1.59385" y1="0.666496875" x2="-1.1938" y2="0.886459375" layer="51" rot="R180"/>
<text x="-1.0541" y="1.2217375" size="0.3048" layer="25">&gt;NAME</text>
</package>
<package name="6-SON">
<wire x1="-0.802640625" y1="-0.635" x2="-0.802640625" y2="0.624840625" width="0.0508" layer="21"/>
<smd name="6" x="-0.510540625" y="0.513078125" dx="0.7993375" dy="0.29971875" layer="1" rot="R90"/>
<smd name="5" x="-0.008" y="0.513078125" dx="0.7993375" dy="0.29971875" layer="1" rot="R90"/>
<smd name="4" x="0.491996875" y="0.513078125" dx="0.7993375" dy="0.29971875" layer="1" rot="R90"/>
<smd name="3" x="0.491996875" y="-0.510540625" dx="0.7993375" dy="0.29971875" layer="1" rot="R90"/>
<smd name="2" x="-0.008" y="-0.510540625" dx="0.7993375" dy="0.29971875" layer="1" rot="R90"/>
<smd name="1" x="-0.510540625" y="-0.510540625" dx="0.7993375" dy="0.29971875" layer="1" rot="R90"/>
<wire x1="0.78231875" y1="-0.64261875" x2="0.78231875" y2="0.617221875" width="0.0508" layer="21"/>
<circle x="-0.904240625" y="-0.370840625" radius="0.0660375" width="0.0508" layer="21"/>
<wire x1="-0.782321875" y1="0.624840625" x2="0.76961875" y2="0.624840625" width="0.0508" layer="51"/>
<wire x1="-0.782321875" y1="-0.645159375" x2="0.76961875" y2="-0.645159375" width="0.0508" layer="51"/>
<text x="1.143" y="-0.635" size="0.254" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="BGA_153">
<description>153-pin BGA based off of Micron MTFC2GMVEA-0M WT eMMC
&lt;p&gt; Modified to remove NC pins that forced miniscule drill sizes</description>
<wire x1="-5.73405" y1="-6.52145" x2="0.01595" y2="-6.52145" width="0.0762" layer="21"/>
<wire x1="0.01595" y1="-6.52145" x2="5.765953125" y2="-6.52145" width="0.0762" layer="21"/>
<wire x1="-5.73405" y1="-6.52145" x2="-5.73405" y2="-0.021590625" width="0.0762" layer="21"/>
<wire x1="-5.73405" y1="-0.021590625" x2="-5.73405" y2="6.47826875" width="0.0762" layer="21"/>
<wire x1="5.765953125" y1="-6.52145" x2="5.765953125" y2="6.47826875" width="0.0762" layer="21"/>
<wire x1="-5.73405" y1="6.47826875" x2="0.01595" y2="6.47826875" width="0.0762" layer="21"/>
<wire x1="0.01595" y1="6.47826875" x2="5.765953125" y2="6.47826875" width="0.0762" layer="21"/>
<smd name="K1" x="-3.23405625" y="-1.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="M1" x="-3.23405625" y="-2.271521875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="L1" x="-3.23405625" y="-1.77165" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="J1" x="-3.23405625" y="-0.771653125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="H1" x="-3.23405625" y="-0.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="G1" x="-3.23405625" y="0.22855" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="F1" x="-3.23405625" y="0.72855" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="E1" x="-3.23405625" y="1.228546875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="D1" x="-3.23405625" y="1.72846875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<wire x1="-6.49605" y1="-0.021590625" x2="-5.73405" y2="-0.021590625" width="0.127" layer="21"/>
<wire x1="6.523990625" y1="-0.021590625" x2="5.761990625" y2="-0.021590625" width="0.0762" layer="21"/>
<smd name="K2" x="-2.73405625" y="-1.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="N2" x="-2.73405625" y="-2.771646875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="M2" x="-2.73405625" y="-2.271521875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="L2" x="-2.73405625" y="-1.77165" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="J2" x="-2.73405625" y="-0.771653125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="H2" x="-2.73405625" y="-0.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="G2" x="-2.73405625" y="0.22855" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="C2" x="-2.73405625" y="2.22854375" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="F2" x="-2.73405625" y="0.72855" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="E2" x="-2.73405625" y="1.228546875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="D2" x="-2.73405625" y="1.72846875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="B2" x="-2.73405625" y="2.728553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="P3" x="-2.23405625" y="-3.27145" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="K3" x="-2.23405625" y="-1.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="N3" x="-2.23405625" y="-2.771646875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="M3" x="-2.23405625" y="-2.271521875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="L3" x="-2.23405625" y="-1.77165" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="J3" x="-2.23405625" y="-0.771653125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="H3" x="-2.23405625" y="-0.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="G3" x="-2.23405625" y="0.22855" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="F3" x="-2.23405625" y="0.72855" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="B3" x="-2.23405625" y="2.728553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="A3" x="-2.23405625" y="3.228553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="P4" x="-1.734059375" y="-3.27145" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="N4" x="-1.734059375" y="-2.771646875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="M4" x="-1.734059375" y="-2.271521875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="C4" x="-1.734059375" y="2.22854375" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="B4" x="-1.734059375" y="2.728553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="A4" x="-1.734059375" y="3.228553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="P5" x="-1.23405625" y="-3.27145" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="K5" x="-1.23405625" y="-1.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="N5" x="-1.23405625" y="-2.771646875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="M5" x="-1.23405625" y="-2.271521875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="J5" x="-1.23405625" y="-0.771653125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="H5" x="-1.23405625" y="-0.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="G5" x="-1.23405625" y="0.22855" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="F5" x="-1.23405625" y="0.72855" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="E5" x="-1.23405625" y="1.228546875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="B5" x="-1.23405625" y="2.728553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="A5" x="-1.23405625" y="3.228553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="P6" x="-0.734059375" y="-3.27145" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="K6" x="-0.734059375" y="-1.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="N6" x="-0.734059375" y="-2.771646875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="M6" x="-0.734059375" y="-2.271521875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="C6" x="-0.734059375" y="2.22854375" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="E6" x="-0.734059375" y="1.228546875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="B6" x="-0.734059375" y="2.728553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="P7" x="-0.234053125" y="-3.27145" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="K7" x="-0.234053125" y="-1.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="C7" x="-0.234053125" y="2.22854375" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="E7" x="-0.234053125" y="1.228546875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="P8" x="0.265946875" y="-3.27145" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="K8" x="0.265946875" y="-1.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="C8" x="0.265946875" y="2.22854375" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="E8" x="0.265946875" y="1.228546875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="B8" x="0.265946875" y="2.728553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="P9" x="0.765946875" y="-3.27145" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="K9" x="0.765946875" y="-1.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="N9" x="0.765946875" y="-2.771646875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="M9" x="0.765946875" y="-2.271521875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="C9" x="0.765946875" y="2.22854375" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="E9" x="0.765946875" y="1.228546875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="B9" x="0.765946875" y="2.728553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="A9" x="0.765946875" y="3.228553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="P10" x="1.265946875" y="-3.27145" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="K10" x="1.265946875" y="-1.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="N10" x="1.265946875" y="-2.771646875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="M10" x="1.265946875" y="-2.271521875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="J10" x="1.265946875" y="-0.771653125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="H10" x="1.265946875" y="-0.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="G10" x="1.265946875" y="0.22855" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="C10" x="1.265946875" y="2.22854375" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="F10" x="1.265946875" y="0.72855" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="E10" x="1.265946875" y="1.228546875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="B10" x="1.265946875" y="2.728553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="A10" x="1.265946875" y="3.228553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="P11" x="1.765946875" y="-3.27145" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="N11" x="1.765946875" y="-2.771646875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="M11" x="1.765946875" y="-2.271521875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="C11" x="1.765946875" y="2.22854375" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="B11" x="1.765946875" y="2.728553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="A11" x="1.765946875" y="3.228553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="P12" x="2.265934375" y="-3.27145" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="K12" x="2.265934375" y="-1.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="N12" x="2.265934375" y="-2.771646875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="M12" x="2.265934375" y="-2.271521875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="L12" x="2.265934375" y="-1.77165" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="J12" x="2.265934375" y="-0.771653125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="H12" x="2.265934375" y="-0.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="G12" x="2.265934375" y="0.22855" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="C12" x="2.265934375" y="2.22854375" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="F12" x="2.265934375" y="0.72855" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="E12" x="2.265934375" y="1.228546875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="D12" x="2.265934375" y="1.72846875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="B12" x="2.265934375" y="2.728553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="A12" x="2.265934375" y="3.228553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="P13" x="2.765946875" y="-3.27145" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="K13" x="2.765946875" y="-1.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="N13" x="2.765946875" y="-2.771646875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="M13" x="2.765946875" y="-2.271521875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="L13" x="2.765946875" y="-1.77165" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="J13" x="2.765946875" y="-0.771653125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="H13" x="2.765946875" y="-0.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="G13" x="2.765946875" y="0.22855" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="C13" x="2.765946875" y="2.22854375" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="F13" x="2.765946875" y="0.72855" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="E13" x="2.765946875" y="1.228546875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="D13" x="2.765946875" y="1.72846875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="B13" x="2.765946875" y="2.728553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="A13" x="2.765946875" y="3.228553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="P14" x="3.265946875" y="-3.27145" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="K14" x="3.265946875" y="-1.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="N14" x="3.265946875" y="-2.771646875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="M14" x="3.265946875" y="-2.271521875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="L14" x="3.265946875" y="-1.77165" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="J14" x="3.265946875" y="-0.771653125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="H14" x="3.265946875" y="-0.271525" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="G14" x="3.265946875" y="0.22855" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="C14" x="3.265946875" y="2.22854375" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="F14" x="3.265946875" y="0.72855" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="E14" x="3.265946875" y="1.228546875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="D14" x="3.265946875" y="1.72846875" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="B14" x="3.265946875" y="2.728553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<smd name="A14" x="3.265946875" y="3.228553125" dx="0.25908125" dy="0.25908125" layer="1" roundness="100" rot="R90"/>
<circle x="-5.038090625" y="5.739128125" radius="0.3976125" width="0.127" layer="21"/>
<wire x1="0.01595" y1="6.47826875" x2="0.01595" y2="7.44855" width="0.0762" layer="21"/>
<wire x1="0.01595" y1="-6.52145" x2="0.01595" y2="-7.55523125" width="0.0762" layer="21"/>
<text x="-6.353809375" y="3.039109375" size="0.508" layer="21">A</text>
<text x="-6.353809375" y="-3.539490625" size="0.508" layer="21">P</text>
<text x="-3.46075" y="6.70686875" size="0.508" layer="21">1</text>
<text x="2.95275" y="6.70686875" size="0.508" layer="21">14</text>
<text x="0.90805" y="-7.29106875" size="0.508" layer="21">&gt;NAME</text>
</package>
<package name="BGA_96">
<description>96-pin BGA, DDR3L chip based off of Micron MT41K256M16HA</description>
<wire x1="-4.445" y1="-6.985" x2="4.5466" y2="-6.985" width="0.0762" layer="21"/>
<wire x1="-4.445" y1="-6.985" x2="-4.445" y2="7.0104" width="0.0762" layer="21"/>
<wire x1="-4.445" y1="7.0104" x2="4.5466" y2="7.0104" width="0.0762" layer="21"/>
<wire x1="4.5466" y1="-6.985" x2="4.5466" y2="6.985" width="0.0762" layer="21"/>
<smd name="R1" x="-3.1201375" y="-5.18921875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="R2" x="-2.3327375" y="-5.18921875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="R3" x="-1.5453375" y="-5.18921875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="T1" x="-3.1201375" y="-5.98678125" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="T2" x="-2.3327375" y="-5.98678125" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="T3" x="-1.5453375" y="-5.98678125" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="P1" x="-3.1201375" y="-4.391659375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="P2" x="-2.3327375" y="-4.391659375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="P3" x="-1.5453375" y="-4.391659375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="N1" x="-3.1201375" y="-3.5941" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="N2" x="-2.3327375" y="-3.5941" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="N3" x="-1.5453375" y="-3.5941" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="M1" x="-3.1201375" y="-2.796540625" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="M2" x="-2.3327375" y="-2.796540625" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="M3" x="-1.5453375" y="-2.796540625" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="L1" x="-3.1201375" y="-1.99898125" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="L2" x="-2.3327375" y="-1.99898125" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="L3" x="-1.5453375" y="-1.99898125" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="K1" x="-3.1201375" y="-1.20141875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="K2" x="-2.3327375" y="-1.20141875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="K3" x="-1.5453375" y="-1.20141875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="J1" x="-3.1201375" y="-0.403859375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="J2" x="-2.3327375" y="-0.403859375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="J3" x="-1.5453375" y="-0.403859375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="H1" x="-3.1201375" y="0.3937" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="H2" x="-2.3327375" y="0.3937" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="H3" x="-1.5453375" y="0.3937" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="G1" x="-3.1201375" y="1.191259375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="G2" x="-2.3327375" y="1.191259375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="G3" x="-1.5453375" y="1.191259375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="F1" x="-3.1201375" y="1.98881875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="F2" x="-2.3327375" y="1.98881875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="F3" x="-1.5453375" y="1.98881875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="E1" x="-3.1201375" y="2.78638125" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="E2" x="-2.3327375" y="2.78638125" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="E3" x="-1.5453375" y="2.78638125" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="D1" x="-3.1201375" y="3.583940625" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="D2" x="-2.3327375" y="3.583940625" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="D3" x="-1.5453375" y="3.583940625" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="C1" x="-3.1201375" y="4.3815" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="C2" x="-2.3327375" y="4.3815" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="C3" x="-1.5453375" y="4.3815" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="B1" x="-3.1201375" y="5.179059375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="B2" x="-2.3327375" y="5.179059375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="B3" x="-1.5453375" y="5.179059375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="A1" x="-3.1201375" y="5.97661875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="A2" x="-2.3327375" y="5.97661875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="A3" x="-1.5453375" y="5.97661875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="R7" x="1.6550625" y="-5.18921875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="R8" x="2.4424625" y="-5.18921875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="R9" x="3.2298625" y="-5.18921875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="T7" x="1.6550625" y="-5.98678125" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="T8" x="2.4424625" y="-5.98678125" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="T9" x="3.2298625" y="-5.98678125" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="P7" x="1.6550625" y="-4.391659375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="P8" x="2.4424625" y="-4.391659375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="P9" x="3.2298625" y="-4.391659375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="N7" x="1.6550625" y="-3.5941" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="N8" x="2.4424625" y="-3.5941" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="N9" x="3.2298625" y="-3.5941" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="M7" x="1.6550625" y="-2.796540625" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="M8" x="2.4424625" y="-2.796540625" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="M9" x="3.2298625" y="-2.796540625" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="L7" x="1.6550625" y="-1.99898125" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="L8" x="2.4424625" y="-1.99898125" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="L9" x="3.2298625" y="-1.99898125" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="K7" x="1.6550625" y="-1.20141875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="K8" x="2.4424625" y="-1.20141875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="K9" x="3.2298625" y="-1.20141875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="J7" x="1.6550625" y="-0.403859375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="J8" x="2.4424625" y="-0.403859375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="J9" x="3.2298625" y="-0.403859375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="H7" x="1.6550625" y="0.3937" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="H8" x="2.4424625" y="0.3937" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="H9" x="3.2298625" y="0.3937" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="G7" x="1.6550625" y="1.191259375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="G8" x="2.4424625" y="1.191259375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="G9" x="3.2298625" y="1.191259375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="F7" x="1.6550625" y="1.98881875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="F8" x="2.4424625" y="1.98881875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="F9" x="3.2298625" y="1.98881875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="E7" x="1.6550625" y="2.78638125" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="E8" x="2.4424625" y="2.78638125" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="E9" x="3.2298625" y="2.78638125" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="D7" x="1.6550625" y="3.583940625" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="D8" x="2.4424625" y="3.583940625" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="D9" x="3.2298625" y="3.583940625" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="C7" x="1.6550625" y="4.3815" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="C8" x="2.4424625" y="4.3815" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="C9" x="3.2298625" y="4.3815" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="B7" x="1.6550625" y="5.179059375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="B8" x="2.4424625" y="5.179059375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="B9" x="3.2298625" y="5.179059375" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="A7" x="1.6550625" y="5.97661875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="A8" x="2.4424625" y="5.97661875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<smd name="A9" x="3.2298625" y="5.97661875" dx="0.3556" dy="0.3556" layer="1" roundness="100"/>
<circle x="-3.937" y="6.5024" radius="0.30585625" width="0.127" layer="21"/>
<text x="-5.0292" y="5.715" size="0.508" layer="21">A</text>
<text x="-3.2766" y="-7.7216" size="0.508" layer="21">1</text>
<text x="3.1242" y="-7.7216" size="0.508" layer="21">9</text>
<text x="-5.0292" y="-5.461" size="0.508" layer="21">R</text>
<text x="-5.0292" y="4.1148" size="0.508" layer="21">C</text>
<text x="-5.0292" y="2.5146" size="0.508" layer="21">E</text>
<text x="-5.0292" y="0.9144" size="0.508" layer="21">G</text>
<text x="-5.0292" y="-0.6604" size="0.508" layer="21">J</text>
<text x="-5.0292" y="-2.2606" size="0.508" layer="21">L</text>
<text x="-5.0292" y="-3.8862" size="0.508" layer="21">N</text>
<text x="-1.651" y="-7.7216" size="0.508" layer="21">3</text>
<text x="1.5748" y="-7.7216" size="0.508" layer="21">7</text>
<text x="-2.3368" y="7.2898" size="0.508" layer="21">&gt;NAME</text>
</package>
<package name="SCHA5B0200">
<wire x1="8.14984375" y1="-7.62" x2="8.14831875" y2="-7.62" width="0.0508" layer="21"/>
<smd name="12" x="-6.63575" y="4.7625" dx="2.0828" dy="1.524" layer="1" rot="R90"/>
<smd name="1" x="-6.6675" y="-2.44025" dx="1.524" dy="0.44958125" layer="1" rot="R180"/>
<smd name="11" x="-6.6675" y="-6.1595" dx="1.499996875" dy="1.524" layer="1" rot="R270"/>
<smd name="13" x="-2.5250125" y="7.60095" dx="1.199896875" dy="1.524" layer="1"/>
<smd name="15" x="8.46734375" y="3.47870625" dx="1.524" dy="2.0828" layer="1"/>
<smd name="2" x="-6.6675" y="-3.14045625" dx="1.524" dy="0.44958125" layer="1" rot="R180"/>
<smd name="3" x="-6.6675" y="-3.840328125" dx="1.524" dy="0.44958125" layer="1" rot="R180"/>
<smd name="4" x="-6.6675" y="-4.54025" dx="1.524" dy="0.44958125" layer="1" rot="R180"/>
<wire x1="8.14831875" y1="-7.62" x2="-6.35" y2="-7.62" width="0.0508" layer="21"/>
<wire x1="-6.35" y1="-2.084071875" x2="-6.35" y2="-4.894578125" width="0.0508" layer="51"/>
<wire x1="-6.35" y1="-5.285740625" x2="-6.35" y2="-7.0485" width="0.0508" layer="51"/>
<smd name="8" x="8.4455" y="-2.44025" dx="1.524" dy="0.44958125" layer="1" rot="R180"/>
<smd name="16" x="8.4455" y="-6.25475" dx="1.499996875" dy="1.524" layer="1" rot="R270"/>
<smd name="7" x="8.4455" y="-3.14045625" dx="1.524" dy="0.44958125" layer="1" rot="R180"/>
<smd name="6" x="8.4455" y="-3.840328125" dx="1.524" dy="0.44958125" layer="1" rot="R180"/>
<smd name="5" x="8.4455" y="-4.54025" dx="1.524" dy="0.44958125" layer="1" rot="R180"/>
<wire x1="8.14831875" y1="-2.079753125" x2="8.14831875" y2="-4.87629375" width="0.0508" layer="51"/>
<wire x1="8.14831875" y1="-5.3975" x2="8.14831875" y2="-7.112" width="0.0508" layer="51"/>
<wire x1="-6.35" y1="5.93374375" x2="-6.35" y2="3.58775" width="0.0508" layer="51"/>
<wire x1="8.14984375" y1="4.66374375" x2="8.14984375" y2="2.31775" width="0.0508" layer="51"/>
<smd name="14" x="3.474921875" y="7.60095" dx="1.199896875" dy="1.524" layer="1"/>
<smd name="10" x="4.67486875" y="7.4295" dx="1.905" dy="0.635" layer="1" rot="R90"/>
<smd name="9" x="-5.106925" y="7.4295" dx="1.905" dy="0.635" layer="1" rot="R90"/>
<wire x1="2.76225" y1="7.3787" x2="5.11175" y2="7.3787" width="0.0508" layer="51"/>
<wire x1="-5.5245" y1="7.3787" x2="-4.699" y2="7.3787" width="0.0508" layer="51"/>
<wire x1="-3.20675" y1="7.3787" x2="-1.8415" y2="7.3787" width="0.0508" layer="51"/>
<wire x1="-6.35" y1="-7.62" x2="-6.35" y2="-7.0485" width="0.0508" layer="21"/>
<wire x1="-6.35" y1="-5.2705" x2="-6.35" y2="-4.899659375" width="0.0508" layer="21"/>
<wire x1="-6.35" y1="-2.07645" x2="-6.35" y2="3.58775" width="0.0508" layer="21"/>
<wire x1="-6.35" y1="5.93725" x2="-6.35" y2="7.3787" width="0.0508" layer="21"/>
<wire x1="-6.35" y1="7.3787" x2="-5.5626" y2="7.3787" width="0.0508" layer="21"/>
<wire x1="-4.65328125" y1="7.3787" x2="-3.25755" y2="7.3787" width="0.0508" layer="21"/>
<wire x1="-1.793240625" y1="7.3787" x2="2.726690625" y2="7.3787" width="0.0508" layer="21"/>
<wire x1="5.132071875" y1="7.3787" x2="8.128" y2="7.3787" width="0.0508" layer="21"/>
<wire x1="8.14831875" y1="-7.62" x2="8.14831875" y2="-7.142478125" width="0.0508" layer="21"/>
<wire x1="8.14831875" y1="-5.3594" x2="8.14831875" y2="-4.909821875" width="0.0508" layer="21"/>
<wire x1="8.14831875" y1="-2.07645" x2="8.14831875" y2="2.29743125" width="0.0508" layer="21"/>
<wire x1="8.14831875" y1="4.66725" x2="8.14831875" y2="7.366" width="0.0508" layer="21"/>
<text x="-0.8128" y="7.5946" size="0.508" layer="25">&gt;NAME</text>
</package>
<package name="MSOP-8_PAD">
<smd name="4" x="-2.0066" y="-0.9906" dx="0.3048" dy="1.524" layer="1" rot="R90"/>
<smd name="3" x="-2.0066" y="-0.3302" dx="0.3048" dy="1.524" layer="1" rot="R90"/>
<smd name="2" x="-2.0066" y="0.3302" dx="0.3048" dy="1.524" layer="1" rot="R90"/>
<smd name="1" x="-2.0066" y="0.9906" dx="0.3048" dy="1.524" layer="1" rot="R90"/>
<smd name="5" x="2.0828" y="-0.9906" dx="0.3048" dy="1.524" layer="1" rot="R90"/>
<smd name="8" x="2.0828" y="0.9906" dx="0.3048" dy="1.524" layer="1" rot="R90"/>
<smd name="7" x="2.0828" y="0.3556" dx="0.3048" dy="1.524" layer="1" rot="R90"/>
<smd name="6" x="2.0828" y="-0.3302" dx="0.3048" dy="1.524" layer="1" rot="R90"/>
<text x="-1.27" y="1.651" size="0.508" layer="21">&gt;NAME</text>
<wire x1="-1.524" y1="1.524" x2="-1.524" y2="-1.524" width="0.127" layer="51"/>
<wire x1="1.524" y1="1.524" x2="1.524" y2="-1.524" width="0.127" layer="51"/>
<wire x1="-1.524" y1="1.524" x2="1.524" y2="1.524" width="0.127" layer="21"/>
<wire x1="-1.524" y1="-1.524" x2="1.524" y2="-1.524" width="0.127" layer="21"/>
<smd name="PAD" x="0" y="0" dx="1.799996875" dy="1.499996875" layer="1" rot="R90"/>
</package>
<package name="USB">
<description>TH USB layout based off of 87520-0010BLF footprint.</description>
<wire x1="-6.35" y1="-8.89" x2="6.78941875" y2="-8.89" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-8.89" x2="-6.35" y2="-3.81" width="0.127" layer="51"/>
<wire x1="-6.35" y1="6.830059375" x2="6.78941875" y2="6.830059375" width="0.127" layer="21"/>
<wire x1="6.78941875" y1="-8.89" x2="6.78941875" y2="-3.81" width="0.127" layer="51"/>
<pad name="SH1" x="-6.35" y="1.397" drill="2.301240625"/>
<pad name="SH2" x="6.78941875" y="1.397" drill="2.2987"/>
<pad name="4" x="3.719703125" y="5.08" drill="0.949959375"/>
<pad name="2" x="-0.778509375" y="5.08" drill="0.949959375"/>
<pad name="3" x="1.21793125" y="5.08" drill="0.949959375"/>
<pad name="1" x="-3.280284375" y="5.08" drill="0.949959375"/>
<wire x1="-6.35" y1="-3.81" x2="6.78941875" y2="-3.81" width="0.127" layer="21"/>
<rectangle x1="-6.35" y1="-8.89" x2="6.78941875" y2="-2.54" layer="39"/>
<wire x1="-6.35" y1="3.81" x2="-6.35" y2="-0.635" width="0.127" layer="51"/>
<wire x1="6.78941875" y1="3.81" x2="6.78941875" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-6.35" y1="6.830059375" x2="-6.35" y2="3.81" width="0.127" layer="21"/>
<wire x1="6.78941875" y1="6.830059375" x2="6.78941875" y2="3.81" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.81" x2="-6.35" y2="-0.635" width="0.127" layer="21"/>
<wire x1="6.78941875" y1="-3.81" x2="6.78941875" y2="-0.635" width="0.127" layer="21"/>
<text x="-2.5908" y="7.0104" size="0.508" layer="25">&gt;NAME</text>
</package>
<package name="QFN-32">
<wire x1="-2.45" y1="2.0828" x2="-2.45" y2="2.45" width="0.1016" layer="21"/>
<wire x1="-2.45" y1="2.45" x2="-2.0828" y2="2.45" width="0.1016" layer="21"/>
<wire x1="2.0828" y1="2.45" x2="2.45" y2="2.45" width="0.1016" layer="21"/>
<wire x1="2.45" y1="2.45" x2="2.45" y2="2.0828" width="0.1016" layer="21"/>
<wire x1="2.45" y1="-2.0828" x2="2.45" y2="-2.45" width="0.1016" layer="21"/>
<wire x1="2.45" y1="-2.45" x2="2.0828" y2="-2.45" width="0.1016" layer="21"/>
<wire x1="-2.0828" y1="-2.45" x2="-2.45" y2="-2.45" width="0.1016" layer="21"/>
<wire x1="-2.45" y1="-2.45" x2="-2.45" y2="-2.0828" width="0.1016" layer="21"/>
<circle x="-2.175" y="2.175" radius="0.15" width="0" layer="21"/>
<smd name="EXP" x="-0.9525" y="0.889" dx="1.27" dy="1.4224" layer="1" roundness="20"/>
<text x="2.54873125" y="2.510859375" size="0.508" layer="25">&gt;NAME</text>
<smd name="5" x="-2.44119375" y="-0.250190625" dx="0.3048" dy="0.889" layer="1" rot="R90"/>
<smd name="2" x="-2.440940625" y="1.2468875" dx="0.3048" dy="0.889" layer="1" rot="R90"/>
<smd name="1" x="-2.441703125" y="1.74980625" dx="0.3048" dy="0.889" layer="1" rot="R90"/>
<smd name="8" x="-2.440434375" y="-1.7518375" dx="0.3048" dy="0.889" layer="1" rot="R90"/>
<smd name="6" x="-2.441446875" y="-0.746759375" dx="0.3048" dy="0.889" layer="1" rot="R90"/>
<smd name="3" x="-2.4406875" y="0.7500625" dx="0.3048" dy="0.889" layer="1" rot="R90"/>
<smd name="7" x="-2.440940625" y="-1.249934375" dx="0.3048" dy="0.889" layer="1" rot="R90"/>
<smd name="4" x="-2.44043125" y="0.248921875" dx="0.3048" dy="0.889" layer="1" rot="R90"/>
<smd name="29" x="-0.250190625" y="2.44119375" dx="0.3048" dy="0.889" layer="1"/>
<smd name="26" x="1.2468875" y="2.440940625" dx="0.3048" dy="0.889" layer="1"/>
<smd name="25" x="1.74980625" y="2.441703125" dx="0.3048" dy="0.889" layer="1"/>
<smd name="32" x="-1.7518375" y="2.440434375" dx="0.3048" dy="0.889" layer="1"/>
<smd name="30" x="-0.746759375" y="2.441446875" dx="0.3048" dy="0.889" layer="1"/>
<smd name="27" x="0.7500625" y="2.4406875" dx="0.3048" dy="0.889" layer="1"/>
<smd name="31" x="-1.249934375" y="2.440940625" dx="0.3048" dy="0.889" layer="1"/>
<smd name="28" x="0.248921875" y="2.44043125" dx="0.3048" dy="0.889" layer="1"/>
<smd name="21" x="2.44119375" y="0.250190625" dx="0.3048" dy="0.889" layer="1" rot="R270"/>
<smd name="18" x="2.440940625" y="-1.2468875" dx="0.3048" dy="0.889" layer="1" rot="R270"/>
<smd name="17" x="2.441703125" y="-1.74980625" dx="0.3048" dy="0.889" layer="1" rot="R270"/>
<smd name="24" x="2.440434375" y="1.7518375" dx="0.3048" dy="0.889" layer="1" rot="R270"/>
<smd name="22" x="2.441446875" y="0.746759375" dx="0.3048" dy="0.889" layer="1" rot="R270"/>
<smd name="19" x="2.4406875" y="-0.7500625" dx="0.3048" dy="0.889" layer="1" rot="R270"/>
<smd name="23" x="2.440940625" y="1.249934375" dx="0.3048" dy="0.889" layer="1" rot="R270"/>
<smd name="20" x="2.44043125" y="-0.248921875" dx="0.3048" dy="0.889" layer="1" rot="R270"/>
<smd name="13" x="0.250190625" y="-2.44119375" dx="0.3048" dy="0.889" layer="1" rot="R180"/>
<smd name="10" x="-1.2468875" y="-2.440940625" dx="0.3048" dy="0.889" layer="1" rot="R180"/>
<smd name="9" x="-1.74980625" y="-2.441703125" dx="0.3048" dy="0.889" layer="1" rot="R180"/>
<smd name="16" x="1.7518375" y="-2.440434375" dx="0.3048" dy="0.889" layer="1" rot="R180"/>
<smd name="14" x="0.746759375" y="-2.441446875" dx="0.3048" dy="0.889" layer="1" rot="R180"/>
<smd name="11" x="-0.7500625" y="-2.4406875" dx="0.3048" dy="0.889" layer="1" rot="R180"/>
<smd name="15" x="1.249934375" y="-2.440940625" dx="0.3048" dy="0.889" layer="1" rot="R180"/>
<smd name="12" x="-0.248921875" y="-2.44043125" dx="0.3048" dy="0.889" layer="1" rot="R180"/>
<smd name="EXP1" x="0.9525" y="0.889" dx="1.27" dy="1.4224" layer="1" roundness="20"/>
<smd name="EXP2" x="-0.9525" y="-0.889" dx="1.27" dy="1.4224" layer="1" roundness="20"/>
<smd name="EXP3" x="0.9525" y="-0.889" dx="1.27" dy="1.4224" layer="1" roundness="20"/>
</package>
<package name="MAGJACK">
<description>Bel Fuse Inc 08B0-1D1T-06-F, CONN MAGJACK 1PORT 100 BASE-T</description>
<wire x1="-9.525" y1="-7.62" x2="-9.525" y2="8.6868" width="0.127" layer="21"/>
<hole x="-3.048" y="6.8834" drill="3.2512"/>
<hole x="-3.048" y="-5.8166" drill="3.2512"/>
<pad name="7" x="10.5918" y="-6.6802" drill="1.1"/>
<pad name="1" x="8.128" y="-4.5466" drill="1.1" shape="square"/>
<pad name="2" x="8.128" y="-2.5146" drill="1.1"/>
<pad name="3" x="8.128" y="-0.4826" drill="1.1"/>
<pad name="4" x="8.128" y="1.5494" drill="1.1"/>
<pad name="5" x="8.128" y="3.5814" drill="1.1"/>
<pad name="6" x="8.128" y="5.6134" drill="1.1"/>
<pad name="8" x="10.5918" y="-4.1402" drill="1.1"/>
<pad name="9" x="10.5918" y="5.207" drill="1.1"/>
<pad name="10" x="10.5918" y="7.747" drill="1.1"/>
<rectangle x1="-13.97" y1="-8.5344" x2="-9.398" y2="9.4996" layer="39"/>
<pad name="S1" x="-6.223" y="8.6106" drill="1.6"/>
<pad name="S2" x="-6.223" y="-7.5184" drill="1.6"/>
<wire x1="-9.3726" y1="8.6868" x2="-13.97" y2="8.6868" width="0.127" layer="51"/>
<wire x1="-13.97" y1="8.6868" x2="-13.97" y2="-7.62" width="0.127" layer="51"/>
<wire x1="-13.97" y1="-7.62" x2="-1.778" y2="-7.62" width="0.127" layer="51"/>
<text x="-0.4318" y="9.0932" size="1.016" layer="21">&gt;NAME</text>
<rectangle x1="-9.017" y1="-3.8735" x2="6.858" y2="5.0165" layer="39"/>
<rectangle x1="-1.016" y1="5.3975" x2="6.985" y2="8.4455" layer="39"/>
<rectangle x1="-1.016" y1="-7.3025" x2="6.985" y2="-4.2545" layer="39"/>
<wire x1="-1.7526" y1="8.6868" x2="-7.62" y2="8.6868" width="0.127" layer="51"/>
<wire x1="-9.525" y1="8.6868" x2="-7.747" y2="8.6868" width="0.127" layer="21"/>
<wire x1="11.557" y1="8.6868" x2="9.652" y2="8.6868" width="0.127" layer="51"/>
<wire x1="-1.651" y1="8.6868" x2="9.525" y2="8.6868" width="0.127" layer="21"/>
<wire x1="11.557" y1="4.318" x2="11.557" y2="8.6868" width="0.127" layer="51"/>
<wire x1="11.557" y1="-3.302" x2="11.557" y2="4.3688" width="0.127" layer="21"/>
<wire x1="11.557" y1="-7.62" x2="11.557" y2="-3.2512" width="0.127" layer="51"/>
<wire x1="-9.525" y1="-7.62" x2="-7.747" y2="-7.62" width="0.127" layer="21"/>
<wire x1="-1.651" y1="-7.62" x2="9.779" y2="-7.62" width="0.127" layer="21"/>
<wire x1="11.557" y1="-7.62" x2="9.652" y2="-7.62" width="0.127" layer="51"/>
</package>
<package name="SMD-0603">
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<smd name="1" x="-0.69125" y="0" dx="0.762" dy="1.1" layer="1"/>
<smd name="2" x="0.69125" y="0" dx="0.762" dy="1.1" layer="1"/>
<text x="1.3335" y="0.0254" size="0.254" layer="25">&gt;NAME</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-1.2458" y1="0.713" x2="1.26485" y2="0.713" width="0.0508" layer="39"/>
<wire x1="-1.2464" y1="-0.71875" x2="-1.2464" y2="0.7124" width="0.0508" layer="39"/>
<wire x1="1.2682" y1="-0.71875" x2="1.2682" y2="0.7124" width="0.0508" layer="39"/>
<wire x1="-1.25095" y1="0.7366" x2="-1.25095" y2="-0.73025" width="0.127" layer="21"/>
<wire x1="1.25095" y1="-0.73025" x2="1.25095" y2="0.7366" width="0.127" layer="21"/>
<wire x1="1.25095" y1="0.7366" x2="-1.25095" y2="0.7366" width="0.127" layer="21"/>
<wire x1="-1.2458" y1="-0.71575" x2="1.26485" y2="-0.71575" width="0.0508" layer="39"/>
<wire x1="1.25095" y1="-0.73025" x2="-1.25095" y2="-0.73025" width="0.127" layer="21"/>
</package>
<package name="MAGJACK_BB">
<wire x1="21.844" y1="0" x2="21.844" y2="15.88008125" width="0.127" layer="21"/>
<pad name="3" x="17.239996875" y="9.845040625" drill="1.016" shape="octagon" rot="R270"/>
<rectangle x1="16.982440625" y1="9.605009375" x2="17.490440625" y2="10.113009375" layer="51" rot="R270"/>
<pad name="5" x="17.239996875" y="7.305040625" drill="1.016" shape="octagon" rot="R270"/>
<rectangle x1="17.00085625" y1="7.075171875" x2="17.50885625" y2="7.583171875" layer="51" rot="R270"/>
<pad name="7" x="17.239996875" y="4.765040625" drill="1.016" shape="octagon" rot="R270"/>
<rectangle x1="16.995140625" y1="4.52246875" x2="17.503140625" y2="5.03046875" layer="51" rot="R270"/>
<pad name="1" x="17.239996875" y="12.385040625" drill="1.016" shape="square" rot="R270"/>
<rectangle x1="17.00085625" y1="12.143740625" x2="17.50885625" y2="12.651740625" layer="51" rot="R270"/>
<pad name="2" x="19.779996875" y="11.115040625" drill="1.016" shape="octagon" rot="R270"/>
<rectangle x1="19.556096875" y1="10.87945625" x2="20.064096875" y2="11.38745625" layer="51" rot="R270"/>
<pad name="4" x="19.779996875" y="8.575040625" drill="1.016" shape="octagon" rot="R270"/>
<rectangle x1="19.544665625" y1="8.33755" x2="20.052665625" y2="8.84555" layer="51" rot="R270"/>
<pad name="6" x="19.779996875" y="6.035040625" drill="1.016" shape="octagon" rot="R270"/>
<rectangle x1="19.530696875" y1="5.785484375" x2="20.038696875" y2="6.293484375" layer="51" rot="R270"/>
<pad name="8" x="19.779996875" y="3.495040625" drill="1.016" shape="octagon" rot="R270"/>
<rectangle x1="19.516725" y1="3.231515625" x2="20.024725" y2="3.739515625" layer="51" rot="R270"/>
<pad name="GC" x="6.1468" y="11.724640625" drill="1.016" shape="octagon" rot="R270"/>
<rectangle x1="4.3662625" y1="3.914140625" x2="4.8742625" y2="4.422140625" layer="52" rot="R270"/>
<pad name="GA" x="4.6228" y="14.264640625" drill="1.016" shape="octagon" rot="R270"/>
<rectangle x1="5.8801" y1="1.361440625" x2="6.3881" y2="1.869440625" layer="51" rot="R270"/>
<pad name="YC" x="4.6228" y="4.155440625" drill="1.016" shape="octagon" rot="R270"/>
<rectangle x1="4.377053125" y1="14.02841875" x2="4.885053125" y2="14.53641875" layer="51" rot="R270"/>
<pad name="YA" x="6.1468" y="1.615440625" drill="1.016" shape="octagon" rot="R270"/>
<rectangle x1="5.882640625" y1="11.46619375" x2="6.390640625" y2="11.97419375" layer="51" rot="R270"/>
<hole x="10.889996875" y="13.655040625" drill="3.302"/>
<hole x="10.889996875" y="2.225040625" drill="3.302"/>
<pad name="SHIELD1" x="13.937996875" y="15.81505625" drill="1.016" shape="octagon" rot="R270"/>
<pad name="SHIELD2" x="13.937996875" y="0.065025" drill="1.016" shape="octagon" rot="R270"/>
<wire x1="12.89558125" y1="0" x2="15.03171875" y2="0" width="0.127" layer="51"/>
<wire x1="12.826365625" y1="15.88008125" x2="15.179040625" y2="15.88008125" width="0.127" layer="51"/>
<wire x1="2.54" y1="0" x2="12.89558125" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="15.88008125" x2="12.89558125" y2="15.88008125" width="0.127" layer="21"/>
<wire x1="21.844" y1="0" x2="15.03171875" y2="0" width="0.127" layer="21"/>
<wire x1="21.844" y1="15.88008125" x2="15.179040625" y2="15.88008125" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="0" y2="0" width="0.127" layer="51"/>
<wire x1="0" y1="0" x2="0" y2="15.88008125" width="0.127" layer="51"/>
<wire x1="0" y1="15.88008125" x2="2.54" y2="15.88008125" width="0.127" layer="51"/>
<text x="3.81" y="16.51" size="1.016" layer="21">&gt;NAME</text>
</package>
<package name="NLFV">
<smd name="1" x="0" y="0" dx="1.524" dy="0.99821875" layer="1" rot="R90"/>
<smd name="2" x="2.54" y="0" dx="1.524" dy="0.99821875" layer="1" rot="R90"/>
<wire x1="-0.6985" y1="1.2065" x2="3.302" y2="1.2065" width="0.127" layer="21"/>
<wire x1="3.302" y1="1.2065" x2="3.302" y2="-1.2065" width="0.127" layer="21"/>
<wire x1="3.302" y1="-1.2065" x2="-0.6985" y2="-1.2065" width="0.127" layer="21"/>
<wire x1="-0.6985" y1="-1.2065" x2="-0.6985" y2="1.2065" width="0.127" layer="21"/>
<rectangle x1="0" y1="-1.016" x2="2.54" y2="1.016" layer="51"/>
<text x="-0.762" y="1.4605" size="0.4064" layer="25">&gt;NAME</text>
</package>
<package name="SLF7032">
<description>SLF7032 Inductor Package (7mm x 7mm)</description>
<smd name="1" x="-0.1524" y="0" dx="1.524" dy="2.199640625" layer="1" rot="R180"/>
<wire x1="-0.1778" y1="3.5154875" x2="6.822184375" y2="3.5154875" width="0.127" layer="21"/>
<smd name="2" x="6.6421" y="0" dx="1.524" dy="2.199640625" layer="1" rot="R180"/>
<wire x1="-0.1778" y1="-3.4845" x2="6.822184375" y2="-3.4845" width="0.127" layer="21"/>
<wire x1="-0.1778" y1="1.27" x2="-0.1778" y2="-1.2954" width="0.127" layer="51"/>
<wire x1="6.822184375" y1="1.2954" x2="6.822184375" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-0.1778" y1="3.5154875" x2="-0.1778" y2="1.27" width="0.127" layer="21"/>
<wire x1="-0.1778" y1="-1.2954" x2="-0.1778" y2="-3.4845" width="0.127" layer="21"/>
<wire x1="6.822184375" y1="1.2954" x2="6.822184375" y2="3.5154875" width="0.127" layer="21"/>
<wire x1="6.822184375" y1="-3.4845" x2="6.822184375" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0.889" y1="0" x2="1.3716" y2="0" width="0.127" layer="21"/>
<wire x1="1.3716" y1="0" x2="2.6416" y2="0" width="0.127" layer="21" curve="-180"/>
<wire x1="2.6416" y1="0" x2="3.9116" y2="0" width="0.127" layer="21" curve="-180"/>
<wire x1="3.9116" y1="0" x2="5.1816" y2="0" width="0.127" layer="21" curve="-180"/>
<wire x1="5.6642" y1="0" x2="5.1816" y2="0" width="0.127" layer="21"/>
<text x="-0.0508" y="4.0386" size="0.4064" layer="25">&gt;NAME</text>
</package>
<package name="TH_0.2&quot;">
<description>Generic Through-Hole - 0.2" Pitch</description>
<pad name="1" x="0" y="0" drill="0.8"/>
<pad name="2" x="5.08" y="0" drill="0.8"/>
<wire x1="-1.905" y1="1.27" x2="-1.905" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="6.985" y2="-1.27" width="0.127" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="6.985" y2="1.27" width="0.127" layer="21"/>
<wire x1="6.985" y1="1.27" x2="-1.905" y2="1.27" width="0.127" layer="21"/>
<text x="-1.5875" y="1.5875" size="0.6096" layer="25">&gt;NAME</text>
</package>
<package name="SSQ">
<description>SSQ Fuse Layout</description>
<smd name="1" x="-2.7305" y="0" dx="3.175" dy="2.286" layer="1" rot="R90"/>
<smd name="2" x="2.921" y="0" dx="3.175" dy="2.286" layer="1" rot="R90"/>
<wire x1="-4.2545" y1="1.905" x2="4.3815" y2="1.905" width="0.127" layer="21"/>
<wire x1="4.3815" y1="1.905" x2="4.3815" y2="-1.905" width="0.127" layer="21"/>
<wire x1="4.3815" y1="-1.905" x2="-4.2545" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-4.2545" y1="-1.905" x2="-4.2545" y2="1.905" width="0.127" layer="21"/>
<text x="-4.064" y="2.159" size="0.6096" layer="21">&gt;NAME</text>
</package>
<package name="SMD-2312(6032)">
<description>Covers 6032 metric packages - 2312, 2413</description>
<wire x1="-2.8" y1="1.55" x2="2.8" y2="1.55" width="0.1016" layer="21"/>
<wire x1="2.8" y1="1.55" x2="2.8" y2="-1.55" width="0.1016" layer="51"/>
<wire x1="2.8" y1="-1.55" x2="-2.8" y2="-1.55" width="0.1016" layer="21"/>
<wire x1="-2.8" y1="-1.55" x2="-2.8" y2="1.55" width="0.1016" layer="51"/>
<smd name="+" x="2.652" y="0" dx="1.905" dy="2.5" layer="1"/>
<smd name="-" x="-2.652" y="0" dx="1.905" dy="2.5" layer="1"/>
<rectangle x1="-3" y1="-1.1" x2="-2.85" y2="1.1" layer="51"/>
<rectangle x1="2.85" y1="-1.1" x2="3" y2="1.1" layer="51"/>
<rectangle x1="1.95" y1="-1.575" x2="2.45" y2="1.575" layer="51"/>
<wire x1="3.1115" y1="2.286" x2="3.1115" y2="1.651" width="0.127" layer="21"/>
<wire x1="2.7305" y1="1.9685" x2="3.429" y2="1.9685" width="0.127" layer="21"/>
<text x="-2.54" y="1.905" size="0.4064" layer="25">&gt;NAME</text>
<wire x1="-3.8112" y1="1.67185" x2="3.80485" y2="1.67185" width="0.0508" layer="39"/>
<wire x1="-3.8112" y1="-1.6492" x2="3.80485" y2="-1.6492" width="0.0508" layer="39"/>
<wire x1="3.8082" y1="-1.64585" x2="3.8082" y2="1.67125" width="0.0508" layer="39"/>
<wire x1="-3.8118" y1="-1.64585" x2="-3.8118" y2="1.67125" width="0.0508" layer="39"/>
</package>
<package name="SMD-1411_POL">
<smd name="1" x="-1.9685" y="0" dx="2.3495" dy="2.2098" layer="1" rot="R90"/>
<smd name="2" x="2.0955" y="0" dx="2.3495" dy="2.2098" layer="1" rot="R90"/>
<wire x1="-3.27025" y1="1.96215" x2="3.40995" y2="1.9685" width="0.127" layer="21"/>
<wire x1="3.40995" y1="1.9685" x2="3.41121875" y2="1.9685" width="0.127" layer="21"/>
<wire x1="-3.27025" y1="-2.03835" x2="-3.27025" y2="1.96215" width="0.127" layer="21"/>
<wire x1="3.40995" y1="-2.02565" x2="3.40995" y2="1.9685" width="0.127" layer="21"/>
<wire x1="-3.27025" y1="-2.03835" x2="3.40486875" y2="-2.032" width="0.127" layer="21"/>
<text x="-3.048" y="2.2225" size="0.6096" layer="25">&gt;NAME</text>
<wire x1="-0.3175" y1="1.4605" x2="-0.3175" y2="-1.4605" width="0.127" layer="21"/>
<wire x1="0.762" y1="-1.143" x2="0.762" y2="1.143" width="0.127" layer="21" curve="-137.726311"/>
<wire x1="-3.2524" y1="1.983" x2="3.41115" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-3.2524" y1="-2.0175" x2="3.41115" y2="-2.0175" width="0.0508" layer="39"/>
<wire x1="-3.253" y1="-2.0205" x2="-3.253" y2="1.9824" width="0.0508" layer="39"/>
<wire x1="3.4145" y1="-2.0205" x2="3.4145" y2="1.9824" width="0.0508" layer="39"/>
</package>
<package name="SMD-2512">
<description>SMD Cap/Res 2512 Package</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="1.473" x2="1.498" y2="1.473" width="0.1524" layer="21"/>
<wire x1="-1.473" y1="-1.473" x2="1.498" y2="-1.473" width="0.1524" layer="21"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="4.01955" y="-0.10795" size="0.6096" layer="25">&gt;NAME</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
<wire x1="-3.86835" y1="1.82425" x2="3.8844" y2="1.82425" width="0.0508" layer="39"/>
<wire x1="-3.86835" y1="-1.827" x2="3.8844" y2="-1.827" width="0.0508" layer="39"/>
<wire x1="-3.86895" y1="-1.8173" x2="-3.86895" y2="1.82365" width="0.0508" layer="39"/>
<wire x1="3.8844" y1="-1.827" x2="3.8844" y2="1.82425" width="0.0508" layer="39"/>
<wire x1="-3.8989" y1="1.8288" x2="-3.8989" y2="-1.83515" width="0.127" layer="21"/>
<wire x1="-3.8989" y1="-1.83515" x2="3.93065" y2="-1.83515" width="0.127" layer="21"/>
<wire x1="3.93065" y1="-1.83515" x2="3.93065" y2="1.83515" width="0.127" layer="21"/>
<wire x1="3.93065" y1="1.83515" x2="-3.8989" y2="1.8288" width="0.127" layer="21"/>
</package>
<package name="RES_TH">
<description>Resistor Through-hole (Tall)</description>
<pad name="1" x="-1.905" y="0" drill="0.8"/>
<pad name="2" x="1.5875" y="0" drill="0.8"/>
<circle x="-1.905" y="0" radius="1.27" width="0.127" layer="21"/>
<wire x1="-0.635" y1="0" x2="0.635" y2="0" width="0.127" layer="21"/>
<text x="-0.635" y="1.27" size="0.4064" layer="21">&gt;NAME</text>
</package>
<package name="CAP_TH">
<description>0.886" pitch</description>
<pad name="1" x="0" y="0" drill="0.8"/>
<pad name="2" x="22.5044" y="0" drill="0.8"/>
<wire x1="-1.27" y1="2.54" x2="24.13" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="24.13" y2="-2.54" width="0.127" layer="21"/>
<wire x1="24.13" y1="-2.54" x2="24.13" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<text x="-0.635" y="3.175" size="0.4064" layer="25">&gt;NAME</text>
</package>
<package name="DIODE_TH">
<description>Protection Diode package</description>
<wire x1="-0.9525" y1="0" x2="-0.3175" y2="0" width="0.1524" layer="21"/>
<wire x1="0.6985" y1="0.635" x2="0.6985" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.6985" y1="-0.635" x2="-0.3175" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.3175" y1="0" x2="1.2065" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.3175" y1="0" x2="0.6985" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.3175" y1="0.635" x2="-0.3175" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.635" width="0.1524" layer="21"/>
<pad name="A" x="2.2225" y="0" drill="1.1176"/>
<pad name="C" x="-2.54" y="0" drill="1.1176"/>
<text x="-0.762" y="1.651" size="0.508" layer="25" ratio="10">&gt;NAME</text>
<circle x="-2.54" y="0" radius="1.419903125" width="0.127" layer="21"/>
</package>
<package name="POWERDI-123">
<smd name="C" x="-1.27" y="0" dx="2.1844" dy="1.397" layer="1"/>
<smd name="A" x="1.3405" y="0" dx="0.889" dy="1.397" layer="1"/>
<wire x1="-1.7272" y1="-0.8636" x2="-1.7272" y2="0.9144" width="0.2032" layer="51"/>
<wire x1="-1.7272" y1="0.9144" x2="1.0668" y2="0.9144" width="0.2032" layer="51"/>
<wire x1="1.0668" y1="0.9144" x2="1.0668" y2="-0.8636" width="0.2032" layer="51"/>
<wire x1="1.0668" y1="-0.8636" x2="-1.7272" y2="-0.8636" width="0.2032" layer="51"/>
<rectangle x1="-1.4732" y1="-0.508" x2="-0.127" y2="0.5842" layer="51"/>
<rectangle x1="0.8636" y1="-0.4826" x2="1.3716" y2="0.5334" layer="51"/>
<wire x1="-0.127" y1="0.9144" x2="0.87121875" y2="0.9144" width="0.127" layer="21"/>
<rectangle x1="-2.0447" y1="-0.4826" x2="-1.4859" y2="0.5334" layer="51"/>
<wire x1="-1.0668" y1="0.9144" x2="-1.0668" y2="-0.8636" width="0.2032" layer="51"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
</package>
<package name="PMDU">
<smd name="C" x="1.143" y="0" dx="1.3716" dy="1.016" layer="1" rot="R270"/>
<smd name="A" x="-2.032" y="0" dx="1.3716" dy="1.016" layer="1" rot="R270"/>
<wire x1="-2.032" y1="0.889" x2="1.016" y2="0.889" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-0.889" x2="1.016" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0" y1="0.5715" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.762" y1="0.5715" x2="-0.762" y2="0" width="0.127" layer="21"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-0.508" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="-0.762" y2="0.5715" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="-0.762" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.762" y1="0" x2="-1.143" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0.3175" y2="0" width="0.127" layer="21"/>
<text x="-1.9685" y="-1.5875" size="0.4064" layer="25">&gt;NAME</text>
</package>
<package name="FAN_30MM">
<wire x1="-5.08" y1="-13.97" x2="5.08" y2="-13.97" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-13.97" x2="-5.08" y2="16.51" width="0.127" layer="21"/>
<wire x1="-5.08" y1="16.51" x2="5.08" y2="16.51" width="0.127" layer="21"/>
<wire x1="5.08" y1="16.51" x2="5.08" y2="-13.97" width="0.127" layer="21"/>
<rectangle x1="-5.08" y1="-13.97" x2="5.08" y2="16.51" layer="39"/>
<text x="-5.715" y="-13.335" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<wire x1="-2.5146" y1="-17.1196" x2="-2.5146" y2="-15.8496" width="0.1524" layer="21"/>
<wire x1="-2.5146" y1="-15.8496" x2="-1.8796" y2="-15.2146" width="0.1524" layer="21"/>
<wire x1="-2.5146" y1="-17.1196" x2="-1.8796" y2="-17.7546" width="0.1524" layer="21"/>
<wire x1="1.9304" y1="-15.2146" x2="2.5654" y2="-15.8496" width="0.1524" layer="21"/>
<wire x1="-1.8796" y1="-17.7546" x2="1.9304" y2="-17.7546" width="0.1524" layer="21"/>
<wire x1="1.9304" y1="-17.7546" x2="2.5654" y2="-17.1196" width="0.1524" layer="21"/>
<wire x1="2.5654" y1="-15.8496" x2="2.5654" y2="-17.1196" width="0.1524" layer="21"/>
<wire x1="-1.8796" y1="-15.2146" x2="1.9304" y2="-15.2146" width="0.1524" layer="21"/>
<pad name="2" x="1.2954" y="-16.4846" drill="0.6" diameter="1.4224" shape="octagon" rot="R90"/>
<pad name="1" x="-1.2446" y="-16.4846" drill="0.6" diameter="1.4224" shape="octagon" rot="R90"/>
</package>
<package name="SOT669">
<description>SOT669, LFPAK POWER SO-8</description>
<wire x1="0" y1="0" x2="0.3175" y2="0" width="0.127" layer="21"/>
<wire x1="0.3175" y1="0" x2="4.7625" y2="0" width="0.127" layer="51"/>
<wire x1="4.7625" y1="0" x2="4.999990625" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-4.099559375" width="0.127" layer="21"/>
<wire x1="0" y1="-4.1275" x2="4.999990625" y2="-4.1275" width="0.127" layer="51"/>
<wire x1="5.0165" y1="0" x2="5.0165" y2="-4.099559375" width="0.127" layer="21"/>
<wire x1="4.7625" y1="0" x2="4.7625" y2="1.299971875" width="0.127" layer="21"/>
<wire x1="0.3175" y1="1.3335" x2="0.889" y2="1.3335" width="0.127" layer="21"/>
<wire x1="0.889" y1="1.3335" x2="4.191" y2="1.3335" width="0.127" layer="51"/>
<wire x1="4.191" y1="1.3335" x2="4.726940625" y2="1.3335" width="0.127" layer="21"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="1.299971875" width="0.127" layer="21"/>
<wire x1="0.889" y1="-2.921" x2="4.191" y2="-2.921" width="0.127" layer="51"/>
<wire x1="0.889" y1="1.3335" x2="0.889" y2="-2.86511875" width="0.127" layer="51"/>
<wire x1="4.191" y1="1.3335" x2="4.191" y2="-2.86511875" width="0.127" layer="51"/>
<rectangle x1="0.3175" y1="-5.49148125" x2="0.8153375" y2="-4.191" layer="51"/>
<rectangle x1="1.5875" y1="-5.49148125" x2="2.0853375" y2="-4.191" layer="51"/>
<rectangle x1="2.921" y1="-5.49148125" x2="3.4188375" y2="-4.191" layer="51"/>
<rectangle x1="4.191" y1="-5.49148125" x2="4.6888375" y2="-4.191" layer="51"/>
<smd name="1" x="0.5715" y="-4.953" dx="1.905" dy="0.762" layer="1" rot="R90"/>
<smd name="3" x="3.175" y="-4.953" dx="1.905" dy="0.762" layer="1" rot="R90"/>
<smd name="4" x="4.445" y="-4.953" dx="1.905" dy="0.762" layer="1" rot="R90"/>
<smd name="2" x="1.8415" y="-4.953" dx="1.905" dy="0.762" layer="1" rot="R90"/>
<smd name="5" x="2.54" y="-0.4445" dx="3.302" dy="4.826" layer="1"/>
</package>
<package name="SOT23-3">
<smd name="1" x="0.1778" y="1.016" dx="1.016" dy="0.635" layer="1" rot="R90"/>
<smd name="3" x="1.0922" y="-0.889" dx="1.016" dy="0.635" layer="1" rot="R90"/>
<smd name="2" x="-0.7366" y="-0.889" dx="1.016" dy="0.635" layer="1" rot="R90"/>
<wire x1="-1.27" y1="-0.635" x2="1.6256" y2="-0.635" width="0.0635" layer="51"/>
<wire x1="1.6256" y1="-0.635" x2="1.6256" y2="0.762" width="0.0635" layer="21"/>
<wire x1="1.6256" y1="0.762" x2="-1.27" y2="0.762" width="0.0635" layer="51"/>
<wire x1="-1.27" y1="0.762" x2="-1.27" y2="-0.635" width="0.0635" layer="21"/>
<text x="-1.513840625" y="-0.624840625" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="SMD-1206">
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<text x="2.5781" y="-0.127" size="0.4064" layer="25">&gt;NAME</text>
<wire x1="-2.35705" y1="1.06225" x2="2.3477" y2="1.06225" width="0.0508" layer="39"/>
<wire x1="-2.35705" y1="-1.0396" x2="2.34435" y2="-1.0396" width="0.0508" layer="39"/>
<wire x1="-2.35765" y1="-1.03625" x2="-2.35765" y2="1.06165" width="0.0508" layer="39"/>
<wire x1="2.3477" y1="-1.03625" x2="2.3477" y2="1.06165" width="0.0508" layer="39"/>
<wire x1="-2.3876" y1="1.08585" x2="-2.3876" y2="-1.09855" width="0.127" layer="21"/>
<wire x1="-2.3876" y1="-1.09855" x2="2.39395" y2="-1.09855" width="0.127" layer="21"/>
<wire x1="2.39395" y1="-1.09855" x2="2.39395" y2="1.08585" width="0.127" layer="21"/>
<wire x1="2.39395" y1="1.08585" x2="-2.3876" y2="1.08585" width="0.127" layer="21"/>
</package>
<package name="SMD-1812">
<wire x1="-3.0619" y1="1.86055" x2="3.0746" y2="1.86055" width="0.0508" layer="39"/>
<wire x1="3.08095" y1="-1.8814" x2="-3.05555" y2="-1.8814" width="0.0508" layer="39"/>
<wire x1="-3.05555" y1="-1.8814" x2="-3.0619" y2="1.86055" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="3.0746" y1="1.86055" x2="3.08095" y2="-1.8814" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-2.413" y="2.159" size="0.4064" layer="25">&gt;NAME</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
<wire x1="-3.10515" y1="1.905" x2="-3.10515" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-3.10515" y1="-1.905" x2="3.13055" y2="-1.905" width="0.127" layer="21"/>
<wire x1="3.13055" y1="-1.905" x2="3.13055" y2="1.905" width="0.127" layer="21"/>
<wire x1="3.13055" y1="1.905" x2="-3.10515" y2="1.905" width="0.127" layer="21"/>
</package>
<package name="SMD-2010">
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.2825" y1="1.5465" x2="3.2825" y2="1.5465" width="0.0508" layer="39"/>
<wire x1="3.2825" y1="1.5465" x2="3.2825" y2="-1.5465" width="0.0508" layer="39"/>
<wire x1="3.2825" y1="-1.5465" x2="-3.2825" y2="-1.5465" width="0.0508" layer="39"/>
<wire x1="-3.2825" y1="-1.5465" x2="-3.2825" y2="1.5465" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.159" y="1.651" size="0.4064" layer="25">&gt;NAME</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<wire x1="-3.2893" y1="1.5494" x2="-3.2893" y2="-1.5494" width="0.127" layer="21"/>
<wire x1="-3.2893" y1="-1.5494" x2="3.28295" y2="-1.5494" width="0.127" layer="21"/>
<wire x1="3.28295" y1="-1.5494" x2="3.28295" y2="1.5494" width="0.127" layer="21"/>
<wire x1="3.28295" y1="1.5494" x2="-3.2893" y2="1.5494" width="0.127" layer="21"/>
</package>
<package name="SOT-223">
<smd name="1" x="-2.159" y="-2.667" dx="2.286" dy="1.27" layer="1" rot="R270"/>
<smd name="4" x="0.2032" y="2.286" dx="5.08" dy="3.048" layer="1" rot="R180"/>
<smd name="2" x="0.1778" y="-2.667" dx="2.286" dy="1.27" layer="1" rot="R270"/>
<smd name="3" x="2.54" y="-2.667" dx="2.286" dy="1.27" layer="1" rot="R270"/>
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-1.905" width="0.127" layer="21"/>
<wire x1="3.4925" y1="1.905" x2="3.4925" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="3.4925" y2="1.905" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-1.905" x2="3.4925" y2="-1.905" width="0.127" layer="51"/>
<text x="-2.54" y="-1.27" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="T092-CBE">
<description>T018 Through-Hole Transistor Package</description>
<text x="-1.8415" y="1.4605" size="0.508" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.3495" size="0.508" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-2.095" y1="-1.651" x2="2.095" y2="-1.651" width="0.127" layer="21"/>
<wire x1="-2.413" y1="1.1359" x2="2.413" y2="1.1359" width="0.127" layer="21" curve="-129.583"/>
<wire x1="1.136" y1="-0.127" x2="-1.136" y2="-0.127" width="0.127" layer="51"/>
<wire x1="-2.413" y1="1.1359" x2="-2.664" y2="-0.127" width="0.127" layer="51" curve="27.9376"/>
<wire x1="-2.664" y1="-0.127" x2="-2.413" y2="-1.1359" width="0.127" layer="51" curve="22.4788"/>
<wire x1="-1.404" y1="-0.127" x2="-2.664" y2="-0.127" width="0.127" layer="51"/>
<wire x1="-2.4135" y1="-1.1359" x2="-2.095" y2="-1.651" width="0.127" layer="21" curve="13.0385"/>
<wire x1="-1.136" y1="-0.127" x2="-1.404" y2="-0.127" width="0.127" layer="51"/>
<wire x1="2.413" y1="-1.1359" x2="2.664" y2="-0.127" width="0.127" layer="51" curve="22.4788"/>
<wire x1="2.664" y1="-0.127" x2="2.413" y2="1.1359" width="0.127" layer="51" curve="27.9376"/>
<wire x1="2.664" y1="-0.127" x2="1.404" y2="-0.127" width="0.127" layer="51"/>
<wire x1="1.404" y1="-0.127" x2="1.136" y2="-0.127" width="0.127" layer="51"/>
<wire x1="2.095" y1="-1.651" x2="2.4247" y2="-1.1118" width="0.127" layer="21" curve="13.6094"/>
<pad name="C" x="-2.032" y="0" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="E" x="2.032" y="0" drill="0.8128" shape="octagon" rot="R90"/>
<pad name="B" x="0" y="0" drill="0.8128" shape="octagon" rot="R90"/>
</package>
<package name="FTDI">
<wire x1="-1.27" y1="-6.985" x2="-1.27" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-5.715" x2="-0.635" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-5.08" x2="-1.27" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-4.445" x2="-1.27" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.175" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-6.985" x2="-0.635" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="-1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-1.27" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="4.445" x2="-0.635" y2="5.08" width="0.1524" layer="21"/>
<pad name="GND" x="0" y="-6.35" drill="1.016" shape="octagon" rot="R90"/>
<pad name="CTS" x="0" y="-3.81" drill="1.016" shape="octagon" rot="R90"/>
<pad name="VCC" x="0" y="-1.27" drill="1.016" shape="octagon" rot="R90"/>
<pad name="TXD" x="0" y="1.27" drill="1.016" shape="octagon" rot="R90"/>
<pad name="RXD" x="0" y="3.81" drill="1.016" shape="octagon" rot="R90"/>
<text x="-1.27" y="8.001" size="1.27" layer="21" ratio="10">FTDI</text>
<rectangle x1="-0.254" y1="-4.064" x2="0.254" y2="-3.556" layer="51" rot="R90"/>
<rectangle x1="-0.254" y1="-6.604" x2="0.254" y2="-6.096" layer="51" rot="R90"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51" rot="R90"/>
<rectangle x1="-0.254" y1="3.556" x2="0.254" y2="4.064" layer="51" rot="R90"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51" rot="R90"/>
<wire x1="-0.635" y1="5.08" x2="-1.27" y2="5.715" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="5.715" x2="-1.27" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-0.635" y2="7.62" width="0.1524" layer="21"/>
<pad name="RTS" x="0" y="6.35" drill="1.016" shape="octagon" rot="R90"/>
<rectangle x1="-0.254" y1="6.096" x2="0.254" y2="6.604" layer="51" rot="R90"/>
<wire x1="-0.635" y1="7.62" x2="13.97" y2="7.62" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-7.62" x2="13.97" y2="-7.62" width="0.127" layer="21"/>
<wire x1="13.97" y1="-7.62" x2="13.97" y2="7.62" width="0.127" layer="21"/>
<rectangle x1="1.27" y1="-7.62" x2="13.97" y2="7.62" layer="39"/>
<text x="-1.524" y="-6.096" size="1.016" layer="21" rot="R180">GND</text>
<text x="-1.524" y="-1.016" size="1.016" layer="21" rot="R180">VCC</text>
<text x="-1.524" y="1.524" size="1.016" layer="21" rot="R180">TXD</text>
<text x="-1.524" y="-3.556" size="1.016" layer="21" rot="R180">CTS</text>
<text x="-1.524" y="6.604" size="1.016" layer="21" rot="R180">RTS</text>
<text x="-1.524" y="4.064" size="1.016" layer="21" rot="R180">RXD</text>
</package>
<package name="RF_RECEIVER">
<wire x1="-1.27" y1="23.495" x2="-1.27" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="1.27" y1="23.495" x2="1.27" y2="-6.985" width="0.1524" layer="21"/>
<pad name="DATA" x="0" y="-0.635" drill="1.016" shape="octagon"/>
<pad name="VCC" x="0" y="4.445" drill="1.016" shape="octagon"/>
<pad name="GND" x="0" y="-3.175" drill="1.016" shape="square"/>
<rectangle x1="-0.254" y1="-0.889" x2="0.254" y2="-0.381" layer="51"/>
<rectangle x1="-0.254" y1="4.191" x2="0.254" y2="4.699" layer="51"/>
<rectangle x1="-0.254" y1="-3.429" x2="0.254" y2="-2.921" layer="51"/>
<wire x1="-1.27" y1="23.495" x2="1.27" y2="23.495" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-6.985" x2="1.27" y2="-6.985" width="0.1524" layer="21"/>
<text x="-1.905" y="-5.08" size="0.6096" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="1.27" y1="-6.985" x2="5.08" y2="23.495" layer="39"/>
<pad name="NC" x="0" y="1.905" drill="1.016" shape="octagon"/>
<rectangle x1="-0.254" y1="1.651" x2="0.254" y2="2.159" layer="51"/>
</package>
<package name="RF_TRANSMITTER">
<description>SMAKN 315/433 MHz RF Transmitter</description>
<wire x1="-1.27" y1="9.525" x2="-1.27" y2="-9.525" width="0.1524" layer="21"/>
<wire x1="1.27" y1="9.525" x2="1.27" y2="-9.525" width="0.1524" layer="21"/>
<pad name="DATA" x="0" y="-2.54" drill="1.016" shape="octagon"/>
<pad name="VCC" x="0" y="0" drill="1.016" shape="octagon"/>
<pad name="GND" x="0" y="2.54" drill="1.016" shape="square"/>
<rectangle x1="-0.254" y1="-2.794" x2="0.254" y2="-2.286" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<wire x1="-1.27" y1="9.525" x2="1.27" y2="9.525" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-9.525" x2="1.27" y2="-9.525" width="0.1524" layer="21"/>
<text x="-1.905" y="-5.08" size="0.6096" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-5.08" y1="-9.525" x2="-1.27" y2="9.525" layer="39"/>
</package>
<package name="LED_1206_PIPE">
<wire x1="-2.6" y1="2.59" x2="2.59" y2="2.59" width="0.1" layer="21"/>
<wire x1="2.59" y1="2.59" x2="2.58" y2="-2.64" width="0.1" layer="21"/>
<wire x1="2.58" y1="-2.64" x2="-2.61" y2="-2.64" width="0.1" layer="21"/>
<wire x1="-2.61" y1="-2.64" x2="-2.6" y2="2.59" width="0.1" layer="21"/>
<hole x="-1.89" y="1.89" drill="1"/>
<hole x="1.89" y="-1.89" drill="1"/>
<smd name="A" x="-1.4" y="0" dx="1.6764" dy="1.6002" layer="1"/>
<smd name="C" x="1.4" y="0" dx="1.6764" dy="1.6002" layer="1"/>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="SMD-0402">
<wire x1="-0.252" y1="0.2148" x2="0.238" y2="0.2148" width="0.1524" layer="51"/>
<wire x1="0.238" y1="-0.2332" x2="-0.252" y2="-0.2332" width="0.1524" layer="51"/>
<smd name="1" x="-0.5935" y="-0.0092" dx="0.508" dy="0.762" layer="1"/>
<smd name="2" x="0.5795" y="-0.0092" dx="0.508" dy="0.762" layer="1"/>
<rectangle x1="-0.561" y1="-0.314" x2="-0.261" y2="0.2859" layer="51"/>
<rectangle x1="0.2518" y1="-0.314" x2="0.5518" y2="0.2859" layer="51"/>
<text x="1.016" y="0" size="0.508" layer="25">&gt;NAME</text>
</package>
<package name="THERMAL_PAD">
<description>10mm x 10mm Thermal SMD Pad</description>
<smd name="PAD" x="0" y="0" dx="9.9822" dy="9.9822" layer="1" rot="R90"/>
</package>
</packages>
<symbols>
<symbol name="TPS65217CRSLR">
<description>TPS65217CRSLR Power Controller for AM335x</description>
<pin name="VLDO2" x="-22.86" y="15.24" length="middle"/>
<pin name="VINLDO" x="-22.86" y="33.02" length="middle"/>
<pin name="VLDO1" x="-22.86" y="17.78" length="middle"/>
<pin name="BAT" x="22.86" y="48.26" length="middle" rot="R180"/>
<pin name="BAT_SENSE" x="22.86" y="45.72" length="middle" rot="R180"/>
<pin name="SYS" x="-22.86" y="48.26" length="middle"/>
<pin name="PWR_EN" x="-22.86" y="-5.08" length="middle"/>
<pin name="AC" x="-22.86" y="43.18" length="middle"/>
<pin name="TS" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="USB" x="-22.86" y="-40.64" length="middle"/>
<pin name="MUX_IN" x="22.86" y="-30.48" length="middle" rot="R180"/>
<pin name="WAKEUP" x="-22.86" y="-22.86" length="middle"/>
<pin name="NC" x="22.86" y="-45.72" length="middle" rot="R180"/>
<pin name="MUX_OUT" x="22.86" y="-33.02" length="middle" rot="R180"/>
<pin name="VIO" x="-22.86" y="38.1" length="middle"/>
<pin name="VDCDC1" x="-22.86" y="10.16" length="middle"/>
<pin name="L1" x="22.86" y="40.64" length="middle" rot="R180"/>
<pin name="VIN_DCDC1" x="-22.86" y="30.48" length="middle"/>
<pin name="VIN_DCDC2" x="-22.86" y="27.94" length="middle"/>
<pin name="L2" x="22.86" y="38.1" length="middle" rot="R180"/>
<pin name="VDCDC2" x="-22.86" y="7.62" length="middle"/>
<pin name="PB_IN" x="-22.86" y="-17.78" length="middle"/>
<pin name="PGOOD" x="-22.86" y="-10.16" length="middle"/>
<pin name="SDA" x="-22.86" y="-33.02" length="middle"/>
<pin name="SCL" x="-22.86" y="-35.56" length="middle"/>
<pin name="VDCDC3" x="-22.86" y="5.08" length="middle"/>
<pin name="PGND" x="22.86" y="10.16" length="middle" rot="R180"/>
<pin name="L3" x="22.86" y="35.56" length="middle" rot="R180"/>
<pin name="VIN_DCDC3" x="-22.86" y="25.4" length="middle"/>
<pin name="ISINK2" x="22.86" y="-17.78" length="middle" rot="R180"/>
<pin name="ISINK1" x="22.86" y="-15.24" length="middle" rot="R180"/>
<pin name="ISET1" x="22.86" y="-22.86" length="middle" rot="R180"/>
<pin name="ISET2" x="22.86" y="-25.4" length="middle" rot="R180"/>
<pin name="L4" x="22.86" y="33.02" length="middle" rot="R180"/>
<pin name="FB_WLED" x="22.86" y="-10.16" length="middle" rot="R180"/>
<pin name="LS1_IN" x="22.86" y="27.94" length="middle" rot="R180"/>
<pin name="LS1_OUT" x="22.86" y="25.4" length="middle" rot="R180"/>
<pin name="AGND" x="22.86" y="7.62" length="middle" rot="R180"/>
<pin name="LS2_IN" x="22.86" y="20.32" length="middle" rot="R180"/>
<pin name="RESET" x="-22.86" y="-27.94" length="middle"/>
<pin name="LS2_OUT" x="22.86" y="17.78" length="middle" rot="R180"/>
<pin name="BYPASS" x="-22.86" y="-45.72" length="middle"/>
<pin name="LDO_PGOOD" x="-22.86" y="-12.7" length="middle"/>
<pin name="INT" x="22.86" y="-38.1" length="middle" rot="R180"/>
<pin name="INT_LDO" x="22.86" y="-40.64" length="middle" rot="R180"/>
<pin name="POWERPAD" x="22.86" y="12.7" length="middle" rot="R180"/>
<wire x1="-10.16" y1="50.8" x2="-17.78" y2="50.8" width="0.254" layer="94"/>
<wire x1="-17.78" y1="50.8" x2="-17.78" y2="2.54" width="0.254" layer="94"/>
<wire x1="-17.78" y1="2.54" x2="-10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="50.8" x2="17.78" y2="50.8" width="0.254" layer="94"/>
<wire x1="17.78" y1="50.8" x2="17.78" y2="2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="2.54" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-2.54" x2="-17.78" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-2.54" x2="-17.78" y2="-48.26" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-48.26" x2="-10.16" y2="-48.26" width="0.254" layer="94"/>
<wire x1="10.16" y1="-48.26" x2="17.78" y2="-48.26" width="0.254" layer="94"/>
<wire x1="17.78" y1="-48.26" x2="17.78" y2="-2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="-2.54" x2="10.16" y2="-2.54" width="0.254" layer="94"/>
<text x="-17.78" y="53.34" size="1.778" layer="95" ratio="1">&gt;NAME</text>
<text x="-17.018" y="-51.308" size="1.778" layer="96" ratio="1">&gt;VALUE</text>
</symbol>
<symbol name="POWER-JACK-CUI">
<wire x1="2.54" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.778" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-2.54" x2="-2.54" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-1.016" x2="-3.302" y2="-2.54" width="0.1524" layer="94"/>
<text x="-5.08" y="3.81" size="1.27" layer="95">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96">&gt;VALUE</text>
<rectangle x1="-5.842" y1="-2.54" x2="-4.318" y2="1.27" layer="94"/>
<pin name="POS" x="5.08" y="2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="GND@0" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="GND@1" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
<polygon width="0.1524" layer="94">
<vertex x="0" y="-2.54"/>
<vertex x="-0.508" y="-1.27"/>
<vertex x="0.508" y="-1.27"/>
</polygon>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.27" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.27" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="RES">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.27" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.27" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="FUSE">
<description>Fuse/Thermistor</description>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short"/>
<pin name="2" x="7.62" y="0" visible="pad" length="short" rot="R180"/>
<text x="-2.286" y="3.048" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-4.064" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="CAP_POL">
<description>Polarized Capacitor</description>
<wire x1="-2.54" y1="-0.127" x2="2.54" y2="-0.127" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat"/>
<text x="1.016" y="0.635" size="1.27" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.27" layer="96">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="3.556" y="-2.032" size="1.27" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-2.032" size="1.27" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="0.381"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.921" y="0.127"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
<symbol name="INDUCTOR">
<wire x1="-5.08" y1="0" x2="-3.81" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="2.54" y1="0" x2="3.81" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="5.08" y1="0" x2="3.81" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<text x="-5.08" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.27" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="HDR_1X1">
<description>Single Pin Header</description>
<wire x1="-1.27" y1="-2.54" x2="3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="2.54" x2="-1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.6096" layer="94"/>
<text x="-2.032" y="-4.318" size="1.27" layer="96">&gt;VALUE</text>
<text x="-1.524" y="3.048" size="1.27" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="TAC-SWITCH">
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="1.905" y1="4.445" x2="1.905" y2="3.175" width="0.254" layer="94"/>
<wire x1="-1.905" y1="4.445" x2="-1.905" y2="3.175" width="0.254" layer="94"/>
<wire x1="1.905" y1="4.445" x2="0" y2="4.445" width="0.254" layer="94"/>
<wire x1="0" y1="4.445" x2="-1.905" y2="4.445" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="4.445" x2="0" y2="3.175" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<text x="-2.54" y="5.588" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.794" y="-2.032" size="1.27" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="2"/>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="TL5209DR">
<pin name="EN" x="-15.24" y="-2.54" length="middle"/>
<pin name="VIN" x="-15.24" y="2.54" length="middle"/>
<pin name="VOUT" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="ADJ/BYP" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="GND" x="17.78" y="-5.08" length="middle" rot="R180"/>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="-7.62" x2="12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<text x="-9.906" y="8.89" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="XTAL_OSC">
<description>Crystal Oscillator - no GND</description>
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.556" size="1.27" layer="96">&gt;VALUE</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="SN74LVC1G06">
<pin name="VCC" x="-15.24" y="5.08" length="middle"/>
<pin name="GND" x="-15.24" y="-5.08" length="middle"/>
<pin name="NC" x="-15.24" y="0" length="middle"/>
<pin name="Y" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="A" x="12.7" y="5.08" length="middle" rot="R180"/>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<text x="-10.16" y="8.382" size="1.27" layer="95">&gt;NAME</text>
<text x="-10.16" y="-9.398" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="SN74LVC2G241">
<pin name="!OE" x="-15.24" y="-2.54" length="middle"/>
<pin name="1A" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="2Y" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="GND" x="-15.24" y="-7.62" length="middle"/>
<pin name="VCC" x="-15.24" y="7.62" length="middle"/>
<pin name="2OE" x="-15.24" y="2.54" length="middle"/>
<pin name="1Y" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="2A" x="15.24" y="-2.54" length="middle" rot="R180"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<text x="-10.16" y="11.176" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="TPD4S012">
<pin name="D+" x="-12.7" y="0" length="middle"/>
<pin name="D-" x="-12.7" y="5.08" length="middle"/>
<pin name="ID" x="-12.7" y="-5.08" length="middle"/>
<pin name="VBUS" x="-12.7" y="10.16" length="middle"/>
<pin name="GND" x="-12.7" y="-10.16" length="middle"/>
<wire x1="-7.62" y1="12.7" x2="-7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-12.7" x2="5.08" y2="-12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="-12.7" x2="5.08" y2="12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="12.7" x2="-7.62" y2="12.7" width="0.254" layer="94"/>
<text x="-7.62" y="13.97" size="1.27" layer="95">&gt;NAME</text>
<text x="-7.62" y="-15.24" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="24LC32AT">
<pin name="SCL" x="-12.7" y="5.08" length="middle"/>
<pin name="SDA" x="-12.7" y="2.54" length="middle"/>
<pin name="VCC" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="WP" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="VSS" x="-12.7" y="-5.08" length="middle"/>
<wire x1="-7.62" y1="7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<text x="-7.62" y="8.89" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="EMMC">
<pin name="CLK" x="-15.24" y="12.7" length="middle"/>
<pin name="NRST" x="-15.24" y="7.62" length="middle"/>
<pin name="CMD" x="-15.24" y="2.54" length="middle"/>
<pin name="DAT0" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="DAT1" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="DAT2" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="DAT3" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="DAT4" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="DAT5" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="DAT6" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="DAT7" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="VCC" x="-15.24" y="-10.16" length="middle"/>
<pin name="VCCQ" x="-15.24" y="-12.7" length="middle"/>
<pin name="VSS" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="VSSQ" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="VDDI" x="-15.24" y="-5.08" length="middle"/>
<wire x1="-10.16" y1="15.24" x2="-10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-15.24" x2="12.7" y2="-15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="-15.24" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="15.24" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<text x="-9.398" y="16.764" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="DDR3L">
<description>DDR3L 96-pin BGA based off Micron MT41K256M16HA</description>
<pin name="VDD" x="-17.78" y="-35.56" length="middle"/>
<pin name="VDDQ" x="-17.78" y="-38.1" length="middle"/>
<pin name="VREFCA" x="-17.78" y="-43.18" length="middle"/>
<pin name="VREFDQ" x="-17.78" y="-45.72" length="middle"/>
<pin name="VSS" x="17.78" y="-35.56" length="middle" rot="R180"/>
<pin name="VSSQ" x="17.78" y="-38.1" length="middle" rot="R180"/>
<pin name="ZQ" x="17.78" y="-43.18" length="middle" rot="R180"/>
<pin name="NC" x="17.78" y="-45.72" length="middle" rot="R180"/>
<pin name="DQ0" x="-17.78" y="45.72" length="middle"/>
<pin name="DQ1" x="-17.78" y="43.18" length="middle"/>
<pin name="DQ2" x="-17.78" y="40.64" length="middle"/>
<pin name="DQ3" x="-17.78" y="38.1" length="middle"/>
<pin name="DQ4" x="-17.78" y="35.56" length="middle"/>
<pin name="DQ5" x="-17.78" y="33.02" length="middle"/>
<pin name="DQ6" x="-17.78" y="30.48" length="middle"/>
<pin name="DQ7" x="-17.78" y="27.94" length="middle"/>
<pin name="DQ8" x="-17.78" y="25.4" length="middle"/>
<pin name="DQ9" x="-17.78" y="22.86" length="middle"/>
<pin name="DQ10" x="-17.78" y="20.32" length="middle"/>
<pin name="DQ11" x="-17.78" y="17.78" length="middle"/>
<pin name="DQ12" x="-17.78" y="15.24" length="middle"/>
<pin name="DQ13" x="-17.78" y="12.7" length="middle"/>
<pin name="DQ14" x="-17.78" y="10.16" length="middle"/>
<pin name="DQ15" x="-17.78" y="7.62" length="middle"/>
<pin name="LDQS" x="-17.78" y="-2.54" length="middle"/>
<pin name="UDQS" x="-17.78" y="-7.62" length="middle"/>
<pin name="UDM" x="-17.78" y="-25.4" length="middle"/>
<pin name="RESET#" x="17.78" y="-25.4" length="middle" rot="R180"/>
<pin name="RAS#" x="17.78" y="-15.24" length="middle" rot="R180"/>
<pin name="WE#" x="17.78" y="-20.32" length="middle" rot="R180"/>
<pin name="LDM" x="-17.78" y="-27.94" length="middle"/>
<pin name="CKE" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="CS#" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="CK#" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="BA0" x="-17.78" y="-15.24" length="middle"/>
<pin name="BA1" x="-17.78" y="-17.78" length="middle"/>
<pin name="BA2" x="-17.78" y="-20.32" length="middle"/>
<pin name="A14" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="A13" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="A12/BC#" x="17.78" y="15.24" length="middle" rot="R180"/>
<pin name="A11" x="17.78" y="17.78" length="middle" rot="R180"/>
<pin name="A10/AP" x="17.78" y="20.32" length="middle" rot="R180"/>
<pin name="A9" x="17.78" y="22.86" length="middle" rot="R180"/>
<pin name="A8" x="17.78" y="25.4" length="middle" rot="R180"/>
<pin name="A7" x="17.78" y="27.94" length="middle" rot="R180"/>
<pin name="A6" x="17.78" y="30.48" length="middle" rot="R180"/>
<pin name="A5" x="17.78" y="33.02" length="middle" rot="R180"/>
<pin name="A4" x="17.78" y="35.56" length="middle" rot="R180"/>
<pin name="A3" x="17.78" y="38.1" length="middle" rot="R180"/>
<pin name="A2" x="17.78" y="40.64" length="middle" rot="R180"/>
<pin name="A1" x="17.78" y="43.18" length="middle" rot="R180"/>
<pin name="A0" x="17.78" y="45.72" length="middle" rot="R180"/>
<pin name="UDQS#" x="-17.78" y="-10.16" length="middle"/>
<pin name="LDQS#" x="-17.78" y="0" length="middle"/>
<pin name="CAS#" x="17.78" y="-17.78" length="middle" rot="R180"/>
<pin name="ODT" x="17.78" y="-27.94" length="middle" rot="R180"/>
<pin name="CK" x="17.78" y="0" length="middle" rot="R180"/>
<wire x1="-7.62" y1="-33.02" x2="-12.7" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-33.02" x2="-12.7" y2="-48.26" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-48.26" x2="-7.62" y2="-48.26" width="0.254" layer="94"/>
<wire x1="7.62" y1="-33.02" x2="12.7" y2="-33.02" width="0.254" layer="94"/>
<wire x1="12.7" y1="-33.02" x2="12.7" y2="-48.26" width="0.254" layer="94"/>
<wire x1="12.7" y1="-48.26" x2="7.62" y2="-48.26" width="0.254" layer="94"/>
<wire x1="-7.62" y1="48.26" x2="-12.7" y2="48.26" width="0.254" layer="94"/>
<wire x1="-12.7" y1="48.26" x2="-12.7" y2="5.08" width="0.254" layer="94"/>
<wire x1="-12.7" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="48.26" x2="12.7" y2="48.26" width="0.254" layer="94"/>
<wire x1="12.7" y1="48.26" x2="12.7" y2="5.08" width="0.254" layer="94"/>
<wire x1="12.7" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-12.7" y2="2.54" width="0.254" layer="94"/>
<wire x1="-12.7" y1="2.54" x2="-12.7" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-30.48" x2="-7.62" y2="-30.48" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="12.7" y2="2.54" width="0.254" layer="94"/>
<wire x1="12.7" y1="2.54" x2="12.7" y2="-30.48" width="0.254" layer="94"/>
<wire x1="12.7" y1="-30.48" x2="7.62" y2="-30.48" width="0.254" layer="94"/>
<text x="-11.43" y="49.53" size="1.778" layer="96">&gt;NAME</text>
<text x="-12.192" y="-51.308" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A15" x="17.78" y="7.62" length="middle" rot="R180"/>
</symbol>
<symbol name="SCHA5B0200">
<pin name="VDD" x="-20.32" y="10.16" length="middle"/>
<pin name="DAT0" x="-20.32" y="5.08" length="middle"/>
<pin name="DAT1" x="-20.32" y="0" length="middle"/>
<pin name="DAT2" x="-20.32" y="-5.08" length="middle"/>
<pin name="DAT3/CD" x="-20.32" y="-10.16" length="middle"/>
<pin name="CMD" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="CLK" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="CD" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="VSS" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="GND" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="SHIELD" x="0" y="-17.78" length="middle" rot="R90"/>
<wire x1="-15.24" y1="12.7" x2="-15.24" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-12.7" x2="12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="-12.7" x2="12.7" y2="12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="12.7" x2="-15.24" y2="12.7" width="0.254" layer="94"/>
<text x="-15.24" y="13.97" size="1.27" layer="95">&gt;NAME</text>
<text x="5.08" y="13.97" size="1.27" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="TPS2051B">
<pin name="IN" x="-12.7" y="7.62" length="middle"/>
<pin name="OUT" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="GND" x="-12.7" y="-7.62" length="middle"/>
<pin name="!OC" x="-12.7" y="2.54" length="middle"/>
<pin name="!EN" x="-12.7" y="-2.54" length="middle"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<text x="-7.62" y="11.176" size="1.27" layer="95">&gt;NAME</text>
<text x="-7.62" y="-12.7" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="USB">
<pin name="VBUS" x="-10.16" y="7.62" length="middle"/>
<pin name="D-" x="-10.16" y="2.54" length="middle"/>
<pin name="D+" x="-10.16" y="-2.54" length="middle"/>
<pin name="GND" x="-10.16" y="-7.62" length="middle"/>
<pin name="SHIELD" x="5.08" y="-15.24" length="middle" rot="R90"/>
<wire x1="-5.08" y1="10.16" x2="-5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-10.16" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-5.08" y2="10.16" width="0.254" layer="94"/>
<text x="-4.318" y="11.176" size="1.778" layer="95">&gt;NAME</text>
<text x="-0.762" y="-11.43" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
</symbol>
<symbol name="LAN8710A">
<pin name="VDD2A" x="-20.32" y="-15.24" length="middle"/>
<pin name="LED2/!INTSEL" x="20.32" y="-12.7" length="middle" rot="R180"/>
<pin name="LED1/!REGOFF" x="20.32" y="-10.16" length="middle" rot="R180"/>
<pin name="XTAL2" x="20.32" y="-20.32" length="middle" rot="R180"/>
<pin name="XTAL/CLKIN" x="20.32" y="-17.78" length="middle" rot="R180"/>
<pin name="VDDCR" x="-20.32" y="-7.62" length="middle"/>
<pin name="RXCLK/!PHYAD1" x="27.94" y="35.56" length="middle" rot="R180"/>
<pin name="RXD3/!PHYAD2" x="-27.94" y="12.7" length="middle"/>
<pin name="RXD2/!RMIISEL" x="-27.94" y="20.32" length="middle"/>
<pin name="RXD1/!MODE1" x="-27.94" y="27.94" length="middle"/>
<pin name="RXD0/!MODE0" x="-27.94" y="35.56" length="middle"/>
<pin name="VDDIO" x="-20.32" y="-5.08" length="middle"/>
<pin name="RXER/RXD4/!PHYAD0" x="-27.94" y="5.08" length="middle"/>
<pin name="CRS" x="27.94" y="5.08" length="middle" rot="R180"/>
<pin name="COL/CRS_DV/!MODE2" x="27.94" y="2.54" length="middle" rot="R180"/>
<pin name="MDIO" x="20.32" y="-25.4" length="middle" rot="R180"/>
<pin name="MDC" x="20.32" y="-27.94" length="middle" rot="R180"/>
<pin name="INT/TXER/TXD4" x="-27.94" y="7.62" length="middle"/>
<pin name="NRST" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="TXCLK" x="27.94" y="38.1" length="middle" rot="R180"/>
<pin name="TXEN" x="27.94" y="30.48" length="middle" rot="R180"/>
<pin name="TXD0" x="-27.94" y="38.1" length="middle"/>
<pin name="TXD1" x="-27.94" y="30.48" length="middle"/>
<pin name="TXD2" x="-27.94" y="22.86" length="middle"/>
<pin name="TXD3" x="-27.94" y="15.24" length="middle"/>
<pin name="RXDV" x="27.94" y="25.4" length="middle" rot="R180"/>
<pin name="VDD1A" x="-20.32" y="-12.7" length="middle"/>
<pin name="TXN" x="27.94" y="17.78" length="middle" rot="R180"/>
<pin name="TXP" x="27.94" y="20.32" length="middle" rot="R180"/>
<pin name="RXP" x="27.94" y="12.7" length="middle" rot="R180"/>
<pin name="RXN" x="27.94" y="10.16" length="middle" rot="R180"/>
<pin name="RBIAS" x="-20.32" y="-20.32" length="middle"/>
<pin name="VSS_PAD" x="-20.32" y="-25.4" length="middle"/>
<wire x1="-15.24" y1="40.64" x2="-22.86" y2="40.64" width="0.254" layer="94"/>
<wire x1="-22.86" y1="40.64" x2="-22.86" y2="0" width="0.254" layer="94"/>
<wire x1="-22.86" y1="0" x2="-15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="40.64" x2="22.86" y2="40.64" width="0.254" layer="94"/>
<wire x1="22.86" y1="40.64" x2="22.86" y2="0" width="0.254" layer="94"/>
<wire x1="22.86" y1="0" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-15.24" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-2.54" x2="-15.24" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-30.48" x2="-7.62" y2="-30.48" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="15.24" y2="-2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-2.54" x2="15.24" y2="-30.48" width="0.254" layer="94"/>
<wire x1="15.24" y1="-30.48" x2="7.62" y2="-30.48" width="0.254" layer="94"/>
<text x="-22.86" y="42.164" size="1.778" layer="95">&gt;NAME</text>
<text x="-15.24" y="-33.02" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="MAGJACK">
<wire x1="-12.7" y1="-12.7" x2="12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="-12.7" x2="12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="12.7" y2="5.08" width="0.254" layer="94"/>
<wire x1="12.7" y1="5.08" x2="12.7" y2="12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="12.7" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="15.24" x2="-12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="-12.7" y1="15.24" x2="-12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="12.7" x2="10.668" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.668" y1="12.7" x2="10.668" y2="9.652" width="0.254" layer="94"/>
<wire x1="10.668" y1="9.652" x2="9.398" y2="9.652" width="0.254" layer="94"/>
<wire x1="9.398" y1="9.652" x2="10.668" y2="8.382" width="0.254" layer="94"/>
<wire x1="10.668" y1="8.382" x2="11.938" y2="9.652" width="0.254" layer="94"/>
<wire x1="11.938" y1="9.652" x2="10.668" y2="9.652" width="0.254" layer="94"/>
<wire x1="10.668" y1="8.382" x2="11.938" y2="8.382" width="0.254" layer="94"/>
<wire x1="9.398" y1="8.382" x2="10.668" y2="8.382" width="0.254" layer="94"/>
<wire x1="10.668" y1="8.382" x2="10.668" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.668" y1="5.08" x2="12.7" y2="5.08" width="0.254" layer="94"/>
<wire x1="12.7" y1="-2.54" x2="10.668" y2="-2.54" width="0.254" layer="94"/>
<wire x1="10.668" y1="-2.54" x2="10.668" y2="-5.588" width="0.254" layer="94"/>
<wire x1="10.668" y1="-5.588" x2="9.398" y2="-5.588" width="0.254" layer="94"/>
<wire x1="9.398" y1="-5.588" x2="10.668" y2="-6.858" width="0.254" layer="94"/>
<wire x1="10.668" y1="-6.858" x2="11.938" y2="-5.588" width="0.254" layer="94"/>
<wire x1="11.938" y1="-5.588" x2="10.668" y2="-5.588" width="0.254" layer="94"/>
<wire x1="10.668" y1="-6.858" x2="11.938" y2="-6.858" width="0.254" layer="94"/>
<wire x1="9.398" y1="-6.858" x2="10.668" y2="-6.858" width="0.254" layer="94"/>
<wire x1="10.668" y1="-6.858" x2="10.668" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.668" y1="-10.16" x2="12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="8.89" y1="8.89" x2="8.128" y2="9.652" width="0.254" layer="94"/>
<wire x1="8.128" y1="9.144" x2="8.128" y2="9.652" width="0.254" layer="94"/>
<wire x1="8.128" y1="9.652" x2="8.636" y2="9.652" width="0.254" layer="94"/>
<wire x1="8.89" y1="7.874" x2="8.128" y2="8.636" width="0.254" layer="94"/>
<wire x1="8.128" y1="8.128" x2="8.128" y2="8.636" width="0.254" layer="94"/>
<wire x1="8.128" y1="8.636" x2="8.636" y2="8.636" width="0.254" layer="94"/>
<wire x1="8.89" y1="-6.35" x2="8.128" y2="-5.588" width="0.254" layer="94"/>
<wire x1="8.128" y1="-6.096" x2="8.128" y2="-5.588" width="0.254" layer="94"/>
<wire x1="8.128" y1="-5.588" x2="8.636" y2="-5.588" width="0.254" layer="94"/>
<wire x1="8.89" y1="-7.366" x2="8.128" y2="-6.604" width="0.254" layer="94"/>
<wire x1="8.128" y1="-7.112" x2="8.128" y2="-6.604" width="0.254" layer="94"/>
<wire x1="8.128" y1="-6.604" x2="8.636" y2="-6.604" width="0.254" layer="94"/>
<text x="-12.7" y="15.748" size="1.27" layer="95">&gt;NAME</text>
<text x="-12.7" y="-14.986" size="1.27" layer="96">&gt;VALUE</text>
<pin name="YLED-" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="YLED+" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="GLED-" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="GLED+" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="RD-" x="-17.78" y="0" length="middle"/>
<pin name="RCT" x="-17.78" y="-2.54" length="middle"/>
<pin name="RD+" x="-17.78" y="2.54" length="middle"/>
<pin name="TD-" x="-17.78" y="7.62" length="middle"/>
<pin name="TCT" x="-17.78" y="12.7" length="middle"/>
<pin name="TD+" x="-17.78" y="10.16" length="middle"/>
<pin name="SHIELD1" x="-17.78" y="-7.62" length="middle"/>
<pin name="SHIELD2" x="-17.78" y="-10.16" length="middle"/>
</symbol>
<symbol name="TLV72033-3.3">
<wire x1="-10.16" y1="-7.62" x2="12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="-7.62" x2="12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<text x="-10.16" y="8.89" size="1.27" layer="95">&gt;NAME</text>
<text x="-10.16" y="-10.16" size="1.27" layer="96">&gt;VALUE</text>
<pin name="VIN" x="-15.24" y="5.08" length="middle"/>
<pin name="GND" x="-15.24" y="-5.08" length="middle"/>
<pin name="EN" x="-15.24" y="0" length="middle"/>
<pin name="NC/FB" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="VOUT" x="17.78" y="5.08" length="middle" rot="R180"/>
</symbol>
<symbol name="DIODE_STD">
<description>Standard diode symbol</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="-3.81" y="2.0066" size="1.27" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.8354" size="1.27" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="FAN">
<pin name="PWR" x="-10.16" y="2.54" length="middle"/>
<pin name="GND" x="-10.16" y="-5.08" length="middle"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<text x="-7.62" y="6.35" size="1.27" layer="95">&gt;NAME</text>
<text x="-7.62" y="-10.16" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="N-MOSFET_SD">
<description>N-MOSFET, mbb076</description>
<wire x1="0.762" y1="0.762" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.175" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.905" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.762" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<wire x1="4.572" y1="0.762" x2="4.318" y2="0.508" width="0.1524" layer="94"/>
<wire x1="4.318" y1="0.508" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.302" y1="0.508" x2="3.048" y2="0.254" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.3592" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.3592" width="0" layer="94"/>
<text x="4.826" y="-2.54" size="1.27" layer="96">&gt;VALUE</text>
<text x="4.826" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="3.81" y="0.508"/>
<vertex x="3.302" y="-0.254"/>
<vertex x="4.318" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="1.016" y="0"/>
<vertex x="2.032" y="0.762"/>
<vertex x="2.032" y="-0.762"/>
</polygon>
</symbol>
<symbol name="TESTPOINT">
<pin name="1" x="0" y="-2.54" visible="off" length="short" rot="R90"/>
<circle x="0" y="1.27" radius="0.254" width="0.254" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.302" size="1.27" layer="95">&gt;NAME</text>
</symbol>
<symbol name="BJT">
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<text x="5.08" y="-5.08" size="1.27" layer="95" rot="R90">&gt;NAME</text>
<text x="7.62" y="-5.08" size="1.27" layer="96" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
</symbol>
<symbol name="FTDI">
<wire x1="1.27" y1="-7.62" x2="-7.62" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<text x="-6.35" y="10.922" size="1.27" layer="95">FTDI</text>
<pin name="CTS" x="5.08" y="-2.54" visible="pin" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="VCC" x="5.08" y="0" visible="pin" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="TXD" x="5.08" y="2.54" visible="pin" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="RXD" x="5.08" y="5.08" visible="pin" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="RTS" x="5.08" y="7.62" visible="pin" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.6096" layer="94"/>
<pin name="GND" x="5.08" y="-5.08" visible="pin" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="RF_RXTX">
<description>SMAKN RF Tranmsmitter - 315/433MHz, 5V, Serial TX output</description>
<pin name="DATA" x="2.54" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="VCC" x="2.54" y="0" visible="pin" length="short" rot="R180"/>
<pin name="GND" x="2.54" y="-5.08" visible="pin" length="short" rot="R180"/>
<wire x1="0" y1="-7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="0" y2="-7.62" width="0.254" layer="94"/>
<text x="-10.16" y="8.636" size="1.27" layer="95">&gt;NAME</text>
<text x="-10.16" y="-10.16" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="HEATSINK">
<pin name="HS" x="0" y="-5.08" visible="pin" length="middle" rot="R90"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="0" width="0.254" layer="94"/>
<text x="-3.556" y="-0.254" size="1.27" layer="95" rot="R90">&gt;NAME</text>
<text x="4.826" y="-0.508" size="1.27" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TPS65217CRSLR">
<description>TPS65217CRSLR Power Controller for AM335x Series</description>
<gates>
<gate name="G$1" symbol="TPS65217CRSLR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN-48">
<connects>
<connect gate="G$1" pin="AC" pad="10"/>
<connect gate="G$1" pin="AGND" pad="41"/>
<connect gate="G$1" pin="BAT" pad="4 5"/>
<connect gate="G$1" pin="BAT_SENSE" pad="6"/>
<connect gate="G$1" pin="BYPASS" pad="47"/>
<connect gate="G$1" pin="FB_WLED" pad="38"/>
<connect gate="G$1" pin="INT" pad="45"/>
<connect gate="G$1" pin="INT_LDO" pad="48"/>
<connect gate="G$1" pin="ISET1" pad="35"/>
<connect gate="G$1" pin="ISET2" pad="36"/>
<connect gate="G$1" pin="ISINK1" pad="34"/>
<connect gate="G$1" pin="ISINK2" pad="33"/>
<connect gate="G$1" pin="L1" pad="20"/>
<connect gate="G$1" pin="L2" pad="23"/>
<connect gate="G$1" pin="L3" pad="31"/>
<connect gate="G$1" pin="L4" pad="37"/>
<connect gate="G$1" pin="LDO_PGOOD" pad="46"/>
<connect gate="G$1" pin="LS1_IN" pad="39"/>
<connect gate="G$1" pin="LS1_OUT" pad="40"/>
<connect gate="G$1" pin="LS2_IN" pad="42"/>
<connect gate="G$1" pin="LS2_OUT" pad="43"/>
<connect gate="G$1" pin="MUX_IN" pad="14"/>
<connect gate="G$1" pin="MUX_OUT" pad="16"/>
<connect gate="G$1" pin="NC" pad="15 17"/>
<connect gate="G$1" pin="PB_IN" pad="25"/>
<connect gate="G$1" pin="PGND" pad="30"/>
<connect gate="G$1" pin="PGOOD" pad="26"/>
<connect gate="G$1" pin="POWERPAD" pad="PAD"/>
<connect gate="G$1" pin="PWR_EN" pad="9"/>
<connect gate="G$1" pin="RESET" pad="44"/>
<connect gate="G$1" pin="SCL" pad="28"/>
<connect gate="G$1" pin="SDA" pad="27"/>
<connect gate="G$1" pin="SYS" pad="7 8"/>
<connect gate="G$1" pin="TS" pad="11"/>
<connect gate="G$1" pin="USB" pad="12"/>
<connect gate="G$1" pin="VDCDC1" pad="19"/>
<connect gate="G$1" pin="VDCDC2" pad="24"/>
<connect gate="G$1" pin="VDCDC3" pad="29"/>
<connect gate="G$1" pin="VINLDO" pad="2"/>
<connect gate="G$1" pin="VIN_DCDC1" pad="21"/>
<connect gate="G$1" pin="VIN_DCDC2" pad="22"/>
<connect gate="G$1" pin="VIN_DCDC3" pad="32"/>
<connect gate="G$1" pin="VIO" pad="18"/>
<connect gate="G$1" pin="VLDO1" pad="3"/>
<connect gate="G$1" pin="VLDO2" pad="1"/>
<connect gate="G$1" pin="WAKEUP" pad="13"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PWR-JACK-CUI">
<description>DC CONN POWER JACK - HI CUR</description>
<gates>
<gate name="DC_JACK" symbol="POWER-JACK-CUI" x="0" y="0"/>
</gates>
<devices>
<device name="_LRG" package="DC-POWER-JACK-CUI">
<connects>
<connect gate="DC_JACK" pin="GND@0" pad="GND@0"/>
<connect gate="DC_JACK" pin="GND@1" pad="GND@1"/>
<connect gate="DC_JACK" pin="POS" pad="POS@0 POS@1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PART#" value="CP-002AHPJCT-ND"/>
<attribute name="SUPPLIER" value="Digikey"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>Capacitors - SMD and Through-Hole</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="_0402" package="SMD-0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1206" package="SMD-1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805" package="SMD-0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2512" package="SMD-2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603" package="SMD-0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1812" package="SMD-1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TH886" package="CAP_TH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES" prefix="R" uservalue="yes">
<description>Resistors - SMD and Through-Hole</description>
<gates>
<gate name="G$1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="_0402" package="SMD-0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1206" package="SMD-1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805" package="SMD-0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2512" package="SMD-2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603" package="SMD-0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2010" package="SMD-2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TALL" package="RES_TH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FUSE" prefix="F" uservalue="yes">
<description>Fuse/Thermistor/PTC Resettable

&lt;p&gt;PTCs -
&lt;br&gt; PTGL13AR6R8M6C01B0 : PTC, 140V, 575mA trip, 0.37A hold, 1A max, TH
&lt;br&gt;1206L075/13.2WR : PTC, 13.2V, 1,5A trip, 0.75A hold, 100A max, 1206 pkg

&lt;p&gt;Non-PTC -
&lt;br&gt;C1Q3 : 3A, 63Vdc, 125Vac, Fast Response, 1206 pkg
&lt;br&gt;0698Q : 5A, 350Vac, 140Vdc, Fast, TH 0.2"
&lt;br&gt;SSQ 750: 750mA, 125Vacdc, Fast, 2-SMD</description>
<gates>
<gate name="FUSE" symbol="FUSE" x="0" y="0"/>
</gates>
<devices>
<device name="_TH" package="FUSE_PTGL">
<connects>
<connect gate="FUSE" pin="1" pad="1"/>
<connect gate="FUSE" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SMD1206" package="SMD-1206">
<connects>
<connect gate="FUSE" pin="1" pad="1"/>
<connect gate="FUSE" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TH-0.2&quot;" package="TH_0.2&quot;">
<connects>
<connect gate="FUSE" pin="1" pad="1"/>
<connect gate="FUSE" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SSQ" package="SSQ">
<connects>
<connect gate="FUSE" pin="1" pad="1"/>
<connect gate="FUSE" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP_POL" prefix="C" uservalue="yes">
<description>Polarized Capacitors (Aluminum/Tantalum)

&lt;p&gt;Aluminum
&lt;br&gt;EEE-FPE101XAP : 100uF 25V 20% 160m Ohm (6.6mm x 6.6mm)
&lt;br&gt;EEE-1EA100WR : 10uF 25V 20% (4.3mm x 4.3mm)
&lt;br&gt;EEE-FC1V6R8AR : 6.8uF 35V 20% 1.8 Ohms, 95mA Irip (5.3mm x 5.3mm)

&lt;p&gt;Tantalum
&lt;br&gt;T491B334M050AT : 0.33uF, 10%, 50V, 10 Ohm, SMD-1411</description>
<gates>
<gate name="G$1" symbol="CAP_POL" x="0" y="0"/>
</gates>
<devices>
<device name="_6.6" package="153CLV-1012_6.6X6.6">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_4.3" package="153CLV-1012_4.3X4.3">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_5.3" package="153CLV-1012_5.3X5.3">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2312" package="SMD-2312(6032)">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1206" package="SMD-1206">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1411" package="SMD-1411_POL">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>LEDs - Catalog of LED types</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="_0402" package="SMD-0402">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_THRU" package="LED_5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ARRAY_9" package="LED_ARRAY_9">
<connects>
<connect gate="G$1" pin="A" pad="A1 A2 A3 A4 A5 A6 A7 A8 A9"/>
<connect gate="G$1" pin="C" pad="K1 K2 K3 K4 K5 K6 K7 K8 K9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805" package="SMD-0805">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805P" package="LED_0805_PIPE">
<connects>
<connect gate="G$1" pin="A" pad="ANODE"/>
<connect gate="G$1" pin="C" pad="CATHODE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ARRAY_50D_1206" package="LED_ARRAY_50DEG_1206">
<connects>
<connect gate="G$1" pin="A" pad="A1 A2 A3 A4 A5 A6 A7"/>
<connect gate="G$1" pin="C" pad="C1 C2 C3 C4 C5 C6 C7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ARRAY_60D_4PLCC" package="LED_ARRAY_60DEG_4PLCC">
<connects>
<connect gate="G$1" pin="A" pad="A1 A2 A3 A4 A5 A6 A7 A8 A9 A10 A11 A12 A13 A14 A15 A16 A17 A18"/>
<connect gate="G$1" pin="C" pad="C C1 C2 C3 C4 C5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603" package="SMD-0603">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1206" package="SMD-1206">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1206P" package="LED_1206_PIPE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INDUCTOR" prefix="L" uservalue="yes">
<description>Inductors - Standard, Chokes, Ferrite Beads</description>
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="_0402" package="SMD-0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805" package="SMD-0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1008" package="SMD-1008(2520)">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_NLFV" package="NLFV">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SLF7032" package="SLF7032">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HDR_1X1" prefix="H" uservalue="yes">
<description>Single Pin Header</description>
<gates>
<gate name="PIN" symbol="HDR_1X1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDR_1X1">
<connects>
<connect gate="PIN" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SW_TACTILE" uservalue="yes">
<description>Switches - Push, Slide, Toggle

&lt;p&gt;K12A: MOM SPST-NO, 100mA 30VDC, 50mOhm RON
&lt;p&gt;PTS525: MOM SPST-NO, 50mA 12VDC, 100mOhm RON
&lt;p&gt;KMR231GLFS: MOM, SPST-NO, 0.05A @ 32VDC, 100mOhm RON</description>
<gates>
<gate name="G$1" symbol="TAC-SWITCH" x="0" y="0"/>
</gates>
<devices>
<device name="_K12A" package="SW_K12A">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PART#" value="CKN10274-ND"/>
<attribute name="SUPPLIER" value="Digikey"/>
</technology>
</technologies>
</device>
<device name="_PTS" package="SM08">
<connects>
<connect gate="G$1" pin="1" pad="A1 A2"/>
<connect gate="G$1" pin="2" pad="B1 B2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1825" package="RST_TAC_SW-TH_RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_KMR" package="SW_KMR">
<connects>
<connect gate="G$1" pin="1" pad="1 2"/>
<connect gate="G$1" pin="2" pad="3 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TL5209DR">
<description>TL5209DR LDO Regulator : ADJ, 0-16Vin, 500mA, 60mV @ 1mA, SOIC-8</description>
<gates>
<gate name="G$1" symbol="TL5209DR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC-8">
<connects>
<connect gate="G$1" pin="ADJ/BYP" pad="4"/>
<connect gate="G$1" pin="EN" pad="1"/>
<connect gate="G$1" pin="GND" pad="5 6 7 8"/>
<connect gate="G$1" pin="VIN" pad="2"/>
<connect gate="G$1" pin="VOUT" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XTAL_OSC" prefix="XTAL" uservalue="yes">
<description>Crystal Oscillators with no GND</description>
<gates>
<gate name="G$1" symbol="XTAL_OSC" x="0" y="0"/>
</gates>
<devices>
<device name="_7XC-7A" package="XTAL_7XC-7A">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_HC49UP" package="XTAL_HC49UP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_HC49US" package="XTAL_HC49US">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_HC49UV" package="XTAL_HC49UV">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_10.5X4.8" package="XTAL_SMD_10.5X4.8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TC26H" package="XTAL_TC26H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TC38H" package="XTAL_TC38H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_11.5X5" package="XTAL_TH_11.5X5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_CM2X0C" package="XTAL_CM2X0C">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SN74LVC1G0X">
<description>SN74LVC1G06: Buffer/line driver, inverting

&lt;p&gt;SN74LVC1G07: Buffer/line driver, non-inverting</description>
<gates>
<gate name="G$1" symbol="SN74LVC1G06" x="0" y="0"/>
</gates>
<devices>
<device name="_DCK" package="SC-70-5">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="NC" pad="1"/>
<connect gate="G$1" pin="VCC" pad="5"/>
<connect gate="G$1" pin="Y" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SN74LVC2G241">
<gates>
<gate name="G$1" symbol="SN74LVC2G241" x="0" y="0"/>
</gates>
<devices>
<device name="_DCU" package="SSOP-8">
<connects>
<connect gate="G$1" pin="!OE" pad="1"/>
<connect gate="G$1" pin="1A" pad="2"/>
<connect gate="G$1" pin="1Y" pad="6"/>
<connect gate="G$1" pin="2A" pad="5"/>
<connect gate="G$1" pin="2OE" pad="7"/>
<connect gate="G$1" pin="2Y" pad="3"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TPD4S012">
<description>USB ESD SOLUTION W/CLAMP 6SON</description>
<gates>
<gate name="G$1" symbol="TPD4S012" x="0" y="0"/>
</gates>
<devices>
<device name="" package="6-SON">
<connects>
<connect gate="G$1" pin="D+" pad="1"/>
<connect gate="G$1" pin="D-" pad="2"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="ID" pad="3"/>
<connect gate="G$1" pin="VBUS" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="24LC32AT">
<description>24LC32AT EEPROM 32K (4kx8), I2C, 400KHz, SOT23-5</description>
<gates>
<gate name="G$1" symbol="24LC32AT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5">
<connects>
<connect gate="G$1" pin="SCL" pad="1"/>
<connect gate="G$1" pin="SDA" pad="3"/>
<connect gate="G$1" pin="VCC" pad="4"/>
<connect gate="G$1" pin="VSS" pad="2"/>
<connect gate="G$1" pin="WP" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="EMMC" uservalue="yes">
<description>MTFC2GMVEA-0M WT 2GB eMMC Chip, 153-pin BGA</description>
<gates>
<gate name="G$1" symbol="EMMC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BGA_153">
<connects>
<connect gate="G$1" pin="CLK" pad="M6"/>
<connect gate="G$1" pin="CMD" pad="M5"/>
<connect gate="G$1" pin="DAT0" pad="A3"/>
<connect gate="G$1" pin="DAT1" pad="A4"/>
<connect gate="G$1" pin="DAT2" pad="A5"/>
<connect gate="G$1" pin="DAT3" pad="B2"/>
<connect gate="G$1" pin="DAT4" pad="B3"/>
<connect gate="G$1" pin="DAT5" pad="B4"/>
<connect gate="G$1" pin="DAT6" pad="B5"/>
<connect gate="G$1" pin="DAT7" pad="B6"/>
<connect gate="G$1" pin="NRST" pad="K5"/>
<connect gate="G$1" pin="VCC" pad="E6 F5 J10 K9"/>
<connect gate="G$1" pin="VCCQ" pad="C6 M4 N4 P3 P5"/>
<connect gate="G$1" pin="VDDI" pad="C2"/>
<connect gate="G$1" pin="VSS" pad="E7 G5 H10 K8"/>
<connect gate="G$1" pin="VSSQ" pad="C4 N2 N5 P4 P6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DDR3L" uservalue="yes">
<description>MT41K256M16HA 4Gb DDR3L Memory, 96-pin BGA</description>
<gates>
<gate name="G$1" symbol="DDR3L" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BGA_96">
<connects>
<connect gate="G$1" pin="A0" pad="N3"/>
<connect gate="G$1" pin="A1" pad="P7"/>
<connect gate="G$1" pin="A10/AP" pad="L7"/>
<connect gate="G$1" pin="A11" pad="R7"/>
<connect gate="G$1" pin="A12/BC#" pad="N7"/>
<connect gate="G$1" pin="A13" pad="T3"/>
<connect gate="G$1" pin="A14" pad="T7"/>
<connect gate="G$1" pin="A15" pad="M7"/>
<connect gate="G$1" pin="A2" pad="P3"/>
<connect gate="G$1" pin="A3" pad="N2"/>
<connect gate="G$1" pin="A4" pad="P8"/>
<connect gate="G$1" pin="A5" pad="P2"/>
<connect gate="G$1" pin="A6" pad="R8"/>
<connect gate="G$1" pin="A7" pad="R2"/>
<connect gate="G$1" pin="A8" pad="T8"/>
<connect gate="G$1" pin="A9" pad="R3"/>
<connect gate="G$1" pin="BA0" pad="M2"/>
<connect gate="G$1" pin="BA1" pad="N8"/>
<connect gate="G$1" pin="BA2" pad="M3"/>
<connect gate="G$1" pin="CAS#" pad="K3"/>
<connect gate="G$1" pin="CK" pad="J7"/>
<connect gate="G$1" pin="CK#" pad="K7"/>
<connect gate="G$1" pin="CKE" pad="K9"/>
<connect gate="G$1" pin="CS#" pad="L2"/>
<connect gate="G$1" pin="DQ0" pad="E3"/>
<connect gate="G$1" pin="DQ1" pad="F7"/>
<connect gate="G$1" pin="DQ10" pad="C8"/>
<connect gate="G$1" pin="DQ11" pad="C2"/>
<connect gate="G$1" pin="DQ12" pad="A7"/>
<connect gate="G$1" pin="DQ13" pad="A2"/>
<connect gate="G$1" pin="DQ14" pad="B8"/>
<connect gate="G$1" pin="DQ15" pad="A3"/>
<connect gate="G$1" pin="DQ2" pad="F2"/>
<connect gate="G$1" pin="DQ3" pad="F8"/>
<connect gate="G$1" pin="DQ4" pad="H3"/>
<connect gate="G$1" pin="DQ5" pad="H8"/>
<connect gate="G$1" pin="DQ6" pad="G2"/>
<connect gate="G$1" pin="DQ7" pad="H7"/>
<connect gate="G$1" pin="DQ8" pad="D7"/>
<connect gate="G$1" pin="DQ9" pad="C3"/>
<connect gate="G$1" pin="LDM" pad="E7"/>
<connect gate="G$1" pin="LDQS" pad="F3"/>
<connect gate="G$1" pin="LDQS#" pad="G3"/>
<connect gate="G$1" pin="NC" pad="J1 J9 L1 L9"/>
<connect gate="G$1" pin="ODT" pad="K1"/>
<connect gate="G$1" pin="RAS#" pad="J3"/>
<connect gate="G$1" pin="RESET#" pad="T2"/>
<connect gate="G$1" pin="UDM" pad="D3"/>
<connect gate="G$1" pin="UDQS" pad="C7"/>
<connect gate="G$1" pin="UDQS#" pad="B7"/>
<connect gate="G$1" pin="VDD" pad="B2 D9 G7 K2 K8 N1 N9 R1 R9"/>
<connect gate="G$1" pin="VDDQ" pad="A1 A8 C1 C9 D2 E9 F1 H2 H9"/>
<connect gate="G$1" pin="VREFCA" pad="M8"/>
<connect gate="G$1" pin="VREFDQ" pad="H1"/>
<connect gate="G$1" pin="VSS" pad="A9 B3 E1 G8 J2 J8 M1 M9 P1 P9 T1 T9"/>
<connect gate="G$1" pin="VSSQ" pad="B1 B9 D1 D8 E2 E8 F9 G1 G9"/>
<connect gate="G$1" pin="WE#" pad="L3"/>
<connect gate="G$1" pin="ZQ" pad="L8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MICRO-SD">
<description>Micro-SD Card Connector

&lt;p&gt;Primarily used for SCHA5B0200 model</description>
<gates>
<gate name="G$1" symbol="SCHA5B0200" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SCHA5B0200">
<connects>
<connect gate="G$1" pin="CD" pad="9"/>
<connect gate="G$1" pin="CLK" pad="5"/>
<connect gate="G$1" pin="CMD" pad="3"/>
<connect gate="G$1" pin="DAT0" pad="7"/>
<connect gate="G$1" pin="DAT1" pad="8"/>
<connect gate="G$1" pin="DAT2" pad="1"/>
<connect gate="G$1" pin="DAT3/CD" pad="2"/>
<connect gate="G$1" pin="GND" pad="10"/>
<connect gate="G$1" pin="SHIELD" pad="11 12 13 14 15 16"/>
<connect gate="G$1" pin="VDD" pad="4"/>
<connect gate="G$1" pin="VSS" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TPS2051B">
<gates>
<gate name="G$1" symbol="TPS2051B" x="0" y="0"/>
</gates>
<devices>
<device name="_DGN" package="MSOP-8_PAD">
<connects>
<connect gate="G$1" pin="!EN" pad="4"/>
<connect gate="G$1" pin="!OC" pad="5"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="IN" pad="2 3"/>
<connect gate="G$1" pin="OUT" pad="6 7 8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB">
<description>USB Type A Receptacle

&lt;p&gt;87520-0010BLF</description>
<gates>
<gate name="G$1" symbol="USB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="USB">
<connects>
<connect gate="G$1" pin="D+" pad="3"/>
<connect gate="G$1" pin="D-" pad="2"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="SHIELD" pad="SH1 SH2"/>
<connect gate="G$1" pin="VBUS" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LAN8710A">
<gates>
<gate name="G$1" symbol="LAN8710A" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN-32">
<connects>
<connect gate="G$1" pin="COL/CRS_DV/!MODE2" pad="15"/>
<connect gate="G$1" pin="CRS" pad="14"/>
<connect gate="G$1" pin="INT/TXER/TXD4" pad="18"/>
<connect gate="G$1" pin="LED1/!REGOFF" pad="3"/>
<connect gate="G$1" pin="LED2/!INTSEL" pad="2"/>
<connect gate="G$1" pin="MDC" pad="17"/>
<connect gate="G$1" pin="MDIO" pad="16"/>
<connect gate="G$1" pin="NRST" pad="19"/>
<connect gate="G$1" pin="RBIAS" pad="32"/>
<connect gate="G$1" pin="RXCLK/!PHYAD1" pad="7"/>
<connect gate="G$1" pin="RXD0/!MODE0" pad="11"/>
<connect gate="G$1" pin="RXD1/!MODE1" pad="10"/>
<connect gate="G$1" pin="RXD2/!RMIISEL" pad="9"/>
<connect gate="G$1" pin="RXD3/!PHYAD2" pad="8"/>
<connect gate="G$1" pin="RXDV" pad="26"/>
<connect gate="G$1" pin="RXER/RXD4/!PHYAD0" pad="13"/>
<connect gate="G$1" pin="RXN" pad="30"/>
<connect gate="G$1" pin="RXP" pad="31"/>
<connect gate="G$1" pin="TXCLK" pad="20"/>
<connect gate="G$1" pin="TXD0" pad="22"/>
<connect gate="G$1" pin="TXD1" pad="23"/>
<connect gate="G$1" pin="TXD2" pad="24"/>
<connect gate="G$1" pin="TXD3" pad="25"/>
<connect gate="G$1" pin="TXEN" pad="21"/>
<connect gate="G$1" pin="TXN" pad="28"/>
<connect gate="G$1" pin="TXP" pad="29"/>
<connect gate="G$1" pin="VDD1A" pad="27"/>
<connect gate="G$1" pin="VDD2A" pad="1"/>
<connect gate="G$1" pin="VDDCR" pad="6"/>
<connect gate="G$1" pin="VDDIO" pad="12"/>
<connect gate="G$1" pin="VSS_PAD" pad="EXP EXP1 EXP2 EXP3"/>
<connect gate="G$1" pin="XTAL/CLKIN" pad="5"/>
<connect gate="G$1" pin="XTAL2" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MAGJACK" uservalue="yes">
<description>RJ45 MAGJACK

&lt;p&gt;Bel Fuse Inc 08B0-1D1T-06-F, CONN MAGJACK 1PORT 100 BASE-T

&lt;p&gt; BB magjack: LPJ0011BBNL
&lt;br&gt;Equivalent: Wurth Elektronik 7499010211A</description>
<gates>
<gate name="MAGJACK_RJ45" symbol="MAGJACK" x="0" y="0"/>
</gates>
<devices>
<device name="_08B0" package="MAGJACK">
<connects>
<connect gate="MAGJACK_RJ45" pin="GLED+" pad="8"/>
<connect gate="MAGJACK_RJ45" pin="GLED-" pad="7"/>
<connect gate="MAGJACK_RJ45" pin="RCT" pad="6"/>
<connect gate="MAGJACK_RJ45" pin="RD+" pad="4"/>
<connect gate="MAGJACK_RJ45" pin="RD-" pad="5"/>
<connect gate="MAGJACK_RJ45" pin="SHIELD1" pad="S1"/>
<connect gate="MAGJACK_RJ45" pin="SHIELD2" pad="S2"/>
<connect gate="MAGJACK_RJ45" pin="TCT" pad="1"/>
<connect gate="MAGJACK_RJ45" pin="TD+" pad="2"/>
<connect gate="MAGJACK_RJ45" pin="TD-" pad="3"/>
<connect gate="MAGJACK_RJ45" pin="YLED+" pad="10"/>
<connect gate="MAGJACK_RJ45" pin="YLED-" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_BB" package="MAGJACK_BB">
<connects>
<connect gate="MAGJACK_RJ45" pin="GLED+" pad="GA"/>
<connect gate="MAGJACK_RJ45" pin="GLED-" pad="GC"/>
<connect gate="MAGJACK_RJ45" pin="RCT" pad="5"/>
<connect gate="MAGJACK_RJ45" pin="RD+" pad="3"/>
<connect gate="MAGJACK_RJ45" pin="RD-" pad="6"/>
<connect gate="MAGJACK_RJ45" pin="SHIELD1" pad="SHIELD1"/>
<connect gate="MAGJACK_RJ45" pin="SHIELD2" pad="8 SHIELD2"/>
<connect gate="MAGJACK_RJ45" pin="TCT" pad="4"/>
<connect gate="MAGJACK_RJ45" pin="TD+" pad="1"/>
<connect gate="MAGJACK_RJ45" pin="TD-" pad="2"/>
<connect gate="MAGJACK_RJ45" pin="YLED+" pad="YA"/>
<connect gate="MAGJACK_RJ45" pin="YLED-" pad="YC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TLV70233-3.3">
<description>&lt;b&gt;TI Voltage Regulators (LDO)&lt;/b&gt;

&lt;p&gt;TLV72033 - IC REG LDO 3.3V 0.3A SOT23-5
&lt;p&gt;TLV71033 - 3.3Vout, 150mA SOT23-5</description>
<gates>
<gate name="TLV72033" symbol="TLV72033-3.3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5">
<connects>
<connect gate="TLV72033" pin="EN" pad="3"/>
<connect gate="TLV72033" pin="GND" pad="2"/>
<connect gate="TLV72033" pin="NC/FB" pad="4"/>
<connect gate="TLV72033" pin="VIN" pad="1"/>
<connect gate="TLV72033" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>Protection Diode

&lt;p&gt;1N4004 - 400V, 1A, 1.1Vf
&lt;br&gt;SBR3U60P1-7: 60V, 3A, 0.65Vf
&lt;br&gt;RR264M-400TR : 400Vr, 700mA If, 1.1Vf @ 700mA, PMDU</description>
<gates>
<gate name="G$1" symbol="DIODE_STD" x="2.54" y="0"/>
</gates>
<devices>
<device name="_TH" package="DIODE_TH">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SMD123" package="POWERDI-123">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_PMDU" package="PMDU">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FAN">
<description>On-board 30mm x 30mm cooling fan</description>
<gates>
<gate name="G$1" symbol="FAN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FAN_30MM">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="PWR" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET_N-SD">
<description>N-Channel MOSFETs with Schottky Protection

&lt;p&gt;PSMN6R5-25YLC,115: 25Vdss, 64A Id, 48W, 1.95Vgs, 6.5mOhm Rds On

&lt;p&gt;IRLML6346TRPBF: 30Vdss, 3.4A Id, 1.3W, 1.1Vgs, 63mOhm Rds On, SOT23-3</description>
<gates>
<gate name="G$1" symbol="N-MOSFET_SD" x="0" y="0"/>
</gates>
<devices>
<device name="_SOT669" package="SOT669">
<connects>
<connect gate="G$1" pin="D" pad="5"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="S" pad="1 2 3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SOT233" package="SOT23-3">
<connects>
<connect gate="G$1" pin="D" pad="1"/>
<connect gate="G$1" pin="G" pad="2"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TESTPOINT" prefix="TP" uservalue="yes">
<description>1-pin header test point</description>
<gates>
<gate name="G$1" symbol="TESTPOINT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDR_1X1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BJT_NPN" uservalue="yes">
<description>NPN BJTs

&lt;p&gt;222A:
&lt;br&gt; 2N222A: 40V 600mA, TH


&lt;p&gt;3904:
&lt;br&gt;2N3904:	40V, 200mA, 625mW, TH
&lt;br&gt;MMBT3904: 40V, 200mA, 350mW, SOT23-3
&lt;br&gt;PZT3904: 40V, 200mA, 1W, SOT-223
&lt;br&gt;DZT5551: 160V, 600mA, 1W, SOT-223</description>
<gates>
<gate name="BJT" symbol="BJT" x="0" y="0"/>
</gates>
<devices>
<device name="_SOT23-3" package="SOT23-3">
<connects>
<connect gate="BJT" pin="B" pad="2"/>
<connect gate="BJT" pin="C" pad="1"/>
<connect gate="BJT" pin="E" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SOT-223" package="SOT-223">
<connects>
<connect gate="BJT" pin="B" pad="1"/>
<connect gate="BJT" pin="C" pad="2 4"/>
<connect gate="BJT" pin="E" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_T092" package="T092-CBE">
<connects>
<connect gate="BJT" pin="B" pad="B"/>
<connect gate="BJT" pin="C" pad="C"/>
<connect gate="BJT" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FTDI">
<description>Standard 6-pin FTDI Connector (Flat)</description>
<gates>
<gate name="G$1" symbol="FTDI" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FTDI">
<connects>
<connect gate="G$1" pin="CTS" pad="CTS"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="RTS" pad="RTS"/>
<connect gate="G$1" pin="RXD" pad="RXD"/>
<connect gate="G$1" pin="TXD" pad="TXD"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RF">
<description>SMAKN RF Transmitter/Receiver - 315/433MHz, 5V, Serial IO</description>
<gates>
<gate name="G$1" symbol="RF_RXTX" x="0" y="0"/>
</gates>
<devices>
<device name="_REC" package="RF_RECEIVER">
<connects>
<connect gate="G$1" pin="DATA" pad="DATA"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_TRANS" package="RF_TRANSMITTER">
<connects>
<connect gate="G$1" pin="DATA" pad="DATA"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEATSINK" prefix="HS">
<description>Heatsink/Thermal Pad

&lt;p&gt;V2017B: 10mm x 10mm</description>
<gates>
<gate name="G$1" symbol="HEATSINK" x="0" y="0"/>
</gates>
<devices>
<device name="_10MM" package="THERMAL_PAD">
<connects>
<connect gate="G$1" pin="HS" pad="PAD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="AM3358BZCZ100">
<packages>
<package name="ZCZ324">
<description>324-pin BGA (from 0.016mm to 0.014mm)</description>
<smd name="A1" x="-6.8072" y="6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="A2" x="-5.9944" y="6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="A3" x="-5.207" y="6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="A4" x="-4.3942" y="6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="A5" x="-3.6068" y="6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="A6" x="-2.794" y="6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="A9" x="-0.4064" y="6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="A10" x="0.4064" y="6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="A18" x="6.8072" y="6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="B1" x="-6.8072" y="5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="B2" x="-5.9944" y="5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="B3" x="-5.207" y="5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="B4" x="-4.3942" y="5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="B5" x="-3.6068" y="5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="B9" x="-0.4064" y="5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="B15" x="4.3942" y="5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="B18" x="6.8072" y="5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="C1" x="-6.8072" y="5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="C2" x="-5.9944" y="5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="C3" x="-5.207" y="5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="C4" x="-4.3942" y="5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="C5" x="-3.6068" y="5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="C6" x="-2.794" y="5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="C9" x="-0.4064" y="5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="C10" x="0.4064" y="5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="C15" x="4.3942" y="5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="C16" x="5.207" y="5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="C17" x="5.9944" y="5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="D1" x="-6.8072" y="4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="D2" x="-5.9944" y="4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="D3" x="-5.207" y="4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="D4" x="-4.3942" y="4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="D5" x="-3.6068" y="4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="D6" x="-2.794" y="4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="D7" x="-2.0066" y="4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="D8" x="-1.1938" y="4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="D9" x="-0.4064" y="4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="D10" x="0.4064" y="4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="D11" x="1.1938" y="4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="D15" x="4.3942" y="4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="D16" x="5.207" y="4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="E1" x="-6.8072" y="3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="E2" x="-5.9944" y="3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="E3" x="-5.207" y="3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="E4" x="-4.3942" y="3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="E5" x="-3.6068" y="3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="E6" x="-2.794" y="3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="E7" x="-2.0066" y="3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="E8" x="-1.1938" y="3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="E9" x="-0.4064" y="3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="E10" x="0.4064" y="3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="E11" x="1.1938" y="3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="E12" x="2.0066" y="3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="E13" x="2.794" y="3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="E14" x="3.6068" y="3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="E15" x="4.3942" y="3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="E16" x="5.207" y="3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="F1" x="-6.8072" y="2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="F2" x="-5.9944" y="2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="F3" x="-5.207" y="2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="F4" x="-4.3942" y="2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="F5" x="-3.6068" y="2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="F6" x="-2.794" y="2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="F7" x="-2.0066" y="2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="F8" x="-1.1938" y="2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="F9" x="-0.4064" y="2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="F10" x="0.4064" y="2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="F11" x="1.1938" y="2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="F12" x="2.0066" y="2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="F13" x="2.794" y="2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="F14" x="3.6068" y="2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="F15" x="4.3942" y="2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="F17" x="5.9944" y="2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="F18" x="6.8072" y="2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="G1" x="-6.8072" y="2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="G2" x="-5.9944" y="2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="G3" x="-5.207" y="2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="G4" x="-4.3942" y="2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="G5" x="-3.6068" y="2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="G6" x="-2.794" y="2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="G7" x="-2.0066" y="2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="G8" x="-1.1938" y="2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="G9" x="-0.4064" y="2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="G10" x="0.4064" y="2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="G11" x="1.1938" y="2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="G12" x="2.0066" y="2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="G13" x="2.794" y="2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="G14" x="3.6068" y="2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="G15" x="4.3942" y="2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="G16" x="5.207" y="2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="G17" x="5.9944" y="2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="G18" x="6.8072" y="2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="H1" x="-6.8072" y="1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="H2" x="-5.9944" y="1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="H3" x="-5.207" y="1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="H4" x="-4.3942" y="1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="H5" x="-3.6068" y="1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="H6" x="-2.794" y="1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="H7" x="-2.0066" y="1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="H8" x="-1.1938" y="1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="H9" x="-0.4064" y="1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="H10" x="0.4064" y="1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="H11" x="1.1938" y="1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="H12" x="2.0066" y="1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="H13" x="2.794" y="1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="H14" x="3.6068" y="1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="H15" x="4.3942" y="1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="H16" x="5.207" y="1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="H17" x="5.9944" y="1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="H18" x="6.8072" y="1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="J1" x="-6.8072" y="0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="J2" x="-5.9944" y="0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="J3" x="-5.207" y="0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="J4" x="-4.3942" y="0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="J5" x="-3.6068" y="0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="J6" x="-2.794" y="0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="J7" x="-2.0066" y="0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="J8" x="-1.1938" y="0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="J9" x="-0.4064" y="0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="J10" x="0.4064" y="0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="J11" x="1.1938" y="0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="J12" x="2.0066" y="0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="J13" x="2.794" y="0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="J14" x="3.6068" y="0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="J15" x="4.3942" y="0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="J16" x="5.207" y="0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="J17" x="5.9944" y="0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="J18" x="6.8072" y="0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="K1" x="-6.8072" y="-0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="K2" x="-5.9944" y="-0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="K3" x="-5.207" y="-0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="K4" x="-4.3942" y="-0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="K5" x="-3.6068" y="-0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="K6" x="-2.794" y="-0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="K7" x="-2.0066" y="-0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="K8" x="-1.1938" y="-0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="K9" x="-0.4064" y="-0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="K10" x="0.4064" y="-0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="K11" x="1.1938" y="-0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="K12" x="2.0066" y="-0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="K13" x="2.794" y="-0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="K14" x="3.6068" y="-0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="K15" x="4.3942" y="-0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="K16" x="5.207" y="-0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="K17" x="5.9944" y="-0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="K18" x="6.8072" y="-0.4064" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="L1" x="-6.8072" y="-1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="L2" x="-5.9944" y="-1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="L3" x="-5.207" y="-1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="L4" x="-4.3942" y="-1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="L5" x="-3.6068" y="-1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="L6" x="-2.794" y="-1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="L7" x="-2.0066" y="-1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="L8" x="-1.1938" y="-1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="L9" x="-0.4064" y="-1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="L10" x="0.4064" y="-1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="L11" x="1.1938" y="-1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="L12" x="2.0066" y="-1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="L13" x="2.794" y="-1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="L14" x="3.6068" y="-1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="L15" x="4.3942" y="-1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="L16" x="5.207" y="-1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="L17" x="5.9944" y="-1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="L18" x="6.8072" y="-1.1938" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="M1" x="-6.8072" y="-2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="M2" x="-5.9944" y="-2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="M3" x="-5.207" y="-2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="M4" x="-4.3942" y="-2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="M6" x="-2.794" y="-2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="M7" x="-2.0066" y="-2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="M8" x="-1.1938" y="-2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="M9" x="-0.4064" y="-2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="M10" x="0.4064" y="-2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="M11" x="1.1938" y="-2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="M12" x="2.0066" y="-2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="M13" x="2.794" y="-2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="M14" x="3.6068" y="-2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="M16" x="5.207" y="-2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="M17" x="5.9944" y="-2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="M18" x="6.8072" y="-2.0066" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="N1" x="-6.8072" y="-2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="N2" x="-5.9944" y="-2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="N3" x="-5.207" y="-2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="N4" x="-4.3942" y="-2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="N5" x="-3.6068" y="-2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="N6" x="-2.794" y="-2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="N7" x="-2.0066" y="-2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="N8" x="-1.1938" y="-2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="N9" x="-0.4064" y="-2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="N10" x="0.4064" y="-2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="N11" x="1.1938" y="-2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="N12" x="2.0066" y="-2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="N13" x="2.794" y="-2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="N14" x="3.6068" y="-2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="N15" x="4.3942" y="-2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="N16" x="5.207" y="-2.794" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="P1" x="-6.8072" y="-3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="P2" x="-5.9944" y="-3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="P3" x="-5.207" y="-3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="P4" x="-4.3942" y="-3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="P5" x="-3.6068" y="-3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="P6" x="-2.794" y="-3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="P7" x="-2.0066" y="-3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="P8" x="-1.1938" y="-3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="P9" x="-0.4064" y="-3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="P10" x="0.4064" y="-3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="P11" x="1.1938" y="-3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="P12" x="2.0066" y="-3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="P13" x="2.794" y="-3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="P14" x="3.6068" y="-3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="P17" x="5.9944" y="-3.6068" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="R1" x="-6.8072" y="-4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="R2" x="-5.9944" y="-4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="R3" x="-5.207" y="-4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="R4" x="-4.3942" y="-4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="R8" x="-1.1938" y="-4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="R9" x="-0.4064" y="-4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="R10" x="0.4064" y="-4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="R11" x="1.1938" y="-4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="R12" x="2.0066" y="-4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="R13" x="2.794" y="-4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="R14" x="3.6068" y="-4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="R15" x="4.3942" y="-4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="R16" x="5.207" y="-4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="R17" x="5.9944" y="-4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="R18" x="6.8072" y="-4.3942" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="T1" x="-6.8072" y="-5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="T2" x="-5.9944" y="-5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="T3" x="-5.207" y="-5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="T4" x="-4.3942" y="-5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="T5" x="-3.6068" y="-5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="T8" x="-1.1938" y="-5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="T9" x="-0.4064" y="-5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="T11" x="1.1938" y="-5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="T12" x="2.0066" y="-5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="T14" x="3.6068" y="-5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="T15" x="4.3942" y="-5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="T16" x="5.207" y="-5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="T17" x="5.9944" y="-5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="T18" x="6.8072" y="-5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="U1" x="-6.8072" y="-5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="U2" x="-5.9944" y="-5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="U3" x="-5.207" y="-5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="U4" x="-4.3942" y="-5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="U7" x="-2.0066" y="-5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="U8" x="-1.1938" y="-5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="U9" x="-0.4064" y="-5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="U11" x="1.1938" y="-5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="U12" x="2.0066" y="-5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="U13" x="2.794" y="-5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="U14" x="3.6068" y="-5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="U15" x="4.3942" y="-5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="U16" x="5.207" y="-5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="U17" x="5.9944" y="-5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="U18" x="6.8072" y="-5.9944" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="V1" x="-6.8072" y="-6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="V2" x="-5.9944" y="-6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="V3" x="-5.207" y="-6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="V4" x="-4.3942" y="-6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="V7" x="-2.0066" y="-6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="V8" x="-1.1938" y="-6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="V9" x="-0.4064" y="-6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="V10" x="0.4064" y="-6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="V11" x="1.1938" y="-6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="V12" x="2.0066" y="-6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="V13" x="2.794" y="-6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="V14" x="3.6068" y="-6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="V15" x="4.3942" y="-6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="V16" x="5.207" y="-6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="V17" x="5.9944" y="-6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<smd name="V18" x="6.8072" y="-6.8072" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
<wire x1="-7.5438" y1="3.6068" x2="-7.7978" y2="3.6068" width="0.1524" layer="21"/>
<wire x1="-7.5438" y1="-0.4064" x2="-7.7978" y2="-0.4064" width="0.1524" layer="21"/>
<wire x1="7.5438" y1="-0.4064" x2="7.7978" y2="-0.4064" width="0.1524" layer="21"/>
<wire x1="-7.5438" y1="-4.3942" x2="-7.7978" y2="-4.3942" width="0.1524" layer="21"/>
<wire x1="7.5438" y1="-4.3942" x2="7.7978" y2="-4.3942" width="0.1524" layer="21"/>
<wire x1="-3.6068" y1="7.5438" x2="-3.6068" y2="7.7978" width="0.1524" layer="21"/>
<wire x1="-3.6068" y1="-7.5438" x2="-3.6068" y2="-7.7978" width="0.1524" layer="21"/>
<wire x1="0.4064" y1="7.5438" x2="0.4064" y2="7.7978" width="0.1524" layer="21"/>
<wire x1="0.4064" y1="-7.5438" x2="0.4064" y2="-7.7978" width="0.1524" layer="21"/>
<wire x1="4.3942" y1="7.5438" x2="4.3942" y2="7.7978" width="0.1524" layer="21"/>
<wire x1="4.3942" y1="-7.5438" x2="4.3942" y2="-7.7978" width="0.1524" layer="21"/>
<wire x1="-7.1374" y1="7.5438" x2="-7.5438" y2="7.1374" width="0.1524" layer="21"/>
<wire x1="-7.5438" y1="-7.5438" x2="7.5438" y2="-7.5438" width="0.1524" layer="21"/>
<wire x1="7.5438" y1="-7.5438" x2="7.5438" y2="7.5438" width="0.1524" layer="21"/>
<wire x1="7.5438" y1="7.5438" x2="-7.5438" y2="7.5438" width="0.1524" layer="21"/>
<wire x1="-7.5438" y1="7.5438" x2="-7.5438" y2="-7.5438" width="0.1524" layer="21"/>
<text x="-8.3693" y="6.5532" size="0.6096" layer="21" ratio="6" rot="SR0">A</text>
<text x="-8.3058" y="-7.1882" size="0.6096" layer="21" ratio="6" rot="SR0">V</text>
<text x="-6.9977" y="7.8867" size="0.6096" layer="21" ratio="6" rot="SR0">1</text>
<text x="6.3373" y="7.7978" size="0.6096" layer="21" ratio="6" rot="SR0">18</text>
<text x="7.7343" y="-8.1915" size="0.6096" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<circle x="-7.874" y="7.874" radius="0.200803125" width="0.127" layer="21"/>
<smd name="T10" x="0.4064" y="-5.207" dx="0.3302" dy="0.3302" layer="1" roundness="100"/>
</package>
</packages>
<symbols>
<symbol name="AM3358_ZCZ_324_A">
<pin name="VSS_2" x="-30.48" y="50.8" length="middle" direction="pwr"/>
<pin name="VDD_MPU_MON" x="-30.48" y="48.26" length="middle" direction="pwr"/>
<pin name="RESERVED" x="-30.48" y="45.72" length="middle" direction="out"/>
<pin name="RTC_XTALOUT" x="-30.48" y="43.18" length="middle" direction="out"/>
<pin name="VSS_RTC" x="-30.48" y="40.64" length="middle" direction="pwr"/>
<pin name="RTC_XTALIN" x="-30.48" y="38.1" length="middle" direction="in"/>
<pin name="VREFN" x="-30.48" y="30.48" length="middle" direction="pwr"/>
<pin name="DDR_A5" x="-30.48" y="27.94" length="middle" direction="out"/>
<pin name="DDR_WEN" x="-30.48" y="25.4" length="middle"/>
<pin name="DDR_BA2" x="-30.48" y="22.86" length="middle"/>
<pin name="RTC_KALDO_ENN" x="-30.48" y="20.32" length="middle"/>
<pin name="RTC_PWRONRSTN" x="-30.48" y="17.78" length="middle"/>
<pin name="VREFP" x="-30.48" y="7.62" length="middle" direction="pwr"/>
<pin name="DDR_A9" x="-30.48" y="5.08" length="middle" direction="out"/>
<pin name="DDR_A4" x="-30.48" y="2.54" length="middle" direction="out"/>
<pin name="DDR_A3" x="-30.48" y="0" length="middle" direction="out"/>
<pin name="DDR_BA0" x="-30.48" y="-2.54" length="middle" direction="out"/>
<pin name="EXT_WAKEUP" x="-30.48" y="-5.08" length="middle" direction="in"/>
<pin name="PMIC_POWER_EN" x="-30.48" y="-7.62" length="middle" direction="out"/>
<pin name="AIN7" x="-30.48" y="-15.24" length="middle" direction="pas"/>
<pin name="DDR_CKN" x="-30.48" y="-17.78" length="middle" direction="out"/>
<pin name="DDR_CK" x="-30.48" y="-20.32" length="middle" direction="out"/>
<pin name="DDR_A15" x="-30.48" y="-22.86" length="middle" direction="out"/>
<pin name="DDR_A8" x="-30.48" y="-25.4" length="middle" direction="out"/>
<pin name="DDR_A6" x="-30.48" y="-27.94" length="middle" direction="out"/>
<pin name="CAP_VDD_RTC" x="-30.48" y="-30.48" length="middle" direction="pas"/>
<pin name="VDDS_RTC" x="-30.48" y="-33.02" length="middle" direction="pwr"/>
<pin name="VDDA_ADC" x="-30.48" y="-35.56" length="middle" direction="pwr"/>
<pin name="CAP_VDD_SRAM_CORE" x="-30.48" y="-38.1" length="middle"/>
<pin name="DDR_BA1" x="-30.48" y="-40.64" length="middle" direction="out"/>
<pin name="DDR_A7" x="-30.48" y="-43.18" length="middle" direction="out"/>
<pin name="DDR_A12" x="-30.48" y="-45.72" length="middle" direction="out"/>
<pin name="DDR_A2" x="-30.48" y="-48.26" length="middle" direction="out"/>
<pin name="VDDS_DDR_2" x="33.02" y="-50.8" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDS_2" x="33.02" y="-48.26" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDS_PLL_DDR" x="33.02" y="-45.72" length="middle" direction="pwr" rot="R180"/>
<pin name="VSSA_ADC" x="33.02" y="-43.18" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDS_SRAM_CORE_BG" x="33.02" y="-40.64" length="middle" direction="pwr" rot="R180"/>
<pin name="DDR_CASN" x="33.02" y="-38.1" length="middle" direction="out" rot="R180"/>
<pin name="DDR_A11" x="33.02" y="-35.56" length="middle" direction="out" rot="R180"/>
<pin name="DDR_A0" x="33.02" y="-33.02" length="middle" direction="out" rot="R180"/>
<pin name="DDR_A10" x="33.02" y="-30.48" length="middle" direction="out" rot="R180"/>
<pin name="VDDS_DDR_3" x="33.02" y="-27.94" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD_CORE_2" x="33.02" y="-25.4" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD_CORE_3" x="33.02" y="-22.86" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS_3" x="33.02" y="-20.32" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDS" x="33.02" y="-17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="DDR_ODT" x="33.02" y="-15.24" length="middle" direction="out" rot="R180"/>
<pin name="DDR_RESETN" x="33.02" y="-12.7" length="middle" direction="out" rot="R180"/>
<pin name="DDR_CKE" x="33.02" y="-10.16" length="middle" direction="out" rot="R180"/>
<pin name="DDR_RASN" x="33.02" y="-7.62" length="middle" direction="out" rot="R180"/>
<pin name="VDDS_DDR_4" x="33.02" y="-5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD_CORE_4" x="33.02" y="-2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD_CORE" x="33.02" y="0" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS_4" x="33.02" y="2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS_5" x="33.02" y="5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="DDR_A1" x="33.02" y="7.62" length="middle" direction="out" rot="R180"/>
<pin name="DDR_CSN0" x="33.02" y="10.16" length="middle" direction="out" rot="R180"/>
<pin name="DDR_A13" x="33.02" y="12.7" length="middle" direction="out" rot="R180"/>
<pin name="DDR_A14" x="33.02" y="15.24" length="middle" direction="out" rot="R180"/>
<pin name="VDDS_DDR_5" x="33.02" y="17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS_6" x="33.02" y="20.32" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS_7" x="33.02" y="22.86" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS_8" x="33.02" y="25.4" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS_9" x="33.02" y="27.94" length="middle" direction="pwr" rot="R180"/>
<pin name="DDR_D8" x="33.02" y="30.48" length="middle" rot="R180"/>
<pin name="DDR_DQM1" x="33.02" y="33.02" length="middle" direction="out" rot="R180"/>
<pin name="DDR_VTP" x="33.02" y="35.56" length="middle" direction="in" rot="R180"/>
<pin name="DDR_VREF" x="33.02" y="38.1" length="middle" direction="pas" rot="R180"/>
<pin name="VDDS_DDR" x="33.02" y="40.64" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS_10" x="33.02" y="43.18" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS_11" x="33.02" y="45.72" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS_12" x="33.02" y="48.26" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS" x="33.02" y="50.8" length="middle" direction="pwr" rot="R180"/>
<wire x1="-25.4" y1="53.34" x2="-25.4" y2="-53.34" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="-53.34" x2="27.94" y2="-53.34" width="0.1524" layer="94"/>
<wire x1="27.94" y1="-53.34" x2="27.94" y2="53.34" width="0.1524" layer="94"/>
<wire x1="27.94" y1="53.34" x2="-25.4" y2="53.34" width="0.1524" layer="94"/>
<text x="-22.5044" y="54.8386" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="4.4958" y="54.8386" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="AM3358_ZCZ_324_B">
<pin name="DDR_D9" x="-30.48" y="45.72" length="middle"/>
<pin name="DDR_D10" x="-30.48" y="43.18" length="middle"/>
<pin name="DDR_D11" x="-30.48" y="40.64" length="middle"/>
<pin name="DDR_D12" x="-30.48" y="38.1" length="middle"/>
<pin name="VDDS_DDR_2" x="-30.48" y="35.56" length="middle" direction="pwr"/>
<pin name="VDD_CORE_2" x="-30.48" y="33.02" length="middle" direction="pwr"/>
<pin name="VSS_2" x="-30.48" y="30.48" length="middle" direction="pwr"/>
<pin name="VDD_CORE_3" x="-30.48" y="27.94" length="middle" direction="pwr"/>
<pin name="VSS_3" x="-30.48" y="25.4" length="middle" direction="pwr"/>
<pin name="DDR_DQS1" x="-30.48" y="22.86" length="middle"/>
<pin name="DDR_DQSN1" x="-30.48" y="20.32" length="middle"/>
<pin name="DDR_D13" x="-30.48" y="17.78" length="middle"/>
<pin name="DDR_D14" x="-30.48" y="15.24" length="middle"/>
<pin name="VDDS_DDR" x="-30.48" y="12.7" length="middle" direction="pwr"/>
<pin name="VDD_CORE_4" x="-30.48" y="10.16" length="middle" direction="pwr"/>
<pin name="VDD_CORE_5" x="-30.48" y="7.62" length="middle" direction="pwr"/>
<pin name="VDD_CORE_6" x="-30.48" y="5.08" length="middle" direction="pwr"/>
<pin name="VDD_CORE_7" x="-30.48" y="2.54" length="middle" direction="pwr"/>
<pin name="DDR_D15" x="-30.48" y="0" length="middle"/>
<pin name="DDR_DQM0" x="-30.48" y="-2.54" length="middle" direction="out"/>
<pin name="DDR_D0" x="-30.48" y="-5.08" length="middle"/>
<pin name="DDR_D1" x="-30.48" y="-7.62" length="middle"/>
<pin name="VSS_4" x="-30.48" y="-12.7" length="middle" direction="pwr"/>
<pin name="VSS_5" x="-30.48" y="-15.24" length="middle" direction="pwr"/>
<pin name="VSS_6" x="-30.48" y="-17.78" length="middle" direction="pwr"/>
<pin name="VSS_7" x="-30.48" y="-20.32" length="middle" direction="pwr"/>
<pin name="DDR_D2" x="-30.48" y="-22.86" length="middle"/>
<pin name="DDR_D3" x="-30.48" y="-25.4" length="middle"/>
<pin name="DDR_D4" x="-30.48" y="-27.94" length="middle"/>
<pin name="DDR_D5" x="-30.48" y="-30.48" length="middle"/>
<pin name="VDDSHV6_2" x="-30.48" y="-33.02" length="middle" direction="pwr"/>
<pin name="VDDS_2" x="-30.48" y="-35.56" length="middle" direction="pwr"/>
<pin name="VSS_8" x="-30.48" y="-38.1" length="middle" direction="pwr"/>
<pin name="VDD_CORE_8" x="-30.48" y="-40.64" length="middle" direction="pwr"/>
<pin name="VDD_CORE" x="-30.48" y="-43.18" length="middle" direction="pwr"/>
<pin name="DDR_DQS0" x="-30.48" y="-45.72" length="middle"/>
<pin name="DDR_DQSN0" x="-30.48" y="-48.26" length="middle"/>
<pin name="DDR_D6" x="-30.48" y="-50.8" length="middle"/>
<pin name="DDR_D7" x="-30.48" y="-53.34" length="middle"/>
<pin name="VDDSHV6_3" x="33.02" y="-55.88" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDSHV6" x="33.02" y="-53.34" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDSHV1_2" x="33.02" y="-50.8" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDSHV1" x="33.02" y="-48.26" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDS" x="33.02" y="-45.72" length="middle" direction="pwr" rot="R180"/>
<pin name="LCD_DATA0" x="33.02" y="-43.18" length="middle" rot="R180"/>
<pin name="LCD_DATA1" x="33.02" y="-40.64" length="middle" rot="R180"/>
<pin name="LCD_DATA2" x="33.02" y="-38.1" length="middle" rot="R180"/>
<pin name="LCD_DATA3" x="33.02" y="-35.56" length="middle" rot="R180"/>
<pin name="GPMC_AD2" x="33.02" y="-25.4" length="middle" rot="R180"/>
<pin name="GPMC_AD6" x="33.02" y="-22.86" length="middle" rot="R180"/>
<pin name="LCD_DATA4" x="33.02" y="-20.32" length="middle" rot="R180"/>
<pin name="LCD_DATA5" x="33.02" y="-17.78" length="middle" rot="R180"/>
<pin name="LCD_DATA6" x="33.02" y="-15.24" length="middle" rot="R180"/>
<pin name="LCD_DATA7" x="33.02" y="-12.7" length="middle" rot="R180"/>
<pin name="LCD_DATA15" x="33.02" y="-10.16" length="middle" rot="R180"/>
<pin name="GPMC_AD3" x="33.02" y="-2.54" length="middle" rot="R180"/>
<pin name="GPMC_AD7" x="33.02" y="0" length="middle" rot="R180"/>
<pin name="LCD_DATA8" x="33.02" y="2.54" length="middle" rot="R180"/>
<pin name="LCD_DATA9" x="33.02" y="5.08" length="middle" rot="R180"/>
<pin name="LCD_DATA10" x="33.02" y="7.62" length="middle" rot="R180"/>
<pin name="LCD_DATA11" x="33.02" y="10.16" length="middle" rot="R180"/>
<pin name="GPMC_AD0" x="33.02" y="17.78" length="middle" rot="R180"/>
<pin name="GPMC_AD4" x="33.02" y="20.32" length="middle" rot="R180"/>
<pin name="GPMC_CSN1" x="33.02" y="22.86" length="middle" rot="R180"/>
<pin name="VSS" x="33.02" y="25.4" length="middle" direction="pwr" rot="R180"/>
<pin name="LCD_DATA12" x="33.02" y="27.94" length="middle" rot="R180"/>
<pin name="LCD_DATA13" x="33.02" y="30.48" length="middle" rot="R180"/>
<pin name="LCD_DATA14" x="33.02" y="33.02" length="middle" rot="R180"/>
<pin name="GPMC_AD1" x="33.02" y="40.64" length="middle" rot="R180"/>
<pin name="GPMC_AD5" x="33.02" y="43.18" length="middle" rot="R180"/>
<pin name="GPMC_CSN2" x="33.02" y="45.72" length="middle" rot="R180"/>
<wire x1="-25.4" y1="48.26" x2="-25.4" y2="-58.42" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="-58.42" x2="27.94" y2="-58.42" width="0.1524" layer="94"/>
<wire x1="27.94" y1="-58.42" x2="27.94" y2="48.26" width="0.1524" layer="94"/>
<wire x1="27.94" y1="48.26" x2="-25.4" y2="48.26" width="0.1524" layer="94"/>
<text x="-22.5044" y="49.7586" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="4.4958" y="49.7586" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="AM3358_ZCZ_324_C">
<pin name="WARMRSTN" x="-30.48" y="48.26" length="middle"/>
<pin name="VSS_2" x="-30.48" y="27.94" length="middle" direction="pwr"/>
<pin name="PWRONRSTN" x="-30.48" y="12.7" length="middle" direction="in"/>
<pin name="EXTINTN" x="-30.48" y="5.08" length="middle" direction="in"/>
<pin name="CAP_VBB_MPU" x="-30.48" y="2.54" length="middle" direction="pas"/>
<pin name="SPI0_CS1" x="-30.48" y="-10.16" length="middle"/>
<pin name="I2C0_SCL" x="-30.48" y="-12.7" length="middle"/>
<pin name="I2C0_SDA" x="-30.48" y="-15.24" length="middle"/>
<pin name="VDDS_SRAM_MPU_BB" x="-30.48" y="-20.32" length="middle"/>
<pin name="CAP_VDD_SRAM_MPU" x="-30.48" y="-22.86" length="middle"/>
<pin name="UART1_TXD" x="-30.48" y="-33.02" length="middle"/>
<pin name="UART1_RXD" x="-30.48" y="-35.56" length="middle"/>
<pin name="VDDSHV6_2" x="-30.48" y="-43.18" length="middle" direction="pwr"/>
<pin name="VDDSHV6_3" x="-30.48" y="-45.72" length="middle" direction="pwr"/>
<pin name="VDDSHV6_4" x="-30.48" y="-48.26" length="middle" direction="pwr"/>
<pin name="VDDSHV6_5" x="-30.48" y="-50.8" length="middle" direction="pwr"/>
<pin name="VDDS" x="33.02" y="-53.34" length="middle" direction="pwr" rot="R180"/>
<pin name="UART0_RXD" x="33.02" y="-50.8" length="middle" rot="R180"/>
<pin name="UART0_TXD" x="33.02" y="-48.26" length="middle" rot="R180"/>
<pin name="VDD_MPU_2" x="33.02" y="-40.64" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD_MPU_3" x="33.02" y="-38.1" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD_MPU_4" x="33.02" y="-35.56" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD_MPU_5" x="33.02" y="-33.02" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDSHV6_6" x="33.02" y="-30.48" length="middle" direction="pwr" rot="R180"/>
<pin name="USB1_DRVVBUS" x="33.02" y="-27.94" length="middle" rot="R180"/>
<pin name="MMC0_DAT3" x="33.02" y="-22.86" length="middle" rot="R180"/>
<pin name="MMC0_DAT2" x="33.02" y="-20.32" length="middle" rot="R180"/>
<pin name="VDD_CORE_2" x="33.02" y="-17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS_3" x="33.02" y="-15.24" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS_4" x="33.02" y="-12.7" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD_MPU_6" x="33.02" y="-10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDSHV6" x="33.02" y="-7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="MMC0_DAT1" x="33.02" y="-5.08" length="middle" rot="R180"/>
<pin name="MMC0_DAT0" x="33.02" y="-2.54" length="middle" rot="R180"/>
<pin name="MMC0_CLK" x="33.02" y="0" length="middle" rot="R180"/>
<pin name="MMC0_CMD" x="33.02" y="2.54" length="middle" rot="R180"/>
<pin name="VSS_5" x="33.02" y="5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD_CORE_3" x="33.02" y="7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS_6" x="33.02" y="10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD_MPU_7" x="33.02" y="12.7" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDSHV4_2" x="33.02" y="15.24" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDS_PLL_MPU" x="33.02" y="17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="MII1_COL" x="33.02" y="20.32" length="middle" rot="R180"/>
<pin name="MII1_CRS" x="33.02" y="22.86" length="middle" rot="R180"/>
<pin name="RMII1_REF_CLK" x="33.02" y="25.4" length="middle" rot="R180"/>
<pin name="VSS_7" x="33.02" y="27.94" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS" x="33.02" y="30.48" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD_CORE" x="33.02" y="33.02" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD_MPU" x="33.02" y="35.56" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDSHV4" x="33.02" y="38.1" length="middle" direction="pwr" rot="R180"/>
<pin name="MII1_RX_ER" x="33.02" y="40.64" length="middle" rot="R180"/>
<pin name="MII1_TX_EN" x="33.02" y="43.18" length="middle" rot="R180"/>
<pin name="MII1_RX_DV" x="33.02" y="45.72" length="middle" rot="R180"/>
<pin name="MII1_TXD3" x="33.02" y="48.26" length="middle" rot="R180"/>
<wire x1="-25.4" y1="50.8" x2="-25.4" y2="-55.88" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="-55.88" x2="27.94" y2="-55.88" width="0.1524" layer="94"/>
<wire x1="27.94" y1="-55.88" x2="27.94" y2="50.8" width="0.1524" layer="94"/>
<wire x1="27.94" y1="50.8" x2="-25.4" y2="50.8" width="0.1524" layer="94"/>
<text x="-22.5044" y="52.2986" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="4.4958" y="52.2986" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="AM3358_ZCZ_324_D">
<pin name="VSS_2" x="-27.94" y="45.72" length="middle" direction="pwr"/>
<pin name="VSS_3" x="-27.94" y="43.18" length="middle" direction="pwr"/>
<pin name="VDD_CORE_2" x="-27.94" y="40.64" length="middle" direction="pwr"/>
<pin name="VDDS_2" x="-27.94" y="38.1" length="middle" direction="pwr"/>
<pin name="VDDSHV5_2" x="-27.94" y="35.56" length="middle" direction="pwr"/>
<pin name="MII1_TXD2" x="-27.94" y="33.02" length="middle"/>
<pin name="MII1_TXD1" x="-27.94" y="30.48" length="middle"/>
<pin name="MII1_TXD0" x="-27.94" y="27.94" length="middle"/>
<pin name="MII1_TX_CLK" x="-27.94" y="25.4" length="middle"/>
<pin name="VSS_4" x="-27.94" y="22.86" length="middle" direction="pwr"/>
<pin name="VSS_5" x="-27.94" y="20.32" length="middle" direction="pwr"/>
<pin name="VSS_6" x="-27.94" y="17.78" length="middle" direction="pwr"/>
<pin name="VSS_7" x="-27.94" y="15.24" length="middle" direction="pwr"/>
<pin name="VDDSHV5" x="-27.94" y="12.7" length="middle" direction="pwr"/>
<pin name="MII1_RXD1" x="-27.94" y="10.16" length="middle"/>
<pin name="MII1_RXD2" x="-27.94" y="7.62" length="middle"/>
<pin name="MII1_RXD3" x="-27.94" y="5.08" length="middle"/>
<pin name="MII1_RX_CLK" x="-27.94" y="2.54" length="middle"/>
<pin name="VSS_8" x="-27.94" y="0" length="middle" direction="pwr"/>
<pin name="VDD_CORE_3" x="-27.94" y="-2.54" length="middle" direction="pwr"/>
<pin name="VSS_9" x="-27.94" y="-5.08" length="middle" direction="pwr"/>
<pin name="VDD_CORE_4" x="-27.94" y="-7.62" length="middle" direction="pwr"/>
<pin name="VSSA_USB_2" x="-27.94" y="-10.16" length="middle" direction="pwr"/>
<pin name="MII1_RXD0" x="-27.94" y="-15.24" length="middle"/>
<pin name="MDIO" x="-27.94" y="-17.78" length="middle"/>
<pin name="MDC" x="-27.94" y="-20.32" length="middle"/>
<pin name="VSS_10" x="-27.94" y="-22.86" length="middle" direction="pwr"/>
<pin name="VSS_11" x="-27.94" y="-25.4" length="middle" direction="pwr"/>
<pin name="VDD_CORE_5" x="-27.94" y="-27.94" length="middle" direction="pwr"/>
<pin name="VDD_CORE" x="-27.94" y="-30.48" length="middle" direction="pwr"/>
<pin name="VSSA_USB" x="-27.94" y="-33.02" length="middle" direction="pwr"/>
<pin name="VDDA3P3V_USB0" x="-27.94" y="-35.56" length="middle" direction="pwr"/>
<pin name="VDDA1P8V_USB0" x="-27.94" y="-38.1" length="middle" direction="pwr"/>
<pin name="VDDSHV2_2" x="-27.94" y="-45.72" length="middle" direction="pwr"/>
<pin name="VDDSHV2" x="-27.94" y="-48.26" length="middle" direction="pwr"/>
<pin name="VDDSHV3_2" x="-27.94" y="-50.8" length="middle" direction="pwr"/>
<pin name="VDDSHV3" x="-27.94" y="-53.34" length="middle" direction="pwr"/>
<pin name="VDDS" x="30.48" y="-55.88" length="middle" direction="pwr" rot="R180"/>
<pin name="USB1_ID" x="30.48" y="-48.26" length="middle" rot="R180"/>
<pin name="VDDS_PLL_CORE_LCD" x="30.48" y="-43.18" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDS_OSC" x="30.48" y="-40.64" length="middle" direction="pwr" rot="R180"/>
<pin name="GPMC_AD13" x="30.48" y="-38.1" length="middle" rot="R180"/>
<pin name="GPMC_A0" x="30.48" y="-35.56" length="middle" rot="R180"/>
<pin name="GPMC_A4" x="30.48" y="-33.02" length="middle" rot="R180"/>
<pin name="VDDA3P3V_USB1" x="30.48" y="-30.48" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDA1P8V_USB1" x="30.48" y="-27.94" length="middle" direction="pwr" rot="R180"/>
<pin name="USB1_DP" x="30.48" y="-25.4" length="middle" rot="R180"/>
<pin name="USB1_DM" x="30.48" y="-22.86" length="middle" rot="R180"/>
<pin name="GPMC_AD10" x="30.48" y="-17.78" length="middle" rot="R180"/>
<pin name="GPMC_AD12" x="30.48" y="-15.24" length="middle" rot="R180"/>
<pin name="GPMC_A3" x="30.48" y="-10.16" length="middle" rot="R180"/>
<pin name="GPMC_A7" x="30.48" y="-7.62" length="middle" rot="R180"/>
<pin name="GPMC_A10" x="30.48" y="-5.08" length="middle" rot="R180"/>
<pin name="GPMC_WAIT0" x="30.48" y="-2.54" length="middle" rot="R180"/>
<pin name="USB1_VBUS" x="30.48" y="0" length="middle" rot="R180"/>
<pin name="XTALOUT" x="30.48" y="5.08" length="middle" direction="out" rot="R180"/>
<pin name="GPMC_AD11" x="30.48" y="7.62" length="middle" rot="R180"/>
<pin name="GPMC_AD15" x="30.48" y="10.16" length="middle" rot="R180"/>
<pin name="GPMC_A2" x="30.48" y="12.7" length="middle" rot="R180"/>
<pin name="GPMC_A6" x="30.48" y="15.24" length="middle" rot="R180"/>
<pin name="GPMC_A9" x="30.48" y="17.78" length="middle" rot="R180"/>
<pin name="GPMC_WPN" x="30.48" y="20.32" length="middle" rot="R180"/>
<pin name="GPMC_BEN1" x="30.48" y="22.86" length="middle" rot="R180"/>
<pin name="XTALIN" x="30.48" y="25.4" length="middle" direction="in" rot="R180"/>
<pin name="VSS_OSC" x="30.48" y="27.94" length="middle" direction="pwr" rot="R180"/>
<pin name="GPMC_CLK" x="30.48" y="30.48" length="middle" rot="R180"/>
<pin name="GPMC_AD14" x="30.48" y="33.02" length="middle" rot="R180"/>
<pin name="GPMC_A1" x="30.48" y="35.56" length="middle" rot="R180"/>
<pin name="GPMC_A5" x="30.48" y="38.1" length="middle" rot="R180"/>
<pin name="GPMC_A8" x="30.48" y="40.64" length="middle" rot="R180"/>
<pin name="GPMC_A11" x="30.48" y="43.18" length="middle" rot="R180"/>
<pin name="VSS" x="30.48" y="45.72" length="middle" direction="pwr" rot="R180"/>
<wire x1="-22.86" y1="48.26" x2="-22.86" y2="-58.42" width="0.1524" layer="94"/>
<wire x1="-22.86" y1="-58.42" x2="25.4" y2="-58.42" width="0.1524" layer="94"/>
<wire x1="25.4" y1="-58.42" x2="25.4" y2="48.26" width="0.1524" layer="94"/>
<wire x1="25.4" y1="48.26" x2="-22.86" y2="48.26" width="0.1524" layer="94"/>
<text x="-19.9644" y="49.7586" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="4.4958" y="49.7586" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
<pin name="EHRPWM2B" x="30.48" y="-20.32" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AM3358_ZCZ_324" prefix="U">
<description>AM335_ZCZ Sitara 1.0 GHz, 32-bit MPU, 324-pin NFBGA</description>
<gates>
<gate name="A" symbol="AM3358_ZCZ_324_A" x="-73.66" y="63.5"/>
<gate name="B" symbol="AM3358_ZCZ_324_B" x="63.5" y="63.5"/>
<gate name="C" symbol="AM3358_ZCZ_324_C" x="-73.66" y="-58.42"/>
<gate name="D" symbol="AM3358_ZCZ_324_D" x="60.96" y="-55.88"/>
</gates>
<devices>
<device name="_BGA" package="ZCZ324">
<connects>
<connect gate="A" pin="AIN7" pad="C9"/>
<connect gate="A" pin="CAP_VDD_RTC" pad="D6"/>
<connect gate="A" pin="CAP_VDD_SRAM_CORE" pad="D9"/>
<connect gate="A" pin="DDR_A0" pad="F3"/>
<connect gate="A" pin="DDR_A1" pad="H1"/>
<connect gate="A" pin="DDR_A10" pad="F4"/>
<connect gate="A" pin="DDR_A11" pad="F2"/>
<connect gate="A" pin="DDR_A12" pad="E3"/>
<connect gate="A" pin="DDR_A13" pad="H3"/>
<connect gate="A" pin="DDR_A14" pad="H4"/>
<connect gate="A" pin="DDR_A15" pad="D3"/>
<connect gate="A" pin="DDR_A2" pad="E4"/>
<connect gate="A" pin="DDR_A3" pad="C3"/>
<connect gate="A" pin="DDR_A4" pad="C2"/>
<connect gate="A" pin="DDR_A5" pad="B1"/>
<connect gate="A" pin="DDR_A6" pad="D5"/>
<connect gate="A" pin="DDR_A7" pad="E2"/>
<connect gate="A" pin="DDR_A8" pad="D4"/>
<connect gate="A" pin="DDR_A9" pad="C1"/>
<connect gate="A" pin="DDR_BA0" pad="C4"/>
<connect gate="A" pin="DDR_BA1" pad="E1"/>
<connect gate="A" pin="DDR_BA2" pad="B3"/>
<connect gate="A" pin="DDR_CASN" pad="F1"/>
<connect gate="A" pin="DDR_CK" pad="D2"/>
<connect gate="A" pin="DDR_CKE" pad="G3"/>
<connect gate="A" pin="DDR_CKN" pad="D1"/>
<connect gate="A" pin="DDR_CSN0" pad="H2"/>
<connect gate="A" pin="DDR_D8" pad="J1"/>
<connect gate="A" pin="DDR_DQM1" pad="J2"/>
<connect gate="A" pin="DDR_ODT" pad="G1"/>
<connect gate="A" pin="DDR_RASN" pad="G4"/>
<connect gate="A" pin="DDR_RESETN" pad="G2"/>
<connect gate="A" pin="DDR_VREF" pad="J4"/>
<connect gate="A" pin="DDR_VTP" pad="J3"/>
<connect gate="A" pin="DDR_WEN" pad="B2"/>
<connect gate="A" pin="EXT_WAKEUP" pad="C5"/>
<connect gate="A" pin="PMIC_POWER_EN" pad="C6"/>
<connect gate="A" pin="RESERVED" pad="A3"/>
<connect gate="A" pin="RTC_KALDO_ENN" pad="B4"/>
<connect gate="A" pin="RTC_PWRONRSTN" pad="B5"/>
<connect gate="A" pin="RTC_XTALIN" pad="A6"/>
<connect gate="A" pin="RTC_XTALOUT" pad="A4"/>
<connect gate="A" pin="VDDA_ADC" pad="D8"/>
<connect gate="A" pin="VDDS" pad="F9"/>
<connect gate="A" pin="VDDS_2" pad="E6"/>
<connect gate="A" pin="VDDS_DDR" pad="J5"/>
<connect gate="A" pin="VDDS_DDR_2" pad="E5"/>
<connect gate="A" pin="VDDS_DDR_3" pad="F5"/>
<connect gate="A" pin="VDDS_DDR_4" pad="G5"/>
<connect gate="A" pin="VDDS_DDR_5" pad="H5"/>
<connect gate="A" pin="VDDS_PLL_DDR" pad="E7"/>
<connect gate="A" pin="VDDS_RTC" pad="D7"/>
<connect gate="A" pin="VDDS_SRAM_CORE_BG" pad="E9"/>
<connect gate="A" pin="VDD_CORE" pad="G7"/>
<connect gate="A" pin="VDD_CORE_2" pad="F6"/>
<connect gate="A" pin="VDD_CORE_3" pad="F7"/>
<connect gate="A" pin="VDD_CORE_4" pad="G6"/>
<connect gate="A" pin="VDD_MPU_MON" pad="A2"/>
<connect gate="A" pin="VREFN" pad="A9"/>
<connect gate="A" pin="VREFP" pad="B9"/>
<connect gate="A" pin="VSS" pad="J9"/>
<connect gate="A" pin="VSSA_ADC" pad="E8"/>
<connect gate="A" pin="VSS_10" pad="J6"/>
<connect gate="A" pin="VSS_11" pad="J7"/>
<connect gate="A" pin="VSS_12" pad="J8"/>
<connect gate="A" pin="VSS_2" pad="A1"/>
<connect gate="A" pin="VSS_3" pad="F8"/>
<connect gate="A" pin="VSS_4" pad="G8"/>
<connect gate="A" pin="VSS_5" pad="G9"/>
<connect gate="A" pin="VSS_6" pad="H6"/>
<connect gate="A" pin="VSS_7" pad="H7"/>
<connect gate="A" pin="VSS_8" pad="H8"/>
<connect gate="A" pin="VSS_9" pad="H9"/>
<connect gate="A" pin="VSS_RTC" pad="A5"/>
<connect gate="B" pin="DDR_D0" pad="M3"/>
<connect gate="B" pin="DDR_D1" pad="M4"/>
<connect gate="B" pin="DDR_D10" pad="K2"/>
<connect gate="B" pin="DDR_D11" pad="K3"/>
<connect gate="B" pin="DDR_D12" pad="K4"/>
<connect gate="B" pin="DDR_D13" pad="L3"/>
<connect gate="B" pin="DDR_D14" pad="L4"/>
<connect gate="B" pin="DDR_D15" pad="M1"/>
<connect gate="B" pin="DDR_D2" pad="N1"/>
<connect gate="B" pin="DDR_D3" pad="N2"/>
<connect gate="B" pin="DDR_D4" pad="N3"/>
<connect gate="B" pin="DDR_D5" pad="N4"/>
<connect gate="B" pin="DDR_D6" pad="P3"/>
<connect gate="B" pin="DDR_D7" pad="P4"/>
<connect gate="B" pin="DDR_D9" pad="K1"/>
<connect gate="B" pin="DDR_DQM0" pad="M2"/>
<connect gate="B" pin="DDR_DQS0" pad="P1"/>
<connect gate="B" pin="DDR_DQS1" pad="L1"/>
<connect gate="B" pin="DDR_DQSN0" pad="P2"/>
<connect gate="B" pin="DDR_DQSN1" pad="L2"/>
<connect gate="B" pin="GPMC_AD0" pad="U7"/>
<connect gate="B" pin="GPMC_AD1" pad="V7"/>
<connect gate="B" pin="GPMC_AD2" pad="R8"/>
<connect gate="B" pin="GPMC_AD3" pad="T8"/>
<connect gate="B" pin="GPMC_AD4" pad="U8"/>
<connect gate="B" pin="GPMC_AD5" pad="V8"/>
<connect gate="B" pin="GPMC_AD6" pad="R9"/>
<connect gate="B" pin="GPMC_AD7" pad="T9"/>
<connect gate="B" pin="GPMC_CSN1" pad="U9"/>
<connect gate="B" pin="GPMC_CSN2" pad="V9"/>
<connect gate="B" pin="LCD_DATA0" pad="R1"/>
<connect gate="B" pin="LCD_DATA1" pad="R2"/>
<connect gate="B" pin="LCD_DATA10" pad="U3"/>
<connect gate="B" pin="LCD_DATA11" pad="U4"/>
<connect gate="B" pin="LCD_DATA12" pad="V2"/>
<connect gate="B" pin="LCD_DATA13" pad="V3"/>
<connect gate="B" pin="LCD_DATA14" pad="V4"/>
<connect gate="B" pin="LCD_DATA15" pad="T5"/>
<connect gate="B" pin="LCD_DATA2" pad="R3"/>
<connect gate="B" pin="LCD_DATA3" pad="R4"/>
<connect gate="B" pin="LCD_DATA4" pad="T1"/>
<connect gate="B" pin="LCD_DATA5" pad="T2"/>
<connect gate="B" pin="LCD_DATA6" pad="T3"/>
<connect gate="B" pin="LCD_DATA7" pad="T4"/>
<connect gate="B" pin="LCD_DATA8" pad="U1"/>
<connect gate="B" pin="LCD_DATA9" pad="U2"/>
<connect gate="B" pin="VDDS" pad="P9"/>
<connect gate="B" pin="VDDSHV1" pad="P8"/>
<connect gate="B" pin="VDDSHV1_2" pad="P7"/>
<connect gate="B" pin="VDDSHV6" pad="P6"/>
<connect gate="B" pin="VDDSHV6_2" pad="N5"/>
<connect gate="B" pin="VDDSHV6_3" pad="P5"/>
<connect gate="B" pin="VDDS_2" pad="N6"/>
<connect gate="B" pin="VDDS_DDR" pad="L5"/>
<connect gate="B" pin="VDDS_DDR_2" pad="K5"/>
<connect gate="B" pin="VDD_CORE" pad="N9"/>
<connect gate="B" pin="VDD_CORE_2" pad="K6"/>
<connect gate="B" pin="VDD_CORE_3" pad="K8"/>
<connect gate="B" pin="VDD_CORE_4" pad="L6"/>
<connect gate="B" pin="VDD_CORE_5" pad="L7"/>
<connect gate="B" pin="VDD_CORE_6" pad="L8"/>
<connect gate="B" pin="VDD_CORE_7" pad="L9"/>
<connect gate="B" pin="VDD_CORE_8" pad="N8"/>
<connect gate="B" pin="VSS" pad="V1"/>
<connect gate="B" pin="VSS_2" pad="K7"/>
<connect gate="B" pin="VSS_3" pad="K9"/>
<connect gate="B" pin="VSS_4" pad="M6"/>
<connect gate="B" pin="VSS_5" pad="M7"/>
<connect gate="B" pin="VSS_6" pad="M8"/>
<connect gate="B" pin="VSS_7" pad="M9"/>
<connect gate="B" pin="VSS_8" pad="N7"/>
<connect gate="C" pin="CAP_VBB_MPU" pad="C10"/>
<connect gate="C" pin="CAP_VDD_SRAM_MPU" pad="D11"/>
<connect gate="C" pin="EXTINTN" pad="B18"/>
<connect gate="C" pin="I2C0_SCL" pad="C16"/>
<connect gate="C" pin="I2C0_SDA" pad="C17"/>
<connect gate="C" pin="MII1_COL" pad="H16"/>
<connect gate="C" pin="MII1_CRS" pad="H17"/>
<connect gate="C" pin="MII1_RX_DV" pad="J17"/>
<connect gate="C" pin="MII1_RX_ER" pad="J15"/>
<connect gate="C" pin="MII1_TXD3" pad="J18"/>
<connect gate="C" pin="MII1_TX_EN" pad="J16"/>
<connect gate="C" pin="MMC0_CLK" pad="G17"/>
<connect gate="C" pin="MMC0_CMD" pad="G18"/>
<connect gate="C" pin="MMC0_DAT0" pad="G16"/>
<connect gate="C" pin="MMC0_DAT1" pad="G15"/>
<connect gate="C" pin="MMC0_DAT2" pad="F18"/>
<connect gate="C" pin="MMC0_DAT3" pad="F17"/>
<connect gate="C" pin="PWRONRSTN" pad="B15"/>
<connect gate="C" pin="RMII1_REF_CLK" pad="H18"/>
<connect gate="C" pin="SPI0_CS1" pad="C15"/>
<connect gate="C" pin="UART0_RXD" pad="E15"/>
<connect gate="C" pin="UART0_TXD" pad="E16"/>
<connect gate="C" pin="UART1_RXD" pad="D16"/>
<connect gate="C" pin="UART1_TXD" pad="D15"/>
<connect gate="C" pin="USB1_DRVVBUS" pad="F15"/>
<connect gate="C" pin="VDDS" pad="E14"/>
<connect gate="C" pin="VDDSHV4" pad="J14"/>
<connect gate="C" pin="VDDSHV4_2" pad="H14"/>
<connect gate="C" pin="VDDSHV6" pad="G14"/>
<connect gate="C" pin="VDDSHV6_2" pad="E10"/>
<connect gate="C" pin="VDDSHV6_3" pad="E11"/>
<connect gate="C" pin="VDDSHV6_4" pad="E12"/>
<connect gate="C" pin="VDDSHV6_5" pad="E13"/>
<connect gate="C" pin="VDDSHV6_6" pad="F14"/>
<connect gate="C" pin="VDDS_PLL_MPU" pad="H15"/>
<connect gate="C" pin="VDDS_SRAM_MPU_BB" pad="D10"/>
<connect gate="C" pin="VDD_CORE" pad="J12"/>
<connect gate="C" pin="VDD_CORE_2" pad="G10"/>
<connect gate="C" pin="VDD_CORE_3" pad="H11"/>
<connect gate="C" pin="VDD_MPU" pad="J13"/>
<connect gate="C" pin="VDD_MPU_2" pad="F10"/>
<connect gate="C" pin="VDD_MPU_3" pad="F11"/>
<connect gate="C" pin="VDD_MPU_4" pad="F12"/>
<connect gate="C" pin="VDD_MPU_5" pad="F13"/>
<connect gate="C" pin="VDD_MPU_6" pad="G13"/>
<connect gate="C" pin="VDD_MPU_7" pad="H13"/>
<connect gate="C" pin="VSS" pad="J11"/>
<connect gate="C" pin="VSS_2" pad="A18"/>
<connect gate="C" pin="VSS_3" pad="G11"/>
<connect gate="C" pin="VSS_4" pad="G12"/>
<connect gate="C" pin="VSS_5" pad="H10"/>
<connect gate="C" pin="VSS_6" pad="H12"/>
<connect gate="C" pin="VSS_7" pad="J10"/>
<connect gate="C" pin="WARMRSTN" pad="A10"/>
<connect gate="D" pin="EHRPWM2B" pad="T10"/>
<connect gate="D" pin="GPMC_A0" pad="R13"/>
<connect gate="D" pin="GPMC_A1" pad="V14"/>
<connect gate="D" pin="GPMC_A10" pad="T16"/>
<connect gate="D" pin="GPMC_A11" pad="V17"/>
<connect gate="D" pin="GPMC_A2" pad="U14"/>
<connect gate="D" pin="GPMC_A3" pad="T14"/>
<connect gate="D" pin="GPMC_A4" pad="R14"/>
<connect gate="D" pin="GPMC_A5" pad="V15"/>
<connect gate="D" pin="GPMC_A6" pad="U15"/>
<connect gate="D" pin="GPMC_A7" pad="T15"/>
<connect gate="D" pin="GPMC_A8" pad="V16"/>
<connect gate="D" pin="GPMC_A9" pad="U16"/>
<connect gate="D" pin="GPMC_AD10" pad="T11"/>
<connect gate="D" pin="GPMC_AD11" pad="U12"/>
<connect gate="D" pin="GPMC_AD12" pad="T12"/>
<connect gate="D" pin="GPMC_AD13" pad="R12"/>
<connect gate="D" pin="GPMC_AD14" pad="V13"/>
<connect gate="D" pin="GPMC_AD15" pad="U13"/>
<connect gate="D" pin="GPMC_BEN1" pad="U18"/>
<connect gate="D" pin="GPMC_CLK" pad="V12"/>
<connect gate="D" pin="GPMC_WAIT0" pad="T17"/>
<connect gate="D" pin="GPMC_WPN" pad="U17"/>
<connect gate="D" pin="MDC" pad="M18"/>
<connect gate="D" pin="MDIO" pad="M17"/>
<connect gate="D" pin="MII1_RXD0" pad="M16"/>
<connect gate="D" pin="MII1_RXD1" pad="L15"/>
<connect gate="D" pin="MII1_RXD2" pad="L16"/>
<connect gate="D" pin="MII1_RXD3" pad="L17"/>
<connect gate="D" pin="MII1_RX_CLK" pad="L18"/>
<connect gate="D" pin="MII1_TXD0" pad="K17"/>
<connect gate="D" pin="MII1_TXD1" pad="K16"/>
<connect gate="D" pin="MII1_TXD2" pad="K15"/>
<connect gate="D" pin="MII1_TX_CLK" pad="K18"/>
<connect gate="D" pin="USB1_DM" pad="R18"/>
<connect gate="D" pin="USB1_DP" pad="R17"/>
<connect gate="D" pin="USB1_ID" pad="P17"/>
<connect gate="D" pin="USB1_VBUS" pad="T18"/>
<connect gate="D" pin="VDDA1P8V_USB0" pad="N16"/>
<connect gate="D" pin="VDDA1P8V_USB1" pad="R16"/>
<connect gate="D" pin="VDDA3P3V_USB0" pad="N15"/>
<connect gate="D" pin="VDDA3P3V_USB1" pad="R15"/>
<connect gate="D" pin="VDDS" pad="P14"/>
<connect gate="D" pin="VDDSHV2" pad="P11"/>
<connect gate="D" pin="VDDSHV2_2" pad="P10"/>
<connect gate="D" pin="VDDSHV3" pad="P13"/>
<connect gate="D" pin="VDDSHV3_2" pad="P12"/>
<connect gate="D" pin="VDDSHV5" pad="L14"/>
<connect gate="D" pin="VDDSHV5_2" pad="K14"/>
<connect gate="D" pin="VDDS_2" pad="K13"/>
<connect gate="D" pin="VDDS_OSC" pad="R11"/>
<connect gate="D" pin="VDDS_PLL_CORE_LCD" pad="R10"/>
<connect gate="D" pin="VDD_CORE" pad="N13"/>
<connect gate="D" pin="VDD_CORE_2" pad="K12"/>
<connect gate="D" pin="VDD_CORE_3" pad="M11"/>
<connect gate="D" pin="VDD_CORE_4" pad="M13"/>
<connect gate="D" pin="VDD_CORE_5" pad="N12"/>
<connect gate="D" pin="VSS" pad="V18"/>
<connect gate="D" pin="VSSA_USB" pad="N14"/>
<connect gate="D" pin="VSSA_USB_2" pad="M14"/>
<connect gate="D" pin="VSS_10" pad="N10"/>
<connect gate="D" pin="VSS_11" pad="N11"/>
<connect gate="D" pin="VSS_2" pad="K10"/>
<connect gate="D" pin="VSS_3" pad="K11"/>
<connect gate="D" pin="VSS_4" pad="L10"/>
<connect gate="D" pin="VSS_5" pad="L11"/>
<connect gate="D" pin="VSS_6" pad="L12"/>
<connect gate="D" pin="VSS_7" pad="L13"/>
<connect gate="D" pin="VSS_8" pad="M10"/>
<connect gate="D" pin="VSS_9" pad="M12"/>
<connect gate="D" pin="VSS_OSC" pad="V11"/>
<connect gate="D" pin="XTALIN" pad="V10"/>
<connect gate="D" pin="XTALOUT" pad="U11"/>
</connects>
<technologies>
<technology name="">
<attribute name="DATASHEETURL" value="http://www.ti.com/lit/gpn/AM3358" constant="no"/>
<attribute name="DESCRIPTION" value="ARM Cortex-A8 Microprocessor" constant="no"/>
<attribute name="FAMILY_NAME" value="AM33x ARM Cortex-A8" constant="no"/>
<attribute name="GENERIC_PART_NUMBER" value="AM3358" constant="no"/>
<attribute name="INDUSTRY_STD_PKG_TYPE" value="NFBGA" constant="no"/>
<attribute name="MANUFACTURER" value="Texas Instruments" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="am3358_zcz_324" constant="no"/>
<attribute name="PACKAGE_DESIGNATOR" value="ZCZ" constant="no"/>
<attribute name="PIN_COUNT" value="324" constant="no"/>
<attribute name="VENDOR" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U2" library="BIT_Library" deviceset="TPS65217CRSLR" device=""/>
<part name="U1" library="AM3358BZCZ100" deviceset="AM3358_ZCZ_324" device="_BGA"/>
<part name="CON1" library="BIT_Library" deviceset="PWR-JACK-CUI" device="_LRG"/>
<part name="F1" library="BIT_Library" deviceset="FUSE" device="_SMD1206" value="1206L075/13.2WR"/>
<part name="SUPPLY5" library="supply2" deviceset="GND" device=""/>
<part name="C5" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="C6" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="C7" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="C8" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="SUPPLY1" library="supply2" deviceset="GND" device=""/>
<part name="C9" library="BIT_Library" deviceset="CAP" device="_0805" value="2.2uF, 50V"/>
<part name="R3" library="BIT_Library" deviceset="RES" device="_0402" value="4.75K, 1%"/>
<part name="LD1" library="BIT_Library" deviceset="LED" device="_0603" value="PWR"/>
<part name="SUPPLY2" library="supply2" deviceset="GND" device=""/>
<part name="C10" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="R4" library="BIT_Library" deviceset="RES" device="_0402" value="0, 1%"/>
<part name="R5" library="BIT_Library" deviceset="RES" device="_0402" value="0, 1%"/>
<part name="R6" library="BIT_Library" deviceset="RES" device="_0402" value="0, 1%"/>
<part name="C11" library="BIT_Library" deviceset="CAP" device="_0805" value="2.2uF, 6.3V"/>
<part name="SUPPLY3" library="supply2" deviceset="GND" device=""/>
<part name="L1" library="BIT_Library" deviceset="INDUCTOR" device="_1008" value="2.2uH, 20%"/>
<part name="L2" library="BIT_Library" deviceset="INDUCTOR" device="_1008" value="2.2uH, 20%"/>
<part name="L3" library="BIT_Library" deviceset="INDUCTOR" device="_1008" value="2.2uH, 20%"/>
<part name="R7" library="BIT_Library" deviceset="RES" device="_0402" value="0, 1%"/>
<part name="C12" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="C13" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="C14" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="SUPPLY7" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY8" library="supply2" deviceset="GND" device=""/>
<part name="R8" library="BIT_Library" deviceset="RES" device="_0402" value="0, 1%"/>
<part name="C15" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="C16" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="H1" library="BIT_Library" deviceset="HDR_1X1" device="" value="PWR_RST"/>
<part name="C17" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="SUPPLY9" library="supply2" deviceset="GND" device=""/>
<part name="C18" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="SUPPLY10" library="supply2" deviceset="GND" device=""/>
<part name="SW1" library="BIT_Library" deviceset="SW_TACTILE" device="_KMR" value="PWR"/>
<part name="SUPPLY11" library="supply2" deviceset="GND" device=""/>
<part name="C19" library="BIT_Library" deviceset="CAP" device="_0805" value="0.1uF, 50V"/>
<part name="SUPPLY12" library="supply2" deviceset="GND" device=""/>
<part name="U8" library="BIT_Library" deviceset="TL5209DR" device=""/>
<part name="C20" library="BIT_Library" deviceset="CAP" device="_0805" value="2.2uF, 50V"/>
<part name="SUPPLY13" library="supply2" deviceset="GND" device=""/>
<part name="C21" library="BIT_Library" deviceset="CAP" device="_0805" value="470pF, 50V"/>
<part name="C22" library="BIT_Library" deviceset="CAP" device="_0805" value="0.1uF, 50V"/>
<part name="SUPPLY14" library="supply2" deviceset="GND" device=""/>
<part name="R9" library="BIT_Library" deviceset="RES" device="_0805" value="470K, 1%"/>
<part name="R10" library="BIT_Library" deviceset="RES" device="_0805" value="280K, 1%"/>
<part name="R11" library="BIT_Library" deviceset="RES" device="_0805" value="100K, 1%"/>
<part name="C23" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="C24" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="C25" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="C26" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C27" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C28" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C29" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C30" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C31" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C32" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C33" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C34" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C35" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C36" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C37" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C38" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C39" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="SUPPLY15" library="supply2" deviceset="GND" device=""/>
<part name="C40" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="C41" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="C42" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C43" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C44" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C45" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C46" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C47" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C48" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="SUPPLY16" library="supply2" deviceset="GND" device=""/>
<part name="C49" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="C50" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C51" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C52" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="SUPPLY18" library="supply2" deviceset="GND" device=""/>
<part name="FB1" library="BIT_Library" deviceset="INDUCTOR" device="_0805" value="150 Ohm, 800mA"/>
<part name="C53" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="SUPPLY19" library="supply2" deviceset="GND" device=""/>
<part name="C54" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C55" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C56" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="FB2" library="BIT_Library" deviceset="INDUCTOR" device="_0805" value="150 Ohm, 800mA"/>
<part name="C57" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="SUPPLY21" library="supply2" deviceset="GND" device=""/>
<part name="C58" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C59" library="BIT_Library" deviceset="CAP" device="_0402" value="1uF, 6.3V"/>
<part name="C60" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="SUPPLY22" library="supply2" deviceset="GND" device=""/>
<part name="C61" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C62" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C63" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C64" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C65" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C66" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C67" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C68" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C69" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C70" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C71" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C72" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C73" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C74" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C75" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C76" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C77" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C78" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C79" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C80" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C81" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="SUPPLY23" library="supply2" deviceset="GND" device=""/>
<part name="C82" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C83" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C84" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C85" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C86" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C87" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="SUPPLY20" library="supply2" deviceset="GND" device=""/>
<part name="C88" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C89" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C90" library="BIT_Library" deviceset="CAP" device="_0402" value="1nF, 16V"/>
<part name="R12" library="BIT_Library" deviceset="RES" device="_0402" value="0, 1%"/>
<part name="FB3" library="BIT_Library" deviceset="INDUCTOR" device="_0805" value="150 Ohm, 800mA"/>
<part name="SUPPLY24" library="supply2" deviceset="GND" device=""/>
<part name="C91" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C92" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C93" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="SUPPLY25" library="supply2" deviceset="GND" device=""/>
<part name="Y1" library="BIT_Library" deviceset="XTAL_OSC" device="_CM2X0C" value="32.768KHz"/>
<part name="C94" library="BIT_Library" deviceset="CAP" device="_0402" value="18pF, 50V"/>
<part name="C95" library="BIT_Library" deviceset="CAP" device="_0402" value="18pF, 50V"/>
<part name="Y2" library="BIT_Library" deviceset="XTAL_OSC" device="_7XC-7A" value="24MHz"/>
<part name="C96" library="BIT_Library" deviceset="CAP" device="_0402" value="18pF, 50V"/>
<part name="C97" library="BIT_Library" deviceset="CAP" device="_0402" value="18pF, 50V"/>
<part name="R15" library="BIT_Library" deviceset="RES" device="_0402" value="1M, 1%"/>
<part name="U9" library="BIT_Library" deviceset="SN74LVC1G0X" device="_DCK"/>
<part name="U10" library="BIT_Library" deviceset="SN74LVC1G0X" device="_DCK"/>
<part name="U11" library="BIT_Library" deviceset="SN74LVC2G241" device="_DCU"/>
<part name="SW2" library="BIT_Library" deviceset="SW_TACTILE" device="_1825" value="RST"/>
<part name="C98" library="BIT_Library" deviceset="CAP" device="_0805" value="2.2uF, 50V"/>
<part name="SUPPLY27" library="supply2" deviceset="GND" device=""/>
<part name="R16" library="BIT_Library" deviceset="RES" device="_0805" value="10K, 5%"/>
<part name="SUPPLY28" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY29" library="supply2" deviceset="GND" device=""/>
<part name="C99" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="R17" library="BIT_Library" deviceset="RES" device="_0402" value="0, 1%"/>
<part name="SUPPLY30" library="supply2" deviceset="GND" device=""/>
<part name="R18" library="BIT_Library" deviceset="RES" device="_0402" value="4.75K, 1%"/>
<part name="C100" library="BIT_Library" deviceset="CAP" device="_0805" value="0.1uF, 50V"/>
<part name="SUPPLY31" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY32" library="supply2" deviceset="GND" device=""/>
<part name="R19" library="BIT_Library" deviceset="RES" device="_0805" value="100K, 1%"/>
<part name="R21" library="BIT_Library" deviceset="RES" device="_0402" value="4.75K, 1%"/>
<part name="R22" library="BIT_Library" deviceset="RES" device="_0402" value="4.75K, 1%"/>
<part name="SUPPLY33" library="supply2" deviceset="GND" device=""/>
<part name="R23" library="BIT_Library" deviceset="RES" device="_0402" value="1.5K, 1%"/>
<part name="R24" library="BIT_Library" deviceset="RES" device="_0402" value="1.5K, 1%"/>
<part name="R26" library="BIT_Library" deviceset="RES" device="_0402" value="0, 1%"/>
<part name="SUPPLY34" library="supply2" deviceset="GND" device=""/>
<part name="H3" library="BIT_Library" deviceset="HDR_1X1" device="" value="VDD_MPU_TP"/>
<part name="C101" library="BIT_Library" deviceset="CAP" device="_0402" value="1uF, 6.3V"/>
<part name="SUPPLY35" library="supply2" deviceset="GND" device=""/>
<part name="C102" library="BIT_Library" deviceset="CAP" device="_0402" value="1uF, 6.3V"/>
<part name="SUPPLY36" library="supply2" deviceset="GND" device=""/>
<part name="C103" library="BIT_Library" deviceset="CAP" device="_0402" value="1uF, 6.3V"/>
<part name="SUPPLY37" library="supply2" deviceset="GND" device=""/>
<part name="R27" library="BIT_Library" deviceset="RES" device="_0402" value="4.75K, 1%"/>
<part name="SUPPLY38" library="supply2" deviceset="GND" device=""/>
<part name="R28" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="R29" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="R30" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="R31" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="R32" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="R33" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="R34" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="R35" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="R36" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="R37" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="R38" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="R39" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="R40" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="R41" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="R42" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="R43" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="SUPPLY39" library="supply2" deviceset="GND" device=""/>
<part name="R44" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="SW3" library="BIT_Library" deviceset="SW_TACTILE" device="_KMR" value="BOOT"/>
<part name="SUPPLY40" library="supply2" deviceset="GND" device=""/>
<part name="U6" library="BIT_Library" deviceset="24LC32AT" device=""/>
<part name="R45" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="C104" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="SUPPLY41" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY42" library="supply2" deviceset="GND" device=""/>
<part name="C105" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="SUPPLY43" library="supply2" deviceset="GND" device=""/>
<part name="C106" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C107" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C108" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C109" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="SUPPLY44" library="supply2" deviceset="GND" device=""/>
<part name="C110" library="BIT_Library" deviceset="CAP" device="_0402" value="2.2uF, 6.3V"/>
<part name="SUPPLY45" library="supply2" deviceset="GND" device=""/>
<part name="U4" library="BIT_Library" deviceset="EMMC" device="" value="SDIN5D2-4G-L"/>
<part name="R46" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R47" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R48" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R49" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R50" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R51" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R52" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R53" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R54" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R55" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R56" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="C111" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="C112" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C113" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C114" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C115" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C116" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C117" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C118" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C119" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C120" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C121" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C122" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C123" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C124" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C125" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C126" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="SUPPLY46" library="supply2" deviceset="GND" device=""/>
<part name="R57" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="SUPPLY47" library="supply2" deviceset="GND" device=""/>
<part name="U3" library="BIT_Library" deviceset="DDR3L" device="" value="AS4C256M16D3L"/>
<part name="SUPPLY48" library="supply2" deviceset="GND" device=""/>
<part name="C127" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="R58" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R59" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="C128" library="BIT_Library" deviceset="CAP" device="_0402" value="1nF, 16V"/>
<part name="SUPPLY49" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY50" library="supply2" deviceset="GND" device=""/>
<part name="CON3" library="BIT_Library" deviceset="MICRO-SD" device=""/>
<part name="R60" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R61" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R62" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R63" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R64" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R65" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R66" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="SUPPLY51" library="supply2" deviceset="GND" device=""/>
<part name="C129" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="C130" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="LD2" library="BIT_Library" deviceset="LED" device="_0603" value="USR1"/>
<part name="R67" library="BIT_Library" deviceset="RES" device="_0402" value="4.7K, 1%"/>
<part name="SUPPLY55" library="supply2" deviceset="GND" device=""/>
<part name="U7" library="BIT_Library" deviceset="TPS2051B" device="_DGN"/>
<part name="CON5" library="BIT_Library" deviceset="USB" device=""/>
<part name="U13" library="BIT_Library" deviceset="TPD4S012" device=""/>
<part name="FB4" library="BIT_Library" deviceset="INDUCTOR" device="_0805" value="150 Ohm, 800mA"/>
<part name="FB5" library="BIT_Library" deviceset="INDUCTOR" device="_0805" value="150 Ohm, 800mA"/>
<part name="SUPPLY59" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY60" library="supply2" deviceset="GND" device=""/>
<part name="C132" library="BIT_Library" deviceset="CAP_POL" device="_5.3" value="100uF, 6.3V"/>
<part name="R75" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R76" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="U5" library="BIT_Library" deviceset="LAN8710A" device=""/>
<part name="CON2" library="BIT_Library" deviceset="MAGJACK" device="_08B0" value="ETH1"/>
<part name="SUPPLY61" library="supply2" deviceset="GND" device=""/>
<part name="C133" library="BIT_Library" deviceset="CAP" device="_0805" value=".022uF, 50V"/>
<part name="R77" library="BIT_Library" deviceset="RES" device="_0402" value="0, 1%"/>
<part name="R78" library="BIT_Library" deviceset="RES" device="_0402" value="49.9, 1%"/>
<part name="R79" library="BIT_Library" deviceset="RES" device="_0402" value="49.9, 1%"/>
<part name="R80" library="BIT_Library" deviceset="RES" device="_0402" value="49.9, 1%"/>
<part name="R81" library="BIT_Library" deviceset="RES" device="_0402" value="49.9, 1%"/>
<part name="SUPPLY62" library="supply2" deviceset="GND" device=""/>
<part name="R82" library="BIT_Library" deviceset="RES" device="_0805" value="10K, 1%"/>
<part name="R83" library="BIT_Library" deviceset="RES" device="_0402" value="470, 1%"/>
<part name="R84" library="BIT_Library" deviceset="RES" device="_0402" value="470, 1%"/>
<part name="R85" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R86" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R87" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R88" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R89" library="BIT_Library" deviceset="RES" device="_0402" value="1.5K, 1%"/>
<part name="R90" library="BIT_Library" deviceset="RES" device="_0402" value="1.5K, 1%"/>
<part name="R91" library="BIT_Library" deviceset="RES" device="_0402" value="1.5K, 1%"/>
<part name="SUPPLY63" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY64" library="supply2" deviceset="GND" device=""/>
<part name="R92" library="BIT_Library" deviceset="RES" device="_0402" value="12.1K, 1%"/>
<part name="C134" library="BIT_Library" deviceset="CAP" device="_0402" value="470pF, 50V"/>
<part name="C135" library="BIT_Library" deviceset="CAP" device="_0402" value="1uF, 6.3V"/>
<part name="SUPPLY65" library="supply2" deviceset="GND" device=""/>
<part name="C136" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="C137" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C138" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="SUPPLY66" library="supply2" deviceset="GND" device=""/>
<part name="FB6" library="BIT_Library" deviceset="INDUCTOR" device="_0805" value="150 Ohm, 800mA"/>
<part name="C139" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="SUPPLY67" library="supply2" deviceset="GND" device=""/>
<part name="R93" library="BIT_Library" deviceset="RES" device="_0402" value="1.5K, 1%"/>
<part name="R94" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R95" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R96" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R97" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R98" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R99" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R100" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R101" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R102" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R103" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R104" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R105" library="BIT_Library" deviceset="RES" device="_0402" value="0, 1%"/>
<part name="R107" library="BIT_Library" deviceset="RES" device="_0402" value="10, 1%"/>
<part name="C140" library="BIT_Library" deviceset="CAP" device="_0402" value="30pF, 50V"/>
<part name="C141" library="BIT_Library" deviceset="CAP" device="_0402" value="30pF, 50V"/>
<part name="SUPPLY68" library="supply2" deviceset="GND" device=""/>
<part name="Y3" library="BIT_Library" deviceset="XTAL_OSC" device="_7XC-7A" value="25MHz"/>
<part name="R106" library="BIT_Library" deviceset="RES" device="_0402" value="49.9, 1%"/>
<part name="SUPPLY69" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY70" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY71" library="supply2" deviceset="GND" device=""/>
<part name="R13" library="BIT_Library" deviceset="RES" device="_0402" value="33, 1%"/>
<part name="R14" library="BIT_Library" deviceset="RES" device="_0402" value="10, 1%, DNI"/>
<part name="R20" library="BIT_Library" deviceset="RES" device="_0402" value="0, 1%, DNI"/>
<part name="U15" library="BIT_Library" deviceset="TLV70233-3.3" device=""/>
<part name="C1" library="BIT_Library" deviceset="CAP" device="_0805" value="0.1uF, 50V"/>
<part name="SUPPLY4" library="supply2" deviceset="GND" device=""/>
<part name="C2" library="BIT_Library" deviceset="CAP" device="_0805" value="1uF, 50V"/>
<part name="SUPPLY6" library="supply2" deviceset="GND" device=""/>
<part name="D1" library="BIT_Library" deviceset="DIODE" device="_SMD123" value="SBR3U60P1"/>
<part name="FAN" library="BIT_Library" deviceset="FAN" device=""/>
<part name="R1" library="BIT_Library" deviceset="RES" device="_2512" value="43P, 1%"/>
<part name="R2" library="BIT_Library" deviceset="RES" device="_2512" value="43P, 1%"/>
<part name="Q1" library="BIT_Library" deviceset="MOSFET_N-SD" device="_SOT233"/>
<part name="SUPPLY17" library="supply2" deviceset="GND" device=""/>
<part name="TP1" library="BIT_Library" deviceset="TESTPOINT" device="" value="PWM"/>
<part name="R69" library="BIT_Library" deviceset="RES" device="_0805" value="100K, 1%"/>
<part name="SUPPLY26" library="supply2" deviceset="GND" device=""/>
<part name="U14" library="BIT_Library" deviceset="TLV70233-3.3" device=""/>
<part name="C3" library="BIT_Library" deviceset="CAP" device="_0805" value="0.1uF, 50V"/>
<part name="C4" library="BIT_Library" deviceset="CAP" device="_0805" value="1uF, 50V"/>
<part name="SUPPLY54" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY56" library="supply2" deviceset="GND" device=""/>
<part name="R111" library="BIT_Library" deviceset="RES" device="_0805" value="300, 5%"/>
<part name="R112" library="BIT_Library" deviceset="RES" device="_0805" value="300, 5%"/>
<part name="R114" library="BIT_Library" deviceset="RES" device="_0805" value="300, 5%"/>
<part name="Q2" library="BIT_Library" deviceset="BJT_NPN" device="_SOT23-3" value="MMBT2222A"/>
<part name="R68" library="BIT_Library" deviceset="RES" device="_0805" value="10K, 1%"/>
<part name="Q3" library="BIT_Library" deviceset="BJT_NPN" device="_SOT23-3" value="MMBT2222A"/>
<part name="Q4" library="BIT_Library" deviceset="BJT_NPN" device="_SOT23-3" value="MMBT2222A"/>
<part name="Q5" library="BIT_Library" deviceset="BJT_NPN" device="_SOT23-3" value="MMBT2222A"/>
<part name="Q6" library="BIT_Library" deviceset="BJT_NPN" device="_SOT23-3" value="MMBT2222A"/>
<part name="R70" library="BIT_Library" deviceset="RES" device="_0805" value="10K, 1%"/>
<part name="R71" library="BIT_Library" deviceset="RES" device="_0805" value="10K, 1%"/>
<part name="R72" library="BIT_Library" deviceset="RES" device="_0805" value="10K, 1%"/>
<part name="R73" library="BIT_Library" deviceset="RES" device="_0805" value="10K, 1%"/>
<part name="U$1" library="BIT_Library" deviceset="FTDI" device=""/>
<part name="U18" library="BIT_Library" deviceset="RF" device="_TRANS"/>
<part name="SUPPLY81" library="supply2" deviceset="GND" device=""/>
<part name="U12" library="BIT_Library" deviceset="SN74LVC2G241" device="_DCU"/>
<part name="SUPPLY82" library="supply2" deviceset="GND" device=""/>
<part name="C164" library="BIT_Library" deviceset="CAP" device="_0805" value="0.1uF, 50V"/>
<part name="SUPPLY83" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY84" library="supply2" deviceset="GND" device=""/>
<part name="U16" library="BIT_Library" deviceset="LAN8710A" device=""/>
<part name="SUPPLY53" library="supply2" deviceset="GND" device=""/>
<part name="CON4" library="BIT_Library" deviceset="MAGJACK" device="_08B0" value="ETH2"/>
<part name="R25" library="BIT_Library" deviceset="RES" device="_0402" value="1.5K, 1%"/>
<part name="R119" library="BIT_Library" deviceset="RES" device="_0402" value="1.5K, 1%"/>
<part name="R120" library="BIT_Library" deviceset="RES" device="_0402" value="1.5K, 1%"/>
<part name="FB7" library="BIT_Library" deviceset="INDUCTOR" device="_0805" value="150 Ohm, 800mA"/>
<part name="C131" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="SUPPLY57" library="supply2" deviceset="GND" device=""/>
<part name="R121" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R122" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R123" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R124" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="R125" library="BIT_Library" deviceset="RES" device="_0402" value="10K, 5%"/>
<part name="SUPPLY58" library="supply2" deviceset="GND" device=""/>
<part name="R126" library="BIT_Library" deviceset="RES" device="_0402" value="49.9, 1%"/>
<part name="R127" library="BIT_Library" deviceset="RES" device="_0402" value="49.9, 1%"/>
<part name="R128" library="BIT_Library" deviceset="RES" device="_0402" value="49.9, 1%"/>
<part name="R129" library="BIT_Library" deviceset="RES" device="_0402" value="49.9, 1%"/>
<part name="R130" library="BIT_Library" deviceset="RES" device="_0402" value="0, 1%"/>
<part name="C142" library="BIT_Library" deviceset="CAP" device="_0805" value=".022uF, 50V"/>
<part name="SUPPLY72" library="supply2" deviceset="GND" device=""/>
<part name="R131" library="BIT_Library" deviceset="RES" device="_0805" value="10K, 1%"/>
<part name="R132" library="BIT_Library" deviceset="RES" device="_0402" value="470, 1%"/>
<part name="R133" library="BIT_Library" deviceset="RES" device="_0402" value="470, 1%"/>
<part name="SUPPLY73" library="supply2" deviceset="GND" device=""/>
<part name="R134" library="BIT_Library" deviceset="RES" device="_0402" value="12.1K, 1%"/>
<part name="SUPPLY74" library="supply2" deviceset="GND" device=""/>
<part name="C143" library="BIT_Library" deviceset="CAP" device="_0402" value="470pF, 50V"/>
<part name="C144" library="BIT_Library" deviceset="CAP" device="_0402" value="1uF, 6.3V"/>
<part name="SUPPLY75" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY76" library="supply2" deviceset="GND" device=""/>
<part name="C145" library="BIT_Library" deviceset="CAP" device="_0805" value="10uF, 10V"/>
<part name="C146" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="C147" library="BIT_Library" deviceset="CAP" device="_0402" value="0.1uF, 6.3V"/>
<part name="SUPPLY77" library="supply2" deviceset="GND" device=""/>
<part name="R135" library="BIT_Library" deviceset="RES" device="_0402" value="10, 1%, DNI"/>
<part name="R136" library="BIT_Library" deviceset="RES" device="_0402" value="0, 1%, DNI"/>
<part name="R137" library="BIT_Library" deviceset="RES" device="_0402" value="33, 1%"/>
<part name="R138" library="BIT_Library" deviceset="RES" device="_0402" value="0, 1%, DNI"/>
<part name="R139" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R140" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R141" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R142" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R143" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R144" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R145" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R146" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R147" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R148" library="BIT_Library" deviceset="RES" device="_0402" value="100, 5%"/>
<part name="R149" library="BIT_Library" deviceset="RES" device="_0402" value="0, 1%"/>
<part name="R150" library="BIT_Library" deviceset="RES" device="_0402" value="10, 1%"/>
<part name="Y4" library="BIT_Library" deviceset="XTAL_OSC" device="_7XC-7A" value="25MHz"/>
<part name="C148" library="BIT_Library" deviceset="CAP" device="_0402" value="30pF, 50V"/>
<part name="C149" library="BIT_Library" deviceset="CAP" device="_0402" value="30pF, 50V"/>
<part name="SUPPLY78" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY79" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY80" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY52" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY85" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY86" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY87" library="supply2" deviceset="GND" device=""/>
<part name="LD3" library="BIT_Library" deviceset="LED" device="_1206P" value="SYS"/>
<part name="LD4" library="BIT_Library" deviceset="LED" device="_1206P" value="ETH"/>
<part name="LD5" library="BIT_Library" deviceset="LED" device="_1206P" value="CAM"/>
<part name="LD6" library="BIT_Library" deviceset="LED" device="_1206P" value="VID"/>
<part name="R74" library="BIT_Library" deviceset="RES" device="_0805" value="300, 5%"/>
<part name="R108" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="R109" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="R110" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="SUPPLY89" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY90" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY91" library="supply2" deviceset="GND" device=""/>
<part name="R113" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="SUPPLY88" library="supply2" deviceset="GND" device=""/>
<part name="R115" library="BIT_Library" deviceset="RES" device="_0402" value="100K, 1%"/>
<part name="SUPPLY92" library="supply2" deviceset="GND" device=""/>
<part name="R116" library="BIT_Library" deviceset="RES" device="_0402" value="1.5K, 1%"/>
<part name="HS1" library="BIT_Library" deviceset="HEATSINK" device="_10MM"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="5.334" y="2.032" size="10.16" layer="97">POWER SYSTEM</text>
<text x="35.56" y="223.52" size="3.048" layer="97">INPUT POWER CIRCUIT</text>
<text x="157.48" y="170.18" size="3.048" layer="97">POWER CONTROLLER CIRCUIT</text>
<text x="172.72" y="228.6" size="3.048" layer="97">SUBSYSTEM REGULATOR CIRCUIT</text>
<text x="5.588" y="107.188" size="2.54" layer="97" rot="MR180">UNIT LIST:

U1: AM3358 MPU
U2: Power Controller
U3: DDR3 Memory
U4: eMMC Memory
U5: PHY - Ethernet 1
U6: EEPROM (Board ID)
U7: USB Power Dist.
U8: Subsystem Reg.
U9: Buffer1 (G07)
U10: Buffer2, Inv (G06)
U11: UART0 Buffer (Dual)
U12: UART1 Buffer (Dual)
U13: USB ESD Prot
U14: Regulator - Ethernet 2
U15: Regulator - Peripheral Hardware
U16: PHY - Ethernet 2</text>
<text x="30.48" y="182.88" size="3.048" layer="97">3V3 REGULATOR - ETH2</text>
<text x="27.94" y="144.78" size="3.048" layer="97">3V3 REGULATOR - PERIPHERAL</text>
<text x="5.588" y="17.272" size="2.54" layer="97">Linux Micro-PC
Rev 1.0
SBS 2015</text>
</plain>
<instances>
<instance part="U2" gate="G$1" x="198.12" y="86.36"/>
<instance part="CON1" gate="DC_JACK" x="48.26" y="205.74"/>
<instance part="F1" gate="FUSE" x="76.2" y="208.28"/>
<instance part="SUPPLY5" gate="GND" x="63.5" y="200.66"/>
<instance part="C5" gate="G$1" x="187.96" y="160.02"/>
<instance part="C6" gate="G$1" x="200.66" y="160.02"/>
<instance part="C7" gate="G$1" x="213.36" y="160.02"/>
<instance part="C8" gate="G$1" x="226.06" y="160.02"/>
<instance part="SUPPLY1" gate="GND" x="187.96" y="152.4"/>
<instance part="C9" gate="G$1" x="157.48" y="160.02"/>
<instance part="R3" gate="G$1" x="142.24" y="157.48" rot="R90"/>
<instance part="LD1" gate="G$1" x="149.86" y="165.1" rot="R270"/>
<instance part="SUPPLY2" gate="GND" x="142.24" y="147.32"/>
<instance part="C10" gate="G$1" x="238.76" y="160.02"/>
<instance part="R4" gate="G$1" x="121.92" y="109.22" rot="R180"/>
<instance part="R5" gate="G$1" x="121.92" y="101.6" rot="R180"/>
<instance part="R6" gate="G$1" x="121.92" y="93.98" rot="R180"/>
<instance part="C11" gate="G$1" x="114.3" y="88.9"/>
<instance part="SUPPLY3" gate="GND" x="114.3" y="81.28"/>
<instance part="L1" gate="G$1" x="119.38" y="66.04"/>
<instance part="L2" gate="G$1" x="119.38" y="58.42"/>
<instance part="L3" gate="G$1" x="119.38" y="50.8"/>
<instance part="R7" gate="G$1" x="96.52" y="66.04" rot="R180"/>
<instance part="C12" gate="G$1" x="83.82" y="60.96"/>
<instance part="C13" gate="G$1" x="93.98" y="45.72"/>
<instance part="C14" gate="G$1" x="109.22" y="38.1"/>
<instance part="SUPPLY7" gate="GND" x="93.98" y="30.48"/>
<instance part="SUPPLY8" gate="GND" x="231.14" y="88.9"/>
<instance part="R8" gate="G$1" x="243.84" y="119.38" rot="R270"/>
<instance part="C15" gate="G$1" x="243.84" y="106.68"/>
<instance part="C16" gate="G$1" x="231.14" y="99.06"/>
<instance part="H1" gate="PIN" x="165.1" y="58.42"/>
<instance part="C17" gate="G$1" x="172.72" y="27.94"/>
<instance part="SUPPLY9" gate="GND" x="172.72" y="20.32"/>
<instance part="C18" gate="G$1" x="162.56" y="35.56"/>
<instance part="SUPPLY10" gate="GND" x="162.56" y="27.94"/>
<instance part="SW1" gate="G$1" x="119.38" y="129.54"/>
<instance part="SUPPLY11" gate="GND" x="114.3" y="127"/>
<instance part="C19" gate="G$1" x="226.06" y="40.64"/>
<instance part="SUPPLY12" gate="GND" x="226.06" y="33.02"/>
<instance part="U8" gate="G$1" x="172.72" y="210.82"/>
<instance part="C20" gate="G$1" x="144.78" y="208.28"/>
<instance part="SUPPLY13" gate="GND" x="144.78" y="200.66"/>
<instance part="C21" gate="G$1" x="203.2" y="205.74"/>
<instance part="C22" gate="G$1" x="233.68" y="205.74"/>
<instance part="SUPPLY14" gate="GND" x="193.04" y="190.5"/>
<instance part="R9" gate="G$1" x="218.44" y="203.2" rot="R90"/>
<instance part="R10" gate="G$1" x="226.06" y="210.82" rot="R180"/>
<instance part="R11" gate="G$1" x="231.14" y="55.88" rot="R90"/>
<instance part="U15" gate="TLV72033" x="53.34" y="170.18"/>
<instance part="C1" gate="G$1" x="25.4" y="170.18"/>
<instance part="SUPPLY4" gate="GND" x="38.1" y="162.56"/>
<instance part="C2" gate="G$1" x="76.2" y="170.18"/>
<instance part="SUPPLY6" gate="GND" x="76.2" y="162.56"/>
<instance part="U14" gate="TLV72033" x="50.8" y="129.54"/>
<instance part="C3" gate="G$1" x="25.4" y="129.54"/>
<instance part="C4" gate="G$1" x="73.66" y="129.54"/>
<instance part="SUPPLY54" gate="GND" x="25.4" y="119.38"/>
<instance part="SUPPLY56" gate="GND" x="73.66" y="121.92"/>
<instance part="SUPPLY53" gate="GND" x="25.4" y="162.56"/>
<instance part="HS1" gate="G$1" x="88.9" y="213.36"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="CON1" gate="DC_JACK" pin="GND@0"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<wire x1="53.34" y1="205.74" x2="63.5" y2="205.74" width="0.1524" layer="91"/>
<wire x1="63.5" y1="205.74" x2="63.5" y2="203.2" width="0.1524" layer="91"/>
<pinref part="CON1" gate="DC_JACK" pin="GND@1"/>
<wire x1="53.34" y1="203.2" x2="63.5" y2="203.2" width="0.1524" layer="91"/>
<junction x="63.5" y="203.2"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="226.06" y1="154.94" x2="213.36" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="213.36" y1="154.94" x2="200.66" y2="154.94" width="0.1524" layer="91"/>
<junction x="213.36" y="154.94"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="200.66" y1="154.94" x2="187.96" y2="154.94" width="0.1524" layer="91"/>
<junction x="200.66" y="154.94"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<junction x="187.96" y="154.94"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="226.06" y1="154.94" x2="238.76" y2="154.94" width="0.1524" layer="91"/>
<junction x="226.06" y="154.94"/>
</segment>
<segment>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="142.24" y1="149.86" x2="142.24" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="142.24" y1="152.4" x2="157.48" y2="152.4" width="0.1524" layer="91"/>
<wire x1="157.48" y1="152.4" x2="157.48" y2="154.94" width="0.1524" layer="91"/>
<junction x="142.24" y="152.4"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="83.82" y1="55.88" x2="83.82" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="83.82" y1="33.02" x2="93.98" y2="33.02" width="0.1524" layer="91"/>
<wire x1="93.98" y1="33.02" x2="109.22" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="93.98" y1="40.64" x2="93.98" y2="33.02" width="0.1524" layer="91"/>
<junction x="93.98" y="33.02"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="231.14" y1="91.44" x2="243.84" y2="91.44" width="0.1524" layer="91"/>
<wire x1="243.84" y1="91.44" x2="243.84" y2="101.6" width="0.1524" layer="91"/>
<junction x="231.14" y="91.44"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<wire x1="231.14" y1="91.44" x2="231.14" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="AGND"/>
<wire x1="220.98" y1="93.98" x2="223.52" y2="93.98" width="0.1524" layer="91"/>
<wire x1="223.52" y1="96.52" x2="223.52" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="PGND"/>
<wire x1="220.98" y1="96.52" x2="223.52" y2="96.52" width="0.1524" layer="91"/>
<junction x="223.52" y="96.52"/>
<pinref part="U2" gate="G$1" pin="POWERPAD"/>
<wire x1="220.98" y1="99.06" x2="223.52" y2="99.06" width="0.1524" layer="91"/>
<wire x1="223.52" y1="99.06" x2="223.52" y2="96.52" width="0.1524" layer="91"/>
<wire x1="223.52" y1="93.98" x2="223.52" y2="91.44" width="0.1524" layer="91"/>
<junction x="223.52" y="93.98"/>
<wire x1="223.52" y1="91.44" x2="231.14" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SW1" gate="G$1" pin="1"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="GND"/>
<wire x1="190.5" y1="205.74" x2="193.04" y2="205.74" width="0.1524" layer="91"/>
<wire x1="193.04" y1="205.74" x2="193.04" y2="195.58" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="193.04" y1="195.58" x2="203.2" y2="195.58" width="0.1524" layer="91"/>
<wire x1="203.2" y1="195.58" x2="203.2" y2="200.66" width="0.1524" layer="91"/>
<wire x1="193.04" y1="195.58" x2="193.04" y2="193.04" width="0.1524" layer="91"/>
<junction x="193.04" y="195.58"/>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="203.2" y1="195.58" x2="218.44" y2="195.58" width="0.1524" layer="91"/>
<wire x1="218.44" y1="195.58" x2="233.68" y2="195.58" width="0.1524" layer="91"/>
<wire x1="233.68" y1="195.58" x2="233.68" y2="200.66" width="0.1524" layer="91"/>
<junction x="203.2" y="195.58"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="218.44" y1="195.58" x2="218.44" y2="198.12" width="0.1524" layer="91"/>
<junction x="218.44" y="195.58"/>
</segment>
<segment>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<pinref part="U15" gate="TLV72033" pin="GND"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="SUPPLY56" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY54" gate="GND" pin="GND"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="25.4" y1="121.92" x2="25.4" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U14" gate="TLV72033" pin="GND"/>
<wire x1="35.56" y1="124.46" x2="35.56" y2="121.92" width="0.1524" layer="91"/>
<wire x1="35.56" y1="121.92" x2="25.4" y2="121.92" width="0.1524" layer="91"/>
<junction x="25.4" y="121.92"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="SUPPLY53" gate="GND" pin="GND"/>
</segment>
</net>
<net name="PWR_IN" class="0">
<segment>
<pinref part="F1" gate="FUSE" pin="1"/>
<label x="58.42" y="208.28" size="1.778" layer="95"/>
<pinref part="CON1" gate="DC_JACK" pin="POS"/>
<wire x1="53.34" y1="208.28" x2="71.12" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="AC"/>
<wire x1="175.26" y1="129.54" x2="170.18" y2="129.54" width="0.1524" layer="91"/>
<label x="170.18" y="129.54" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="F1" gate="FUSE" pin="2"/>
<wire x1="83.82" y1="208.28" x2="88.9" y2="208.28" width="0.1524" layer="91"/>
<label x="93.98" y="208.28" size="1.778" layer="95"/>
<pinref part="HS1" gate="G$1" pin="HS"/>
<junction x="88.9" y="208.28"/>
<wire x1="88.9" y1="208.28" x2="93.98" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SYS_5V" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SYS"/>
<wire x1="175.26" y1="134.62" x2="170.18" y2="134.62" width="0.1524" layer="91"/>
<label x="170.18" y="134.62" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="187.96" y1="162.56" x2="187.96" y2="165.1" width="0.1524" layer="91"/>
<wire x1="187.96" y1="165.1" x2="200.66" y2="165.1" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="200.66" y1="165.1" x2="213.36" y2="165.1" width="0.1524" layer="91"/>
<wire x1="213.36" y1="165.1" x2="226.06" y2="165.1" width="0.1524" layer="91"/>
<wire x1="200.66" y1="162.56" x2="200.66" y2="165.1" width="0.1524" layer="91"/>
<junction x="200.66" y="165.1"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="213.36" y1="162.56" x2="213.36" y2="165.1" width="0.1524" layer="91"/>
<junction x="213.36" y="165.1"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="226.06" y1="162.56" x2="226.06" y2="165.1" width="0.1524" layer="91"/>
<label x="187.96" y="165.1" size="1.778" layer="95"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="238.76" y1="162.56" x2="238.76" y2="165.1" width="0.1524" layer="91"/>
<wire x1="238.76" y1="165.1" x2="226.06" y2="165.1" width="0.1524" layer="91"/>
<junction x="226.06" y="165.1"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VINLDO"/>
<wire x1="175.26" y1="119.38" x2="170.18" y2="119.38" width="0.1524" layer="91"/>
<label x="170.18" y="119.38" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VIN_DCDC2"/>
<wire x1="172.72" y1="114.3" x2="175.26" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VIN_DCDC3"/>
<wire x1="175.26" y1="111.76" x2="172.72" y2="111.76" width="0.1524" layer="91"/>
<wire x1="172.72" y1="111.76" x2="172.72" y2="114.3" width="0.1524" layer="91"/>
<junction x="172.72" y="114.3"/>
<wire x1="172.72" y1="116.84" x2="172.72" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VIN_DCDC1"/>
<wire x1="175.26" y1="116.84" x2="172.72" y2="116.84" width="0.1524" layer="91"/>
<wire x1="172.72" y1="116.84" x2="170.18" y2="116.84" width="0.1524" layer="91"/>
<junction x="172.72" y="116.84"/>
<label x="170.18" y="116.84" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="LS1_IN"/>
<wire x1="220.98" y1="114.3" x2="226.06" y2="114.3" width="0.1524" layer="91"/>
<label x="226.06" y="114.3" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="LS2_IN"/>
<wire x1="220.98" y1="106.68" x2="226.06" y2="106.68" width="0.1524" layer="91"/>
<label x="226.06" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="VIN"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="157.48" y1="213.36" x2="144.78" y2="213.36" width="0.1524" layer="91"/>
<wire x1="144.78" y1="213.36" x2="144.78" y2="210.82" width="0.1524" layer="91"/>
<wire x1="144.78" y1="213.36" x2="142.24" y2="213.36" width="0.1524" layer="91"/>
<junction x="144.78" y="213.36"/>
<label x="142.24" y="213.36" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U15" gate="TLV72033" pin="VIN"/>
<wire x1="38.1" y1="175.26" x2="35.56" y2="175.26" width="0.1524" layer="91"/>
<wire x1="35.56" y1="175.26" x2="25.4" y2="175.26" width="0.1524" layer="91"/>
<wire x1="25.4" y1="175.26" x2="22.86" y2="175.26" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="25.4" y1="172.72" x2="25.4" y2="175.26" width="0.1524" layer="91"/>
<junction x="25.4" y="175.26"/>
<label x="22.86" y="175.26" size="1.778" layer="95" rot="R180"/>
<pinref part="U15" gate="TLV72033" pin="EN"/>
<wire x1="38.1" y1="170.18" x2="35.56" y2="170.18" width="0.1524" layer="91"/>
<wire x1="35.56" y1="170.18" x2="35.56" y2="175.26" width="0.1524" layer="91"/>
<junction x="35.56" y="175.26"/>
</segment>
<segment>
<pinref part="U14" gate="TLV72033" pin="VIN"/>
<wire x1="35.56" y1="134.62" x2="33.02" y2="134.62" width="0.1524" layer="91"/>
<pinref part="U14" gate="TLV72033" pin="EN"/>
<wire x1="33.02" y1="134.62" x2="25.4" y2="134.62" width="0.1524" layer="91"/>
<wire x1="25.4" y1="134.62" x2="22.86" y2="134.62" width="0.1524" layer="91"/>
<wire x1="35.56" y1="129.54" x2="33.02" y2="129.54" width="0.1524" layer="91"/>
<wire x1="33.02" y1="129.54" x2="33.02" y2="134.62" width="0.1524" layer="91"/>
<junction x="33.02" y="134.62"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="25.4" y1="132.08" x2="25.4" y2="134.62" width="0.1524" layer="91"/>
<junction x="25.4" y="134.62"/>
<label x="22.86" y="134.62" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VIO" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VIO"/>
<wire x1="175.26" y1="124.46" x2="170.18" y2="124.46" width="0.1524" layer="91"/>
<label x="170.18" y="124.46" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="116.84" y1="109.22" x2="114.3" y2="109.22" width="0.1524" layer="91"/>
<label x="114.3" y="109.22" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VLDO1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VLDO1"/>
<wire x1="175.26" y1="104.14" x2="170.18" y2="104.14" width="0.1524" layer="91"/>
<label x="170.18" y="104.14" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="129.54" y1="109.22" x2="129.54" y2="101.6" width="0.1524" layer="91"/>
<wire x1="129.54" y1="101.6" x2="129.54" y2="93.98" width="0.1524" layer="91"/>
<wire x1="129.54" y1="93.98" x2="127" y2="93.98" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="127" y1="109.22" x2="129.54" y2="109.22" width="0.1524" layer="91"/>
<junction x="129.54" y="109.22"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="127" y1="101.6" x2="129.54" y2="101.6" width="0.1524" layer="91"/>
<junction x="129.54" y="101.6"/>
<wire x1="129.54" y1="109.22" x2="132.08" y2="109.22" width="0.1524" layer="91"/>
<label x="132.08" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD_3V3AUX" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VLDO2"/>
<wire x1="175.26" y1="101.6" x2="170.18" y2="101.6" width="0.1524" layer="91"/>
<label x="170.18" y="101.6" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="LD1" gate="G$1" pin="A"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="154.94" y1="165.1" x2="157.48" y2="165.1" width="0.1524" layer="91"/>
<wire x1="157.48" y1="165.1" x2="157.48" y2="162.56" width="0.1524" layer="91"/>
<wire x1="157.48" y1="165.1" x2="160.02" y2="165.1" width="0.1524" layer="91"/>
<junction x="157.48" y="165.1"/>
<label x="160.02" y="165.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="LD1_C" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="142.24" y1="162.56" x2="142.24" y2="165.1" width="0.1524" layer="91"/>
<pinref part="LD1" gate="G$1" pin="C"/>
<wire x1="142.24" y1="165.1" x2="147.32" y2="165.1" width="0.1524" layer="91"/>
<label x="142.24" y="165.1" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VRTC" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="116.84" y1="93.98" x2="114.3" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="114.3" y1="93.98" x2="114.3" y2="91.44" width="0.1524" layer="91"/>
<label x="114.3" y="93.98" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VDDS" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="116.84" y1="101.6" x2="114.3" y2="101.6" width="0.1524" layer="91"/>
<label x="114.3" y="101.6" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="243.84" y1="124.46" x2="243.84" y2="127" width="0.1524" layer="91"/>
<label x="243.84" y="127" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDCDC1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VDCDC1"/>
<wire x1="175.26" y1="96.52" x2="170.18" y2="96.52" width="0.1524" layer="91"/>
<label x="170.18" y="96.52" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="101.6" y1="66.04" x2="111.76" y2="66.04" width="0.1524" layer="91"/>
<label x="101.6" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="P_L1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="L1"/>
<wire x1="220.98" y1="127" x2="226.06" y2="127" width="0.1524" layer="91"/>
<label x="226.06" y="127" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="127" y1="66.04" x2="129.54" y2="66.04" width="0.1524" layer="91"/>
<label x="129.54" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="P_L2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="L2"/>
<wire x1="220.98" y1="124.46" x2="226.06" y2="124.46" width="0.1524" layer="91"/>
<label x="226.06" y="124.46" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="127" y1="58.42" x2="129.54" y2="58.42" width="0.1524" layer="91"/>
<label x="129.54" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="P_L3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="L3"/>
<wire x1="220.98" y1="121.92" x2="226.06" y2="121.92" width="0.1524" layer="91"/>
<label x="226.06" y="121.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="L3" gate="G$1" pin="2"/>
<wire x1="127" y1="50.8" x2="129.54" y2="50.8" width="0.1524" layer="91"/>
<label x="129.54" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDDS_DDR" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="91.44" y1="66.04" x2="83.82" y2="66.04" width="0.1524" layer="91"/>
<wire x1="83.82" y1="66.04" x2="83.82" y2="63.5" width="0.1524" layer="91"/>
<label x="83.82" y="66.04" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VDD_MPU" class="0">
<segment>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="93.98" y1="48.26" x2="93.98" y2="58.42" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="93.98" y1="58.42" x2="111.76" y2="58.42" width="0.1524" layer="91"/>
<label x="96.52" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VDCDC2"/>
<wire x1="175.26" y1="93.98" x2="170.18" y2="93.98" width="0.1524" layer="91"/>
<label x="170.18" y="93.98" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VDD_CORE" class="0">
<segment>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="109.22" y1="40.64" x2="109.22" y2="50.8" width="0.1524" layer="91"/>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="109.22" y1="50.8" x2="111.76" y2="50.8" width="0.1524" layer="91"/>
<label x="109.22" y="50.8" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VDCDC3"/>
<wire x1="175.26" y1="91.44" x2="170.18" y2="91.44" width="0.1524" layer="91"/>
<label x="170.18" y="91.44" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VDD_1V8" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="LS1_OUT"/>
<wire x1="220.98" y1="111.76" x2="243.84" y2="111.76" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="243.84" y1="111.76" x2="243.84" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="243.84" y1="111.76" x2="243.84" y2="109.22" width="0.1524" layer="91"/>
<junction x="243.84" y="111.76"/>
<wire x1="243.84" y1="111.76" x2="246.38" y2="111.76" width="0.1524" layer="91"/>
<label x="246.38" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD_3V3A" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="LS2_OUT"/>
<wire x1="220.98" y1="104.14" x2="223.52" y2="104.14" width="0.1524" layer="91"/>
<wire x1="223.52" y1="104.14" x2="223.52" y2="101.6" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="223.52" y1="101.6" x2="231.14" y2="101.6" width="0.1524" layer="91"/>
<label x="223.52" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="EN"/>
<wire x1="157.48" y1="208.28" x2="154.94" y2="208.28" width="0.1524" layer="91"/>
<wire x1="154.94" y1="208.28" x2="154.94" y2="218.44" width="0.1524" layer="91"/>
<label x="154.94" y="218.44" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="231.14" y1="60.96" x2="231.14" y2="63.5" width="0.1524" layer="91"/>
<label x="231.14" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="PMIC_PWR_EN" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PWR_EN"/>
<wire x1="175.26" y1="81.28" x2="170.18" y2="81.28" width="0.1524" layer="91"/>
<label x="170.18" y="81.28" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="PWR_NRST" class="0">
<segment>
<pinref part="H1" gate="PIN" pin="1"/>
<pinref part="U2" gate="G$1" pin="RESET"/>
<wire x1="172.72" y1="58.42" x2="175.26" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="P_BYPASS" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="BYPASS"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="175.26" y1="40.64" x2="172.72" y2="40.64" width="0.1524" layer="91"/>
<wire x1="172.72" y1="40.64" x2="172.72" y2="30.48" width="0.1524" layer="91"/>
<label x="172.72" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="USB_DC" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="USB"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="175.26" y1="45.72" x2="162.56" y2="45.72" width="0.1524" layer="91"/>
<wire x1="162.56" y1="45.72" x2="162.56" y2="38.1" width="0.1524" layer="91"/>
<label x="162.56" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C0_SDA" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SDA"/>
<wire x1="175.26" y1="53.34" x2="160.02" y2="53.34" width="0.1524" layer="91"/>
<label x="160.02" y="53.34" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="I2C0_SCL" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SCL"/>
<wire x1="175.26" y1="50.8" x2="160.02" y2="50.8" width="0.1524" layer="91"/>
<label x="160.02" y="50.8" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="WAKEUP" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="WAKEUP"/>
<wire x1="175.26" y1="63.5" x2="160.02" y2="63.5" width="0.1524" layer="91"/>
<label x="160.02" y="63.5" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="LDO_PGOOD" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="LDO_PGOOD"/>
<wire x1="175.26" y1="73.66" x2="170.18" y2="73.66" width="0.1524" layer="91"/>
<label x="170.18" y="73.66" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="PB_IN" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB_IN"/>
<wire x1="175.26" y1="68.58" x2="170.18" y2="68.58" width="0.1524" layer="91"/>
<label x="170.18" y="68.58" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="SW1" gate="G$1" pin="2"/>
<wire x1="124.46" y1="129.54" x2="129.54" y2="129.54" width="0.1524" layer="91"/>
<label x="129.54" y="129.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="P_INT_LDO" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="INT_LDO"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="220.98" y1="45.72" x2="226.06" y2="45.72" width="0.1524" layer="91"/>
<wire x1="226.06" y1="45.72" x2="226.06" y2="43.18" width="0.1524" layer="91"/>
<label x="226.06" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="PMIC_INT" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="INT"/>
<wire x1="220.98" y1="48.26" x2="231.14" y2="48.26" width="0.1524" layer="91"/>
<label x="233.68" y="48.26" size="1.778" layer="95"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="231.14" y1="48.26" x2="231.14" y2="50.8" width="0.1524" layer="91"/>
<wire x1="231.14" y1="48.26" x2="233.68" y2="48.26" width="0.1524" layer="91"/>
<junction x="231.14" y="48.26"/>
</segment>
</net>
<net name="U6_ADJ" class="0">
<segment>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="203.2" y1="208.28" x2="203.2" y2="210.82" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="ADJ/BYP"/>
<wire x1="203.2" y1="210.82" x2="190.5" y2="210.82" width="0.1524" layer="91"/>
<wire x1="203.2" y1="210.82" x2="218.44" y2="210.82" width="0.1524" layer="91"/>
<junction x="203.2" y="210.82"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="218.44" y1="210.82" x2="218.44" y2="208.28" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="218.44" y1="210.82" x2="220.98" y2="210.82" width="0.1524" layer="91"/>
<junction x="218.44" y="210.82"/>
<label x="203.2" y="210.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD_3V3B" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="VOUT"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="190.5" y1="215.9" x2="233.68" y2="215.9" width="0.1524" layer="91"/>
<wire x1="233.68" y1="215.9" x2="233.68" y2="210.82" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="233.68" y1="210.82" x2="233.68" y2="208.28" width="0.1524" layer="91"/>
<wire x1="231.14" y1="210.82" x2="233.68" y2="210.82" width="0.1524" layer="91"/>
<junction x="233.68" y="210.82"/>
<label x="233.68" y="215.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="PMIC_PGOOD" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PGOOD"/>
<wire x1="175.26" y1="76.2" x2="170.18" y2="76.2" width="0.1524" layer="91"/>
<label x="170.18" y="76.2" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="+3V3_ETH2" class="0">
<segment>
<pinref part="U15" gate="TLV72033" pin="VOUT"/>
<wire x1="71.12" y1="175.26" x2="76.2" y2="175.26" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="76.2" y1="175.26" x2="78.74" y2="175.26" width="0.1524" layer="91"/>
<wire x1="76.2" y1="172.72" x2="76.2" y2="175.26" width="0.1524" layer="91"/>
<junction x="76.2" y="175.26"/>
<label x="78.74" y="175.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="+3V3_PER" class="0">
<segment>
<pinref part="U14" gate="TLV72033" pin="VOUT"/>
<wire x1="68.58" y1="134.62" x2="73.66" y2="134.62" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="73.66" y1="134.62" x2="76.2" y2="134.62" width="0.1524" layer="91"/>
<wire x1="73.66" y1="132.08" x2="73.66" y2="134.62" width="0.1524" layer="91"/>
<junction x="73.66" y="134.62"/>
<label x="76.2" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="4.318" y="5.334" size="10.16" layer="97">PRIMARY MPU (1)</text>
<text x="5.588" y="19.812" size="2.54" layer="97">Linux Micro-PC
Rev 1.0
SBS 2015</text>
</plain>
<instances>
<instance part="U1" gate="A" x="101.6" y="213.36"/>
<instance part="U1" gate="B" x="215.9" y="218.44"/>
<instance part="U1" gate="C" x="101.6" y="88.9"/>
<instance part="U1" gate="D" x="218.44" y="91.44"/>
<instance part="R21" gate="G$1" x="45.72" y="190.5" rot="R90"/>
<instance part="R22" gate="G$1" x="40.64" y="198.12"/>
<instance part="SUPPLY33" gate="GND" x="45.72" y="182.88"/>
<instance part="R23" gate="G$1" x="48.26" y="83.82" rot="R90"/>
<instance part="R24" gate="G$1" x="40.64" y="83.82" rot="R90"/>
<instance part="R26" gate="G$1" x="274.32" y="43.18"/>
<instance part="SUPPLY34" gate="GND" x="279.4" y="40.64"/>
<instance part="H3" gate="PIN" x="43.18" y="261.62"/>
<instance part="C101" gate="G$1" x="45.72" y="170.18"/>
<instance part="SUPPLY35" gate="GND" x="45.72" y="162.56"/>
<instance part="C102" gate="G$1" x="30.48" y="58.42"/>
<instance part="SUPPLY36" gate="GND" x="30.48" y="50.8"/>
<instance part="C103" gate="G$1" x="17.78" y="53.34"/>
<instance part="SUPPLY37" gate="GND" x="17.78" y="45.72"/>
<instance part="R27" gate="G$1" x="43.18" y="228.6" rot="R90"/>
<instance part="SUPPLY38" gate="GND" x="43.18" y="220.98"/>
<instance part="R106" gate="G$1" x="30.48" y="246.38" rot="R90"/>
<instance part="SUPPLY69" gate="GND" x="30.48" y="238.76"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U1" gate="A" pin="VSS_2"/>
<wire x1="71.12" y1="264.16" x2="66.04" y2="264.16" width="0.1524" layer="91"/>
<label x="66.04" y="264.16" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="VSS_3"/>
<wire x1="134.62" y1="193.04" x2="137.16" y2="193.04" width="0.1524" layer="91"/>
<label x="137.16" y="193.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="VSS_5"/>
<wire x1="134.62" y1="218.44" x2="137.16" y2="218.44" width="0.1524" layer="91"/>
<wire x1="137.16" y1="218.44" x2="137.16" y2="215.9" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VSS_4"/>
<wire x1="137.16" y1="215.9" x2="134.62" y2="215.9" width="0.1524" layer="91"/>
<label x="137.16" y="215.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="VSS_9"/>
<wire x1="134.62" y1="241.3" x2="137.16" y2="241.3" width="0.1524" layer="91"/>
<wire x1="137.16" y1="241.3" x2="137.16" y2="238.76" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VSS_6"/>
<wire x1="137.16" y1="238.76" x2="137.16" y2="236.22" width="0.1524" layer="91"/>
<wire x1="137.16" y1="236.22" x2="137.16" y2="233.68" width="0.1524" layer="91"/>
<wire x1="137.16" y1="233.68" x2="134.62" y2="233.68" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VSS_7"/>
<wire x1="134.62" y1="236.22" x2="137.16" y2="236.22" width="0.1524" layer="91"/>
<junction x="137.16" y="236.22"/>
<pinref part="U1" gate="A" pin="VSS_8"/>
<wire x1="134.62" y1="238.76" x2="137.16" y2="238.76" width="0.1524" layer="91"/>
<junction x="137.16" y="238.76"/>
<label x="137.16" y="238.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="VSS"/>
<wire x1="134.62" y1="264.16" x2="137.16" y2="264.16" width="0.1524" layer="91"/>
<wire x1="137.16" y1="264.16" x2="137.16" y2="261.62" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VSS_10"/>
<wire x1="137.16" y1="261.62" x2="137.16" y2="259.08" width="0.1524" layer="91"/>
<wire x1="137.16" y1="259.08" x2="137.16" y2="256.54" width="0.1524" layer="91"/>
<wire x1="137.16" y1="256.54" x2="134.62" y2="256.54" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VSS_11"/>
<wire x1="134.62" y1="259.08" x2="137.16" y2="259.08" width="0.1524" layer="91"/>
<junction x="137.16" y="259.08"/>
<pinref part="U1" gate="A" pin="VSS_12"/>
<wire x1="134.62" y1="261.62" x2="137.16" y2="261.62" width="0.1524" layer="91"/>
<junction x="137.16" y="261.62"/>
<label x="137.16" y="264.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="VSS_2"/>
<wire x1="185.42" y1="248.92" x2="182.88" y2="248.92" width="0.1524" layer="91"/>
<label x="182.88" y="248.92" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="VSS_3"/>
<wire x1="185.42" y1="243.84" x2="182.88" y2="243.84" width="0.1524" layer="91"/>
<label x="182.88" y="243.84" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="VSS_4"/>
<wire x1="185.42" y1="205.74" x2="182.88" y2="205.74" width="0.1524" layer="91"/>
<wire x1="182.88" y1="205.74" x2="182.88" y2="203.2" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VSS_7"/>
<wire x1="182.88" y1="203.2" x2="182.88" y2="200.66" width="0.1524" layer="91"/>
<wire x1="182.88" y1="200.66" x2="182.88" y2="198.12" width="0.1524" layer="91"/>
<wire x1="182.88" y1="198.12" x2="185.42" y2="198.12" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VSS_6"/>
<wire x1="185.42" y1="200.66" x2="182.88" y2="200.66" width="0.1524" layer="91"/>
<junction x="182.88" y="200.66"/>
<pinref part="U1" gate="B" pin="VSS_5"/>
<wire x1="185.42" y1="203.2" x2="182.88" y2="203.2" width="0.1524" layer="91"/>
<junction x="182.88" y="203.2"/>
<label x="182.88" y="203.2" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="VSS_8"/>
<wire x1="185.42" y1="180.34" x2="182.88" y2="180.34" width="0.1524" layer="91"/>
<label x="182.88" y="180.34" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="VSS"/>
<wire x1="248.92" y1="243.84" x2="251.46" y2="243.84" width="0.1524" layer="91"/>
<label x="251.46" y="243.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VSS"/>
<wire x1="248.92" y1="137.16" x2="251.46" y2="137.16" width="0.1524" layer="91"/>
<label x="251.46" y="137.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VSS_2"/>
<wire x1="190.5" y1="137.16" x2="187.96" y2="137.16" width="0.1524" layer="91"/>
<wire x1="187.96" y1="137.16" x2="187.96" y2="134.62" width="0.1524" layer="91"/>
<pinref part="U1" gate="D" pin="VSS_3"/>
<wire x1="187.96" y1="134.62" x2="190.5" y2="134.62" width="0.1524" layer="91"/>
<label x="187.96" y="137.16" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VSS_4"/>
<wire x1="190.5" y1="114.3" x2="187.96" y2="114.3" width="0.1524" layer="91"/>
<wire x1="187.96" y1="114.3" x2="187.96" y2="111.76" width="0.1524" layer="91"/>
<pinref part="U1" gate="D" pin="VSS_7"/>
<wire x1="187.96" y1="111.76" x2="187.96" y2="109.22" width="0.1524" layer="91"/>
<wire x1="187.96" y1="109.22" x2="187.96" y2="106.68" width="0.1524" layer="91"/>
<wire x1="187.96" y1="106.68" x2="190.5" y2="106.68" width="0.1524" layer="91"/>
<pinref part="U1" gate="D" pin="VSS_6"/>
<wire x1="190.5" y1="109.22" x2="187.96" y2="109.22" width="0.1524" layer="91"/>
<junction x="187.96" y="109.22"/>
<pinref part="U1" gate="D" pin="VSS_5"/>
<wire x1="190.5" y1="111.76" x2="187.96" y2="111.76" width="0.1524" layer="91"/>
<junction x="187.96" y="111.76"/>
<label x="187.96" y="111.76" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VSS_8"/>
<wire x1="190.5" y1="91.44" x2="187.96" y2="91.44" width="0.1524" layer="91"/>
<label x="187.96" y="91.44" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VSS_9"/>
<wire x1="190.5" y1="86.36" x2="187.96" y2="86.36" width="0.1524" layer="91"/>
<label x="187.96" y="86.36" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VSS_10"/>
<wire x1="190.5" y1="68.58" x2="187.96" y2="68.58" width="0.1524" layer="91"/>
<wire x1="187.96" y1="68.58" x2="187.96" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U1" gate="D" pin="VSS_11"/>
<wire x1="187.96" y1="66.04" x2="190.5" y2="66.04" width="0.1524" layer="91"/>
<label x="187.96" y="68.58" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="VSS_2"/>
<wire x1="71.12" y1="116.84" x2="68.58" y2="116.84" width="0.1524" layer="91"/>
<label x="68.58" y="116.84" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="VSS"/>
<wire x1="134.62" y1="119.38" x2="137.16" y2="119.38" width="0.1524" layer="91"/>
<wire x1="137.16" y1="119.38" x2="137.16" y2="116.84" width="0.1524" layer="91"/>
<pinref part="U1" gate="C" pin="VSS_7"/>
<wire x1="137.16" y1="116.84" x2="134.62" y2="116.84" width="0.1524" layer="91"/>
<label x="137.16" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="VSS_6"/>
<wire x1="134.62" y1="99.06" x2="137.16" y2="99.06" width="0.1524" layer="91"/>
<label x="137.16" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="VSS_5"/>
<wire x1="134.62" y1="93.98" x2="137.16" y2="93.98" width="0.1524" layer="91"/>
<label x="137.16" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="VSS_4"/>
<wire x1="134.62" y1="76.2" x2="137.16" y2="76.2" width="0.1524" layer="91"/>
<wire x1="137.16" y1="76.2" x2="137.16" y2="73.66" width="0.1524" layer="91"/>
<pinref part="U1" gate="C" pin="VSS_3"/>
<wire x1="137.16" y1="73.66" x2="134.62" y2="73.66" width="0.1524" layer="91"/>
<label x="137.16" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<pinref part="SUPPLY33" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<pinref part="SUPPLY34" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C101" gate="G$1" pin="2"/>
<pinref part="SUPPLY35" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C102" gate="G$1" pin="2"/>
<pinref part="SUPPLY36" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C103" gate="G$1" pin="2"/>
<pinref part="SUPPLY37" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VSSA_USB_2"/>
<wire x1="190.5" y1="81.28" x2="187.96" y2="81.28" width="0.1524" layer="91"/>
<label x="187.96" y="81.28" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VSSA_USB"/>
<wire x1="190.5" y1="58.42" x2="187.96" y2="58.42" width="0.1524" layer="91"/>
<label x="187.96" y="58.42" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R27" gate="G$1" pin="1"/>
<pinref part="SUPPLY38" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R106" gate="G$1" pin="1"/>
<pinref part="SUPPLY69" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="VSS_RTC"/>
<wire x1="71.12" y1="254" x2="66.04" y2="254" width="0.1524" layer="91"/>
<label x="66.04" y="254" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VSS_OSC"/>
<wire x1="248.92" y1="119.38" x2="254" y2="119.38" width="0.1524" layer="91"/>
<label x="254" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="VRTC" class="0">
<segment>
<pinref part="U1" gate="A" pin="VDDS_RTC"/>
<wire x1="71.12" y1="180.34" x2="68.58" y2="180.34" width="0.1524" layer="91"/>
<label x="68.58" y="180.34" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VDDS" class="0">
<segment>
<pinref part="U1" gate="D" pin="VDDS"/>
<wire x1="248.92" y1="35.56" x2="251.46" y2="35.56" width="0.1524" layer="91"/>
<label x="251.46" y="33.02" size="1.778" layer="95" rot="R270"/>
<wire x1="251.46" y1="35.56" x2="251.46" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VDDS_2"/>
<wire x1="190.5" y1="129.54" x2="187.96" y2="129.54" width="0.1524" layer="91"/>
<label x="187.96" y="129.54" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="VDDS"/>
<wire x1="134.62" y1="35.56" x2="137.16" y2="35.56" width="0.1524" layer="91"/>
<label x="137.16" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="VDDS"/>
<wire x1="248.92" y1="172.72" x2="251.46" y2="172.72" width="0.1524" layer="91"/>
<label x="251.46" y="172.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="VDDS_2"/>
<wire x1="134.62" y1="165.1" x2="137.16" y2="165.1" width="0.1524" layer="91"/>
<label x="137.16" y="165.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="VDDS"/>
<wire x1="134.62" y1="195.58" x2="137.16" y2="195.58" width="0.1524" layer="91"/>
<label x="137.16" y="195.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="VDDS_2"/>
<wire x1="185.42" y1="182.88" x2="182.88" y2="182.88" width="0.1524" layer="91"/>
<label x="182.88" y="182.88" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VDDS_DDR" class="0">
<segment>
<pinref part="U1" gate="A" pin="VDDS_DDR_2"/>
<wire x1="134.62" y1="162.56" x2="137.16" y2="162.56" width="0.1524" layer="91"/>
<label x="137.16" y="162.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="VDDS_DDR_3"/>
<wire x1="134.62" y1="185.42" x2="137.16" y2="185.42" width="0.1524" layer="91"/>
<label x="137.16" y="185.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="VDDS_DDR_4"/>
<wire x1="134.62" y1="208.28" x2="137.16" y2="208.28" width="0.1524" layer="91"/>
<label x="137.16" y="208.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="VDDS_DDR_5"/>
<wire x1="134.62" y1="231.14" x2="137.16" y2="231.14" width="0.1524" layer="91"/>
<label x="137.16" y="231.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="VDDS_DDR"/>
<wire x1="134.62" y1="254" x2="137.16" y2="254" width="0.1524" layer="91"/>
<label x="137.16" y="254" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="VDDS_DDR_2"/>
<wire x1="185.42" y1="254" x2="182.88" y2="254" width="0.1524" layer="91"/>
<label x="182.88" y="254" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="VDDS_DDR"/>
<wire x1="185.42" y1="231.14" x2="182.88" y2="231.14" width="0.1524" layer="91"/>
<label x="182.88" y="231.14" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VDD_MPU" class="0">
<segment>
<pinref part="U1" gate="C" pin="VDD_MPU_2"/>
<wire x1="134.62" y1="48.26" x2="137.16" y2="48.26" width="0.1524" layer="91"/>
<label x="137.16" y="48.26" size="1.778" layer="95"/>
<wire x1="137.16" y1="48.26" x2="137.16" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U1" gate="C" pin="VDD_MPU_5"/>
<wire x1="137.16" y1="50.8" x2="137.16" y2="53.34" width="0.1524" layer="91"/>
<wire x1="137.16" y1="53.34" x2="137.16" y2="55.88" width="0.1524" layer="91"/>
<wire x1="137.16" y1="55.88" x2="134.62" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U1" gate="C" pin="VDD_MPU_4"/>
<wire x1="134.62" y1="53.34" x2="137.16" y2="53.34" width="0.1524" layer="91"/>
<junction x="137.16" y="53.34"/>
<pinref part="U1" gate="C" pin="VDD_MPU_3"/>
<wire x1="134.62" y1="50.8" x2="137.16" y2="50.8" width="0.1524" layer="91"/>
<junction x="137.16" y="50.8"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="VDD_MPU_6"/>
<wire x1="134.62" y1="78.74" x2="137.16" y2="78.74" width="0.1524" layer="91"/>
<label x="137.16" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="VDD_MPU"/>
<wire x1="134.62" y1="124.46" x2="137.16" y2="124.46" width="0.1524" layer="91"/>
<label x="137.16" y="124.46" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="VDD_MPU_7"/>
<wire x1="134.62" y1="101.6" x2="137.16" y2="101.6" width="0.1524" layer="91"/>
<label x="137.16" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD_CORE" class="0">
<segment>
<pinref part="U1" gate="A" pin="VDD_CORE"/>
<wire x1="134.62" y1="213.36" x2="137.16" y2="213.36" width="0.1524" layer="91"/>
<wire x1="137.16" y1="213.36" x2="137.16" y2="210.82" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VDD_CORE_4"/>
<wire x1="137.16" y1="210.82" x2="134.62" y2="210.82" width="0.1524" layer="91"/>
<label x="137.16" y="210.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="VDD_CORE_3"/>
<wire x1="134.62" y1="190.5" x2="137.16" y2="190.5" width="0.1524" layer="91"/>
<wire x1="137.16" y1="190.5" x2="137.16" y2="187.96" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VDD_CORE_2"/>
<wire x1="137.16" y1="187.96" x2="134.62" y2="187.96" width="0.1524" layer="91"/>
<label x="137.16" y="187.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="VDD_CORE_2"/>
<wire x1="185.42" y1="251.46" x2="182.88" y2="251.46" width="0.1524" layer="91"/>
<label x="182.88" y="251.46" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="VDD_CORE_3"/>
<wire x1="185.42" y1="246.38" x2="182.88" y2="246.38" width="0.1524" layer="91"/>
<label x="182.88" y="246.38" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="VDD_CORE_4"/>
<wire x1="185.42" y1="228.6" x2="182.88" y2="228.6" width="0.1524" layer="91"/>
<wire x1="182.88" y1="228.6" x2="182.88" y2="226.06" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDD_CORE_7"/>
<wire x1="182.88" y1="226.06" x2="182.88" y2="223.52" width="0.1524" layer="91"/>
<wire x1="182.88" y1="223.52" x2="182.88" y2="220.98" width="0.1524" layer="91"/>
<wire x1="182.88" y1="220.98" x2="185.42" y2="220.98" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDD_CORE_6"/>
<wire x1="185.42" y1="223.52" x2="182.88" y2="223.52" width="0.1524" layer="91"/>
<junction x="182.88" y="223.52"/>
<pinref part="U1" gate="B" pin="VDD_CORE_5"/>
<wire x1="185.42" y1="226.06" x2="182.88" y2="226.06" width="0.1524" layer="91"/>
<junction x="182.88" y="226.06"/>
<label x="182.88" y="226.06" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="VDD_CORE_8"/>
<wire x1="185.42" y1="177.8" x2="182.88" y2="177.8" width="0.1524" layer="91"/>
<wire x1="182.88" y1="177.8" x2="182.88" y2="175.26" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDD_CORE"/>
<wire x1="182.88" y1="175.26" x2="185.42" y2="175.26" width="0.1524" layer="91"/>
<label x="182.88" y="177.8" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="VDD_CORE"/>
<wire x1="134.62" y1="121.92" x2="137.16" y2="121.92" width="0.1524" layer="91"/>
<label x="137.16" y="121.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="VDD_CORE_3"/>
<wire x1="134.62" y1="96.52" x2="137.16" y2="96.52" width="0.1524" layer="91"/>
<label x="137.16" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="VDD_CORE_2"/>
<wire x1="134.62" y1="71.12" x2="137.16" y2="71.12" width="0.1524" layer="91"/>
<label x="137.16" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VDD_CORE_2"/>
<wire x1="190.5" y1="132.08" x2="187.96" y2="132.08" width="0.1524" layer="91"/>
<label x="187.96" y="132.08" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VDD_CORE_3"/>
<wire x1="190.5" y1="88.9" x2="187.96" y2="88.9" width="0.1524" layer="91"/>
<label x="187.96" y="88.9" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VDD_CORE_4"/>
<wire x1="190.5" y1="83.82" x2="187.96" y2="83.82" width="0.1524" layer="91"/>
<label x="187.96" y="83.82" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VDD_CORE_5"/>
<wire x1="190.5" y1="63.5" x2="187.96" y2="63.5" width="0.1524" layer="91"/>
<wire x1="187.96" y1="63.5" x2="187.96" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U1" gate="D" pin="VDD_CORE"/>
<wire x1="187.96" y1="60.96" x2="190.5" y2="60.96" width="0.1524" layer="91"/>
<label x="187.96" y="63.5" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VDD_1V8" class="0">
<segment>
<pinref part="U1" gate="C" pin="VDDS_SRAM_MPU_BB"/>
<wire x1="71.12" y1="68.58" x2="68.58" y2="68.58" width="0.1524" layer="91"/>
<label x="68.58" y="68.58" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="VDDS_SRAM_CORE_BG"/>
<wire x1="134.62" y1="172.72" x2="137.16" y2="172.72" width="0.1524" layer="91"/>
<label x="137.16" y="172.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VDDA1P8V_USB0"/>
<wire x1="190.5" y1="53.34" x2="187.96" y2="53.34" width="0.1524" layer="91"/>
<label x="187.96" y="53.34" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VDDA1P8V_USB1"/>
<wire x1="248.92" y1="63.5" x2="251.46" y2="63.5" width="0.1524" layer="91"/>
<label x="251.46" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD_3V3A" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="48.26" y1="88.9" x2="48.26" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="40.64" y1="88.9" x2="40.64" y2="91.44" width="0.1524" layer="91"/>
<wire x1="40.64" y1="91.44" x2="48.26" y2="91.44" width="0.1524" layer="91"/>
<label x="40.64" y="91.44" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VDDA3P3V_USB0"/>
<wire x1="190.5" y1="55.88" x2="187.96" y2="55.88" width="0.1524" layer="91"/>
<label x="187.96" y="55.88" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VDDA3P3V_USB1"/>
<wire x1="248.92" y1="60.96" x2="251.46" y2="60.96" width="0.1524" layer="91"/>
<label x="251.46" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VDDSHV3"/>
<wire x1="190.5" y1="38.1" x2="187.96" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U1" gate="D" pin="VDDSHV2_2"/>
<wire x1="190.5" y1="45.72" x2="187.96" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U1" gate="D" pin="VDDSHV2"/>
<wire x1="190.5" y1="43.18" x2="187.96" y2="43.18" width="0.1524" layer="91"/>
<wire x1="187.96" y1="45.72" x2="187.96" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U1" gate="D" pin="VDDSHV3_2"/>
<wire x1="190.5" y1="40.64" x2="187.96" y2="40.64" width="0.1524" layer="91"/>
<wire x1="187.96" y1="43.18" x2="187.96" y2="40.64" width="0.1524" layer="91"/>
<junction x="187.96" y="43.18"/>
<wire x1="187.96" y1="38.1" x2="187.96" y2="40.64" width="0.1524" layer="91"/>
<junction x="187.96" y="40.64"/>
<label x="187.96" y="43.18" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VDDSHV5"/>
<wire x1="190.5" y1="104.14" x2="187.96" y2="104.14" width="0.1524" layer="91"/>
<label x="187.96" y="104.14" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VDDSHV5_2"/>
<wire x1="190.5" y1="127" x2="187.96" y2="127" width="0.1524" layer="91"/>
<label x="187.96" y="127" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="VDDSHV4"/>
<wire x1="134.62" y1="127" x2="137.16" y2="127" width="0.1524" layer="91"/>
<label x="137.16" y="127" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="VDDSHV4_2"/>
<wire x1="134.62" y1="104.14" x2="137.16" y2="104.14" width="0.1524" layer="91"/>
<label x="137.16" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="VDDSHV6"/>
<wire x1="134.62" y1="81.28" x2="137.16" y2="81.28" width="0.1524" layer="91"/>
<label x="137.16" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="VDDSHV6_6"/>
<wire x1="134.62" y1="58.42" x2="137.16" y2="58.42" width="0.1524" layer="91"/>
<label x="137.16" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="VDDSHV6_2"/>
<wire x1="71.12" y1="45.72" x2="68.58" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U1" gate="C" pin="VDDSHV6_3"/>
<wire x1="71.12" y1="43.18" x2="68.58" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U1" gate="C" pin="VDDSHV6_5"/>
<wire x1="71.12" y1="38.1" x2="68.58" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U1" gate="C" pin="VDDSHV6_4"/>
<wire x1="71.12" y1="40.64" x2="68.58" y2="40.64" width="0.1524" layer="91"/>
<wire x1="68.58" y1="38.1" x2="68.58" y2="40.64" width="0.1524" layer="91"/>
<wire x1="68.58" y1="43.18" x2="68.58" y2="40.64" width="0.1524" layer="91"/>
<junction x="68.58" y="40.64"/>
<wire x1="68.58" y1="45.72" x2="68.58" y2="43.18" width="0.1524" layer="91"/>
<junction x="68.58" y="43.18"/>
<label x="68.58" y="43.18" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="VDDSHV1"/>
<wire x1="248.92" y1="170.18" x2="251.46" y2="170.18" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDDSHV1_2"/>
<wire x1="248.92" y1="167.64" x2="251.46" y2="167.64" width="0.1524" layer="91"/>
<wire x1="251.46" y1="170.18" x2="251.46" y2="167.64" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDDSHV6"/>
<wire x1="248.92" y1="165.1" x2="251.46" y2="165.1" width="0.1524" layer="91"/>
<wire x1="251.46" y1="167.64" x2="251.46" y2="165.1" width="0.1524" layer="91"/>
<junction x="251.46" y="167.64"/>
<pinref part="U1" gate="B" pin="VDDSHV6_3"/>
<wire x1="248.92" y1="162.56" x2="251.46" y2="162.56" width="0.1524" layer="91"/>
<wire x1="251.46" y1="165.1" x2="251.46" y2="162.56" width="0.1524" layer="91"/>
<junction x="251.46" y="165.1"/>
<label x="251.46" y="167.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="VDDSHV6_2"/>
<wire x1="185.42" y1="185.42" x2="182.88" y2="185.42" width="0.1524" layer="91"/>
<label x="182.88" y="185.42" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="PMIC_PWR_EN" class="0">
<segment>
<pinref part="U1" gate="A" pin="PMIC_POWER_EN"/>
<wire x1="71.12" y1="205.74" x2="66.04" y2="205.74" width="0.1524" layer="91"/>
<label x="66.04" y="205.74" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="I2C0_SDA" class="0">
<segment>
<pinref part="U1" gate="C" pin="I2C0_SDA"/>
<label x="45.72" y="73.66" size="1.778" layer="95" rot="R180"/>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="48.26" y1="78.74" x2="48.26" y2="73.66" width="0.1524" layer="91"/>
<wire x1="48.26" y1="73.66" x2="71.12" y2="73.66" width="0.1524" layer="91"/>
<wire x1="48.26" y1="73.66" x2="45.72" y2="73.66" width="0.1524" layer="91"/>
<junction x="48.26" y="73.66"/>
</segment>
</net>
<net name="I2C0_SCL" class="0">
<segment>
<pinref part="U1" gate="C" pin="I2C0_SCL"/>
<label x="38.1" y="76.2" size="1.778" layer="95" rot="R180"/>
<wire x1="71.12" y1="76.2" x2="40.64" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="40.64" y1="76.2" x2="38.1" y2="76.2" width="0.1524" layer="91"/>
<wire x1="40.64" y1="78.74" x2="40.64" y2="76.2" width="0.1524" layer="91"/>
<junction x="40.64" y="76.2"/>
</segment>
</net>
<net name="WAKEUP" class="0">
<segment>
<pinref part="U1" gate="A" pin="EXT_WAKEUP"/>
<wire x1="71.12" y1="208.28" x2="66.04" y2="208.28" width="0.1524" layer="91"/>
<label x="66.04" y="208.28" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="LDO_PGOOD" class="0">
<segment>
<pinref part="U1" gate="A" pin="RTC_PWRONRSTN"/>
<wire x1="71.12" y1="231.14" x2="66.04" y2="231.14" width="0.1524" layer="91"/>
<label x="66.04" y="231.14" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="PMIC_INT" class="0">
<segment>
<pinref part="U1" gate="C" pin="EXTINTN"/>
<wire x1="71.12" y1="93.98" x2="66.04" y2="93.98" width="0.1524" layer="91"/>
<label x="66.04" y="93.98" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VDD_3V3B" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="35.56" y1="198.12" x2="33.02" y2="198.12" width="0.1524" layer="91"/>
<label x="33.02" y="198.12" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VDD_PLL" class="0">
<segment>
<pinref part="U1" gate="C" pin="VDDS_PLL_MPU"/>
<wire x1="134.62" y1="106.68" x2="137.16" y2="106.68" width="0.1524" layer="91"/>
<label x="137.16" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="VDDS_PLL_DDR"/>
<wire x1="134.62" y1="167.64" x2="137.16" y2="167.64" width="0.1524" layer="91"/>
<label x="137.16" y="167.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="D" pin="VDDS_PLL_CORE_LCD"/>
<wire x1="248.92" y1="48.26" x2="251.46" y2="48.26" width="0.1524" layer="91"/>
<label x="251.46" y="48.26" size="1.778" layer="95"/>
<pinref part="U1" gate="D" pin="VDDS_OSC"/>
<wire x1="248.92" y1="50.8" x2="251.46" y2="50.8" width="0.1524" layer="91"/>
<wire x1="251.46" y1="50.8" x2="251.46" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VDD_ADC" class="0">
<segment>
<pinref part="U1" gate="A" pin="VDDA_ADC"/>
<wire x1="71.12" y1="177.8" x2="68.58" y2="177.8" width="0.1524" layer="91"/>
<label x="68.58" y="177.8" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="GNDA_ADC" class="0">
<segment>
<pinref part="U1" gate="A" pin="VSSA_ADC"/>
<wire x1="134.62" y1="170.18" x2="137.16" y2="170.18" width="0.1524" layer="91"/>
<label x="137.16" y="170.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="VREFN"/>
<wire x1="71.12" y1="243.84" x2="66.04" y2="243.84" width="0.1524" layer="91"/>
<label x="66.04" y="243.84" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VREFP_ADC" class="0">
<segment>
<pinref part="U1" gate="A" pin="VREFP"/>
<wire x1="71.12" y1="220.98" x2="66.04" y2="220.98" width="0.1524" layer="91"/>
<label x="66.04" y="220.98" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="CAP_VDD_RTC" class="0">
<segment>
<pinref part="U1" gate="A" pin="CAP_VDD_RTC"/>
<wire x1="71.12" y1="182.88" x2="68.58" y2="182.88" width="0.1524" layer="91"/>
<label x="68.58" y="182.88" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="OSC1_OUT" class="0">
<segment>
<pinref part="U1" gate="A" pin="RTC_XTALOUT"/>
<wire x1="71.12" y1="256.54" x2="66.04" y2="256.54" width="0.1524" layer="91"/>
<label x="66.04" y="256.54" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="OSC1_IN" class="0">
<segment>
<pinref part="U1" gate="A" pin="RTC_XTALIN"/>
<wire x1="71.12" y1="251.46" x2="66.04" y2="251.46" width="0.1524" layer="91"/>
<label x="66.04" y="251.46" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="OSC0_OUT" class="0">
<segment>
<pinref part="U1" gate="D" pin="XTALOUT"/>
<wire x1="248.92" y1="96.52" x2="254" y2="96.52" width="0.1524" layer="91"/>
<label x="254" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="OSC0_IN" class="0">
<segment>
<pinref part="U1" gate="D" pin="XTALIN"/>
<wire x1="248.92" y1="116.84" x2="254" y2="116.84" width="0.1524" layer="91"/>
<label x="254" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="MPU_NRST" class="0">
<segment>
<pinref part="U1" gate="C" pin="WARMRSTN"/>
<wire x1="71.12" y1="137.16" x2="66.04" y2="137.16" width="0.1524" layer="91"/>
<label x="66.04" y="137.16" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="UART0_RX" class="0">
<segment>
<pinref part="U1" gate="C" pin="UART0_RXD"/>
<wire x1="134.62" y1="38.1" x2="139.7" y2="38.1" width="0.1524" layer="91"/>
<label x="139.7" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART0_TX" class="0">
<segment>
<pinref part="U1" gate="C" pin="UART0_TXD"/>
<wire x1="134.62" y1="40.64" x2="139.7" y2="40.64" width="0.1524" layer="91"/>
<label x="139.7" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A0" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_A0"/>
<wire x1="134.62" y1="180.34" x2="139.7" y2="180.34" width="0.1524" layer="91"/>
<label x="139.7" y="180.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A1" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_A1"/>
<wire x1="134.62" y1="220.98" x2="139.7" y2="220.98" width="0.1524" layer="91"/>
<label x="139.7" y="220.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A2" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_A2"/>
<wire x1="71.12" y1="165.1" x2="66.04" y2="165.1" width="0.1524" layer="91"/>
<label x="66.04" y="165.1" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_A9" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_A9"/>
<wire x1="71.12" y1="218.44" x2="66.04" y2="218.44" width="0.1524" layer="91"/>
<label x="66.04" y="218.44" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_A4" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_A4"/>
<wire x1="71.12" y1="215.9" x2="66.04" y2="215.9" width="0.1524" layer="91"/>
<label x="66.04" y="215.9" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_A3" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_A3"/>
<wire x1="71.12" y1="213.36" x2="66.04" y2="213.36" width="0.1524" layer="91"/>
<label x="66.04" y="213.36" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_A15" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_A15"/>
<wire x1="71.12" y1="190.5" x2="66.04" y2="190.5" width="0.1524" layer="91"/>
<label x="66.04" y="190.5" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_A8" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_A8"/>
<wire x1="71.12" y1="187.96" x2="66.04" y2="187.96" width="0.1524" layer="91"/>
<label x="66.04" y="187.96" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_A6" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_A6"/>
<wire x1="71.12" y1="185.42" x2="66.04" y2="185.42" width="0.1524" layer="91"/>
<label x="66.04" y="185.42" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_A7" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_A7"/>
<wire x1="71.12" y1="170.18" x2="66.04" y2="170.18" width="0.1524" layer="91"/>
<label x="66.04" y="170.18" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_A12" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_A12"/>
<wire x1="71.12" y1="167.64" x2="66.04" y2="167.64" width="0.1524" layer="91"/>
<label x="66.04" y="167.64" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_A10" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_A10"/>
<wire x1="134.62" y1="182.88" x2="139.7" y2="182.88" width="0.1524" layer="91"/>
<label x="139.7" y="182.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A11" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_A11"/>
<wire x1="134.62" y1="177.8" x2="139.7" y2="177.8" width="0.1524" layer="91"/>
<label x="139.7" y="177.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A14" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_A14"/>
<wire x1="134.62" y1="228.6" x2="139.7" y2="228.6" width="0.1524" layer="91"/>
<label x="139.7" y="228.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A13" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_A13"/>
<wire x1="134.62" y1="226.06" x2="139.7" y2="226.06" width="0.1524" layer="91"/>
<label x="139.7" y="226.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A5" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_A5"/>
<wire x1="71.12" y1="241.3" x2="66.04" y2="241.3" width="0.1524" layer="91"/>
<label x="66.04" y="241.3" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_BA2" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_BA2"/>
<wire x1="71.12" y1="236.22" x2="66.04" y2="236.22" width="0.1524" layer="91"/>
<label x="66.04" y="236.22" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_BA0" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_BA0"/>
<wire x1="71.12" y1="210.82" x2="66.04" y2="210.82" width="0.1524" layer="91"/>
<label x="66.04" y="210.82" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_BA1" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_BA1"/>
<wire x1="71.12" y1="172.72" x2="66.04" y2="172.72" width="0.1524" layer="91"/>
<label x="66.04" y="172.72" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D8" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_D8"/>
<wire x1="134.62" y1="243.84" x2="139.7" y2="243.84" width="0.1524" layer="91"/>
<label x="139.7" y="243.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_D0" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_D0"/>
<wire x1="185.42" y1="213.36" x2="180.34" y2="213.36" width="0.1524" layer="91"/>
<label x="180.34" y="213.36" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D1" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_D1"/>
<wire x1="185.42" y1="210.82" x2="180.34" y2="210.82" width="0.1524" layer="91"/>
<label x="180.34" y="210.82" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D15" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_D15"/>
<wire x1="185.42" y1="218.44" x2="180.34" y2="218.44" width="0.1524" layer="91"/>
<label x="180.34" y="218.44" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D2" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_D2"/>
<wire x1="185.42" y1="195.58" x2="180.34" y2="195.58" width="0.1524" layer="91"/>
<label x="180.34" y="195.58" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D3" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_D3"/>
<wire x1="185.42" y1="193.04" x2="180.34" y2="193.04" width="0.1524" layer="91"/>
<label x="180.34" y="193.04" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D4" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_D4"/>
<wire x1="185.42" y1="190.5" x2="180.34" y2="190.5" width="0.1524" layer="91"/>
<label x="180.34" y="190.5" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D5" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_D5"/>
<wire x1="185.42" y1="187.96" x2="180.34" y2="187.96" width="0.1524" layer="91"/>
<label x="180.34" y="187.96" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D6" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_D6"/>
<wire x1="185.42" y1="167.64" x2="180.34" y2="167.64" width="0.1524" layer="91"/>
<label x="180.34" y="167.64" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D7" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_D7"/>
<wire x1="185.42" y1="165.1" x2="180.34" y2="165.1" width="0.1524" layer="91"/>
<label x="180.34" y="165.1" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D9" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_D9"/>
<wire x1="185.42" y1="264.16" x2="180.34" y2="264.16" width="0.1524" layer="91"/>
<label x="180.34" y="264.16" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D10" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_D10"/>
<wire x1="185.42" y1="261.62" x2="180.34" y2="261.62" width="0.1524" layer="91"/>
<label x="180.34" y="261.62" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D11" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_D11"/>
<wire x1="185.42" y1="259.08" x2="180.34" y2="259.08" width="0.1524" layer="91"/>
<label x="180.34" y="259.08" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D12" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_D12"/>
<wire x1="185.42" y1="256.54" x2="180.34" y2="256.54" width="0.1524" layer="91"/>
<label x="180.34" y="256.54" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D13" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_D13"/>
<wire x1="185.42" y1="236.22" x2="180.34" y2="236.22" width="0.1524" layer="91"/>
<label x="180.34" y="236.22" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D14" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_D14"/>
<wire x1="185.42" y1="233.68" x2="180.34" y2="233.68" width="0.1524" layer="91"/>
<label x="180.34" y="233.68" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_CLK" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_CK"/>
<wire x1="71.12" y1="193.04" x2="66.04" y2="193.04" width="0.1524" layer="91"/>
<label x="66.04" y="193.04" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_CLKN" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_CKN"/>
<wire x1="71.12" y1="195.58" x2="66.04" y2="195.58" width="0.1524" layer="91"/>
<label x="66.04" y="195.58" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_CKE" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_CKE"/>
<wire x1="134.62" y1="203.2" x2="139.7" y2="203.2" width="0.1524" layer="91"/>
<label x="139.7" y="203.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_ODT" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_ODT"/>
<wire x1="134.62" y1="198.12" x2="139.7" y2="198.12" width="0.1524" layer="91"/>
<label x="139.7" y="198.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_RASN" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_RASN"/>
<wire x1="134.62" y1="205.74" x2="139.7" y2="205.74" width="0.1524" layer="91"/>
<label x="139.7" y="205.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_CSN" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_CSN0"/>
<wire x1="134.62" y1="223.52" x2="139.7" y2="223.52" width="0.1524" layer="91"/>
<label x="139.7" y="223.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_CASN" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_CASN"/>
<wire x1="134.62" y1="175.26" x2="139.7" y2="175.26" width="0.1524" layer="91"/>
<label x="139.7" y="175.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_WEN" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_WEN"/>
<wire x1="71.12" y1="238.76" x2="66.04" y2="238.76" width="0.1524" layer="91"/>
<label x="66.04" y="238.76" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_DQM0" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_DQM0"/>
<wire x1="185.42" y1="215.9" x2="180.34" y2="215.9" width="0.1524" layer="91"/>
<label x="180.34" y="215.9" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_DQSN0" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_DQSN0"/>
<wire x1="185.42" y1="170.18" x2="180.34" y2="170.18" width="0.1524" layer="91"/>
<label x="180.34" y="170.18" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_DQM1" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_DQM1"/>
<wire x1="134.62" y1="246.38" x2="139.7" y2="246.38" width="0.1524" layer="91"/>
<label x="139.7" y="246.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_DQS1" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_DQS1"/>
<wire x1="185.42" y1="241.3" x2="180.34" y2="241.3" width="0.1524" layer="91"/>
<label x="180.34" y="241.3" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_DQSN1" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_DQSN1"/>
<wire x1="185.42" y1="238.76" x2="180.34" y2="238.76" width="0.1524" layer="91"/>
<label x="180.34" y="238.76" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_NRST" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_RESETN"/>
<wire x1="134.62" y1="200.66" x2="139.7" y2="200.66" width="0.1524" layer="91"/>
<label x="139.7" y="200.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_VTP" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_VTP"/>
<wire x1="134.62" y1="248.92" x2="139.7" y2="248.92" width="0.1524" layer="91"/>
<label x="139.7" y="248.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R106" gate="G$1" pin="2"/>
<wire x1="30.48" y1="251.46" x2="30.48" y2="254" width="0.1524" layer="91"/>
<label x="30.48" y="254" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_VREF" class="0">
<segment>
<pinref part="U1" gate="A" pin="DDR_VREF"/>
<wire x1="134.62" y1="251.46" x2="139.7" y2="251.46" width="0.1524" layer="91"/>
<label x="139.7" y="251.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="PMIC_PGOOD" class="0">
<segment>
<pinref part="U1" gate="C" pin="PWRONRSTN"/>
<wire x1="71.12" y1="101.6" x2="66.04" y2="101.6" width="0.1524" layer="91"/>
<label x="66.04" y="101.6" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MMC1_CLK" class="0">
<segment>
<pinref part="U1" gate="B" pin="GPMC_CSN1"/>
<wire x1="248.92" y1="241.3" x2="254" y2="241.3" width="0.1524" layer="91"/>
<label x="254" y="241.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC1_CMD" class="0">
<segment>
<pinref part="U1" gate="B" pin="GPMC_CSN2"/>
<wire x1="248.92" y1="264.16" x2="254" y2="264.16" width="0.1524" layer="91"/>
<label x="254" y="264.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC1_DAT0" class="0">
<segment>
<pinref part="U1" gate="B" pin="GPMC_AD0"/>
<wire x1="248.92" y1="236.22" x2="254" y2="236.22" width="0.1524" layer="91"/>
<label x="254" y="236.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC1_DAT1" class="0">
<segment>
<pinref part="U1" gate="B" pin="GPMC_AD1"/>
<wire x1="248.92" y1="259.08" x2="254" y2="259.08" width="0.1524" layer="91"/>
<label x="254" y="259.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC1_DAT2" class="0">
<segment>
<pinref part="U1" gate="B" pin="GPMC_AD2"/>
<wire x1="248.92" y1="193.04" x2="254" y2="193.04" width="0.1524" layer="91"/>
<label x="254" y="193.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC1_DAT3" class="0">
<segment>
<pinref part="U1" gate="B" pin="GPMC_AD3"/>
<wire x1="248.92" y1="215.9" x2="254" y2="215.9" width="0.1524" layer="91"/>
<label x="254" y="215.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC1_DAT4" class="0">
<segment>
<pinref part="U1" gate="B" pin="GPMC_AD4"/>
<wire x1="248.92" y1="238.76" x2="254" y2="238.76" width="0.1524" layer="91"/>
<label x="254" y="238.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC1_DAT5" class="0">
<segment>
<pinref part="U1" gate="B" pin="GPMC_AD5"/>
<wire x1="248.92" y1="261.62" x2="254" y2="261.62" width="0.1524" layer="91"/>
<label x="254" y="261.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC1_DAT6" class="0">
<segment>
<pinref part="U1" gate="B" pin="GPMC_AD6"/>
<wire x1="248.92" y1="195.58" x2="254" y2="195.58" width="0.1524" layer="91"/>
<label x="254" y="195.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC1_DAT7" class="0">
<segment>
<pinref part="U1" gate="B" pin="GPMC_AD7"/>
<wire x1="248.92" y1="218.44" x2="254" y2="218.44" width="0.1524" layer="91"/>
<label x="254" y="218.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC0_CLK" class="0">
<segment>
<pinref part="U1" gate="C" pin="MMC0_CLK"/>
<wire x1="134.62" y1="88.9" x2="139.7" y2="88.9" width="0.1524" layer="91"/>
<label x="139.7" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC0_CMD" class="0">
<segment>
<pinref part="U1" gate="C" pin="MMC0_CMD"/>
<wire x1="134.62" y1="91.44" x2="139.7" y2="91.44" width="0.1524" layer="91"/>
<label x="139.7" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC0_DAT0" class="0">
<segment>
<pinref part="U1" gate="C" pin="MMC0_DAT0"/>
<wire x1="134.62" y1="86.36" x2="139.7" y2="86.36" width="0.1524" layer="91"/>
<label x="139.7" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC0_DAT1" class="0">
<segment>
<pinref part="U1" gate="C" pin="MMC0_DAT1"/>
<wire x1="134.62" y1="83.82" x2="139.7" y2="83.82" width="0.1524" layer="91"/>
<label x="139.7" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC0_DAT2" class="0">
<segment>
<pinref part="U1" gate="C" pin="MMC0_DAT2"/>
<wire x1="134.62" y1="68.58" x2="139.7" y2="68.58" width="0.1524" layer="91"/>
<label x="139.7" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC0_DAT3" class="0">
<segment>
<pinref part="U1" gate="C" pin="MMC0_DAT3"/>
<wire x1="134.62" y1="66.04" x2="139.7" y2="66.04" width="0.1524" layer="91"/>
<label x="139.7" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="AIN7" class="0">
<segment>
<pinref part="U1" gate="A" pin="AIN7"/>
<label x="63.5" y="198.12" size="1.778" layer="95"/>
<wire x1="71.12" y1="198.12" x2="45.72" y2="198.12" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="2"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="45.72" y1="198.12" x2="45.72" y2="195.58" width="0.1524" layer="91"/>
<junction x="45.72" y="198.12"/>
</segment>
</net>
<net name="MMC0_CD" class="0">
<segment>
<pinref part="U1" gate="C" pin="SPI0_CS1"/>
<wire x1="71.12" y1="78.74" x2="66.04" y2="78.74" width="0.1524" layer="91"/>
<label x="66.04" y="78.74" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="USB1_ID" class="0">
<segment>
<pinref part="U1" gate="D" pin="USB1_ID"/>
<label x="254" y="43.18" size="1.778" layer="95"/>
<wire x1="248.92" y1="43.18" x2="269.24" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="1"/>
</segment>
</net>
<net name="USB1_DP" class="0">
<segment>
<pinref part="U1" gate="D" pin="USB1_DP"/>
<wire x1="248.92" y1="66.04" x2="254" y2="66.04" width="0.1524" layer="91"/>
<label x="254" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="USB1_DN" class="0">
<segment>
<pinref part="U1" gate="D" pin="USB1_DM"/>
<wire x1="248.92" y1="68.58" x2="254" y2="68.58" width="0.1524" layer="91"/>
<label x="254" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="USB1_DRVVBUS" class="0">
<segment>
<pinref part="U1" gate="C" pin="USB1_DRVVBUS"/>
<wire x1="134.62" y1="60.96" x2="139.7" y2="60.96" width="0.1524" layer="91"/>
<label x="139.7" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="USB1_VBUS" class="0">
<segment>
<pinref part="U1" gate="D" pin="USB1_VBUS"/>
<wire x1="248.92" y1="91.44" x2="254" y2="91.44" width="0.1524" layer="91"/>
<label x="254" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII1_TXD2" class="0">
<segment>
<pinref part="U1" gate="D" pin="MII1_TXD2"/>
<wire x1="190.5" y1="124.46" x2="185.42" y2="124.46" width="0.1524" layer="91"/>
<label x="185.42" y="124.46" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_TXD1" class="0">
<segment>
<pinref part="U1" gate="D" pin="MII1_TXD1"/>
<wire x1="190.5" y1="121.92" x2="185.42" y2="121.92" width="0.1524" layer="91"/>
<label x="185.42" y="121.92" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_TXD0" class="0">
<segment>
<pinref part="U1" gate="D" pin="MII1_TXD0"/>
<wire x1="190.5" y1="119.38" x2="185.42" y2="119.38" width="0.1524" layer="91"/>
<label x="185.42" y="119.38" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_TX_EN" class="0">
<segment>
<pinref part="U1" gate="C" pin="MII1_TX_EN"/>
<wire x1="134.62" y1="132.08" x2="139.7" y2="132.08" width="0.1524" layer="91"/>
<label x="139.7" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII1_CRS_DV" class="0">
<segment>
<pinref part="U1" gate="C" pin="MII1_CRS"/>
<wire x1="134.62" y1="111.76" x2="139.7" y2="111.76" width="0.1524" layer="91"/>
<label x="139.7" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII1_COL" class="0">
<segment>
<pinref part="U1" gate="C" pin="MII1_COL"/>
<wire x1="134.62" y1="109.22" x2="139.7" y2="109.22" width="0.1524" layer="91"/>
<label x="139.7" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII1_TXD3" class="0">
<segment>
<pinref part="U1" gate="C" pin="MII1_TXD3"/>
<wire x1="134.62" y1="137.16" x2="139.7" y2="137.16" width="0.1524" layer="91"/>
<label x="139.7" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII1_RXD3" class="0">
<segment>
<pinref part="U1" gate="D" pin="MII1_RXD3"/>
<wire x1="190.5" y1="96.52" x2="185.42" y2="96.52" width="0.1524" layer="91"/>
<label x="185.42" y="96.52" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_RXD2" class="0">
<segment>
<pinref part="U1" gate="D" pin="MII1_RXD2"/>
<wire x1="190.5" y1="99.06" x2="185.42" y2="99.06" width="0.1524" layer="91"/>
<label x="185.42" y="99.06" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_RXD1" class="0">
<segment>
<pinref part="U1" gate="D" pin="MII1_RXD1"/>
<wire x1="190.5" y1="101.6" x2="185.42" y2="101.6" width="0.1524" layer="91"/>
<label x="185.42" y="101.6" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_RXD0" class="0">
<segment>
<pinref part="U1" gate="D" pin="MII1_RXD0"/>
<wire x1="190.5" y1="76.2" x2="185.42" y2="76.2" width="0.1524" layer="91"/>
<label x="185.42" y="76.2" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_RXERR" class="0">
<segment>
<pinref part="U1" gate="C" pin="MII1_RX_ER"/>
<wire x1="134.62" y1="129.54" x2="139.7" y2="129.54" width="0.1524" layer="91"/>
<label x="139.7" y="129.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII1_RXDV" class="0">
<segment>
<pinref part="U1" gate="C" pin="MII1_RX_DV"/>
<wire x1="134.62" y1="134.62" x2="139.7" y2="134.62" width="0.1524" layer="91"/>
<label x="139.7" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="RMII1_REF_CLK" class="0">
<segment>
<pinref part="U1" gate="C" pin="RMII1_REF_CLK"/>
<wire x1="134.62" y1="114.3" x2="139.7" y2="114.3" width="0.1524" layer="91"/>
<label x="139.7" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="MDIO_CLK" class="0">
<segment>
<pinref part="U1" gate="D" pin="MDC"/>
<wire x1="190.5" y1="71.12" x2="185.42" y2="71.12" width="0.1524" layer="91"/>
<label x="185.42" y="71.12" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MDIO_DATA" class="0">
<segment>
<pinref part="U1" gate="D" pin="MDIO"/>
<wire x1="190.5" y1="73.66" x2="185.42" y2="73.66" width="0.1524" layer="91"/>
<label x="185.42" y="73.66" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VDD_MPU_MON" class="0">
<segment>
<pinref part="U1" gate="A" pin="VDD_MPU_MON"/>
<label x="71.12" y="261.62" size="1.778" layer="95" rot="R180"/>
<pinref part="H3" gate="PIN" pin="1"/>
<wire x1="71.12" y1="261.62" x2="50.8" y2="261.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CAP_VDD_SRAM_CORE" class="0">
<segment>
<pinref part="U1" gate="A" pin="CAP_VDD_SRAM_CORE"/>
<pinref part="C101" gate="G$1" pin="1"/>
<wire x1="71.12" y1="175.26" x2="45.72" y2="175.26" width="0.1524" layer="91"/>
<wire x1="45.72" y1="175.26" x2="45.72" y2="172.72" width="0.1524" layer="91"/>
<label x="45.72" y="175.26" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="CAP_VDD_SRAM_MPU" class="0">
<segment>
<wire x1="30.48" y1="63.5" x2="30.48" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C102" gate="G$1" pin="1"/>
<label x="30.48" y="63.5" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="CAP_VDD_SRAM_MPU"/>
<wire x1="71.12" y1="66.04" x2="68.58" y2="66.04" width="0.1524" layer="91"/>
<label x="68.58" y="66.04" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="CAP_VBB_MPU" class="0">
<segment>
<wire x1="17.78" y1="58.42" x2="17.78" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C103" gate="G$1" pin="1"/>
<label x="17.78" y="58.42" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U1" gate="C" pin="CAP_VBB_MPU"/>
<wire x1="71.12" y1="91.44" x2="68.58" y2="91.44" width="0.1524" layer="91"/>
<label x="68.58" y="91.44" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="ENZ_KALDO_1P8V" class="0">
<segment>
<pinref part="U1" gate="A" pin="RTC_KALDO_ENN"/>
<wire x1="71.12" y1="233.68" x2="43.18" y2="233.68" width="0.1524" layer="91"/>
<pinref part="R27" gate="G$1" pin="2"/>
<label x="43.18" y="233.68" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="SYSBOOT_15" class="0">
<segment>
<pinref part="U1" gate="B" pin="LCD_DATA15"/>
<wire x1="248.92" y1="208.28" x2="254" y2="208.28" width="0.1524" layer="91"/>
<label x="254" y="208.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_14" class="0">
<segment>
<pinref part="U1" gate="B" pin="LCD_DATA14"/>
<wire x1="248.92" y1="251.46" x2="254" y2="251.46" width="0.1524" layer="91"/>
<label x="254" y="251.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_13" class="0">
<segment>
<pinref part="U1" gate="B" pin="LCD_DATA13"/>
<wire x1="248.92" y1="248.92" x2="254" y2="248.92" width="0.1524" layer="91"/>
<label x="254" y="248.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_12" class="0">
<segment>
<pinref part="U1" gate="B" pin="LCD_DATA12"/>
<wire x1="248.92" y1="246.38" x2="254" y2="246.38" width="0.1524" layer="91"/>
<label x="254" y="246.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_11" class="0">
<segment>
<pinref part="U1" gate="B" pin="LCD_DATA11"/>
<wire x1="248.92" y1="228.6" x2="254" y2="228.6" width="0.1524" layer="91"/>
<label x="254" y="228.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_10" class="0">
<segment>
<pinref part="U1" gate="B" pin="LCD_DATA10"/>
<wire x1="248.92" y1="226.06" x2="254" y2="226.06" width="0.1524" layer="91"/>
<label x="254" y="226.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_9" class="0">
<segment>
<pinref part="U1" gate="B" pin="LCD_DATA9"/>
<wire x1="248.92" y1="223.52" x2="254" y2="223.52" width="0.1524" layer="91"/>
<label x="254" y="223.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_8" class="0">
<segment>
<pinref part="U1" gate="B" pin="LCD_DATA8"/>
<wire x1="248.92" y1="220.98" x2="254" y2="220.98" width="0.1524" layer="91"/>
<label x="254" y="220.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_7" class="0">
<segment>
<pinref part="U1" gate="B" pin="LCD_DATA7"/>
<wire x1="248.92" y1="205.74" x2="254" y2="205.74" width="0.1524" layer="91"/>
<label x="254" y="205.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_6" class="0">
<segment>
<pinref part="U1" gate="B" pin="LCD_DATA6"/>
<wire x1="248.92" y1="203.2" x2="254" y2="203.2" width="0.1524" layer="91"/>
<label x="254" y="203.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_5" class="0">
<segment>
<pinref part="U1" gate="B" pin="LCD_DATA5"/>
<wire x1="248.92" y1="200.66" x2="254" y2="200.66" width="0.1524" layer="91"/>
<label x="254" y="200.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_4" class="0">
<segment>
<pinref part="U1" gate="B" pin="LCD_DATA4"/>
<wire x1="248.92" y1="198.12" x2="254" y2="198.12" width="0.1524" layer="91"/>
<label x="254" y="198.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_3" class="0">
<segment>
<pinref part="U1" gate="B" pin="LCD_DATA3"/>
<wire x1="248.92" y1="182.88" x2="254" y2="182.88" width="0.1524" layer="91"/>
<label x="254" y="182.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_2" class="0">
<segment>
<pinref part="U1" gate="B" pin="LCD_DATA2"/>
<wire x1="248.92" y1="180.34" x2="254" y2="180.34" width="0.1524" layer="91"/>
<label x="254" y="180.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_1" class="0">
<segment>
<pinref part="U1" gate="B" pin="LCD_DATA1"/>
<wire x1="248.92" y1="177.8" x2="254" y2="177.8" width="0.1524" layer="91"/>
<label x="254" y="177.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_0" class="0">
<segment>
<pinref part="U1" gate="B" pin="LCD_DATA0"/>
<wire x1="248.92" y1="175.26" x2="254" y2="175.26" width="0.1524" layer="91"/>
<label x="254" y="175.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_DQS0" class="0">
<segment>
<pinref part="U1" gate="B" pin="DDR_DQS0"/>
<wire x1="185.42" y1="172.72" x2="180.34" y2="172.72" width="0.1524" layer="91"/>
<label x="180.34" y="172.72" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_TXCLK" class="0">
<segment>
<pinref part="U1" gate="D" pin="MII1_TX_CLK"/>
<wire x1="190.5" y1="116.84" x2="185.42" y2="116.84" width="0.1524" layer="91"/>
<label x="185.42" y="116.84" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_RXCLK" class="0">
<segment>
<pinref part="U1" gate="D" pin="MII1_RX_CLK"/>
<wire x1="190.5" y1="93.98" x2="185.42" y2="93.98" width="0.1524" layer="91"/>
<label x="185.42" y="93.98" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="UART1_TX" class="0">
<segment>
<pinref part="U1" gate="C" pin="UART1_TXD"/>
<wire x1="71.12" y1="55.88" x2="66.04" y2="55.88" width="0.1524" layer="91"/>
<label x="66.04" y="55.88" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII2_TXCLK" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_A6"/>
<wire x1="248.92" y1="106.68" x2="254" y2="106.68" width="0.1524" layer="91"/>
<label x="254" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII2_RXCLK" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_A7"/>
<wire x1="248.92" y1="83.82" x2="254" y2="83.82" width="0.1524" layer="91"/>
<label x="254" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII2_RXDV" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_A1"/>
<wire x1="248.92" y1="127" x2="254" y2="127" width="0.1524" layer="91"/>
<label x="254" y="127" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII2_TXD0" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_A5"/>
<wire x1="248.92" y1="129.54" x2="254" y2="129.54" width="0.1524" layer="91"/>
<label x="254" y="129.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="MPU_R14" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_AD10"/>
<wire x1="248.92" y1="73.66" x2="254" y2="73.66" width="0.1524" layer="91"/>
<label x="254" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII2_TXD1" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_A4"/>
<wire x1="248.92" y1="58.42" x2="254" y2="58.42" width="0.1524" layer="91"/>
<label x="254" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="LD2_HB" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_AD11"/>
<wire x1="248.92" y1="99.06" x2="254" y2="99.06" width="0.1524" layer="91"/>
<label x="254" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII2_TXD3" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_A2"/>
<wire x1="248.92" y1="104.14" x2="254" y2="104.14" width="0.1524" layer="91"/>
<label x="254" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII2_RXD0" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_A11"/>
<wire x1="248.92" y1="134.62" x2="254" y2="134.62" width="0.1524" layer="91"/>
<label x="254" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="USB1_OCN" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_AD12"/>
<wire x1="248.92" y1="76.2" x2="254" y2="76.2" width="0.1524" layer="91"/>
<label x="254" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII2_RXD1" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_A10"/>
<wire x1="248.92" y1="86.36" x2="254" y2="86.36" width="0.1524" layer="91"/>
<label x="254" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII2_RXD2" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_A9"/>
<wire x1="248.92" y1="109.22" x2="254" y2="109.22" width="0.1524" layer="91"/>
<label x="254" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII2_RXD3" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_A8"/>
<wire x1="248.92" y1="132.08" x2="254" y2="132.08" width="0.1524" layer="91"/>
<label x="254" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII2_RXER" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_WPN"/>
<wire x1="248.92" y1="111.76" x2="254" y2="111.76" width="0.1524" layer="91"/>
<label x="254" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII2_TXEN" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_A0"/>
<wire x1="248.92" y1="55.88" x2="254" y2="55.88" width="0.1524" layer="91"/>
<label x="254" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII2_COL" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_BEN1"/>
<wire x1="248.92" y1="114.3" x2="254" y2="114.3" width="0.1524" layer="91"/>
<label x="254" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII2_CRS_DV" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_WAIT0"/>
<wire x1="248.92" y1="88.9" x2="254" y2="88.9" width="0.1524" layer="91"/>
<label x="254" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="LD3_USR1" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_AD13"/>
<wire x1="248.92" y1="53.34" x2="254" y2="53.34" width="0.1524" layer="91"/>
<label x="254" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="LD4_USR2" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_AD14"/>
<wire x1="248.92" y1="124.46" x2="254" y2="124.46" width="0.1524" layer="91"/>
<label x="254" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="LD5_USR3" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_CLK"/>
<wire x1="248.92" y1="121.92" x2="254" y2="121.92" width="0.1524" layer="91"/>
<label x="254" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="LD6_USR4" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_AD15"/>
<wire x1="248.92" y1="101.6" x2="254" y2="101.6" width="0.1524" layer="91"/>
<label x="254" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII2_TXD2" class="0">
<segment>
<pinref part="U1" gate="D" pin="GPMC_A3"/>
<wire x1="248.92" y1="81.28" x2="254" y2="81.28" width="0.1524" layer="91"/>
<label x="254" y="81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM_FAN" class="0">
<segment>
<pinref part="U1" gate="D" pin="EHRPWM2B"/>
<wire x1="248.92" y1="71.12" x2="254" y2="71.12" width="0.1524" layer="91"/>
<label x="254" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="4.572" y="4.318" size="10.16" layer="97">PRIMARY MPU (2)</text>
<text x="21.336" y="283.21" size="3.048" layer="97">MPU CORE DECOUPLING</text>
<text x="27.94" y="246.38" size="3.048" layer="97">MPU DDR DECOUPLING</text>
<text x="185.42" y="226.06" size="3.048" layer="97">MPU DIRECT DECOUPLING</text>
<text x="25.4" y="177.8" size="3.048" layer="97">MPU 1.8V, PLL DECOUPLING</text>
<text x="27.94" y="218.44" size="3.048" layer="97">MPU 3.3V (A) DECOUPLING</text>
<text x="170.18" y="254" size="3.048" layer="97">MPU VDDS DECOUPLING</text>
<text x="183.896" y="280.416" size="3.048" layer="97">MPU ADC DECOUPLING</text>
<text x="38.1" y="119.38" size="3.048" layer="97">MPU EXT OSCILLATORS</text>
<text x="17.78" y="81.28" size="3.048" layer="97">MPU RESET BUTTON</text>
<text x="91.44" y="63.5" size="3.048" layer="97">INV BUFFER - eMMC RESET</text>
<text x="121.92" y="101.6" size="3.048" layer="97">BUFFER - MPU RESET</text>
<text x="170.18" y="63.5" size="3.048" layer="97">MPU UART0 POWER ISOLATION</text>
<text x="190.5" y="193.04" size="2.032" layer="97" rot="MR180">System Boot Settings:
Value: 0x403C

[15:14] : 01b = 24Mhz
[13:6] : Reserved/Don't Care
[5] : 1 = CLKOUT1 Disabled
[4:0] : 11100b = Boot Order (1st -&gt; Last):
     MMC1 -&gt; MMC0 -&gt; UART0 -&gt; USB0
SW3 Pressed = 11000b (SPI0 NC)
    SPI0 -&gt; MMC0 -&gt; USB0 -&gt; UART0</text>
<text x="116.84" y="139.7" size="3.048" layer="97">MPU RTC DECOUPLING</text>
<text x="185.42" y="195.58" size="3.048" layer="97">MPU SYSTEM BOOT SETUP</text>
<text x="5.588" y="19.812" size="2.54" layer="97">Linux Micro-PC
Rev 1.0
SBS 2015</text>
</plain>
<instances>
<instance part="C23" gate="G$1" x="27.94" y="264.16"/>
<instance part="C24" gate="G$1" x="40.64" y="264.16"/>
<instance part="C25" gate="G$1" x="53.34" y="264.16"/>
<instance part="C26" gate="G$1" x="66.04" y="264.16"/>
<instance part="C27" gate="G$1" x="78.74" y="264.16"/>
<instance part="C28" gate="G$1" x="91.44" y="264.16"/>
<instance part="C29" gate="G$1" x="104.14" y="264.16"/>
<instance part="C30" gate="G$1" x="116.84" y="264.16"/>
<instance part="C31" gate="G$1" x="129.54" y="264.16"/>
<instance part="C32" gate="G$1" x="27.94" y="274.32" rot="R180"/>
<instance part="C33" gate="G$1" x="40.64" y="274.32" rot="R180"/>
<instance part="C34" gate="G$1" x="66.04" y="274.32" rot="R180"/>
<instance part="C35" gate="G$1" x="53.34" y="274.32" rot="R180"/>
<instance part="C36" gate="G$1" x="78.74" y="274.32" rot="R180"/>
<instance part="C37" gate="G$1" x="91.44" y="274.32" rot="R180"/>
<instance part="C38" gate="G$1" x="104.14" y="274.32" rot="R180"/>
<instance part="C39" gate="G$1" x="116.84" y="274.32" rot="R180"/>
<instance part="SUPPLY15" gate="GND" x="134.62" y="254"/>
<instance part="C40" gate="G$1" x="27.94" y="238.76"/>
<instance part="C41" gate="G$1" x="40.64" y="238.76"/>
<instance part="C42" gate="G$1" x="53.34" y="238.76"/>
<instance part="C43" gate="G$1" x="66.04" y="238.76"/>
<instance part="C44" gate="G$1" x="78.74" y="238.76"/>
<instance part="C45" gate="G$1" x="91.44" y="238.76"/>
<instance part="C46" gate="G$1" x="104.14" y="238.76"/>
<instance part="C47" gate="G$1" x="116.84" y="238.76"/>
<instance part="C48" gate="G$1" x="129.54" y="238.76"/>
<instance part="SUPPLY16" gate="GND" x="129.54" y="228.6"/>
<instance part="C49" gate="G$1" x="185.42" y="218.44"/>
<instance part="C50" gate="G$1" x="198.12" y="218.44"/>
<instance part="C51" gate="G$1" x="210.82" y="218.44"/>
<instance part="C52" gate="G$1" x="223.52" y="218.44"/>
<instance part="SUPPLY18" gate="GND" x="223.52" y="208.28"/>
<instance part="FB1" gate="G$1" x="76.2" y="172.72"/>
<instance part="C53" gate="G$1" x="27.94" y="167.64"/>
<instance part="SUPPLY19" gate="GND" x="27.94" y="157.48"/>
<instance part="C54" gate="G$1" x="91.44" y="167.64"/>
<instance part="C55" gate="G$1" x="40.64" y="167.64"/>
<instance part="C56" gate="G$1" x="53.34" y="167.64"/>
<instance part="FB2" gate="G$1" x="68.58" y="162.56" rot="R270"/>
<instance part="C57" gate="G$1" x="68.58" y="147.32"/>
<instance part="SUPPLY21" gate="GND" x="68.58" y="139.7"/>
<instance part="C58" gate="G$1" x="116.84" y="167.64"/>
<instance part="C59" gate="G$1" x="104.14" y="167.64"/>
<instance part="C60" gate="G$1" x="129.54" y="167.64"/>
<instance part="SUPPLY22" gate="GND" x="129.54" y="157.48"/>
<instance part="C61" gate="G$1" x="40.64" y="198.12"/>
<instance part="C62" gate="G$1" x="53.34" y="198.12"/>
<instance part="C63" gate="G$1" x="66.04" y="198.12"/>
<instance part="C64" gate="G$1" x="78.74" y="198.12"/>
<instance part="C65" gate="G$1" x="91.44" y="198.12"/>
<instance part="C66" gate="G$1" x="104.14" y="198.12"/>
<instance part="C67" gate="G$1" x="116.84" y="198.12"/>
<instance part="C68" gate="G$1" x="129.54" y="198.12"/>
<instance part="C69" gate="G$1" x="142.24" y="198.12"/>
<instance part="C70" gate="G$1" x="154.94" y="198.12"/>
<instance part="C71" gate="G$1" x="27.94" y="208.28" rot="R180"/>
<instance part="C72" gate="G$1" x="40.64" y="208.28" rot="R180"/>
<instance part="C73" gate="G$1" x="53.34" y="208.28" rot="R180"/>
<instance part="C74" gate="G$1" x="66.04" y="208.28" rot="R180"/>
<instance part="C75" gate="G$1" x="78.74" y="208.28" rot="R180"/>
<instance part="C76" gate="G$1" x="91.44" y="208.28" rot="R180"/>
<instance part="C77" gate="G$1" x="104.14" y="208.28" rot="R180"/>
<instance part="C78" gate="G$1" x="116.84" y="208.28" rot="R180"/>
<instance part="C79" gate="G$1" x="129.54" y="208.28" rot="R180"/>
<instance part="C80" gate="G$1" x="142.24" y="208.28" rot="R180"/>
<instance part="C81" gate="G$1" x="27.94" y="198.12"/>
<instance part="SUPPLY23" gate="GND" x="160.02" y="187.96"/>
<instance part="C82" gate="G$1" x="172.72" y="246.38"/>
<instance part="C83" gate="G$1" x="185.42" y="246.38"/>
<instance part="C84" gate="G$1" x="198.12" y="246.38"/>
<instance part="C85" gate="G$1" x="210.82" y="246.38"/>
<instance part="C86" gate="G$1" x="223.52" y="246.38"/>
<instance part="C87" gate="G$1" x="236.22" y="246.38"/>
<instance part="SUPPLY20" gate="GND" x="236.22" y="236.22"/>
<instance part="C88" gate="G$1" x="185.42" y="271.78"/>
<instance part="C89" gate="G$1" x="198.12" y="271.78"/>
<instance part="C90" gate="G$1" x="210.82" y="271.78"/>
<instance part="R12" gate="G$1" x="177.8" y="276.86"/>
<instance part="FB3" gate="G$1" x="243.84" y="264.16"/>
<instance part="SUPPLY24" gate="GND" x="254" y="259.08"/>
<instance part="C91" gate="G$1" x="226.06" y="271.78"/>
<instance part="C92" gate="G$1" x="127" y="127"/>
<instance part="C93" gate="G$1" x="142.24" y="127"/>
<instance part="SUPPLY25" gate="GND" x="134.62" y="116.84"/>
<instance part="Y1" gate="G$1" x="30.48" y="114.3"/>
<instance part="C94" gate="G$1" x="22.86" y="109.22"/>
<instance part="C95" gate="G$1" x="38.1" y="109.22"/>
<instance part="Y2" gate="G$1" x="81.28" y="99.06"/>
<instance part="C96" gate="G$1" x="73.66" y="93.98"/>
<instance part="C97" gate="G$1" x="88.9" y="93.98"/>
<instance part="R15" gate="G$1" x="81.28" y="109.22"/>
<instance part="U9" gate="G$1" x="142.24" y="88.9"/>
<instance part="U10" gate="G$1" x="116.84" y="50.8"/>
<instance part="U11" gate="G$1" x="203.2" y="48.26"/>
<instance part="SW2" gate="G$1" x="30.48" y="60.96"/>
<instance part="C98" gate="G$1" x="40.64" y="55.88"/>
<instance part="SUPPLY27" gate="GND" x="25.4" y="58.42"/>
<instance part="R16" gate="G$1" x="40.64" y="68.58" rot="R90"/>
<instance part="SUPPLY28" gate="GND" x="40.64" y="48.26"/>
<instance part="SUPPLY29" gate="GND" x="101.6" y="43.18"/>
<instance part="C99" gate="G$1" x="88.9" y="50.8"/>
<instance part="R17" gate="G$1" x="134.62" y="38.1" rot="R90"/>
<instance part="SUPPLY30" gate="GND" x="127" y="81.28"/>
<instance part="R18" gate="G$1" x="152.4" y="134.62"/>
<instance part="C100" gate="G$1" x="172.72" y="45.72"/>
<instance part="SUPPLY31" gate="GND" x="177.8" y="35.56"/>
<instance part="SUPPLY32" gate="GND" x="246.38" y="30.48"/>
<instance part="R19" gate="G$1" x="246.38" y="40.64" rot="R90"/>
<instance part="R28" gate="G$1" x="213.36" y="149.86" rot="R90"/>
<instance part="R29" gate="G$1" x="243.84" y="149.86" rot="R90"/>
<instance part="R30" gate="G$1" x="236.22" y="149.86" rot="R90"/>
<instance part="R31" gate="G$1" x="228.6" y="149.86" rot="R90"/>
<instance part="R32" gate="G$1" x="220.98" y="149.86" rot="R90"/>
<instance part="R33" gate="G$1" x="195.58" y="96.52" rot="R90"/>
<instance part="R34" gate="G$1" x="200.66" y="86.36" rot="R90"/>
<instance part="R35" gate="G$1" x="246.38" y="96.52" rot="R90"/>
<instance part="R36" gate="G$1" x="241.3" y="86.36" rot="R90"/>
<instance part="R37" gate="G$1" x="236.22" y="96.52" rot="R90"/>
<instance part="R38" gate="G$1" x="231.14" y="86.36" rot="R90"/>
<instance part="R39" gate="G$1" x="226.06" y="96.52" rot="R90"/>
<instance part="R40" gate="G$1" x="220.98" y="86.36" rot="R90"/>
<instance part="R41" gate="G$1" x="215.9" y="96.52" rot="R90"/>
<instance part="R42" gate="G$1" x="210.82" y="86.36" rot="R90"/>
<instance part="R43" gate="G$1" x="205.74" y="96.52" rot="R90"/>
<instance part="SUPPLY39" gate="GND" x="246.38" y="76.2"/>
<instance part="R44" gate="G$1" x="205.74" y="142.24"/>
<instance part="SW3" gate="G$1" x="193.04" y="142.24"/>
<instance part="SUPPLY40" gate="GND" x="187.96" y="139.7"/>
<instance part="SUPPLY70" gate="GND" x="30.48" y="96.52"/>
<instance part="SUPPLY71" gate="GND" x="81.28" y="83.82"/>
<instance part="U$1" gate="G$1" x="256.54" y="45.72" rot="MR0"/>
<instance part="SUPPLY84" gate="GND" x="88.9" y="43.18"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="27.94" y1="259.08" x2="27.94" y2="256.54" width="0.1524" layer="91"/>
<wire x1="27.94" y1="256.54" x2="40.64" y2="256.54" width="0.1524" layer="91"/>
<wire x1="40.64" y1="256.54" x2="53.34" y2="256.54" width="0.1524" layer="91"/>
<wire x1="53.34" y1="256.54" x2="66.04" y2="256.54" width="0.1524" layer="91"/>
<wire x1="66.04" y1="256.54" x2="78.74" y2="256.54" width="0.1524" layer="91"/>
<wire x1="78.74" y1="256.54" x2="91.44" y2="256.54" width="0.1524" layer="91"/>
<wire x1="91.44" y1="256.54" x2="104.14" y2="256.54" width="0.1524" layer="91"/>
<wire x1="104.14" y1="256.54" x2="116.84" y2="256.54" width="0.1524" layer="91"/>
<wire x1="116.84" y1="256.54" x2="129.54" y2="256.54" width="0.1524" layer="91"/>
<wire x1="129.54" y1="256.54" x2="134.62" y2="256.54" width="0.1524" layer="91"/>
<wire x1="134.62" y1="256.54" x2="134.62" y2="281.94" width="0.1524" layer="91"/>
<pinref part="C32" gate="G$1" pin="2"/>
<wire x1="134.62" y1="281.94" x2="116.84" y2="281.94" width="0.1524" layer="91"/>
<wire x1="116.84" y1="281.94" x2="104.14" y2="281.94" width="0.1524" layer="91"/>
<wire x1="104.14" y1="281.94" x2="91.44" y2="281.94" width="0.1524" layer="91"/>
<wire x1="91.44" y1="281.94" x2="78.74" y2="281.94" width="0.1524" layer="91"/>
<wire x1="78.74" y1="281.94" x2="66.04" y2="281.94" width="0.1524" layer="91"/>
<wire x1="66.04" y1="281.94" x2="53.34" y2="281.94" width="0.1524" layer="91"/>
<wire x1="53.34" y1="281.94" x2="40.64" y2="281.94" width="0.1524" layer="91"/>
<wire x1="40.64" y1="281.94" x2="27.94" y2="281.94" width="0.1524" layer="91"/>
<wire x1="27.94" y1="281.94" x2="27.94" y2="279.4" width="0.1524" layer="91"/>
<pinref part="C33" gate="G$1" pin="2"/>
<wire x1="40.64" y1="279.4" x2="40.64" y2="281.94" width="0.1524" layer="91"/>
<junction x="40.64" y="281.94"/>
<pinref part="C35" gate="G$1" pin="2"/>
<wire x1="53.34" y1="279.4" x2="53.34" y2="281.94" width="0.1524" layer="91"/>
<junction x="53.34" y="281.94"/>
<pinref part="C34" gate="G$1" pin="2"/>
<wire x1="66.04" y1="279.4" x2="66.04" y2="281.94" width="0.1524" layer="91"/>
<junction x="66.04" y="281.94"/>
<pinref part="C36" gate="G$1" pin="2"/>
<wire x1="78.74" y1="279.4" x2="78.74" y2="281.94" width="0.1524" layer="91"/>
<junction x="78.74" y="281.94"/>
<pinref part="C37" gate="G$1" pin="2"/>
<wire x1="91.44" y1="279.4" x2="91.44" y2="281.94" width="0.1524" layer="91"/>
<junction x="91.44" y="281.94"/>
<pinref part="C38" gate="G$1" pin="2"/>
<wire x1="104.14" y1="279.4" x2="104.14" y2="281.94" width="0.1524" layer="91"/>
<junction x="104.14" y="281.94"/>
<pinref part="C39" gate="G$1" pin="2"/>
<wire x1="116.84" y1="279.4" x2="116.84" y2="281.94" width="0.1524" layer="91"/>
<junction x="116.84" y="281.94"/>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="129.54" y1="259.08" x2="129.54" y2="256.54" width="0.1524" layer="91"/>
<junction x="129.54" y="256.54"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="116.84" y1="259.08" x2="116.84" y2="256.54" width="0.1524" layer="91"/>
<junction x="116.84" y="256.54"/>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="104.14" y1="259.08" x2="104.14" y2="256.54" width="0.1524" layer="91"/>
<junction x="104.14" y="256.54"/>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="91.44" y1="259.08" x2="91.44" y2="256.54" width="0.1524" layer="91"/>
<junction x="91.44" y="256.54"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="78.74" y1="259.08" x2="78.74" y2="256.54" width="0.1524" layer="91"/>
<junction x="78.74" y="256.54"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="66.04" y1="259.08" x2="66.04" y2="256.54" width="0.1524" layer="91"/>
<junction x="66.04" y="256.54"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="53.34" y1="259.08" x2="53.34" y2="256.54" width="0.1524" layer="91"/>
<junction x="53.34" y="256.54"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="40.64" y1="259.08" x2="40.64" y2="256.54" width="0.1524" layer="91"/>
<junction x="40.64" y="256.54"/>
<label x="121.92" y="281.94" size="1.778" layer="95"/>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<junction x="134.62" y="256.54"/>
</segment>
<segment>
<pinref part="C40" gate="G$1" pin="2"/>
<wire x1="27.94" y1="233.68" x2="27.94" y2="231.14" width="0.1524" layer="91"/>
<pinref part="C48" gate="G$1" pin="2"/>
<wire x1="27.94" y1="231.14" x2="40.64" y2="231.14" width="0.1524" layer="91"/>
<wire x1="40.64" y1="231.14" x2="53.34" y2="231.14" width="0.1524" layer="91"/>
<wire x1="53.34" y1="231.14" x2="66.04" y2="231.14" width="0.1524" layer="91"/>
<wire x1="66.04" y1="231.14" x2="78.74" y2="231.14" width="0.1524" layer="91"/>
<wire x1="78.74" y1="231.14" x2="91.44" y2="231.14" width="0.1524" layer="91"/>
<wire x1="91.44" y1="231.14" x2="104.14" y2="231.14" width="0.1524" layer="91"/>
<wire x1="104.14" y1="231.14" x2="116.84" y2="231.14" width="0.1524" layer="91"/>
<wire x1="116.84" y1="231.14" x2="129.54" y2="231.14" width="0.1524" layer="91"/>
<wire x1="129.54" y1="231.14" x2="129.54" y2="233.68" width="0.1524" layer="91"/>
<pinref part="C47" gate="G$1" pin="2"/>
<wire x1="116.84" y1="233.68" x2="116.84" y2="231.14" width="0.1524" layer="91"/>
<junction x="116.84" y="231.14"/>
<pinref part="C46" gate="G$1" pin="2"/>
<wire x1="104.14" y1="233.68" x2="104.14" y2="231.14" width="0.1524" layer="91"/>
<junction x="104.14" y="231.14"/>
<pinref part="C45" gate="G$1" pin="2"/>
<wire x1="91.44" y1="233.68" x2="91.44" y2="231.14" width="0.1524" layer="91"/>
<junction x="91.44" y="231.14"/>
<pinref part="C44" gate="G$1" pin="2"/>
<wire x1="78.74" y1="233.68" x2="78.74" y2="231.14" width="0.1524" layer="91"/>
<junction x="78.74" y="231.14"/>
<pinref part="C43" gate="G$1" pin="2"/>
<wire x1="66.04" y1="233.68" x2="66.04" y2="231.14" width="0.1524" layer="91"/>
<junction x="66.04" y="231.14"/>
<pinref part="C42" gate="G$1" pin="2"/>
<wire x1="53.34" y1="233.68" x2="53.34" y2="231.14" width="0.1524" layer="91"/>
<junction x="53.34" y="231.14"/>
<pinref part="C41" gate="G$1" pin="2"/>
<wire x1="40.64" y1="233.68" x2="40.64" y2="231.14" width="0.1524" layer="91"/>
<junction x="40.64" y="231.14"/>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
<junction x="129.54" y="231.14"/>
</segment>
<segment>
<pinref part="C49" gate="G$1" pin="2"/>
<wire x1="185.42" y1="213.36" x2="185.42" y2="210.82" width="0.1524" layer="91"/>
<pinref part="C52" gate="G$1" pin="2"/>
<wire x1="185.42" y1="210.82" x2="198.12" y2="210.82" width="0.1524" layer="91"/>
<wire x1="198.12" y1="210.82" x2="210.82" y2="210.82" width="0.1524" layer="91"/>
<wire x1="210.82" y1="210.82" x2="223.52" y2="210.82" width="0.1524" layer="91"/>
<wire x1="223.52" y1="210.82" x2="223.52" y2="213.36" width="0.1524" layer="91"/>
<pinref part="C50" gate="G$1" pin="2"/>
<wire x1="198.12" y1="213.36" x2="198.12" y2="210.82" width="0.1524" layer="91"/>
<junction x="198.12" y="210.82"/>
<pinref part="C51" gate="G$1" pin="2"/>
<wire x1="210.82" y1="213.36" x2="210.82" y2="210.82" width="0.1524" layer="91"/>
<junction x="210.82" y="210.82"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
<junction x="223.52" y="210.82"/>
</segment>
<segment>
<pinref part="C53" gate="G$1" pin="2"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
<pinref part="C55" gate="G$1" pin="2"/>
<wire x1="27.94" y1="160.02" x2="27.94" y2="162.56" width="0.1524" layer="91"/>
<wire x1="27.94" y1="160.02" x2="40.64" y2="160.02" width="0.1524" layer="91"/>
<wire x1="40.64" y1="160.02" x2="40.64" y2="162.56" width="0.1524" layer="91"/>
<junction x="27.94" y="160.02"/>
<pinref part="C56" gate="G$1" pin="2"/>
<wire x1="40.64" y1="160.02" x2="53.34" y2="160.02" width="0.1524" layer="91"/>
<wire x1="53.34" y1="160.02" x2="53.34" y2="162.56" width="0.1524" layer="91"/>
<junction x="40.64" y="160.02"/>
</segment>
<segment>
<pinref part="C57" gate="G$1" pin="2"/>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C59" gate="G$1" pin="2"/>
<wire x1="104.14" y1="162.56" x2="104.14" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C60" gate="G$1" pin="2"/>
<wire x1="104.14" y1="160.02" x2="116.84" y2="160.02" width="0.1524" layer="91"/>
<wire x1="116.84" y1="160.02" x2="129.54" y2="160.02" width="0.1524" layer="91"/>
<wire x1="129.54" y1="160.02" x2="129.54" y2="162.56" width="0.1524" layer="91"/>
<pinref part="C58" gate="G$1" pin="2"/>
<wire x1="116.84" y1="162.56" x2="116.84" y2="160.02" width="0.1524" layer="91"/>
<junction x="116.84" y="160.02"/>
<pinref part="SUPPLY22" gate="GND" pin="GND"/>
<junction x="129.54" y="160.02"/>
<pinref part="C54" gate="G$1" pin="2"/>
<wire x1="91.44" y1="162.56" x2="91.44" y2="160.02" width="0.1524" layer="91"/>
<wire x1="91.44" y1="160.02" x2="104.14" y2="160.02" width="0.1524" layer="91"/>
<junction x="104.14" y="160.02"/>
</segment>
<segment>
<pinref part="C71" gate="G$1" pin="2"/>
<wire x1="27.94" y1="213.36" x2="27.94" y2="215.9" width="0.1524" layer="91"/>
<wire x1="27.94" y1="215.9" x2="40.64" y2="215.9" width="0.1524" layer="91"/>
<wire x1="40.64" y1="215.9" x2="53.34" y2="215.9" width="0.1524" layer="91"/>
<wire x1="53.34" y1="215.9" x2="66.04" y2="215.9" width="0.1524" layer="91"/>
<wire x1="66.04" y1="215.9" x2="78.74" y2="215.9" width="0.1524" layer="91"/>
<wire x1="78.74" y1="215.9" x2="91.44" y2="215.9" width="0.1524" layer="91"/>
<wire x1="91.44" y1="215.9" x2="104.14" y2="215.9" width="0.1524" layer="91"/>
<wire x1="104.14" y1="215.9" x2="116.84" y2="215.9" width="0.1524" layer="91"/>
<wire x1="116.84" y1="215.9" x2="129.54" y2="215.9" width="0.1524" layer="91"/>
<wire x1="129.54" y1="215.9" x2="142.24" y2="215.9" width="0.1524" layer="91"/>
<wire x1="142.24" y1="215.9" x2="160.02" y2="215.9" width="0.1524" layer="91"/>
<wire x1="160.02" y1="215.9" x2="160.02" y2="190.5" width="0.1524" layer="91"/>
<pinref part="C81" gate="G$1" pin="2"/>
<wire x1="160.02" y1="190.5" x2="154.94" y2="190.5" width="0.1524" layer="91"/>
<wire x1="154.94" y1="190.5" x2="142.24" y2="190.5" width="0.1524" layer="91"/>
<wire x1="142.24" y1="190.5" x2="129.54" y2="190.5" width="0.1524" layer="91"/>
<wire x1="129.54" y1="190.5" x2="116.84" y2="190.5" width="0.1524" layer="91"/>
<wire x1="116.84" y1="190.5" x2="104.14" y2="190.5" width="0.1524" layer="91"/>
<wire x1="104.14" y1="190.5" x2="91.44" y2="190.5" width="0.1524" layer="91"/>
<wire x1="91.44" y1="190.5" x2="78.74" y2="190.5" width="0.1524" layer="91"/>
<wire x1="78.74" y1="190.5" x2="66.04" y2="190.5" width="0.1524" layer="91"/>
<wire x1="66.04" y1="190.5" x2="53.34" y2="190.5" width="0.1524" layer="91"/>
<wire x1="53.34" y1="190.5" x2="40.64" y2="190.5" width="0.1524" layer="91"/>
<wire x1="40.64" y1="190.5" x2="27.94" y2="190.5" width="0.1524" layer="91"/>
<wire x1="27.94" y1="190.5" x2="27.94" y2="193.04" width="0.1524" layer="91"/>
<pinref part="C61" gate="G$1" pin="2"/>
<wire x1="40.64" y1="193.04" x2="40.64" y2="190.5" width="0.1524" layer="91"/>
<junction x="40.64" y="190.5"/>
<pinref part="C62" gate="G$1" pin="2"/>
<wire x1="53.34" y1="193.04" x2="53.34" y2="190.5" width="0.1524" layer="91"/>
<junction x="53.34" y="190.5"/>
<pinref part="C63" gate="G$1" pin="2"/>
<wire x1="66.04" y1="193.04" x2="66.04" y2="190.5" width="0.1524" layer="91"/>
<junction x="66.04" y="190.5"/>
<pinref part="C64" gate="G$1" pin="2"/>
<wire x1="78.74" y1="193.04" x2="78.74" y2="190.5" width="0.1524" layer="91"/>
<junction x="78.74" y="190.5"/>
<pinref part="C65" gate="G$1" pin="2"/>
<wire x1="91.44" y1="193.04" x2="91.44" y2="190.5" width="0.1524" layer="91"/>
<junction x="91.44" y="190.5"/>
<pinref part="C66" gate="G$1" pin="2"/>
<wire x1="104.14" y1="193.04" x2="104.14" y2="190.5" width="0.1524" layer="91"/>
<junction x="104.14" y="190.5"/>
<pinref part="C67" gate="G$1" pin="2"/>
<wire x1="116.84" y1="193.04" x2="116.84" y2="190.5" width="0.1524" layer="91"/>
<junction x="116.84" y="190.5"/>
<pinref part="C68" gate="G$1" pin="2"/>
<wire x1="129.54" y1="193.04" x2="129.54" y2="190.5" width="0.1524" layer="91"/>
<junction x="129.54" y="190.5"/>
<pinref part="C69" gate="G$1" pin="2"/>
<wire x1="142.24" y1="193.04" x2="142.24" y2="190.5" width="0.1524" layer="91"/>
<junction x="142.24" y="190.5"/>
<pinref part="C70" gate="G$1" pin="2"/>
<wire x1="154.94" y1="193.04" x2="154.94" y2="190.5" width="0.1524" layer="91"/>
<junction x="154.94" y="190.5"/>
<pinref part="C72" gate="G$1" pin="2"/>
<wire x1="40.64" y1="213.36" x2="40.64" y2="215.9" width="0.1524" layer="91"/>
<junction x="40.64" y="215.9"/>
<pinref part="C73" gate="G$1" pin="2"/>
<wire x1="53.34" y1="213.36" x2="53.34" y2="215.9" width="0.1524" layer="91"/>
<junction x="53.34" y="215.9"/>
<pinref part="C74" gate="G$1" pin="2"/>
<wire x1="66.04" y1="213.36" x2="66.04" y2="215.9" width="0.1524" layer="91"/>
<junction x="66.04" y="215.9"/>
<pinref part="C75" gate="G$1" pin="2"/>
<wire x1="78.74" y1="213.36" x2="78.74" y2="215.9" width="0.1524" layer="91"/>
<junction x="78.74" y="215.9"/>
<pinref part="C76" gate="G$1" pin="2"/>
<wire x1="91.44" y1="213.36" x2="91.44" y2="215.9" width="0.1524" layer="91"/>
<junction x="91.44" y="215.9"/>
<pinref part="C77" gate="G$1" pin="2"/>
<wire x1="104.14" y1="213.36" x2="104.14" y2="215.9" width="0.1524" layer="91"/>
<junction x="104.14" y="215.9"/>
<pinref part="C78" gate="G$1" pin="2"/>
<wire x1="116.84" y1="213.36" x2="116.84" y2="215.9" width="0.1524" layer="91"/>
<junction x="116.84" y="215.9"/>
<pinref part="C79" gate="G$1" pin="2"/>
<wire x1="129.54" y1="213.36" x2="129.54" y2="215.9" width="0.1524" layer="91"/>
<junction x="129.54" y="215.9"/>
<pinref part="C80" gate="G$1" pin="2"/>
<wire x1="142.24" y1="213.36" x2="142.24" y2="215.9" width="0.1524" layer="91"/>
<junction x="142.24" y="215.9"/>
<label x="147.32" y="215.9" size="1.778" layer="95"/>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
<junction x="160.02" y="190.5"/>
</segment>
<segment>
<pinref part="C82" gate="G$1" pin="2"/>
<wire x1="172.72" y1="241.3" x2="172.72" y2="238.76" width="0.1524" layer="91"/>
<pinref part="C87" gate="G$1" pin="2"/>
<wire x1="172.72" y1="238.76" x2="185.42" y2="238.76" width="0.1524" layer="91"/>
<wire x1="185.42" y1="238.76" x2="198.12" y2="238.76" width="0.1524" layer="91"/>
<wire x1="198.12" y1="238.76" x2="210.82" y2="238.76" width="0.1524" layer="91"/>
<wire x1="210.82" y1="238.76" x2="223.52" y2="238.76" width="0.1524" layer="91"/>
<wire x1="223.52" y1="238.76" x2="236.22" y2="238.76" width="0.1524" layer="91"/>
<wire x1="236.22" y1="238.76" x2="236.22" y2="241.3" width="0.1524" layer="91"/>
<pinref part="C86" gate="G$1" pin="2"/>
<wire x1="223.52" y1="241.3" x2="223.52" y2="238.76" width="0.1524" layer="91"/>
<junction x="223.52" y="238.76"/>
<pinref part="C85" gate="G$1" pin="2"/>
<wire x1="210.82" y1="241.3" x2="210.82" y2="238.76" width="0.1524" layer="91"/>
<junction x="210.82" y="238.76"/>
<pinref part="C84" gate="G$1" pin="2"/>
<wire x1="198.12" y1="241.3" x2="198.12" y2="238.76" width="0.1524" layer="91"/>
<junction x="198.12" y="238.76"/>
<pinref part="C83" gate="G$1" pin="2"/>
<wire x1="185.42" y1="241.3" x2="185.42" y2="238.76" width="0.1524" layer="91"/>
<junction x="185.42" y="238.76"/>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
<junction x="236.22" y="238.76"/>
</segment>
<segment>
<pinref part="FB3" gate="G$1" pin="2"/>
<pinref part="SUPPLY24" gate="GND" pin="GND"/>
<wire x1="251.46" y1="264.16" x2="254" y2="264.16" width="0.1524" layer="91"/>
<wire x1="254" y1="264.16" x2="254" y2="261.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C92" gate="G$1" pin="2"/>
<wire x1="127" y1="121.92" x2="127" y2="119.38" width="0.1524" layer="91"/>
<pinref part="C93" gate="G$1" pin="2"/>
<wire x1="127" y1="119.38" x2="134.62" y2="119.38" width="0.1524" layer="91"/>
<wire x1="134.62" y1="119.38" x2="142.24" y2="119.38" width="0.1524" layer="91"/>
<wire x1="142.24" y1="119.38" x2="142.24" y2="121.92" width="0.1524" layer="91"/>
<pinref part="SUPPLY25" gate="GND" pin="GND"/>
<junction x="134.62" y="119.38"/>
</segment>
<segment>
<pinref part="SW2" gate="G$1" pin="1"/>
<pinref part="SUPPLY27" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C98" gate="G$1" pin="2"/>
<pinref part="SUPPLY28" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U11" gate="G$1" pin="!OE"/>
<wire x1="187.96" y1="45.72" x2="185.42" y2="45.72" width="0.1524" layer="91"/>
<wire x1="185.42" y1="45.72" x2="185.42" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C100" gate="G$1" pin="2"/>
<wire x1="185.42" y1="40.64" x2="185.42" y2="38.1" width="0.1524" layer="91"/>
<wire x1="185.42" y1="38.1" x2="177.8" y2="38.1" width="0.1524" layer="91"/>
<wire x1="177.8" y1="38.1" x2="172.72" y2="38.1" width="0.1524" layer="91"/>
<wire x1="172.72" y1="38.1" x2="172.72" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="GND"/>
<wire x1="187.96" y1="40.64" x2="185.42" y2="40.64" width="0.1524" layer="91"/>
<junction x="185.42" y="40.64"/>
<pinref part="SUPPLY31" gate="GND" pin="GND"/>
<junction x="177.8" y="38.1"/>
</segment>
<segment>
<pinref part="R19" gate="G$1" pin="1"/>
<pinref part="SUPPLY32" gate="GND" pin="GND"/>
<wire x1="246.38" y1="35.56" x2="246.38" y2="33.02" width="0.1524" layer="91"/>
<wire x1="251.46" y1="40.64" x2="251.46" y2="33.02" width="0.1524" layer="91"/>
<wire x1="251.46" y1="33.02" x2="246.38" y2="33.02" width="0.1524" layer="91"/>
<junction x="246.38" y="33.02"/>
<pinref part="U$1" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="R33" gate="G$1" pin="1"/>
<wire x1="195.58" y1="91.44" x2="195.58" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="195.58" y1="78.74" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<wire x1="200.66" y1="78.74" x2="205.74" y2="78.74" width="0.1524" layer="91"/>
<wire x1="205.74" y1="78.74" x2="210.82" y2="78.74" width="0.1524" layer="91"/>
<wire x1="210.82" y1="78.74" x2="215.9" y2="78.74" width="0.1524" layer="91"/>
<wire x1="215.9" y1="78.74" x2="220.98" y2="78.74" width="0.1524" layer="91"/>
<wire x1="220.98" y1="78.74" x2="226.06" y2="78.74" width="0.1524" layer="91"/>
<wire x1="226.06" y1="78.74" x2="231.14" y2="78.74" width="0.1524" layer="91"/>
<wire x1="231.14" y1="78.74" x2="236.22" y2="78.74" width="0.1524" layer="91"/>
<wire x1="236.22" y1="78.74" x2="241.3" y2="78.74" width="0.1524" layer="91"/>
<wire x1="241.3" y1="78.74" x2="246.38" y2="78.74" width="0.1524" layer="91"/>
<wire x1="246.38" y1="78.74" x2="246.38" y2="91.44" width="0.1524" layer="91"/>
<pinref part="SUPPLY39" gate="GND" pin="GND"/>
<junction x="246.38" y="78.74"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="200.66" y1="81.28" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<junction x="200.66" y="78.74"/>
<pinref part="R43" gate="G$1" pin="1"/>
<wire x1="205.74" y1="91.44" x2="205.74" y2="78.74" width="0.1524" layer="91"/>
<junction x="205.74" y="78.74"/>
<pinref part="R42" gate="G$1" pin="1"/>
<wire x1="210.82" y1="81.28" x2="210.82" y2="78.74" width="0.1524" layer="91"/>
<junction x="210.82" y="78.74"/>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="215.9" y1="91.44" x2="215.9" y2="78.74" width="0.1524" layer="91"/>
<junction x="215.9" y="78.74"/>
<pinref part="R40" gate="G$1" pin="1"/>
<wire x1="220.98" y1="81.28" x2="220.98" y2="78.74" width="0.1524" layer="91"/>
<junction x="220.98" y="78.74"/>
<pinref part="R39" gate="G$1" pin="1"/>
<wire x1="226.06" y1="91.44" x2="226.06" y2="78.74" width="0.1524" layer="91"/>
<junction x="226.06" y="78.74"/>
<pinref part="R38" gate="G$1" pin="1"/>
<wire x1="231.14" y1="81.28" x2="231.14" y2="78.74" width="0.1524" layer="91"/>
<junction x="231.14" y="78.74"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="236.22" y1="91.44" x2="236.22" y2="78.74" width="0.1524" layer="91"/>
<junction x="236.22" y="78.74"/>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="241.3" y1="81.28" x2="241.3" y2="78.74" width="0.1524" layer="91"/>
<junction x="241.3" y="78.74"/>
</segment>
<segment>
<pinref part="SW3" gate="G$1" pin="1"/>
<pinref part="SUPPLY40" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C94" gate="G$1" pin="2"/>
<wire x1="22.86" y1="104.14" x2="22.86" y2="101.6" width="0.1524" layer="91"/>
<wire x1="22.86" y1="101.6" x2="30.48" y2="101.6" width="0.1524" layer="91"/>
<wire x1="30.48" y1="101.6" x2="30.48" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C95" gate="G$1" pin="2"/>
<wire x1="30.48" y1="101.6" x2="38.1" y2="101.6" width="0.1524" layer="91"/>
<wire x1="38.1" y1="101.6" x2="38.1" y2="104.14" width="0.1524" layer="91"/>
<junction x="30.48" y="101.6"/>
<label x="22.86" y="101.6" size="1.778" layer="95" rot="R180"/>
<pinref part="SUPPLY70" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C96" gate="G$1" pin="2"/>
<wire x1="73.66" y1="88.9" x2="73.66" y2="86.36" width="0.1524" layer="91"/>
<pinref part="SUPPLY71" gate="GND" pin="GND"/>
<wire x1="73.66" y1="86.36" x2="81.28" y2="86.36" width="0.1524" layer="91"/>
<pinref part="C97" gate="G$1" pin="2"/>
<wire x1="88.9" y1="88.9" x2="88.9" y2="86.36" width="0.1524" layer="91"/>
<wire x1="88.9" y1="86.36" x2="81.28" y2="86.36" width="0.1524" layer="91"/>
<junction x="81.28" y="86.36"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="GND"/>
<pinref part="SUPPLY30" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C99" gate="G$1" pin="2"/>
<pinref part="SUPPLY84" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="GND"/>
<pinref part="SUPPLY29" gate="GND" pin="GND"/>
</segment>
</net>
<net name="VIO" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="VCC"/>
<wire x1="127" y1="93.98" x2="121.92" y2="93.98" width="0.1524" layer="91"/>
<label x="121.92" y="93.98" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VRTC" class="0">
<segment>
<pinref part="C93" gate="G$1" pin="1"/>
<wire x1="142.24" y1="129.54" x2="142.24" y2="134.62" width="0.1524" layer="91"/>
<wire x1="142.24" y1="134.62" x2="139.7" y2="134.62" width="0.1524" layer="91"/>
<label x="139.7" y="134.62" size="1.778" layer="95" rot="R180"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="142.24" y1="134.62" x2="147.32" y2="134.62" width="0.1524" layer="91"/>
<junction x="142.24" y="134.62"/>
</segment>
</net>
<net name="VDDS" class="0">
<segment>
<pinref part="C87" gate="G$1" pin="1"/>
<wire x1="167.64" y1="251.46" x2="172.72" y2="251.46" width="0.1524" layer="91"/>
<wire x1="172.72" y1="251.46" x2="185.42" y2="251.46" width="0.1524" layer="91"/>
<wire x1="185.42" y1="251.46" x2="198.12" y2="251.46" width="0.1524" layer="91"/>
<wire x1="198.12" y1="251.46" x2="210.82" y2="251.46" width="0.1524" layer="91"/>
<wire x1="210.82" y1="251.46" x2="223.52" y2="251.46" width="0.1524" layer="91"/>
<wire x1="223.52" y1="251.46" x2="236.22" y2="251.46" width="0.1524" layer="91"/>
<wire x1="236.22" y1="251.46" x2="236.22" y2="248.92" width="0.1524" layer="91"/>
<pinref part="C86" gate="G$1" pin="1"/>
<wire x1="223.52" y1="248.92" x2="223.52" y2="251.46" width="0.1524" layer="91"/>
<junction x="223.52" y="251.46"/>
<pinref part="C85" gate="G$1" pin="1"/>
<wire x1="210.82" y1="248.92" x2="210.82" y2="251.46" width="0.1524" layer="91"/>
<junction x="210.82" y="251.46"/>
<pinref part="C84" gate="G$1" pin="1"/>
<wire x1="198.12" y1="248.92" x2="198.12" y2="251.46" width="0.1524" layer="91"/>
<junction x="198.12" y="251.46"/>
<pinref part="C83" gate="G$1" pin="1"/>
<wire x1="185.42" y1="248.92" x2="185.42" y2="251.46" width="0.1524" layer="91"/>
<junction x="185.42" y="251.46"/>
<pinref part="C82" gate="G$1" pin="1"/>
<wire x1="172.72" y1="248.92" x2="172.72" y2="251.46" width="0.1524" layer="91"/>
<junction x="172.72" y="251.46"/>
<label x="167.64" y="251.46" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VDDS_DDR" class="0">
<segment>
<pinref part="C48" gate="G$1" pin="1"/>
<wire x1="22.86" y1="243.84" x2="27.94" y2="243.84" width="0.1524" layer="91"/>
<wire x1="27.94" y1="243.84" x2="40.64" y2="243.84" width="0.1524" layer="91"/>
<wire x1="40.64" y1="243.84" x2="53.34" y2="243.84" width="0.1524" layer="91"/>
<wire x1="53.34" y1="243.84" x2="66.04" y2="243.84" width="0.1524" layer="91"/>
<wire x1="66.04" y1="243.84" x2="78.74" y2="243.84" width="0.1524" layer="91"/>
<wire x1="78.74" y1="243.84" x2="91.44" y2="243.84" width="0.1524" layer="91"/>
<wire x1="91.44" y1="243.84" x2="104.14" y2="243.84" width="0.1524" layer="91"/>
<wire x1="104.14" y1="243.84" x2="116.84" y2="243.84" width="0.1524" layer="91"/>
<wire x1="116.84" y1="243.84" x2="129.54" y2="243.84" width="0.1524" layer="91"/>
<wire x1="129.54" y1="243.84" x2="129.54" y2="241.3" width="0.1524" layer="91"/>
<pinref part="C40" gate="G$1" pin="1"/>
<wire x1="27.94" y1="241.3" x2="27.94" y2="243.84" width="0.1524" layer="91"/>
<junction x="27.94" y="243.84"/>
<pinref part="C41" gate="G$1" pin="1"/>
<wire x1="40.64" y1="241.3" x2="40.64" y2="243.84" width="0.1524" layer="91"/>
<junction x="40.64" y="243.84"/>
<pinref part="C42" gate="G$1" pin="1"/>
<wire x1="53.34" y1="241.3" x2="53.34" y2="243.84" width="0.1524" layer="91"/>
<junction x="53.34" y="243.84"/>
<pinref part="C43" gate="G$1" pin="1"/>
<wire x1="66.04" y1="241.3" x2="66.04" y2="243.84" width="0.1524" layer="91"/>
<junction x="66.04" y="243.84"/>
<pinref part="C44" gate="G$1" pin="1"/>
<wire x1="78.74" y1="241.3" x2="78.74" y2="243.84" width="0.1524" layer="91"/>
<junction x="78.74" y="243.84"/>
<pinref part="C45" gate="G$1" pin="1"/>
<wire x1="91.44" y1="241.3" x2="91.44" y2="243.84" width="0.1524" layer="91"/>
<junction x="91.44" y="243.84"/>
<pinref part="C46" gate="G$1" pin="1"/>
<wire x1="104.14" y1="241.3" x2="104.14" y2="243.84" width="0.1524" layer="91"/>
<junction x="104.14" y="243.84"/>
<pinref part="C47" gate="G$1" pin="1"/>
<wire x1="116.84" y1="241.3" x2="116.84" y2="243.84" width="0.1524" layer="91"/>
<junction x="116.84" y="243.84"/>
<label x="22.86" y="243.84" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VDD_MPU" class="0">
<segment>
<pinref part="C52" gate="G$1" pin="1"/>
<wire x1="180.34" y1="223.52" x2="185.42" y2="223.52" width="0.1524" layer="91"/>
<wire x1="185.42" y1="223.52" x2="198.12" y2="223.52" width="0.1524" layer="91"/>
<wire x1="198.12" y1="223.52" x2="210.82" y2="223.52" width="0.1524" layer="91"/>
<wire x1="210.82" y1="223.52" x2="223.52" y2="223.52" width="0.1524" layer="91"/>
<wire x1="223.52" y1="223.52" x2="223.52" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C51" gate="G$1" pin="1"/>
<wire x1="210.82" y1="220.98" x2="210.82" y2="223.52" width="0.1524" layer="91"/>
<junction x="210.82" y="223.52"/>
<pinref part="C50" gate="G$1" pin="1"/>
<wire x1="198.12" y1="220.98" x2="198.12" y2="223.52" width="0.1524" layer="91"/>
<junction x="198.12" y="223.52"/>
<pinref part="C49" gate="G$1" pin="1"/>
<wire x1="185.42" y1="220.98" x2="185.42" y2="223.52" width="0.1524" layer="91"/>
<junction x="185.42" y="223.52"/>
<label x="180.34" y="223.52" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VDD_CORE" class="0">
<segment>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="22.86" y1="269.24" x2="27.94" y2="269.24" width="0.1524" layer="91"/>
<wire x1="27.94" y1="269.24" x2="40.64" y2="269.24" width="0.1524" layer="91"/>
<wire x1="40.64" y1="269.24" x2="53.34" y2="269.24" width="0.1524" layer="91"/>
<wire x1="53.34" y1="269.24" x2="66.04" y2="269.24" width="0.1524" layer="91"/>
<wire x1="66.04" y1="269.24" x2="78.74" y2="269.24" width="0.1524" layer="91"/>
<wire x1="78.74" y1="269.24" x2="91.44" y2="269.24" width="0.1524" layer="91"/>
<wire x1="91.44" y1="269.24" x2="104.14" y2="269.24" width="0.1524" layer="91"/>
<wire x1="104.14" y1="269.24" x2="116.84" y2="269.24" width="0.1524" layer="91"/>
<wire x1="116.84" y1="269.24" x2="129.54" y2="269.24" width="0.1524" layer="91"/>
<wire x1="129.54" y1="269.24" x2="129.54" y2="266.7" width="0.1524" layer="91"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="27.94" y1="271.78" x2="27.94" y2="269.24" width="0.1524" layer="91"/>
<junction x="27.94" y="269.24"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="40.64" y1="271.78" x2="40.64" y2="269.24" width="0.1524" layer="91"/>
<junction x="40.64" y="269.24"/>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="53.34" y1="271.78" x2="53.34" y2="269.24" width="0.1524" layer="91"/>
<junction x="53.34" y="269.24"/>
<pinref part="C34" gate="G$1" pin="1"/>
<wire x1="66.04" y1="271.78" x2="66.04" y2="269.24" width="0.1524" layer="91"/>
<junction x="66.04" y="269.24"/>
<pinref part="C36" gate="G$1" pin="1"/>
<wire x1="78.74" y1="271.78" x2="78.74" y2="269.24" width="0.1524" layer="91"/>
<junction x="78.74" y="269.24"/>
<pinref part="C37" gate="G$1" pin="1"/>
<wire x1="91.44" y1="271.78" x2="91.44" y2="269.24" width="0.1524" layer="91"/>
<junction x="91.44" y="269.24"/>
<pinref part="C38" gate="G$1" pin="1"/>
<wire x1="104.14" y1="271.78" x2="104.14" y2="269.24" width="0.1524" layer="91"/>
<junction x="104.14" y="269.24"/>
<pinref part="C39" gate="G$1" pin="1"/>
<wire x1="116.84" y1="271.78" x2="116.84" y2="269.24" width="0.1524" layer="91"/>
<junction x="116.84" y="269.24"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="116.84" y1="266.7" x2="116.84" y2="269.24" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="104.14" y1="266.7" x2="104.14" y2="269.24" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="91.44" y1="266.7" x2="91.44" y2="269.24" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="78.74" y1="266.7" x2="78.74" y2="269.24" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="66.04" y1="266.7" x2="66.04" y2="269.24" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="53.34" y1="266.7" x2="53.34" y2="269.24" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="40.64" y1="266.7" x2="40.64" y2="269.24" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="27.94" y1="266.7" x2="27.94" y2="269.24" width="0.1524" layer="91"/>
<label x="22.86" y="269.24" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VDD_1V8" class="0">
<segment>
<pinref part="FB1" gate="G$1" pin="1"/>
<pinref part="C53" gate="G$1" pin="1"/>
<wire x1="68.58" y1="172.72" x2="53.34" y2="172.72" width="0.1524" layer="91"/>
<wire x1="53.34" y1="172.72" x2="40.64" y2="172.72" width="0.1524" layer="91"/>
<wire x1="40.64" y1="172.72" x2="27.94" y2="172.72" width="0.1524" layer="91"/>
<wire x1="27.94" y1="172.72" x2="22.86" y2="172.72" width="0.1524" layer="91"/>
<junction x="27.94" y="172.72"/>
<wire x1="27.94" y1="170.18" x2="27.94" y2="172.72" width="0.1524" layer="91"/>
<label x="22.86" y="172.72" size="1.778" layer="95" rot="R180"/>
<pinref part="C55" gate="G$1" pin="1"/>
<wire x1="40.64" y1="170.18" x2="40.64" y2="172.72" width="0.1524" layer="91"/>
<junction x="40.64" y="172.72"/>
<pinref part="C56" gate="G$1" pin="1"/>
<wire x1="53.34" y1="170.18" x2="53.34" y2="172.72" width="0.1524" layer="91"/>
<junction x="53.34" y="172.72"/>
<pinref part="FB2" gate="G$1" pin="1"/>
<wire x1="68.58" y1="170.18" x2="68.58" y2="172.72" width="0.1524" layer="91"/>
<junction x="68.58" y="172.72"/>
</segment>
</net>
<net name="VDD_3V3A" class="0">
<segment>
<pinref part="C70" gate="G$1" pin="1"/>
<wire x1="20.32" y1="203.2" x2="27.94" y2="203.2" width="0.1524" layer="91"/>
<wire x1="27.94" y1="203.2" x2="40.64" y2="203.2" width="0.1524" layer="91"/>
<wire x1="40.64" y1="203.2" x2="53.34" y2="203.2" width="0.1524" layer="91"/>
<wire x1="53.34" y1="203.2" x2="66.04" y2="203.2" width="0.1524" layer="91"/>
<wire x1="66.04" y1="203.2" x2="78.74" y2="203.2" width="0.1524" layer="91"/>
<wire x1="78.74" y1="203.2" x2="91.44" y2="203.2" width="0.1524" layer="91"/>
<wire x1="91.44" y1="203.2" x2="104.14" y2="203.2" width="0.1524" layer="91"/>
<wire x1="104.14" y1="203.2" x2="116.84" y2="203.2" width="0.1524" layer="91"/>
<wire x1="116.84" y1="203.2" x2="129.54" y2="203.2" width="0.1524" layer="91"/>
<wire x1="129.54" y1="203.2" x2="142.24" y2="203.2" width="0.1524" layer="91"/>
<wire x1="142.24" y1="203.2" x2="154.94" y2="203.2" width="0.1524" layer="91"/>
<wire x1="154.94" y1="203.2" x2="154.94" y2="200.66" width="0.1524" layer="91"/>
<pinref part="C71" gate="G$1" pin="1"/>
<wire x1="27.94" y1="205.74" x2="27.94" y2="203.2" width="0.1524" layer="91"/>
<junction x="27.94" y="203.2"/>
<pinref part="C81" gate="G$1" pin="1"/>
<wire x1="27.94" y1="200.66" x2="27.94" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C61" gate="G$1" pin="1"/>
<wire x1="40.64" y1="200.66" x2="40.64" y2="203.2" width="0.1524" layer="91"/>
<junction x="40.64" y="203.2"/>
<pinref part="C72" gate="G$1" pin="1"/>
<wire x1="40.64" y1="205.74" x2="40.64" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C80" gate="G$1" pin="1"/>
<wire x1="142.24" y1="205.74" x2="142.24" y2="203.2" width="0.1524" layer="91"/>
<junction x="142.24" y="203.2"/>
<pinref part="C79" gate="G$1" pin="1"/>
<wire x1="129.54" y1="205.74" x2="129.54" y2="203.2" width="0.1524" layer="91"/>
<junction x="129.54" y="203.2"/>
<pinref part="C78" gate="G$1" pin="1"/>
<wire x1="116.84" y1="205.74" x2="116.84" y2="203.2" width="0.1524" layer="91"/>
<junction x="116.84" y="203.2"/>
<pinref part="C77" gate="G$1" pin="1"/>
<wire x1="104.14" y1="205.74" x2="104.14" y2="203.2" width="0.1524" layer="91"/>
<junction x="104.14" y="203.2"/>
<pinref part="C76" gate="G$1" pin="1"/>
<wire x1="91.44" y1="205.74" x2="91.44" y2="203.2" width="0.1524" layer="91"/>
<junction x="91.44" y="203.2"/>
<pinref part="C75" gate="G$1" pin="1"/>
<wire x1="78.74" y1="205.74" x2="78.74" y2="203.2" width="0.1524" layer="91"/>
<junction x="78.74" y="203.2"/>
<pinref part="C74" gate="G$1" pin="1"/>
<wire x1="66.04" y1="205.74" x2="66.04" y2="203.2" width="0.1524" layer="91"/>
<junction x="66.04" y="203.2"/>
<pinref part="C73" gate="G$1" pin="1"/>
<wire x1="53.34" y1="205.74" x2="53.34" y2="203.2" width="0.1524" layer="91"/>
<junction x="53.34" y="203.2"/>
<pinref part="C62" gate="G$1" pin="1"/>
<wire x1="53.34" y1="200.66" x2="53.34" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C63" gate="G$1" pin="1"/>
<wire x1="66.04" y1="200.66" x2="66.04" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C64" gate="G$1" pin="1"/>
<wire x1="78.74" y1="200.66" x2="78.74" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C65" gate="G$1" pin="1"/>
<wire x1="91.44" y1="200.66" x2="91.44" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C66" gate="G$1" pin="1"/>
<wire x1="104.14" y1="200.66" x2="104.14" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C67" gate="G$1" pin="1"/>
<wire x1="116.84" y1="200.66" x2="116.84" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C68" gate="G$1" pin="1"/>
<wire x1="129.54" y1="200.66" x2="129.54" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C69" gate="G$1" pin="1"/>
<wire x1="142.24" y1="200.66" x2="142.24" y2="203.2" width="0.1524" layer="91"/>
<label x="20.32" y="203.2" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="40.64" y1="73.66" x2="40.64" y2="76.2" width="0.1524" layer="91"/>
<label x="40.64" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="VCC"/>
<label x="88.9" y="55.88" size="1.778" layer="95"/>
<wire x1="101.6" y1="55.88" x2="88.9" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C99" gate="G$1" pin="1"/>
<wire x1="88.9" y1="55.88" x2="88.9" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="213.36" y1="154.94" x2="213.36" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="213.36" y1="157.48" x2="220.98" y2="157.48" width="0.1524" layer="91"/>
<wire x1="220.98" y1="157.48" x2="228.6" y2="157.48" width="0.1524" layer="91"/>
<wire x1="228.6" y1="157.48" x2="236.22" y2="157.48" width="0.1524" layer="91"/>
<wire x1="236.22" y1="157.48" x2="243.84" y2="157.48" width="0.1524" layer="91"/>
<wire x1="243.84" y1="157.48" x2="243.84" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R30" gate="G$1" pin="2"/>
<wire x1="236.22" y1="154.94" x2="236.22" y2="157.48" width="0.1524" layer="91"/>
<junction x="236.22" y="157.48"/>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="228.6" y1="154.94" x2="228.6" y2="157.48" width="0.1524" layer="91"/>
<junction x="228.6" y="157.48"/>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="220.98" y1="154.94" x2="220.98" y2="157.48" width="0.1524" layer="91"/>
<junction x="220.98" y="157.48"/>
<label x="215.9" y="157.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="WAKEUP" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="157.48" y1="134.62" x2="160.02" y2="134.62" width="0.1524" layer="91"/>
<label x="160.02" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD_3V3B" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="VCC"/>
<wire x1="187.96" y1="55.88" x2="185.42" y2="55.88" width="0.1524" layer="91"/>
<label x="185.42" y="53.34" size="1.778" layer="95" rot="R180"/>
<pinref part="C100" gate="G$1" pin="1"/>
<wire x1="172.72" y1="48.26" x2="172.72" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="2OE"/>
<wire x1="172.72" y1="50.8" x2="185.42" y2="50.8" width="0.1524" layer="91"/>
<wire x1="185.42" y1="50.8" x2="187.96" y2="50.8" width="0.1524" layer="91"/>
<wire x1="185.42" y1="55.88" x2="185.42" y2="50.8" width="0.1524" layer="91"/>
<junction x="185.42" y="50.8"/>
</segment>
</net>
<net name="VDD_PLL" class="0">
<segment>
<pinref part="C60" gate="G$1" pin="1"/>
<wire x1="104.14" y1="172.72" x2="116.84" y2="172.72" width="0.1524" layer="91"/>
<wire x1="116.84" y1="172.72" x2="129.54" y2="172.72" width="0.1524" layer="91"/>
<wire x1="129.54" y1="172.72" x2="129.54" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C58" gate="G$1" pin="1"/>
<wire x1="116.84" y1="170.18" x2="116.84" y2="172.72" width="0.1524" layer="91"/>
<junction x="116.84" y="172.72"/>
<pinref part="C59" gate="G$1" pin="1"/>
<wire x1="104.14" y1="170.18" x2="104.14" y2="172.72" width="0.1524" layer="91"/>
<junction x="104.14" y="172.72"/>
<pinref part="C54" gate="G$1" pin="1"/>
<wire x1="91.44" y1="170.18" x2="91.44" y2="172.72" width="0.1524" layer="91"/>
<pinref part="FB1" gate="G$1" pin="2"/>
<wire x1="91.44" y1="172.72" x2="83.82" y2="172.72" width="0.1524" layer="91"/>
<wire x1="91.44" y1="172.72" x2="104.14" y2="172.72" width="0.1524" layer="91"/>
<junction x="91.44" y="172.72"/>
<label x="129.54" y="172.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD_ADC" class="0">
<segment>
<pinref part="FB2" gate="G$1" pin="2"/>
<pinref part="C57" gate="G$1" pin="1"/>
<wire x1="68.58" y1="154.94" x2="68.58" y2="149.86" width="0.1524" layer="91"/>
<label x="68.58" y="152.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="172.72" y1="276.86" x2="167.64" y2="276.86" width="0.1524" layer="91"/>
<label x="167.64" y="276.86" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="GNDA_ADC" class="0">
<segment>
<pinref part="C88" gate="G$1" pin="2"/>
<wire x1="185.42" y1="266.7" x2="185.42" y2="264.16" width="0.1524" layer="91"/>
<pinref part="C90" gate="G$1" pin="2"/>
<wire x1="185.42" y1="264.16" x2="198.12" y2="264.16" width="0.1524" layer="91"/>
<wire x1="198.12" y1="264.16" x2="210.82" y2="264.16" width="0.1524" layer="91"/>
<wire x1="210.82" y1="264.16" x2="210.82" y2="266.7" width="0.1524" layer="91"/>
<pinref part="C89" gate="G$1" pin="2"/>
<wire x1="198.12" y1="266.7" x2="198.12" y2="264.16" width="0.1524" layer="91"/>
<junction x="198.12" y="264.16"/>
<pinref part="FB3" gate="G$1" pin="1"/>
<wire x1="236.22" y1="264.16" x2="226.06" y2="264.16" width="0.1524" layer="91"/>
<junction x="210.82" y="264.16"/>
<label x="185.42" y="264.16" size="1.778" layer="95" rot="R180"/>
<pinref part="C91" gate="G$1" pin="2"/>
<wire x1="226.06" y1="264.16" x2="210.82" y2="264.16" width="0.1524" layer="91"/>
<wire x1="226.06" y1="266.7" x2="226.06" y2="264.16" width="0.1524" layer="91"/>
<junction x="226.06" y="264.16"/>
</segment>
</net>
<net name="VREFP_ADC" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<pinref part="C90" gate="G$1" pin="1"/>
<wire x1="182.88" y1="276.86" x2="185.42" y2="276.86" width="0.1524" layer="91"/>
<wire x1="185.42" y1="276.86" x2="198.12" y2="276.86" width="0.1524" layer="91"/>
<wire x1="198.12" y1="276.86" x2="210.82" y2="276.86" width="0.1524" layer="91"/>
<wire x1="210.82" y1="276.86" x2="210.82" y2="274.32" width="0.1524" layer="91"/>
<pinref part="C89" gate="G$1" pin="1"/>
<wire x1="198.12" y1="274.32" x2="198.12" y2="276.86" width="0.1524" layer="91"/>
<junction x="198.12" y="276.86"/>
<pinref part="C88" gate="G$1" pin="1"/>
<wire x1="185.42" y1="274.32" x2="185.42" y2="276.86" width="0.1524" layer="91"/>
<junction x="185.42" y="276.86"/>
<label x="226.06" y="276.86" size="1.778" layer="95"/>
<pinref part="C91" gate="G$1" pin="1"/>
<wire x1="226.06" y1="274.32" x2="226.06" y2="276.86" width="0.1524" layer="91"/>
<wire x1="226.06" y1="276.86" x2="210.82" y2="276.86" width="0.1524" layer="91"/>
<junction x="210.82" y="276.86"/>
</segment>
</net>
<net name="CAP_VDD_RTC" class="0">
<segment>
<pinref part="C92" gate="G$1" pin="1"/>
<wire x1="127" y1="129.54" x2="127" y2="132.08" width="0.1524" layer="91"/>
<label x="127" y="132.08" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="OSC1_OUT" class="0">
<segment>
<pinref part="Y1" gate="G$1" pin="1"/>
<wire x1="27.94" y1="114.3" x2="22.86" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C94" gate="G$1" pin="1"/>
<wire x1="22.86" y1="114.3" x2="20.32" y2="114.3" width="0.1524" layer="91"/>
<wire x1="22.86" y1="114.3" x2="22.86" y2="111.76" width="0.1524" layer="91"/>
<junction x="22.86" y="114.3"/>
<label x="20.32" y="114.3" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="OSC1_IN" class="0">
<segment>
<pinref part="Y1" gate="G$1" pin="2"/>
<wire x1="33.02" y1="114.3" x2="38.1" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C95" gate="G$1" pin="1"/>
<wire x1="38.1" y1="114.3" x2="40.64" y2="114.3" width="0.1524" layer="91"/>
<wire x1="38.1" y1="114.3" x2="38.1" y2="111.76" width="0.1524" layer="91"/>
<junction x="38.1" y="114.3"/>
<label x="40.64" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="OSC0_OUT" class="0">
<segment>
<pinref part="C96" gate="G$1" pin="1"/>
<wire x1="73.66" y1="96.52" x2="73.66" y2="99.06" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="73.66" y1="99.06" x2="73.66" y2="109.22" width="0.1524" layer="91"/>
<wire x1="73.66" y1="109.22" x2="76.2" y2="109.22" width="0.1524" layer="91"/>
<pinref part="Y2" gate="G$1" pin="1"/>
<wire x1="78.74" y1="99.06" x2="73.66" y2="99.06" width="0.1524" layer="91"/>
<junction x="73.66" y="99.06"/>
<wire x1="73.66" y1="109.22" x2="71.12" y2="109.22" width="0.1524" layer="91"/>
<junction x="73.66" y="109.22"/>
<label x="71.12" y="109.22" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="OSC0_IN" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<pinref part="C97" gate="G$1" pin="1"/>
<wire x1="86.36" y1="109.22" x2="88.9" y2="109.22" width="0.1524" layer="91"/>
<wire x1="88.9" y1="109.22" x2="88.9" y2="99.06" width="0.1524" layer="91"/>
<pinref part="Y2" gate="G$1" pin="2"/>
<wire x1="88.9" y1="99.06" x2="88.9" y2="96.52" width="0.1524" layer="91"/>
<wire x1="83.82" y1="99.06" x2="88.9" y2="99.06" width="0.1524" layer="91"/>
<junction x="88.9" y="99.06"/>
<wire x1="88.9" y1="109.22" x2="91.44" y2="109.22" width="0.1524" layer="91"/>
<junction x="88.9" y="109.22"/>
<label x="91.44" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="MPU_NRST" class="0">
<segment>
<pinref part="SW2" gate="G$1" pin="2"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="35.56" y1="60.96" x2="40.64" y2="60.96" width="0.1524" layer="91"/>
<wire x1="40.64" y1="60.96" x2="40.64" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C98" gate="G$1" pin="1"/>
<wire x1="40.64" y1="60.96" x2="40.64" y2="58.42" width="0.1524" layer="91"/>
<junction x="40.64" y="60.96"/>
<wire x1="40.64" y1="60.96" x2="43.18" y2="60.96" width="0.1524" layer="91"/>
<label x="43.18" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U9" gate="G$1" pin="Y"/>
<wire x1="154.94" y1="83.82" x2="160.02" y2="83.82" width="0.1524" layer="91"/>
<label x="160.02" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART0_RX" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="1Y"/>
<wire x1="218.44" y1="50.8" x2="223.52" y2="50.8" width="0.1524" layer="91"/>
<label x="223.52" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART0_TX" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="2A"/>
<wire x1="218.44" y1="45.72" x2="223.52" y2="45.72" width="0.1524" layer="91"/>
<label x="223.52" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_15" class="0">
<segment>
<wire x1="195.58" y1="104.14" x2="195.58" y2="101.6" width="0.1524" layer="91"/>
<wire x1="195.58" y1="104.14" x2="248.92" y2="104.14" width="0.1524" layer="91"/>
<pinref part="R33" gate="G$1" pin="2"/>
<label x="248.92" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_14" class="0">
<segment>
<wire x1="213.36" y1="144.78" x2="213.36" y2="106.68" width="0.1524" layer="91"/>
<wire x1="213.36" y1="106.68" x2="248.92" y2="106.68" width="0.1524" layer="91"/>
<pinref part="R28" gate="G$1" pin="1"/>
<label x="248.92" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_13" class="0">
<segment>
<wire x1="200.66" y1="109.22" x2="200.66" y2="91.44" width="0.1524" layer="91"/>
<wire x1="200.66" y1="109.22" x2="248.92" y2="109.22" width="0.1524" layer="91"/>
<pinref part="R34" gate="G$1" pin="2"/>
<label x="248.92" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_12" class="0">
<segment>
<wire x1="205.74" y1="111.76" x2="205.74" y2="101.6" width="0.1524" layer="91"/>
<wire x1="205.74" y1="111.76" x2="248.92" y2="111.76" width="0.1524" layer="91"/>
<pinref part="R43" gate="G$1" pin="2"/>
<label x="248.92" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_11" class="0">
<segment>
<wire x1="210.82" y1="114.3" x2="210.82" y2="91.44" width="0.1524" layer="91"/>
<wire x1="210.82" y1="114.3" x2="248.92" y2="114.3" width="0.1524" layer="91"/>
<pinref part="R42" gate="G$1" pin="2"/>
<label x="248.92" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_10" class="0">
<segment>
<wire x1="215.9" y1="116.84" x2="215.9" y2="101.6" width="0.1524" layer="91"/>
<wire x1="215.9" y1="116.84" x2="248.92" y2="116.84" width="0.1524" layer="91"/>
<pinref part="R41" gate="G$1" pin="2"/>
<label x="248.92" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_9" class="0">
<segment>
<wire x1="220.98" y1="119.38" x2="220.98" y2="91.44" width="0.1524" layer="91"/>
<wire x1="220.98" y1="119.38" x2="248.92" y2="119.38" width="0.1524" layer="91"/>
<pinref part="R40" gate="G$1" pin="2"/>
<label x="248.92" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_8" class="0">
<segment>
<wire x1="226.06" y1="121.92" x2="226.06" y2="101.6" width="0.1524" layer="91"/>
<wire x1="226.06" y1="121.92" x2="248.92" y2="121.92" width="0.1524" layer="91"/>
<pinref part="R39" gate="G$1" pin="2"/>
<label x="248.92" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_7" class="0">
<segment>
<wire x1="231.14" y1="124.46" x2="231.14" y2="91.44" width="0.1524" layer="91"/>
<wire x1="231.14" y1="124.46" x2="248.92" y2="124.46" width="0.1524" layer="91"/>
<pinref part="R38" gate="G$1" pin="2"/>
<label x="248.92" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_6" class="0">
<segment>
<wire x1="236.22" y1="127" x2="236.22" y2="101.6" width="0.1524" layer="91"/>
<wire x1="236.22" y1="127" x2="248.92" y2="127" width="0.1524" layer="91"/>
<pinref part="R37" gate="G$1" pin="2"/>
<label x="248.92" y="127" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_5" class="0">
<segment>
<wire x1="220.98" y1="144.78" x2="220.98" y2="129.54" width="0.1524" layer="91"/>
<wire x1="220.98" y1="129.54" x2="248.92" y2="129.54" width="0.1524" layer="91"/>
<pinref part="R32" gate="G$1" pin="1"/>
<label x="248.92" y="129.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_4" class="0">
<segment>
<wire x1="228.6" y1="144.78" x2="228.6" y2="132.08" width="0.1524" layer="91"/>
<wire x1="228.6" y1="132.08" x2="248.92" y2="132.08" width="0.1524" layer="91"/>
<pinref part="R31" gate="G$1" pin="1"/>
<label x="248.92" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_3" class="0">
<segment>
<wire x1="236.22" y1="144.78" x2="236.22" y2="134.62" width="0.1524" layer="91"/>
<wire x1="236.22" y1="134.62" x2="248.92" y2="134.62" width="0.1524" layer="91"/>
<pinref part="R30" gate="G$1" pin="1"/>
<label x="248.92" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_2" class="0">
<segment>
<wire x1="243.84" y1="144.78" x2="243.84" y2="142.24" width="0.1524" layer="91"/>
<wire x1="243.84" y1="142.24" x2="243.84" y2="137.16" width="0.1524" layer="91"/>
<wire x1="243.84" y1="137.16" x2="248.92" y2="137.16" width="0.1524" layer="91"/>
<pinref part="R29" gate="G$1" pin="1"/>
<label x="248.92" y="137.16" size="1.778" layer="95"/>
<pinref part="R44" gate="G$1" pin="2"/>
<wire x1="210.82" y1="142.24" x2="243.84" y2="142.24" width="0.1524" layer="91"/>
<junction x="243.84" y="142.24"/>
</segment>
</net>
<net name="SYSBOOT_1" class="0">
<segment>
<wire x1="241.3" y1="139.7" x2="241.3" y2="91.44" width="0.1524" layer="91"/>
<wire x1="241.3" y1="139.7" x2="248.92" y2="139.7" width="0.1524" layer="91"/>
<pinref part="R36" gate="G$1" pin="2"/>
<label x="248.92" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYSBOOT_0" class="0">
<segment>
<wire x1="246.38" y1="142.24" x2="246.38" y2="101.6" width="0.1524" layer="91"/>
<wire x1="246.38" y1="142.24" x2="248.92" y2="142.24" width="0.1524" layer="91"/>
<pinref part="R35" gate="G$1" pin="2"/>
<label x="248.92" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="INV_BUFF_OUT" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="Y"/>
<wire x1="129.54" y1="45.72" x2="134.62" y2="45.72" width="0.1524" layer="91"/>
<wire x1="134.62" y1="45.72" x2="134.62" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="2"/>
<label x="134.62" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="EMMC_NRST" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="134.62" y1="33.02" x2="134.62" y2="30.48" width="0.1524" layer="91"/>
<label x="134.62" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="MPU_R14" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="A"/>
<wire x1="129.54" y1="55.88" x2="134.62" y2="55.88" width="0.1524" layer="91"/>
<label x="134.62" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="PMIC_PGOOD" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="A"/>
<wire x1="154.94" y1="93.98" x2="160.02" y2="93.98" width="0.1524" layer="91"/>
<label x="160.02" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="H2_UART0_RX" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="1A"/>
<label x="223.52" y="55.88" size="1.778" layer="95"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="218.44" y1="55.88" x2="246.38" y2="55.88" width="0.1524" layer="91"/>
<wire x1="246.38" y1="55.88" x2="246.38" y2="50.8" width="0.1524" layer="91"/>
<wire x1="246.38" y1="50.8" x2="246.38" y2="45.72" width="0.1524" layer="91"/>
<wire x1="251.46" y1="50.8" x2="246.38" y2="50.8" width="0.1524" layer="91"/>
<junction x="246.38" y="50.8"/>
<pinref part="U$1" gate="G$1" pin="RXD"/>
</segment>
</net>
<net name="H2_UART0_TX" class="0">
<segment>
<pinref part="U11" gate="G$1" pin="2Y"/>
<label x="223.52" y="40.64" size="1.778" layer="95"/>
<wire x1="218.44" y1="40.64" x2="241.3" y2="40.64" width="0.1524" layer="91"/>
<wire x1="241.3" y1="40.64" x2="241.3" y2="48.26" width="0.1524" layer="91"/>
<wire x1="241.3" y1="48.26" x2="251.46" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="TXD"/>
</segment>
</net>
<net name="USD_BOOT" class="0">
<segment>
<pinref part="R44" gate="G$1" pin="1"/>
<wire x1="200.66" y1="142.24" x2="198.12" y2="142.24" width="0.1524" layer="91"/>
<pinref part="SW3" gate="G$1" pin="2"/>
<label x="198.12" y="142.24" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="5.842" y="2.286" size="10.16" layer="97">MEMORY MODULES</text>
<text x="12.954" y="232.918" size="3.048" layer="97">DDR3 Memory</text>
<text x="173.482" y="164.592" size="3.048" layer="97">eMMC MEMORY (MMC1)</text>
<text x="106.934" y="71.12" size="3.048" layer="97">EEPROM - BOARD ID</text>
<text x="183.642" y="233.172" size="3.048" layer="97">uSD CONNECTOR (MMC0)</text>
<text x="5.588" y="17.272" size="2.54" layer="97">Linux Micro-PC
Rev 1.0
SBS 2015</text>
</plain>
<instances>
<instance part="U6" gate="G$1" x="118.872" y="56.134"/>
<instance part="R45" gate="G$1" x="146.812" y="51.054"/>
<instance part="C104" gate="G$1" x="156.972" y="56.134"/>
<instance part="SUPPLY41" gate="GND" x="156.972" y="48.514"/>
<instance part="SUPPLY42" gate="GND" x="101.092" y="48.514"/>
<instance part="C105" gate="G$1" x="195.58" y="96.52"/>
<instance part="SUPPLY43" gate="GND" x="246.38" y="86.36"/>
<instance part="C106" gate="G$1" x="208.28" y="96.52"/>
<instance part="C107" gate="G$1" x="220.98" y="96.52"/>
<instance part="C108" gate="G$1" x="233.68" y="96.52"/>
<instance part="C109" gate="G$1" x="246.38" y="96.52"/>
<instance part="SUPPLY44" gate="GND" x="205.74" y="111.76"/>
<instance part="C110" gate="G$1" x="167.64" y="109.22"/>
<instance part="SUPPLY45" gate="GND" x="167.64" y="101.6"/>
<instance part="U4" gate="G$1" x="185.42" y="129.54"/>
<instance part="R46" gate="G$1" x="205.74" y="149.86" rot="R90"/>
<instance part="R47" gate="G$1" x="213.36" y="149.86" rot="R90"/>
<instance part="R48" gate="G$1" x="220.98" y="149.86" rot="R90"/>
<instance part="R49" gate="G$1" x="228.6" y="149.86" rot="R90"/>
<instance part="R50" gate="G$1" x="236.22" y="149.86" rot="R90"/>
<instance part="R51" gate="G$1" x="243.84" y="149.86" rot="R90"/>
<instance part="R52" gate="G$1" x="251.46" y="149.86" rot="R90"/>
<instance part="R53" gate="G$1" x="259.08" y="149.86" rot="R90"/>
<instance part="R54" gate="G$1" x="167.64" y="149.86" rot="R90"/>
<instance part="R55" gate="G$1" x="160.02" y="149.86" rot="R90"/>
<instance part="R56" gate="G$1" x="152.4" y="149.86" rot="R90"/>
<instance part="C111" gate="G$1" x="27.94" y="210.82"/>
<instance part="C112" gate="G$1" x="40.64" y="210.82"/>
<instance part="C113" gate="G$1" x="53.34" y="210.82"/>
<instance part="C114" gate="G$1" x="66.04" y="210.82"/>
<instance part="C115" gate="G$1" x="78.74" y="210.82"/>
<instance part="C116" gate="G$1" x="91.44" y="210.82"/>
<instance part="C117" gate="G$1" x="104.14" y="210.82"/>
<instance part="C118" gate="G$1" x="116.84" y="210.82"/>
<instance part="C119" gate="G$1" x="27.94" y="220.98" rot="R180"/>
<instance part="C120" gate="G$1" x="40.64" y="220.98" rot="R180"/>
<instance part="C121" gate="G$1" x="53.34" y="220.98" rot="R180"/>
<instance part="C122" gate="G$1" x="66.04" y="220.98" rot="R180"/>
<instance part="C123" gate="G$1" x="78.74" y="220.98" rot="R180"/>
<instance part="C124" gate="G$1" x="91.44" y="220.98" rot="R180"/>
<instance part="C125" gate="G$1" x="104.14" y="220.98" rot="R180"/>
<instance part="C126" gate="G$1" x="116.84" y="220.98" rot="R180"/>
<instance part="SUPPLY46" gate="GND" x="129.54" y="200.66"/>
<instance part="R57" gate="G$1" x="83.82" y="96.52" rot="R90"/>
<instance part="SUPPLY47" gate="GND" x="83.82" y="88.9"/>
<instance part="U3" gate="G$1" x="63.5" y="147.32"/>
<instance part="SUPPLY48" gate="GND" x="93.98" y="109.22"/>
<instance part="C127" gate="G$1" x="43.18" y="96.52"/>
<instance part="R58" gate="G$1" x="25.4" y="99.06" rot="R180"/>
<instance part="R59" gate="G$1" x="25.4" y="86.36" rot="R90"/>
<instance part="C128" gate="G$1" x="33.02" y="86.36"/>
<instance part="SUPPLY49" gate="GND" x="33.02" y="76.2"/>
<instance part="SUPPLY50" gate="GND" x="43.18" y="88.9"/>
<instance part="CON3" gate="G$1" x="220.98" y="203.2"/>
<instance part="R60" gate="G$1" x="193.04" y="220.98" rot="R90"/>
<instance part="R61" gate="G$1" x="185.42" y="220.98" rot="R90"/>
<instance part="R62" gate="G$1" x="177.8" y="220.98" rot="R90"/>
<instance part="R63" gate="G$1" x="170.18" y="220.98" rot="R90"/>
<instance part="R64" gate="G$1" x="243.84" y="220.98" rot="R90"/>
<instance part="R65" gate="G$1" x="251.46" y="220.98" rot="R90"/>
<instance part="R66" gate="G$1" x="259.08" y="220.98" rot="R90"/>
<instance part="SUPPLY51" gate="GND" x="241.3" y="180.34"/>
<instance part="C129" gate="G$1" x="248.92" y="190.5"/>
<instance part="C130" gate="G$1" x="261.62" y="190.5"/>
<instance part="R116" gate="G$1" x="109.22" y="127" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C104" gate="G$1" pin="2"/>
<pinref part="SUPPLY41" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY42" gate="GND" pin="GND"/>
<pinref part="U6" gate="G$1" pin="VSS"/>
<wire x1="101.092" y1="51.054" x2="106.172" y2="51.054" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C105" gate="G$1" pin="2"/>
<wire x1="195.58" y1="91.44" x2="195.58" y2="88.9" width="0.1524" layer="91"/>
<pinref part="SUPPLY43" gate="GND" pin="GND"/>
<wire x1="195.58" y1="88.9" x2="208.28" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C109" gate="G$1" pin="2"/>
<wire x1="208.28" y1="88.9" x2="220.98" y2="88.9" width="0.1524" layer="91"/>
<wire x1="220.98" y1="88.9" x2="233.68" y2="88.9" width="0.1524" layer="91"/>
<wire x1="233.68" y1="88.9" x2="246.38" y2="88.9" width="0.1524" layer="91"/>
<wire x1="246.38" y1="88.9" x2="246.38" y2="91.44" width="0.1524" layer="91"/>
<junction x="246.38" y="88.9"/>
<pinref part="C106" gate="G$1" pin="2"/>
<wire x1="208.28" y1="91.44" x2="208.28" y2="88.9" width="0.1524" layer="91"/>
<junction x="208.28" y="88.9"/>
<pinref part="C107" gate="G$1" pin="2"/>
<wire x1="220.98" y1="91.44" x2="220.98" y2="88.9" width="0.1524" layer="91"/>
<junction x="220.98" y="88.9"/>
<pinref part="C108" gate="G$1" pin="2"/>
<wire x1="233.68" y1="91.44" x2="233.68" y2="88.9" width="0.1524" layer="91"/>
<junction x="233.68" y="88.9"/>
</segment>
<segment>
<wire x1="203.2" y1="119.38" x2="205.74" y2="119.38" width="0.1524" layer="91"/>
<pinref part="SUPPLY44" gate="GND" pin="GND"/>
<wire x1="205.74" y1="119.38" x2="205.74" y2="116.84" width="0.1524" layer="91"/>
<wire x1="205.74" y1="116.84" x2="205.74" y2="114.3" width="0.1524" layer="91"/>
<wire x1="203.2" y1="116.84" x2="205.74" y2="116.84" width="0.1524" layer="91"/>
<junction x="205.74" y="116.84"/>
<pinref part="U4" gate="G$1" pin="VSS"/>
<pinref part="U4" gate="G$1" pin="VSSQ"/>
</segment>
<segment>
<pinref part="C110" gate="G$1" pin="2"/>
<pinref part="SUPPLY45" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C119" gate="G$1" pin="2"/>
<wire x1="27.94" y1="226.06" x2="27.94" y2="228.6" width="0.1524" layer="91"/>
<wire x1="27.94" y1="228.6" x2="40.64" y2="228.6" width="0.1524" layer="91"/>
<wire x1="40.64" y1="228.6" x2="53.34" y2="228.6" width="0.1524" layer="91"/>
<wire x1="53.34" y1="228.6" x2="66.04" y2="228.6" width="0.1524" layer="91"/>
<wire x1="66.04" y1="228.6" x2="78.74" y2="228.6" width="0.1524" layer="91"/>
<wire x1="78.74" y1="228.6" x2="91.44" y2="228.6" width="0.1524" layer="91"/>
<wire x1="91.44" y1="228.6" x2="104.14" y2="228.6" width="0.1524" layer="91"/>
<wire x1="104.14" y1="228.6" x2="116.84" y2="228.6" width="0.1524" layer="91"/>
<wire x1="116.84" y1="228.6" x2="129.54" y2="228.6" width="0.1524" layer="91"/>
<wire x1="129.54" y1="228.6" x2="129.54" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C111" gate="G$1" pin="2"/>
<wire x1="129.54" y1="203.2" x2="116.84" y2="203.2" width="0.1524" layer="91"/>
<wire x1="116.84" y1="203.2" x2="104.14" y2="203.2" width="0.1524" layer="91"/>
<wire x1="104.14" y1="203.2" x2="91.44" y2="203.2" width="0.1524" layer="91"/>
<wire x1="91.44" y1="203.2" x2="78.74" y2="203.2" width="0.1524" layer="91"/>
<wire x1="78.74" y1="203.2" x2="66.04" y2="203.2" width="0.1524" layer="91"/>
<wire x1="66.04" y1="203.2" x2="53.34" y2="203.2" width="0.1524" layer="91"/>
<wire x1="53.34" y1="203.2" x2="40.64" y2="203.2" width="0.1524" layer="91"/>
<wire x1="40.64" y1="203.2" x2="27.94" y2="203.2" width="0.1524" layer="91"/>
<wire x1="27.94" y1="203.2" x2="27.94" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C120" gate="G$1" pin="2"/>
<wire x1="40.64" y1="226.06" x2="40.64" y2="228.6" width="0.1524" layer="91"/>
<junction x="40.64" y="228.6"/>
<pinref part="C121" gate="G$1" pin="2"/>
<wire x1="53.34" y1="226.06" x2="53.34" y2="228.6" width="0.1524" layer="91"/>
<junction x="53.34" y="228.6"/>
<pinref part="C122" gate="G$1" pin="2"/>
<wire x1="66.04" y1="226.06" x2="66.04" y2="228.6" width="0.1524" layer="91"/>
<junction x="66.04" y="228.6"/>
<pinref part="C123" gate="G$1" pin="2"/>
<wire x1="78.74" y1="226.06" x2="78.74" y2="228.6" width="0.1524" layer="91"/>
<junction x="78.74" y="228.6"/>
<pinref part="C124" gate="G$1" pin="2"/>
<wire x1="91.44" y1="226.06" x2="91.44" y2="228.6" width="0.1524" layer="91"/>
<junction x="91.44" y="228.6"/>
<pinref part="C125" gate="G$1" pin="2"/>
<wire x1="104.14" y1="226.06" x2="104.14" y2="228.6" width="0.1524" layer="91"/>
<junction x="104.14" y="228.6"/>
<pinref part="C126" gate="G$1" pin="2"/>
<wire x1="116.84" y1="226.06" x2="116.84" y2="228.6" width="0.1524" layer="91"/>
<junction x="116.84" y="228.6"/>
<pinref part="C118" gate="G$1" pin="2"/>
<wire x1="116.84" y1="205.74" x2="116.84" y2="203.2" width="0.1524" layer="91"/>
<junction x="116.84" y="203.2"/>
<pinref part="C117" gate="G$1" pin="2"/>
<wire x1="104.14" y1="205.74" x2="104.14" y2="203.2" width="0.1524" layer="91"/>
<junction x="104.14" y="203.2"/>
<pinref part="C116" gate="G$1" pin="2"/>
<wire x1="91.44" y1="205.74" x2="91.44" y2="203.2" width="0.1524" layer="91"/>
<junction x="91.44" y="203.2"/>
<pinref part="C115" gate="G$1" pin="2"/>
<wire x1="78.74" y1="205.74" x2="78.74" y2="203.2" width="0.1524" layer="91"/>
<junction x="78.74" y="203.2"/>
<pinref part="C114" gate="G$1" pin="2"/>
<wire x1="66.04" y1="205.74" x2="66.04" y2="203.2" width="0.1524" layer="91"/>
<junction x="66.04" y="203.2"/>
<pinref part="C113" gate="G$1" pin="2"/>
<wire x1="53.34" y1="205.74" x2="53.34" y2="203.2" width="0.1524" layer="91"/>
<junction x="53.34" y="203.2"/>
<pinref part="C112" gate="G$1" pin="2"/>
<wire x1="40.64" y1="205.74" x2="40.64" y2="203.2" width="0.1524" layer="91"/>
<junction x="40.64" y="203.2"/>
<pinref part="SUPPLY46" gate="GND" pin="GND"/>
<junction x="129.54" y="203.2"/>
<label x="68.58" y="228.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R57" gate="G$1" pin="1"/>
<pinref part="SUPPLY47" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VSS"/>
<wire x1="81.28" y1="111.76" x2="86.36" y2="111.76" width="0.1524" layer="91"/>
<pinref part="SUPPLY48" gate="GND" pin="GND"/>
<wire x1="86.36" y1="111.76" x2="93.98" y2="111.76" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VSSQ"/>
<wire x1="81.28" y1="109.22" x2="86.36" y2="109.22" width="0.1524" layer="91"/>
<wire x1="86.36" y1="109.22" x2="86.36" y2="111.76" width="0.1524" layer="91"/>
<junction x="86.36" y="111.76"/>
</segment>
<segment>
<pinref part="C128" gate="G$1" pin="2"/>
<pinref part="SUPPLY49" gate="GND" pin="GND"/>
<wire x1="33.02" y1="78.74" x2="33.02" y2="81.28" width="0.1524" layer="91"/>
<pinref part="R59" gate="G$1" pin="1"/>
<wire x1="25.4" y1="81.28" x2="25.4" y2="78.74" width="0.1524" layer="91"/>
<wire x1="25.4" y1="78.74" x2="33.02" y2="78.74" width="0.1524" layer="91"/>
<junction x="33.02" y="78.74"/>
</segment>
<segment>
<pinref part="C127" gate="G$1" pin="2"/>
<pinref part="SUPPLY50" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="CON3" gate="G$1" pin="SHIELD"/>
<wire x1="220.98" y1="185.42" x2="220.98" y2="182.88" width="0.1524" layer="91"/>
<pinref part="SUPPLY51" gate="GND" pin="GND"/>
<wire x1="220.98" y1="182.88" x2="241.3" y2="182.88" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="GND"/>
<wire x1="238.76" y1="193.04" x2="241.3" y2="193.04" width="0.1524" layer="91"/>
<wire x1="241.3" y1="193.04" x2="241.3" y2="182.88" width="0.1524" layer="91"/>
<junction x="241.3" y="182.88"/>
<pinref part="CON3" gate="G$1" pin="VSS"/>
<wire x1="238.76" y1="198.12" x2="241.3" y2="198.12" width="0.1524" layer="91"/>
<wire x1="241.3" y1="198.12" x2="241.3" y2="193.04" width="0.1524" layer="91"/>
<junction x="241.3" y="193.04"/>
<pinref part="C130" gate="G$1" pin="2"/>
<wire x1="241.3" y1="182.88" x2="248.92" y2="182.88" width="0.1524" layer="91"/>
<wire x1="248.92" y1="182.88" x2="261.62" y2="182.88" width="0.1524" layer="91"/>
<wire x1="261.62" y1="182.88" x2="261.62" y2="185.42" width="0.1524" layer="91"/>
<pinref part="C129" gate="G$1" pin="2"/>
<wire x1="248.92" y1="182.88" x2="248.92" y2="185.42" width="0.1524" layer="91"/>
<junction x="248.92" y="182.88"/>
</segment>
</net>
<net name="VDDS_DDR" class="0">
<segment>
<pinref part="R58" gate="G$1" pin="2"/>
<wire x1="20.32" y1="99.06" x2="17.78" y2="99.06" width="0.1524" layer="91"/>
<label x="17.78" y="99.06" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VDDQ"/>
<wire x1="45.72" y1="109.22" x2="43.18" y2="109.22" width="0.1524" layer="91"/>
<wire x1="43.18" y1="109.22" x2="43.18" y2="111.76" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VDD"/>
<wire x1="43.18" y1="111.76" x2="45.72" y2="111.76" width="0.1524" layer="91"/>
<wire x1="43.18" y1="111.76" x2="40.64" y2="111.76" width="0.1524" layer="91"/>
<junction x="43.18" y="111.76"/>
<label x="40.64" y="111.76" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="22.86" y1="215.9" x2="27.94" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C118" gate="G$1" pin="1"/>
<wire x1="27.94" y1="215.9" x2="40.64" y2="215.9" width="0.1524" layer="91"/>
<wire x1="40.64" y1="215.9" x2="53.34" y2="215.9" width="0.1524" layer="91"/>
<wire x1="53.34" y1="215.9" x2="66.04" y2="215.9" width="0.1524" layer="91"/>
<wire x1="66.04" y1="215.9" x2="78.74" y2="215.9" width="0.1524" layer="91"/>
<wire x1="78.74" y1="215.9" x2="91.44" y2="215.9" width="0.1524" layer="91"/>
<wire x1="91.44" y1="215.9" x2="104.14" y2="215.9" width="0.1524" layer="91"/>
<wire x1="104.14" y1="215.9" x2="116.84" y2="215.9" width="0.1524" layer="91"/>
<wire x1="116.84" y1="213.36" x2="116.84" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C126" gate="G$1" pin="1"/>
<wire x1="116.84" y1="215.9" x2="116.84" y2="218.44" width="0.1524" layer="91"/>
<junction x="116.84" y="215.9"/>
<pinref part="C125" gate="G$1" pin="1"/>
<wire x1="104.14" y1="215.9" x2="104.14" y2="218.44" width="0.1524" layer="91"/>
<junction x="104.14" y="215.9"/>
<pinref part="C124" gate="G$1" pin="1"/>
<wire x1="91.44" y1="215.9" x2="91.44" y2="218.44" width="0.1524" layer="91"/>
<junction x="91.44" y="215.9"/>
<pinref part="C123" gate="G$1" pin="1"/>
<wire x1="78.74" y1="215.9" x2="78.74" y2="218.44" width="0.1524" layer="91"/>
<junction x="78.74" y="215.9"/>
<pinref part="C122" gate="G$1" pin="1"/>
<wire x1="66.04" y1="215.9" x2="66.04" y2="218.44" width="0.1524" layer="91"/>
<junction x="66.04" y="215.9"/>
<pinref part="C121" gate="G$1" pin="1"/>
<wire x1="53.34" y1="215.9" x2="53.34" y2="218.44" width="0.1524" layer="91"/>
<junction x="53.34" y="215.9"/>
<pinref part="C120" gate="G$1" pin="1"/>
<wire x1="40.64" y1="215.9" x2="40.64" y2="218.44" width="0.1524" layer="91"/>
<junction x="40.64" y="215.9"/>
<pinref part="C119" gate="G$1" pin="1"/>
<wire x1="27.94" y1="215.9" x2="27.94" y2="218.44" width="0.1524" layer="91"/>
<junction x="27.94" y="215.9"/>
<pinref part="C111" gate="G$1" pin="1"/>
<wire x1="27.94" y1="215.9" x2="27.94" y2="213.36" width="0.1524" layer="91"/>
<pinref part="C112" gate="G$1" pin="1"/>
<wire x1="40.64" y1="215.9" x2="40.64" y2="213.36" width="0.1524" layer="91"/>
<pinref part="C113" gate="G$1" pin="1"/>
<wire x1="53.34" y1="215.9" x2="53.34" y2="213.36" width="0.1524" layer="91"/>
<pinref part="C114" gate="G$1" pin="1"/>
<wire x1="66.04" y1="215.9" x2="66.04" y2="213.36" width="0.1524" layer="91"/>
<pinref part="C115" gate="G$1" pin="1"/>
<wire x1="78.74" y1="215.9" x2="78.74" y2="213.36" width="0.1524" layer="91"/>
<pinref part="C116" gate="G$1" pin="1"/>
<wire x1="91.44" y1="215.9" x2="91.44" y2="213.36" width="0.1524" layer="91"/>
<pinref part="C117" gate="G$1" pin="1"/>
<wire x1="104.14" y1="215.9" x2="104.14" y2="213.36" width="0.1524" layer="91"/>
<label x="22.86" y="215.9" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R116" gate="G$1" pin="2"/>
<wire x1="109.22" y1="132.08" x2="109.22" y2="134.62" width="0.1524" layer="91"/>
<label x="109.22" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD_3V3A" class="0">
<segment>
<pinref part="R45" gate="G$1" pin="2"/>
<wire x1="151.892" y1="51.054" x2="151.892" y2="61.214" width="0.1524" layer="91"/>
<pinref part="C104" gate="G$1" pin="1"/>
<wire x1="151.892" y1="61.214" x2="156.972" y2="61.214" width="0.1524" layer="91"/>
<wire x1="156.972" y1="61.214" x2="156.972" y2="58.674" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="VCC"/>
<wire x1="151.892" y1="61.214" x2="134.112" y2="61.214" width="0.1524" layer="91"/>
<junction x="151.892" y="61.214"/>
<label x="136.652" y="61.214" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C0_SDA" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="SDA"/>
<wire x1="106.172" y1="58.674" x2="101.092" y2="58.674" width="0.1524" layer="91"/>
<label x="101.092" y="58.674" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="I2C0_SCL" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="SCL"/>
<wire x1="106.172" y1="61.214" x2="101.092" y2="61.214" width="0.1524" layer="91"/>
<label x="101.092" y="61.214" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VDD_3V3B" class="0">
<segment>
<pinref part="C109" gate="G$1" pin="1"/>
<wire x1="193.04" y1="101.6" x2="195.58" y2="101.6" width="0.1524" layer="91"/>
<wire x1="195.58" y1="101.6" x2="208.28" y2="101.6" width="0.1524" layer="91"/>
<wire x1="208.28" y1="101.6" x2="220.98" y2="101.6" width="0.1524" layer="91"/>
<wire x1="220.98" y1="101.6" x2="233.68" y2="101.6" width="0.1524" layer="91"/>
<wire x1="233.68" y1="101.6" x2="246.38" y2="101.6" width="0.1524" layer="91"/>
<wire x1="246.38" y1="101.6" x2="246.38" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C108" gate="G$1" pin="1"/>
<wire x1="233.68" y1="99.06" x2="233.68" y2="101.6" width="0.1524" layer="91"/>
<junction x="233.68" y="101.6"/>
<pinref part="C107" gate="G$1" pin="1"/>
<wire x1="220.98" y1="99.06" x2="220.98" y2="101.6" width="0.1524" layer="91"/>
<junction x="220.98" y="101.6"/>
<pinref part="C106" gate="G$1" pin="1"/>
<wire x1="208.28" y1="99.06" x2="208.28" y2="101.6" width="0.1524" layer="91"/>
<junction x="208.28" y="101.6"/>
<pinref part="C105" gate="G$1" pin="1"/>
<wire x1="195.58" y1="99.06" x2="195.58" y2="101.6" width="0.1524" layer="91"/>
<junction x="195.58" y="101.6"/>
<label x="193.04" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="170.18" y1="119.38" x2="165.1" y2="119.38" width="0.1524" layer="91"/>
<wire x1="165.1" y1="119.38" x2="165.1" y2="116.84" width="0.1524" layer="91"/>
<wire x1="165.1" y1="116.84" x2="170.18" y2="116.84" width="0.1524" layer="91"/>
<wire x1="165.1" y1="116.84" x2="160.02" y2="116.84" width="0.1524" layer="91"/>
<junction x="165.1" y="116.84"/>
<label x="160.02" y="116.84" size="1.778" layer="95" rot="R180"/>
<pinref part="U4" gate="G$1" pin="VCC"/>
<pinref part="U4" gate="G$1" pin="VCCQ"/>
</segment>
<segment>
<pinref part="R46" gate="G$1" pin="2"/>
<wire x1="205.74" y1="154.94" x2="205.74" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R53" gate="G$1" pin="2"/>
<wire x1="205.74" y1="157.48" x2="213.36" y2="157.48" width="0.1524" layer="91"/>
<wire x1="213.36" y1="157.48" x2="220.98" y2="157.48" width="0.1524" layer="91"/>
<wire x1="220.98" y1="157.48" x2="228.6" y2="157.48" width="0.1524" layer="91"/>
<wire x1="228.6" y1="157.48" x2="236.22" y2="157.48" width="0.1524" layer="91"/>
<wire x1="236.22" y1="157.48" x2="243.84" y2="157.48" width="0.1524" layer="91"/>
<wire x1="243.84" y1="157.48" x2="251.46" y2="157.48" width="0.1524" layer="91"/>
<wire x1="251.46" y1="157.48" x2="259.08" y2="157.48" width="0.1524" layer="91"/>
<wire x1="259.08" y1="157.48" x2="259.08" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R47" gate="G$1" pin="2"/>
<wire x1="213.36" y1="154.94" x2="213.36" y2="157.48" width="0.1524" layer="91"/>
<junction x="213.36" y="157.48"/>
<pinref part="R48" gate="G$1" pin="2"/>
<wire x1="220.98" y1="154.94" x2="220.98" y2="157.48" width="0.1524" layer="91"/>
<junction x="220.98" y="157.48"/>
<pinref part="R49" gate="G$1" pin="2"/>
<wire x1="228.6" y1="154.94" x2="228.6" y2="157.48" width="0.1524" layer="91"/>
<junction x="228.6" y="157.48"/>
<pinref part="R50" gate="G$1" pin="2"/>
<wire x1="236.22" y1="154.94" x2="236.22" y2="157.48" width="0.1524" layer="91"/>
<junction x="236.22" y="157.48"/>
<pinref part="R51" gate="G$1" pin="2"/>
<wire x1="243.84" y1="154.94" x2="243.84" y2="157.48" width="0.1524" layer="91"/>
<junction x="243.84" y="157.48"/>
<pinref part="R52" gate="G$1" pin="2"/>
<wire x1="251.46" y1="154.94" x2="251.46" y2="157.48" width="0.1524" layer="91"/>
<junction x="251.46" y="157.48"/>
<label x="185.42" y="157.48" size="1.778" layer="95"/>
<pinref part="R54" gate="G$1" pin="2"/>
<wire x1="167.64" y1="154.94" x2="167.64" y2="157.48" width="0.1524" layer="91"/>
<wire x1="167.64" y1="157.48" x2="160.02" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R56" gate="G$1" pin="2"/>
<wire x1="160.02" y1="157.48" x2="152.4" y2="157.48" width="0.1524" layer="91"/>
<wire x1="152.4" y1="157.48" x2="152.4" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R55" gate="G$1" pin="2"/>
<wire x1="160.02" y1="154.94" x2="160.02" y2="157.48" width="0.1524" layer="91"/>
<junction x="160.02" y="157.48"/>
<wire x1="167.64" y1="157.48" x2="205.74" y2="157.48" width="0.1524" layer="91"/>
<junction x="167.64" y="157.48"/>
<junction x="205.74" y="157.48"/>
</segment>
<segment>
<pinref part="R63" gate="G$1" pin="2"/>
<wire x1="170.18" y1="226.06" x2="170.18" y2="228.6" width="0.1524" layer="91"/>
<pinref part="R66" gate="G$1" pin="2"/>
<wire x1="170.18" y1="228.6" x2="177.8" y2="228.6" width="0.1524" layer="91"/>
<wire x1="177.8" y1="228.6" x2="185.42" y2="228.6" width="0.1524" layer="91"/>
<wire x1="185.42" y1="228.6" x2="193.04" y2="228.6" width="0.1524" layer="91"/>
<wire x1="193.04" y1="228.6" x2="198.12" y2="228.6" width="0.1524" layer="91"/>
<wire x1="198.12" y1="228.6" x2="243.84" y2="228.6" width="0.1524" layer="91"/>
<wire x1="243.84" y1="228.6" x2="251.46" y2="228.6" width="0.1524" layer="91"/>
<wire x1="251.46" y1="228.6" x2="259.08" y2="228.6" width="0.1524" layer="91"/>
<wire x1="259.08" y1="228.6" x2="259.08" y2="226.06" width="0.1524" layer="91"/>
<pinref part="R62" gate="G$1" pin="2"/>
<wire x1="177.8" y1="226.06" x2="177.8" y2="228.6" width="0.1524" layer="91"/>
<junction x="177.8" y="228.6"/>
<pinref part="R61" gate="G$1" pin="2"/>
<wire x1="185.42" y1="226.06" x2="185.42" y2="228.6" width="0.1524" layer="91"/>
<junction x="185.42" y="228.6"/>
<pinref part="R60" gate="G$1" pin="2"/>
<wire x1="193.04" y1="226.06" x2="193.04" y2="228.6" width="0.1524" layer="91"/>
<junction x="193.04" y="228.6"/>
<pinref part="R64" gate="G$1" pin="2"/>
<wire x1="243.84" y1="226.06" x2="243.84" y2="228.6" width="0.1524" layer="91"/>
<junction x="243.84" y="228.6"/>
<pinref part="R65" gate="G$1" pin="2"/>
<wire x1="251.46" y1="226.06" x2="251.46" y2="228.6" width="0.1524" layer="91"/>
<junction x="251.46" y="228.6"/>
<pinref part="CON3" gate="G$1" pin="VDD"/>
<wire x1="200.66" y1="213.36" x2="198.12" y2="213.36" width="0.1524" layer="91"/>
<wire x1="198.12" y1="213.36" x2="198.12" y2="228.6" width="0.1524" layer="91"/>
<junction x="198.12" y="228.6"/>
<label x="213.36" y="228.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C129" gate="G$1" pin="1"/>
<wire x1="248.92" y1="193.04" x2="248.92" y2="195.58" width="0.1524" layer="91"/>
<pinref part="C130" gate="G$1" pin="1"/>
<wire x1="248.92" y1="195.58" x2="261.62" y2="195.58" width="0.1524" layer="91"/>
<wire x1="261.62" y1="195.58" x2="261.62" y2="193.04" width="0.1524" layer="91"/>
<wire x1="261.62" y1="195.58" x2="264.16" y2="195.58" width="0.1524" layer="91"/>
<junction x="261.62" y="195.58"/>
<label x="264.16" y="195.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A0" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A0"/>
<wire x1="81.28" y1="193.04" x2="86.36" y2="193.04" width="0.1524" layer="91"/>
<label x="86.36" y="193.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A1"/>
<wire x1="81.28" y1="190.5" x2="86.36" y2="190.5" width="0.1524" layer="91"/>
<label x="86.36" y="190.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A2"/>
<wire x1="81.28" y1="187.96" x2="86.36" y2="187.96" width="0.1524" layer="91"/>
<label x="86.36" y="187.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A9" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A9"/>
<wire x1="81.28" y1="170.18" x2="86.36" y2="170.18" width="0.1524" layer="91"/>
<label x="86.36" y="170.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A4" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A4"/>
<wire x1="81.28" y1="182.88" x2="86.36" y2="182.88" width="0.1524" layer="91"/>
<label x="86.36" y="182.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A3"/>
<wire x1="81.28" y1="185.42" x2="86.36" y2="185.42" width="0.1524" layer="91"/>
<label x="86.36" y="185.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A8" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A8"/>
<wire x1="81.28" y1="172.72" x2="86.36" y2="172.72" width="0.1524" layer="91"/>
<label x="86.36" y="172.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A6" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A6"/>
<wire x1="81.28" y1="177.8" x2="86.36" y2="177.8" width="0.1524" layer="91"/>
<label x="86.36" y="177.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A7" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A7"/>
<wire x1="81.28" y1="175.26" x2="86.36" y2="175.26" width="0.1524" layer="91"/>
<label x="86.36" y="175.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A12" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A12/BC#"/>
<wire x1="81.28" y1="162.56" x2="86.36" y2="162.56" width="0.1524" layer="91"/>
<label x="86.36" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A10" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A10/AP"/>
<wire x1="81.28" y1="167.64" x2="86.36" y2="167.64" width="0.1524" layer="91"/>
<label x="86.36" y="167.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A11" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A11"/>
<wire x1="81.28" y1="165.1" x2="86.36" y2="165.1" width="0.1524" layer="91"/>
<label x="86.36" y="165.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A14" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A14"/>
<wire x1="81.28" y1="157.48" x2="86.36" y2="157.48" width="0.1524" layer="91"/>
<label x="86.36" y="157.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A13" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A13"/>
<wire x1="81.28" y1="160.02" x2="86.36" y2="160.02" width="0.1524" layer="91"/>
<label x="86.36" y="160.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_A5" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A5"/>
<wire x1="81.28" y1="180.34" x2="86.36" y2="180.34" width="0.1524" layer="91"/>
<label x="86.36" y="180.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_BA2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="BA2"/>
<wire x1="45.72" y1="127" x2="40.64" y2="127" width="0.1524" layer="91"/>
<label x="40.64" y="127" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_BA0" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="BA0"/>
<wire x1="45.72" y1="132.08" x2="40.64" y2="132.08" width="0.1524" layer="91"/>
<label x="40.64" y="132.08" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_BA1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="BA1"/>
<wire x1="45.72" y1="129.54" x2="40.64" y2="129.54" width="0.1524" layer="91"/>
<label x="40.64" y="129.54" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D8" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="DQ8"/>
<wire x1="45.72" y1="172.72" x2="40.64" y2="172.72" width="0.1524" layer="91"/>
<label x="40.64" y="172.72" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D0" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="DQ0"/>
<wire x1="45.72" y1="193.04" x2="40.64" y2="193.04" width="0.1524" layer="91"/>
<label x="40.64" y="193.04" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="DQ1"/>
<wire x1="45.72" y1="190.5" x2="40.64" y2="190.5" width="0.1524" layer="91"/>
<label x="40.64" y="190.5" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D15" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="DQ15"/>
<wire x1="45.72" y1="154.94" x2="40.64" y2="154.94" width="0.1524" layer="91"/>
<label x="40.64" y="154.94" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="DQ2"/>
<wire x1="45.72" y1="187.96" x2="40.64" y2="187.96" width="0.1524" layer="91"/>
<label x="40.64" y="187.96" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="DQ3"/>
<wire x1="45.72" y1="185.42" x2="40.64" y2="185.42" width="0.1524" layer="91"/>
<label x="40.64" y="185.42" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D4" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="DQ4"/>
<wire x1="45.72" y1="182.88" x2="40.64" y2="182.88" width="0.1524" layer="91"/>
<label x="40.64" y="182.88" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D5" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="DQ5"/>
<wire x1="45.72" y1="180.34" x2="40.64" y2="180.34" width="0.1524" layer="91"/>
<label x="40.64" y="180.34" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D6" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="DQ6"/>
<wire x1="45.72" y1="177.8" x2="40.64" y2="177.8" width="0.1524" layer="91"/>
<label x="40.64" y="177.8" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D7" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="DQ7"/>
<wire x1="45.72" y1="175.26" x2="40.64" y2="175.26" width="0.1524" layer="91"/>
<label x="40.64" y="175.26" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D9" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="DQ9"/>
<wire x1="45.72" y1="170.18" x2="40.64" y2="170.18" width="0.1524" layer="91"/>
<label x="40.64" y="170.18" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D10" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="DQ10"/>
<wire x1="45.72" y1="167.64" x2="40.64" y2="167.64" width="0.1524" layer="91"/>
<label x="40.64" y="167.64" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D11" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="DQ11"/>
<wire x1="45.72" y1="165.1" x2="40.64" y2="165.1" width="0.1524" layer="91"/>
<label x="40.64" y="165.1" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D12" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="DQ12"/>
<wire x1="45.72" y1="162.56" x2="40.64" y2="162.56" width="0.1524" layer="91"/>
<label x="40.64" y="162.56" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D13" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="DQ13"/>
<wire x1="45.72" y1="160.02" x2="40.64" y2="160.02" width="0.1524" layer="91"/>
<label x="40.64" y="160.02" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_D14" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="DQ14"/>
<wire x1="45.72" y1="157.48" x2="40.64" y2="157.48" width="0.1524" layer="91"/>
<label x="40.64" y="157.48" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_CLK" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="CK"/>
<wire x1="81.28" y1="147.32" x2="86.36" y2="147.32" width="0.1524" layer="91"/>
<label x="86.36" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_CLKN" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="CK#"/>
<wire x1="81.28" y1="144.78" x2="86.36" y2="144.78" width="0.1524" layer="91"/>
<label x="86.36" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_CKE" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="CKE"/>
<wire x1="81.28" y1="142.24" x2="86.36" y2="142.24" width="0.1524" layer="91"/>
<label x="86.36" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_ODT" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="ODT"/>
<wire x1="81.28" y1="119.38" x2="86.36" y2="119.38" width="0.1524" layer="91"/>
<label x="86.36" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_RASN" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="RAS#"/>
<wire x1="81.28" y1="132.08" x2="86.36" y2="132.08" width="0.1524" layer="91"/>
<label x="86.36" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_CSN" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="CS#"/>
<wire x1="81.28" y1="137.16" x2="86.36" y2="137.16" width="0.1524" layer="91"/>
<label x="86.36" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_CASN" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="CAS#"/>
<wire x1="81.28" y1="129.54" x2="86.36" y2="129.54" width="0.1524" layer="91"/>
<label x="86.36" y="129.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_WEN" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="WE#"/>
<wire x1="81.28" y1="127" x2="86.36" y2="127" width="0.1524" layer="91"/>
<label x="86.36" y="127" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_DQM0" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="LDM"/>
<wire x1="45.72" y1="119.38" x2="40.64" y2="119.38" width="0.1524" layer="91"/>
<label x="40.64" y="119.38" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_DQSN0" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="LDQS#"/>
<wire x1="45.72" y1="147.32" x2="40.64" y2="147.32" width="0.1524" layer="91"/>
<label x="40.64" y="147.32" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_DQM1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="UDM"/>
<wire x1="45.72" y1="121.92" x2="40.64" y2="121.92" width="0.1524" layer="91"/>
<label x="40.64" y="121.92" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_DQS1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="UDQS"/>
<wire x1="45.72" y1="139.7" x2="40.64" y2="139.7" width="0.1524" layer="91"/>
<label x="40.64" y="139.7" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_DQSN1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="UDQS#"/>
<wire x1="45.72" y1="137.16" x2="40.64" y2="137.16" width="0.1524" layer="91"/>
<label x="40.64" y="137.16" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_NRST" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="RESET#"/>
<label x="86.36" y="121.92" size="1.778" layer="95"/>
<pinref part="R116" gate="G$1" pin="1"/>
<wire x1="81.28" y1="121.92" x2="109.22" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DDR_VREF" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="VREFDQ"/>
<pinref part="C127" gate="G$1" pin="1"/>
<wire x1="45.72" y1="101.6" x2="43.18" y2="101.6" width="0.1524" layer="91"/>
<wire x1="43.18" y1="101.6" x2="43.18" y2="99.06" width="0.1524" layer="91"/>
<pinref part="R59" gate="G$1" pin="2"/>
<wire x1="25.4" y1="91.44" x2="33.02" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C128" gate="G$1" pin="1"/>
<wire x1="33.02" y1="91.44" x2="33.02" y2="88.9" width="0.1524" layer="91"/>
<wire x1="33.02" y1="91.44" x2="33.02" y2="99.06" width="0.1524" layer="91"/>
<junction x="33.02" y="91.44"/>
<pinref part="R58" gate="G$1" pin="1"/>
<wire x1="33.02" y1="99.06" x2="30.48" y2="99.06" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VREFCA"/>
<wire x1="45.72" y1="104.14" x2="43.18" y2="104.14" width="0.1524" layer="91"/>
<wire x1="43.18" y1="104.14" x2="33.02" y2="104.14" width="0.1524" layer="91"/>
<wire x1="33.02" y1="104.14" x2="33.02" y2="99.06" width="0.1524" layer="91"/>
<junction x="33.02" y="99.06"/>
<wire x1="43.18" y1="101.6" x2="43.18" y2="104.14" width="0.1524" layer="91"/>
<junction x="43.18" y="101.6"/>
<junction x="43.18" y="104.14"/>
<label x="30.48" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC1_CLK" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="CLK"/>
<pinref part="R54" gate="G$1" pin="1"/>
<wire x1="170.18" y1="142.24" x2="167.64" y2="142.24" width="0.1524" layer="91"/>
<wire x1="167.64" y1="142.24" x2="167.64" y2="144.78" width="0.1524" layer="91"/>
<wire x1="167.64" y1="142.24" x2="149.86" y2="142.24" width="0.1524" layer="91"/>
<junction x="167.64" y="142.24"/>
<label x="149.86" y="142.24" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MMC1_CMD" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="CMD"/>
<pinref part="R56" gate="G$1" pin="1"/>
<wire x1="170.18" y1="132.08" x2="152.4" y2="132.08" width="0.1524" layer="91"/>
<wire x1="152.4" y1="132.08" x2="152.4" y2="144.78" width="0.1524" layer="91"/>
<wire x1="152.4" y1="132.08" x2="149.86" y2="132.08" width="0.1524" layer="91"/>
<junction x="152.4" y="132.08"/>
<label x="149.86" y="132.08" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MMC1_DAT0" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="DAT0"/>
<pinref part="R46" gate="G$1" pin="1"/>
<wire x1="203.2" y1="142.24" x2="205.74" y2="142.24" width="0.1524" layer="91"/>
<wire x1="205.74" y1="142.24" x2="205.74" y2="144.78" width="0.1524" layer="91"/>
<wire x1="205.74" y1="142.24" x2="261.62" y2="142.24" width="0.1524" layer="91"/>
<junction x="205.74" y="142.24"/>
<label x="261.62" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC1_DAT1" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="DAT1"/>
<pinref part="R47" gate="G$1" pin="1"/>
<wire x1="203.2" y1="139.7" x2="213.36" y2="139.7" width="0.1524" layer="91"/>
<wire x1="213.36" y1="139.7" x2="213.36" y2="144.78" width="0.1524" layer="91"/>
<wire x1="213.36" y1="139.7" x2="261.62" y2="139.7" width="0.1524" layer="91"/>
<junction x="213.36" y="139.7"/>
<label x="261.62" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC1_DAT2" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="DAT2"/>
<pinref part="R48" gate="G$1" pin="1"/>
<wire x1="203.2" y1="137.16" x2="220.98" y2="137.16" width="0.1524" layer="91"/>
<wire x1="220.98" y1="137.16" x2="220.98" y2="144.78" width="0.1524" layer="91"/>
<wire x1="220.98" y1="137.16" x2="261.62" y2="137.16" width="0.1524" layer="91"/>
<junction x="220.98" y="137.16"/>
<label x="261.62" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC1_DAT3" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="DAT3"/>
<pinref part="R49" gate="G$1" pin="1"/>
<wire x1="203.2" y1="134.62" x2="228.6" y2="134.62" width="0.1524" layer="91"/>
<wire x1="228.6" y1="134.62" x2="228.6" y2="144.78" width="0.1524" layer="91"/>
<wire x1="228.6" y1="134.62" x2="261.62" y2="134.62" width="0.1524" layer="91"/>
<junction x="228.6" y="134.62"/>
<label x="261.62" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC1_DAT4" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="DAT4"/>
<pinref part="R50" gate="G$1" pin="1"/>
<wire x1="203.2" y1="132.08" x2="236.22" y2="132.08" width="0.1524" layer="91"/>
<wire x1="236.22" y1="132.08" x2="236.22" y2="144.78" width="0.1524" layer="91"/>
<wire x1="236.22" y1="132.08" x2="261.62" y2="132.08" width="0.1524" layer="91"/>
<junction x="236.22" y="132.08"/>
<label x="261.62" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC1_DAT5" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="DAT5"/>
<pinref part="R51" gate="G$1" pin="1"/>
<wire x1="203.2" y1="129.54" x2="243.84" y2="129.54" width="0.1524" layer="91"/>
<wire x1="243.84" y1="129.54" x2="243.84" y2="144.78" width="0.1524" layer="91"/>
<wire x1="243.84" y1="129.54" x2="261.62" y2="129.54" width="0.1524" layer="91"/>
<junction x="243.84" y="129.54"/>
<label x="261.62" y="129.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC1_DAT6" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="DAT6"/>
<pinref part="R52" gate="G$1" pin="1"/>
<wire x1="203.2" y1="127" x2="251.46" y2="127" width="0.1524" layer="91"/>
<wire x1="251.46" y1="127" x2="251.46" y2="144.78" width="0.1524" layer="91"/>
<wire x1="251.46" y1="127" x2="261.62" y2="127" width="0.1524" layer="91"/>
<junction x="251.46" y="127"/>
<label x="261.62" y="127" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC1_DAT7" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="DAT7"/>
<pinref part="R53" gate="G$1" pin="1"/>
<wire x1="203.2" y1="124.46" x2="259.08" y2="124.46" width="0.1524" layer="91"/>
<wire x1="259.08" y1="124.46" x2="259.08" y2="144.78" width="0.1524" layer="91"/>
<wire x1="259.08" y1="124.46" x2="261.62" y2="124.46" width="0.1524" layer="91"/>
<junction x="259.08" y="124.46"/>
<label x="261.62" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC0_CLK" class="0">
<segment>
<pinref part="CON3" gate="G$1" pin="CLK"/>
<wire x1="238.76" y1="208.28" x2="251.46" y2="208.28" width="0.1524" layer="91"/>
<pinref part="R65" gate="G$1" pin="1"/>
<wire x1="251.46" y1="208.28" x2="261.62" y2="208.28" width="0.1524" layer="91"/>
<wire x1="251.46" y1="215.9" x2="251.46" y2="208.28" width="0.1524" layer="91"/>
<junction x="251.46" y="208.28"/>
<label x="261.62" y="208.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC0_CMD" class="0">
<segment>
<pinref part="CON3" gate="G$1" pin="CMD"/>
<wire x1="238.76" y1="213.36" x2="243.84" y2="213.36" width="0.1524" layer="91"/>
<pinref part="R64" gate="G$1" pin="1"/>
<wire x1="243.84" y1="213.36" x2="261.62" y2="213.36" width="0.1524" layer="91"/>
<wire x1="243.84" y1="215.9" x2="243.84" y2="213.36" width="0.1524" layer="91"/>
<junction x="243.84" y="213.36"/>
<label x="261.62" y="213.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="MMC0_DAT0" class="0">
<segment>
<pinref part="R60" gate="G$1" pin="1"/>
<wire x1="193.04" y1="215.9" x2="193.04" y2="208.28" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="DAT0"/>
<wire x1="193.04" y1="208.28" x2="200.66" y2="208.28" width="0.1524" layer="91"/>
<wire x1="193.04" y1="208.28" x2="167.64" y2="208.28" width="0.1524" layer="91"/>
<junction x="193.04" y="208.28"/>
<label x="167.64" y="208.28" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MMC0_DAT1" class="0">
<segment>
<pinref part="R61" gate="G$1" pin="1"/>
<wire x1="185.42" y1="215.9" x2="185.42" y2="203.2" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="DAT1"/>
<wire x1="185.42" y1="203.2" x2="200.66" y2="203.2" width="0.1524" layer="91"/>
<wire x1="185.42" y1="203.2" x2="167.64" y2="203.2" width="0.1524" layer="91"/>
<junction x="185.42" y="203.2"/>
<label x="167.64" y="203.2" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MMC0_DAT2" class="0">
<segment>
<pinref part="R62" gate="G$1" pin="1"/>
<wire x1="177.8" y1="215.9" x2="177.8" y2="198.12" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="DAT2"/>
<wire x1="177.8" y1="198.12" x2="200.66" y2="198.12" width="0.1524" layer="91"/>
<wire x1="177.8" y1="198.12" x2="167.64" y2="198.12" width="0.1524" layer="91"/>
<junction x="177.8" y="198.12"/>
<label x="167.64" y="198.12" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MMC0_DAT3" class="0">
<segment>
<pinref part="CON3" gate="G$1" pin="DAT3/CD"/>
<pinref part="R63" gate="G$1" pin="1"/>
<wire x1="200.66" y1="193.04" x2="170.18" y2="193.04" width="0.1524" layer="91"/>
<wire x1="170.18" y1="193.04" x2="170.18" y2="215.9" width="0.1524" layer="91"/>
<wire x1="170.18" y1="193.04" x2="167.64" y2="193.04" width="0.1524" layer="91"/>
<junction x="170.18" y="193.04"/>
<label x="167.64" y="193.04" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MMC0_CD" class="0">
<segment>
<pinref part="CON3" gate="G$1" pin="CD"/>
<wire x1="238.76" y1="203.2" x2="259.08" y2="203.2" width="0.1524" layer="91"/>
<pinref part="R66" gate="G$1" pin="1"/>
<wire x1="259.08" y1="203.2" x2="261.62" y2="203.2" width="0.1524" layer="91"/>
<wire x1="259.08" y1="215.9" x2="259.08" y2="203.2" width="0.1524" layer="91"/>
<junction x="259.08" y="203.2"/>
<label x="261.62" y="203.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="ID_WP" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="WP"/>
<pinref part="R45" gate="G$1" pin="1"/>
<wire x1="134.112" y1="51.054" x2="141.732" y2="51.054" width="0.1524" layer="91"/>
<label x="134.112" y="51.054" size="1.778" layer="95"/>
</segment>
</net>
<net name="EMMC_NRST" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="NRST"/>
<pinref part="R55" gate="G$1" pin="1"/>
<wire x1="170.18" y1="137.16" x2="160.02" y2="137.16" width="0.1524" layer="91"/>
<wire x1="160.02" y1="137.16" x2="160.02" y2="144.78" width="0.1524" layer="91"/>
<wire x1="160.02" y1="137.16" x2="149.86" y2="137.16" width="0.1524" layer="91"/>
<junction x="160.02" y="137.16"/>
<label x="149.86" y="137.16" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="EMMC_VDDI" class="0">
<segment>
<wire x1="170.18" y1="124.46" x2="167.64" y2="124.46" width="0.1524" layer="91"/>
<wire x1="167.64" y1="124.46" x2="167.64" y2="111.76" width="0.1524" layer="91"/>
<pinref part="C110" gate="G$1" pin="1"/>
<label x="167.64" y="124.46" size="1.778" layer="95" rot="R180"/>
<pinref part="U4" gate="G$1" pin="VDDI"/>
</segment>
</net>
<net name="DDR3_ZQ" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="ZQ"/>
<pinref part="R57" gate="G$1" pin="2"/>
<wire x1="81.28" y1="104.14" x2="83.82" y2="104.14" width="0.1524" layer="91"/>
<wire x1="83.82" y1="104.14" x2="83.82" y2="101.6" width="0.1524" layer="91"/>
<label x="83.82" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="DDR_DQS0" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="LDQS"/>
<wire x1="45.72" y1="144.78" x2="40.64" y2="144.78" width="0.1524" layer="91"/>
<label x="40.64" y="144.78" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DDR_A15" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="A15"/>
<wire x1="81.28" y1="154.94" x2="86.36" y2="154.94" width="0.1524" layer="91"/>
<label x="86.36" y="154.94" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="119.38" y="312.42" size="3.048" layer="97">ETHERNET-1 CIRCUIT</text>
<text x="3.302" y="2.286" size="10.16" layer="97">ETHERNET MODULES</text>
<text x="78.74" y="157.48" size="3.048" layer="97">ETHERNET-2 CIRCUIT</text>
<text x="3.048" y="17.272" size="2.54" layer="97">Linux Micro-PC
Rev 1.0
SBS 2015</text>
</plain>
<instances>
<instance part="U5" gate="G$1" x="88.9" y="226.06"/>
<instance part="CON2" gate="MAGJACK_RJ45" x="218.44" y="236.22"/>
<instance part="SUPPLY61" gate="GND" x="198.12" y="213.36"/>
<instance part="C133" gate="G$1" x="185.42" y="223.52"/>
<instance part="R77" gate="G$1" x="185.42" y="256.54" rot="R90"/>
<instance part="R78" gate="G$1" x="172.72" y="256.54" rot="R90"/>
<instance part="R79" gate="G$1" x="162.56" y="256.54" rot="R90"/>
<instance part="R80" gate="G$1" x="152.4" y="256.54" rot="R90"/>
<instance part="R81" gate="G$1" x="142.24" y="256.54" rot="R90"/>
<instance part="SUPPLY62" gate="GND" x="259.08" y="220.98"/>
<instance part="R82" gate="G$1" x="251.46" y="248.92"/>
<instance part="R83" gate="G$1" x="251.46" y="241.3"/>
<instance part="R84" gate="G$1" x="251.46" y="226.06"/>
<instance part="R85" gate="G$1" x="243.84" y="294.64"/>
<instance part="R86" gate="G$1" x="243.84" y="287.02"/>
<instance part="R87" gate="G$1" x="243.84" y="279.4"/>
<instance part="R88" gate="G$1" x="243.84" y="271.78"/>
<instance part="R89" gate="G$1" x="185.42" y="304.8"/>
<instance part="R90" gate="G$1" x="185.42" y="297.18"/>
<instance part="R91" gate="G$1" x="185.42" y="289.56"/>
<instance part="SUPPLY63" gate="GND" x="251.46" y="266.7"/>
<instance part="SUPPLY64" gate="GND" x="68.58" y="187.96"/>
<instance part="R92" gate="G$1" x="60.96" y="198.12" rot="R90"/>
<instance part="C134" gate="G$1" x="20.32" y="193.04"/>
<instance part="C135" gate="G$1" x="33.02" y="193.04"/>
<instance part="SUPPLY65" gate="GND" x="20.32" y="182.88"/>
<instance part="C136" gate="G$1" x="12.7" y="215.9"/>
<instance part="C137" gate="G$1" x="25.4" y="215.9"/>
<instance part="C138" gate="G$1" x="38.1" y="215.9"/>
<instance part="SUPPLY66" gate="GND" x="12.7" y="205.74"/>
<instance part="FB6" gate="G$1" x="165.1" y="289.56"/>
<instance part="C139" gate="G$1" x="177.8" y="281.94"/>
<instance part="SUPPLY67" gate="GND" x="177.8" y="274.32"/>
<instance part="R93" gate="G$1" x="137.16" y="289.56"/>
<instance part="R94" gate="G$1" x="33.02" y="261.62"/>
<instance part="R95" gate="G$1" x="33.02" y="254"/>
<instance part="R96" gate="G$1" x="33.02" y="246.38"/>
<instance part="R97" gate="G$1" x="33.02" y="238.76"/>
<instance part="R98" gate="G$1" x="33.02" y="231.14"/>
<instance part="R99" gate="G$1" x="243.84" y="302.26"/>
<instance part="R100" gate="G$1" x="45.72" y="279.4"/>
<instance part="R101" gate="G$1" x="45.72" y="287.02"/>
<instance part="R102" gate="G$1" x="45.72" y="294.64"/>
<instance part="R103" gate="G$1" x="132.08" y="231.14"/>
<instance part="R104" gate="G$1" x="142.24" y="226.06"/>
<instance part="R105" gate="G$1" x="139.7" y="208.28"/>
<instance part="R107" gate="G$1" x="132.08" y="198.12" rot="R270"/>
<instance part="C140" gate="G$1" x="132.08" y="182.88"/>
<instance part="C141" gate="G$1" x="147.32" y="182.88"/>
<instance part="SUPPLY68" gate="GND" x="147.32" y="175.26"/>
<instance part="Y3" gate="G$1" x="139.7" y="190.5" rot="MR0"/>
<instance part="R13" gate="G$1" x="81.28" y="302.26"/>
<instance part="R14" gate="G$1" x="101.6" y="302.26" rot="R180"/>
<instance part="R20" gate="G$1" x="91.44" y="289.56" rot="R90"/>
<instance part="U16" gate="G$1" x="88.9" y="66.04"/>
<instance part="CON4" gate="MAGJACK_RJ45" x="218.44" y="76.2"/>
<instance part="R25" gate="G$1" x="157.48" y="134.62"/>
<instance part="R119" gate="G$1" x="157.48" y="142.24"/>
<instance part="R120" gate="G$1" x="157.48" y="149.86"/>
<instance part="FB7" gate="G$1" x="137.16" y="134.62"/>
<instance part="C131" gate="G$1" x="149.86" y="127"/>
<instance part="SUPPLY57" gate="GND" x="149.86" y="119.38"/>
<instance part="R121" gate="G$1" x="215.9" y="142.24"/>
<instance part="R122" gate="G$1" x="215.9" y="134.62"/>
<instance part="R123" gate="G$1" x="215.9" y="127"/>
<instance part="R124" gate="G$1" x="215.9" y="119.38"/>
<instance part="R125" gate="G$1" x="215.9" y="149.86"/>
<instance part="SUPPLY58" gate="GND" x="226.06" y="114.3"/>
<instance part="R126" gate="G$1" x="142.24" y="96.52" rot="R90"/>
<instance part="R127" gate="G$1" x="152.4" y="96.52" rot="R90"/>
<instance part="R128" gate="G$1" x="162.56" y="96.52" rot="R90"/>
<instance part="R129" gate="G$1" x="172.72" y="96.52" rot="R90"/>
<instance part="R130" gate="G$1" x="185.42" y="96.52" rot="R90"/>
<instance part="C142" gate="G$1" x="185.42" y="68.58"/>
<instance part="SUPPLY72" gate="GND" x="198.12" y="58.42"/>
<instance part="R131" gate="G$1" x="251.46" y="88.9"/>
<instance part="R132" gate="G$1" x="251.46" y="81.28"/>
<instance part="R133" gate="G$1" x="251.46" y="66.04"/>
<instance part="SUPPLY73" gate="GND" x="259.08" y="60.96"/>
<instance part="R134" gate="G$1" x="60.96" y="38.1" rot="R90"/>
<instance part="SUPPLY74" gate="GND" x="66.04" y="27.94"/>
<instance part="C143" gate="G$1" x="182.88" y="38.1"/>
<instance part="C144" gate="G$1" x="198.12" y="38.1"/>
<instance part="SUPPLY75" gate="GND" x="198.12" y="30.48"/>
<instance part="SUPPLY76" gate="GND" x="182.88" y="30.48"/>
<instance part="C145" gate="G$1" x="7.62" y="55.88"/>
<instance part="C146" gate="G$1" x="20.32" y="55.88"/>
<instance part="C147" gate="G$1" x="33.02" y="55.88"/>
<instance part="SUPPLY77" gate="GND" x="7.62" y="45.72"/>
<instance part="R135" gate="G$1" x="86.36" y="149.86" rot="R180"/>
<instance part="R136" gate="G$1" x="76.2" y="137.16" rot="R90"/>
<instance part="R137" gate="G$1" x="66.04" y="149.86"/>
<instance part="R138" gate="G$1" x="58.42" y="142.24" rot="R270"/>
<instance part="R139" gate="G$1" x="38.1" y="127"/>
<instance part="R140" gate="G$1" x="38.1" y="119.38"/>
<instance part="R141" gate="G$1" x="38.1" y="111.76"/>
<instance part="R142" gate="G$1" x="40.64" y="101.6"/>
<instance part="R143" gate="G$1" x="40.64" y="93.98"/>
<instance part="R144" gate="G$1" x="40.64" y="86.36"/>
<instance part="R145" gate="G$1" x="40.64" y="78.74"/>
<instance part="R146" gate="G$1" x="40.64" y="71.12"/>
<instance part="R147" gate="G$1" x="132.08" y="71.12"/>
<instance part="R148" gate="G$1" x="132.08" y="63.5"/>
<instance part="R149" gate="G$1" x="144.78" y="48.26"/>
<instance part="R150" gate="G$1" x="134.62" y="40.64" rot="R270"/>
<instance part="Y4" gate="G$1" x="144.78" y="33.02" rot="MR0"/>
<instance part="C148" gate="G$1" x="134.62" y="27.94"/>
<instance part="C149" gate="G$1" x="152.4" y="27.94"/>
<instance part="SUPPLY78" gate="GND" x="134.62" y="20.32"/>
<instance part="SUPPLY79" gate="GND" x="152.4" y="20.32"/>
<instance part="SUPPLY80" gate="GND" x="132.08" y="175.26"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="CON2" gate="MAGJACK_RJ45" pin="SHIELD1"/>
<pinref part="SUPPLY61" gate="GND" pin="GND"/>
<pinref part="C133" gate="G$1" pin="2"/>
<wire x1="185.42" y1="218.44" x2="185.42" y2="215.9" width="0.1524" layer="91"/>
<wire x1="185.42" y1="215.9" x2="198.12" y2="215.9" width="0.1524" layer="91"/>
<junction x="198.12" y="215.9"/>
<wire x1="198.12" y1="215.9" x2="198.12" y2="226.06" width="0.1524" layer="91"/>
<pinref part="CON2" gate="MAGJACK_RJ45" pin="SHIELD2"/>
<wire x1="200.66" y1="226.06" x2="198.12" y2="226.06" width="0.1524" layer="91"/>
<junction x="198.12" y="226.06"/>
<wire x1="198.12" y1="226.06" x2="198.12" y2="228.6" width="0.1524" layer="91"/>
<wire x1="198.12" y1="228.6" x2="200.66" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="256.54" y1="241.3" x2="259.08" y2="241.3" width="0.1524" layer="91"/>
<wire x1="259.08" y1="241.3" x2="259.08" y2="226.06" width="0.1524" layer="91"/>
<wire x1="259.08" y1="226.06" x2="259.08" y2="223.52" width="0.1524" layer="91"/>
<wire x1="256.54" y1="226.06" x2="259.08" y2="226.06" width="0.1524" layer="91"/>
<junction x="259.08" y="226.06"/>
<pinref part="SUPPLY62" gate="GND" pin="GND"/>
<wire x1="256.54" y1="248.92" x2="259.08" y2="248.92" width="0.1524" layer="91"/>
<wire x1="259.08" y1="248.92" x2="259.08" y2="241.3" width="0.1524" layer="91"/>
<junction x="259.08" y="241.3"/>
<pinref part="R82" gate="G$1" pin="2"/>
<pinref part="R83" gate="G$1" pin="2"/>
<pinref part="R84" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="SUPPLY63" gate="GND" pin="GND"/>
<wire x1="251.46" y1="269.24" x2="251.46" y2="271.78" width="0.1524" layer="91"/>
<pinref part="R85" gate="G$1" pin="2"/>
<wire x1="251.46" y1="271.78" x2="251.46" y2="279.4" width="0.1524" layer="91"/>
<wire x1="251.46" y1="279.4" x2="251.46" y2="287.02" width="0.1524" layer="91"/>
<wire x1="251.46" y1="287.02" x2="251.46" y2="294.64" width="0.1524" layer="91"/>
<wire x1="251.46" y1="294.64" x2="248.92" y2="294.64" width="0.1524" layer="91"/>
<pinref part="R86" gate="G$1" pin="2"/>
<wire x1="248.92" y1="287.02" x2="251.46" y2="287.02" width="0.1524" layer="91"/>
<junction x="251.46" y="287.02"/>
<pinref part="R87" gate="G$1" pin="2"/>
<wire x1="248.92" y1="279.4" x2="251.46" y2="279.4" width="0.1524" layer="91"/>
<junction x="251.46" y="279.4"/>
<pinref part="R88" gate="G$1" pin="2"/>
<wire x1="248.92" y1="271.78" x2="251.46" y2="271.78" width="0.1524" layer="91"/>
<junction x="251.46" y="271.78"/>
<wire x1="251.46" y1="294.64" x2="251.46" y2="302.26" width="0.1524" layer="91"/>
<junction x="251.46" y="294.64"/>
<pinref part="R99" gate="G$1" pin="2"/>
<wire x1="251.46" y1="302.26" x2="248.92" y2="302.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="VSS_PAD"/>
<pinref part="SUPPLY64" gate="GND" pin="GND"/>
<wire x1="68.58" y1="190.5" x2="68.58" y2="200.66" width="0.1524" layer="91"/>
<pinref part="R92" gate="G$1" pin="1"/>
<wire x1="60.96" y1="193.04" x2="60.96" y2="190.5" width="0.1524" layer="91"/>
<wire x1="60.96" y1="190.5" x2="68.58" y2="190.5" width="0.1524" layer="91"/>
<junction x="68.58" y="190.5"/>
</segment>
<segment>
<pinref part="SUPPLY65" gate="GND" pin="GND"/>
<pinref part="C135" gate="G$1" pin="2"/>
<wire x1="20.32" y1="185.42" x2="33.02" y2="185.42" width="0.1524" layer="91"/>
<wire x1="33.02" y1="185.42" x2="33.02" y2="187.96" width="0.1524" layer="91"/>
<pinref part="C134" gate="G$1" pin="2"/>
<wire x1="20.32" y1="185.42" x2="20.32" y2="187.96" width="0.1524" layer="91"/>
<junction x="20.32" y="185.42"/>
</segment>
<segment>
<pinref part="SUPPLY66" gate="GND" pin="GND"/>
<pinref part="C136" gate="G$1" pin="2"/>
<wire x1="12.7" y1="208.28" x2="12.7" y2="210.82" width="0.1524" layer="91"/>
<pinref part="C137" gate="G$1" pin="2"/>
<wire x1="12.7" y1="208.28" x2="25.4" y2="208.28" width="0.1524" layer="91"/>
<wire x1="25.4" y1="208.28" x2="25.4" y2="210.82" width="0.1524" layer="91"/>
<junction x="12.7" y="208.28"/>
<pinref part="C138" gate="G$1" pin="2"/>
<wire x1="25.4" y1="208.28" x2="38.1" y2="208.28" width="0.1524" layer="91"/>
<wire x1="38.1" y1="208.28" x2="38.1" y2="210.82" width="0.1524" layer="91"/>
<junction x="25.4" y="208.28"/>
</segment>
<segment>
<pinref part="C139" gate="G$1" pin="2"/>
<pinref part="SUPPLY67" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C131" gate="G$1" pin="2"/>
<pinref part="SUPPLY57" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY58" gate="GND" pin="GND"/>
<pinref part="R121" gate="G$1" pin="2"/>
<wire x1="226.06" y1="116.84" x2="226.06" y2="119.38" width="0.1524" layer="91"/>
<wire x1="226.06" y1="119.38" x2="226.06" y2="127" width="0.1524" layer="91"/>
<wire x1="226.06" y1="127" x2="226.06" y2="134.62" width="0.1524" layer="91"/>
<wire x1="226.06" y1="134.62" x2="226.06" y2="142.24" width="0.1524" layer="91"/>
<wire x1="226.06" y1="142.24" x2="220.98" y2="142.24" width="0.1524" layer="91"/>
<pinref part="R122" gate="G$1" pin="2"/>
<wire x1="220.98" y1="134.62" x2="226.06" y2="134.62" width="0.1524" layer="91"/>
<junction x="226.06" y="134.62"/>
<pinref part="R123" gate="G$1" pin="2"/>
<wire x1="220.98" y1="127" x2="226.06" y2="127" width="0.1524" layer="91"/>
<junction x="226.06" y="127"/>
<pinref part="R124" gate="G$1" pin="2"/>
<wire x1="220.98" y1="119.38" x2="226.06" y2="119.38" width="0.1524" layer="91"/>
<junction x="226.06" y="119.38"/>
</segment>
<segment>
<pinref part="CON4" gate="MAGJACK_RJ45" pin="SHIELD1"/>
<wire x1="200.66" y1="68.58" x2="198.12" y2="68.58" width="0.1524" layer="91"/>
<wire x1="198.12" y1="68.58" x2="198.12" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C142" gate="G$1" pin="2"/>
<wire x1="198.12" y1="66.04" x2="198.12" y2="60.96" width="0.1524" layer="91"/>
<wire x1="198.12" y1="60.96" x2="185.42" y2="60.96" width="0.1524" layer="91"/>
<wire x1="185.42" y1="60.96" x2="185.42" y2="63.5" width="0.1524" layer="91"/>
<pinref part="CON4" gate="MAGJACK_RJ45" pin="SHIELD2"/>
<wire x1="200.66" y1="66.04" x2="198.12" y2="66.04" width="0.1524" layer="91"/>
<junction x="198.12" y="66.04"/>
<pinref part="SUPPLY72" gate="GND" pin="GND"/>
<junction x="198.12" y="60.96"/>
</segment>
<segment>
<pinref part="SUPPLY73" gate="GND" pin="GND"/>
<wire x1="259.08" y1="63.5" x2="259.08" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R131" gate="G$1" pin="2"/>
<wire x1="259.08" y1="66.04" x2="259.08" y2="81.28" width="0.1524" layer="91"/>
<wire x1="259.08" y1="81.28" x2="259.08" y2="88.9" width="0.1524" layer="91"/>
<wire x1="259.08" y1="88.9" x2="256.54" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R132" gate="G$1" pin="2"/>
<wire x1="256.54" y1="81.28" x2="259.08" y2="81.28" width="0.1524" layer="91"/>
<junction x="259.08" y="81.28"/>
<pinref part="R133" gate="G$1" pin="2"/>
<wire x1="256.54" y1="66.04" x2="259.08" y2="66.04" width="0.1524" layer="91"/>
<junction x="259.08" y="66.04"/>
</segment>
<segment>
<pinref part="R134" gate="G$1" pin="1"/>
<wire x1="60.96" y1="33.02" x2="60.96" y2="30.48" width="0.1524" layer="91"/>
<pinref part="SUPPLY74" gate="GND" pin="GND"/>
<wire x1="60.96" y1="30.48" x2="66.04" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U16" gate="G$1" pin="VSS_PAD"/>
<wire x1="66.04" y1="30.48" x2="68.58" y2="30.48" width="0.1524" layer="91"/>
<wire x1="68.58" y1="30.48" x2="68.58" y2="40.64" width="0.1524" layer="91"/>
<junction x="66.04" y="30.48"/>
</segment>
<segment>
<pinref part="C144" gate="G$1" pin="2"/>
<pinref part="SUPPLY75" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C143" gate="G$1" pin="2"/>
<pinref part="SUPPLY76" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C145" gate="G$1" pin="2"/>
<pinref part="SUPPLY77" gate="GND" pin="GND"/>
<wire x1="7.62" y1="50.8" x2="7.62" y2="48.26" width="0.1524" layer="91"/>
<pinref part="C147" gate="G$1" pin="2"/>
<wire x1="7.62" y1="48.26" x2="20.32" y2="48.26" width="0.1524" layer="91"/>
<wire x1="20.32" y1="48.26" x2="33.02" y2="48.26" width="0.1524" layer="91"/>
<wire x1="33.02" y1="48.26" x2="33.02" y2="50.8" width="0.1524" layer="91"/>
<junction x="7.62" y="48.26"/>
<pinref part="C146" gate="G$1" pin="2"/>
<wire x1="20.32" y1="48.26" x2="20.32" y2="50.8" width="0.1524" layer="91"/>
<junction x="20.32" y="48.26"/>
</segment>
<segment>
<pinref part="C148" gate="G$1" pin="2"/>
<pinref part="SUPPLY78" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C149" gate="G$1" pin="2"/>
<pinref part="SUPPLY79" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C140" gate="G$1" pin="2"/>
<pinref part="SUPPLY80" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C141" gate="G$1" pin="2"/>
<pinref part="SUPPLY68" gate="GND" pin="GND"/>
</segment>
</net>
<net name="VDD_3V3B" class="0">
<segment>
<pinref part="R91" gate="G$1" pin="1"/>
<wire x1="180.34" y1="289.56" x2="177.8" y2="289.56" width="0.1524" layer="91"/>
<wire x1="177.8" y1="289.56" x2="177.8" y2="297.18" width="0.1524" layer="91"/>
<pinref part="R89" gate="G$1" pin="1"/>
<wire x1="177.8" y1="297.18" x2="177.8" y2="304.8" width="0.1524" layer="91"/>
<wire x1="177.8" y1="304.8" x2="180.34" y2="304.8" width="0.1524" layer="91"/>
<pinref part="R90" gate="G$1" pin="1"/>
<wire x1="180.34" y1="297.18" x2="177.8" y2="297.18" width="0.1524" layer="91"/>
<junction x="177.8" y="297.18"/>
<wire x1="177.8" y1="304.8" x2="175.26" y2="304.8" width="0.1524" layer="91"/>
<junction x="177.8" y="304.8"/>
<label x="175.26" y="304.8" size="1.778" layer="95" rot="R180"/>
<pinref part="FB6" gate="G$1" pin="2"/>
<wire x1="172.72" y1="289.56" x2="177.8" y2="289.56" width="0.1524" layer="91"/>
<junction x="177.8" y="289.56"/>
<pinref part="C139" gate="G$1" pin="1"/>
<wire x1="177.8" y1="284.48" x2="177.8" y2="289.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="VDDIO"/>
<wire x1="68.58" y1="220.98" x2="66.04" y2="220.98" width="0.1524" layer="91"/>
<label x="66.04" y="220.98" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MPU_NRST" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="NRST"/>
<wire x1="109.22" y1="220.98" x2="114.3" y2="220.98" width="0.1524" layer="91"/>
<label x="114.3" y="220.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U16" gate="G$1" pin="NRST"/>
<wire x1="109.22" y1="60.96" x2="111.76" y2="60.96" width="0.1524" layer="91"/>
<label x="111.76" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII1_TXD2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="TXD2"/>
<wire x1="60.96" y1="248.92" x2="58.42" y2="248.92" width="0.1524" layer="91"/>
<label x="58.42" y="248.92" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_TXD1" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="TXD1"/>
<wire x1="60.96" y1="256.54" x2="58.42" y2="256.54" width="0.1524" layer="91"/>
<label x="58.42" y="256.54" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_TXD0" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="TXD0"/>
<wire x1="60.96" y1="264.16" x2="58.42" y2="264.16" width="0.1524" layer="91"/>
<label x="58.42" y="264.16" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_CRS_DV" class="0">
<segment>
<pinref part="R103" gate="G$1" pin="2"/>
<wire x1="137.16" y1="231.14" x2="142.24" y2="231.14" width="0.1524" layer="91"/>
<label x="142.24" y="231.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII1_COL" class="0">
<segment>
<pinref part="R104" gate="G$1" pin="2"/>
<wire x1="147.32" y1="226.06" x2="149.86" y2="226.06" width="0.1524" layer="91"/>
<label x="149.86" y="226.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R138" gate="G$1" pin="2"/>
<wire x1="58.42" y1="137.16" x2="58.42" y2="134.62" width="0.1524" layer="91"/>
<label x="58.42" y="134.62" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_TXD3" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="TXD3"/>
<wire x1="60.96" y1="241.3" x2="58.42" y2="241.3" width="0.1524" layer="91"/>
<label x="58.42" y="241.3" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_RXD3" class="0">
<segment>
<pinref part="R97" gate="G$1" pin="1"/>
<wire x1="27.94" y1="238.76" x2="25.4" y2="238.76" width="0.1524" layer="91"/>
<label x="25.4" y="238.76" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_RXD2" class="0">
<segment>
<pinref part="R96" gate="G$1" pin="1"/>
<wire x1="27.94" y1="246.38" x2="25.4" y2="246.38" width="0.1524" layer="91"/>
<label x="25.4" y="246.38" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_RXD1" class="0">
<segment>
<pinref part="R95" gate="G$1" pin="1"/>
<wire x1="27.94" y1="254" x2="25.4" y2="254" width="0.1524" layer="91"/>
<label x="25.4" y="254" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_RXD0" class="0">
<segment>
<pinref part="R94" gate="G$1" pin="1"/>
<wire x1="27.94" y1="261.62" x2="25.4" y2="261.62" width="0.1524" layer="91"/>
<label x="25.4" y="261.62" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_RXERR" class="0">
<segment>
<pinref part="R98" gate="G$1" pin="1"/>
<wire x1="27.94" y1="231.14" x2="25.4" y2="231.14" width="0.1524" layer="91"/>
<label x="25.4" y="231.14" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_RXDV" class="0">
<segment>
<pinref part="R100" gate="G$1" pin="1"/>
<wire x1="40.64" y1="279.4" x2="38.1" y2="279.4" width="0.1524" layer="91"/>
<label x="38.1" y="279.4" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MDIO_CLK" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="MDC"/>
<wire x1="109.22" y1="198.12" x2="111.76" y2="198.12" width="0.1524" layer="91"/>
<wire x1="111.76" y1="198.12" x2="111.76" y2="190.5" width="0.1524" layer="91"/>
<wire x1="111.76" y1="190.5" x2="104.14" y2="190.5" width="0.1524" layer="91"/>
<label x="104.14" y="190.5" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U16" gate="G$1" pin="MDC"/>
<wire x1="109.22" y1="38.1" x2="111.76" y2="38.1" width="0.1524" layer="91"/>
<label x="111.76" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="MDIO_DATA" class="0">
<segment>
<pinref part="R93" gate="G$1" pin="1"/>
<wire x1="132.08" y1="289.56" x2="129.54" y2="289.56" width="0.1524" layer="91"/>
<label x="129.54" y="289.56" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="MDIO"/>
<wire x1="109.22" y1="200.66" x2="114.3" y2="200.66" width="0.1524" layer="91"/>
<wire x1="114.3" y1="200.66" x2="114.3" y2="187.96" width="0.1524" layer="91"/>
<wire x1="114.3" y1="187.96" x2="104.14" y2="187.96" width="0.1524" layer="91"/>
<label x="104.14" y="187.96" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U16" gate="G$1" pin="MDIO"/>
<wire x1="109.22" y1="40.64" x2="111.76" y2="40.64" width="0.1524" layer="91"/>
<label x="111.76" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="YEL_A" class="0">
<segment>
<pinref part="CON2" gate="MAGJACK_RJ45" pin="YLED+"/>
<label x="236.22" y="248.92" size="1.778" layer="95"/>
<wire x1="236.22" y1="248.92" x2="246.38" y2="248.92" width="0.1524" layer="91"/>
<pinref part="R82" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="LED2/!INTSEL"/>
<wire x1="109.22" y1="213.36" x2="114.3" y2="213.36" width="0.1524" layer="91"/>
<label x="114.3" y="213.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="YEL_C" class="0">
<segment>
<pinref part="CON2" gate="MAGJACK_RJ45" pin="YLED-"/>
<label x="236.22" y="241.3" size="1.778" layer="95"/>
<wire x1="236.22" y1="241.3" x2="246.38" y2="241.3" width="0.1524" layer="91"/>
<pinref part="R83" gate="G$1" pin="1"/>
</segment>
</net>
<net name="GRN_A" class="0">
<segment>
<pinref part="CON2" gate="MAGJACK_RJ45" pin="GLED+"/>
<wire x1="236.22" y1="233.68" x2="241.3" y2="233.68" width="0.1524" layer="91"/>
<label x="241.3" y="233.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="LED1/!REGOFF"/>
<wire x1="109.22" y1="215.9" x2="114.3" y2="215.9" width="0.1524" layer="91"/>
<label x="114.3" y="215.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="GRN_C" class="0">
<segment>
<pinref part="CON2" gate="MAGJACK_RJ45" pin="GLED-"/>
<label x="236.22" y="226.06" size="1.778" layer="95"/>
<wire x1="236.22" y1="226.06" x2="246.38" y2="226.06" width="0.1524" layer="91"/>
<pinref part="R84" gate="G$1" pin="1"/>
</segment>
</net>
<net name="TCT_RCT" class="0">
<segment>
<pinref part="C133" gate="G$1" pin="1"/>
<wire x1="185.42" y1="226.06" x2="185.42" y2="233.68" width="0.1524" layer="91"/>
<pinref part="CON2" gate="MAGJACK_RJ45" pin="RCT"/>
<wire x1="185.42" y1="233.68" x2="200.66" y2="233.68" width="0.1524" layer="91"/>
<pinref part="CON2" gate="MAGJACK_RJ45" pin="TCT"/>
<wire x1="200.66" y1="248.92" x2="185.42" y2="248.92" width="0.1524" layer="91"/>
<wire x1="185.42" y1="248.92" x2="185.42" y2="233.68" width="0.1524" layer="91"/>
<junction x="185.42" y="233.68"/>
<pinref part="R77" gate="G$1" pin="1"/>
<wire x1="185.42" y1="248.92" x2="185.42" y2="251.46" width="0.1524" layer="91"/>
<junction x="185.42" y="248.92"/>
<label x="185.42" y="248.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD_PHYA" class="0">
<segment>
<pinref part="R77" gate="G$1" pin="2"/>
<wire x1="185.42" y1="261.62" x2="185.42" y2="264.16" width="0.1524" layer="91"/>
<label x="185.42" y="266.7" size="1.778" layer="95"/>
<pinref part="R81" gate="G$1" pin="2"/>
<wire x1="185.42" y1="264.16" x2="172.72" y2="264.16" width="0.1524" layer="91"/>
<wire x1="172.72" y1="264.16" x2="162.56" y2="264.16" width="0.1524" layer="91"/>
<wire x1="162.56" y1="264.16" x2="152.4" y2="264.16" width="0.1524" layer="91"/>
<wire x1="152.4" y1="264.16" x2="142.24" y2="264.16" width="0.1524" layer="91"/>
<wire x1="142.24" y1="264.16" x2="142.24" y2="261.62" width="0.1524" layer="91"/>
<wire x1="185.42" y1="264.16" x2="185.42" y2="266.7" width="0.1524" layer="91"/>
<junction x="185.42" y="264.16"/>
<pinref part="R78" gate="G$1" pin="2"/>
<wire x1="172.72" y1="261.62" x2="172.72" y2="264.16" width="0.1524" layer="91"/>
<junction x="172.72" y="264.16"/>
<pinref part="R79" gate="G$1" pin="2"/>
<wire x1="162.56" y1="261.62" x2="162.56" y2="264.16" width="0.1524" layer="91"/>
<junction x="162.56" y="264.16"/>
<pinref part="R80" gate="G$1" pin="2"/>
<wire x1="152.4" y1="261.62" x2="152.4" y2="264.16" width="0.1524" layer="91"/>
<junction x="152.4" y="264.16"/>
</segment>
<segment>
<pinref part="C138" gate="G$1" pin="1"/>
<wire x1="38.1" y1="218.44" x2="38.1" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C136" gate="G$1" pin="1"/>
<wire x1="38.1" y1="220.98" x2="25.4" y2="220.98" width="0.1524" layer="91"/>
<wire x1="25.4" y1="220.98" x2="12.7" y2="220.98" width="0.1524" layer="91"/>
<wire x1="12.7" y1="220.98" x2="12.7" y2="218.44" width="0.1524" layer="91"/>
<pinref part="C137" gate="G$1" pin="1"/>
<wire x1="25.4" y1="218.44" x2="25.4" y2="220.98" width="0.1524" layer="91"/>
<junction x="25.4" y="220.98"/>
<label x="15.24" y="220.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="VDD2A"/>
<wire x1="68.58" y1="210.82" x2="66.04" y2="210.82" width="0.1524" layer="91"/>
<wire x1="66.04" y1="210.82" x2="66.04" y2="213.36" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="VDD1A"/>
<wire x1="66.04" y1="213.36" x2="68.58" y2="213.36" width="0.1524" layer="91"/>
<wire x1="66.04" y1="213.36" x2="63.5" y2="213.36" width="0.1524" layer="91"/>
<junction x="66.04" y="213.36"/>
<label x="63.5" y="213.36" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="FB6" gate="G$1" pin="1"/>
<wire x1="157.48" y1="289.56" x2="142.24" y2="289.56" width="0.1524" layer="91"/>
<pinref part="R93" gate="G$1" pin="2"/>
<label x="144.78" y="289.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="RXP" class="0">
<segment>
<pinref part="R80" gate="G$1" pin="1"/>
<wire x1="152.4" y1="251.46" x2="152.4" y2="238.76" width="0.1524" layer="91"/>
<pinref part="CON2" gate="MAGJACK_RJ45" pin="RD+"/>
<wire x1="200.66" y1="238.76" x2="152.4" y2="238.76" width="0.1524" layer="91"/>
<junction x="152.4" y="238.76"/>
<pinref part="U5" gate="G$1" pin="RXP"/>
<wire x1="152.4" y1="238.76" x2="116.84" y2="238.76" width="0.1524" layer="91"/>
<label x="134.62" y="238.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="RXN" class="0">
<segment>
<pinref part="CON2" gate="MAGJACK_RJ45" pin="RD-"/>
<wire x1="142.24" y1="236.22" x2="200.66" y2="236.22" width="0.1524" layer="91"/>
<pinref part="R81" gate="G$1" pin="1"/>
<wire x1="142.24" y1="236.22" x2="142.24" y2="251.46" width="0.1524" layer="91"/>
<junction x="142.24" y="236.22"/>
<pinref part="U5" gate="G$1" pin="RXN"/>
<wire x1="142.24" y1="236.22" x2="116.84" y2="236.22" width="0.1524" layer="91"/>
<label x="134.62" y="236.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="TXN" class="0">
<segment>
<pinref part="R79" gate="G$1" pin="1"/>
<pinref part="CON2" gate="MAGJACK_RJ45" pin="TD-"/>
<wire x1="162.56" y1="243.84" x2="162.56" y2="251.46" width="0.1524" layer="91"/>
<wire x1="200.66" y1="243.84" x2="162.56" y2="243.84" width="0.1524" layer="91"/>
<junction x="162.56" y="243.84"/>
<pinref part="U5" gate="G$1" pin="TXN"/>
<wire x1="162.56" y1="243.84" x2="116.84" y2="243.84" width="0.1524" layer="91"/>
<label x="134.62" y="243.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="TXP" class="0">
<segment>
<pinref part="R78" gate="G$1" pin="1"/>
<wire x1="172.72" y1="251.46" x2="172.72" y2="246.38" width="0.1524" layer="91"/>
<pinref part="CON2" gate="MAGJACK_RJ45" pin="TD+"/>
<wire x1="200.66" y1="246.38" x2="172.72" y2="246.38" width="0.1524" layer="91"/>
<junction x="172.72" y="246.38"/>
<pinref part="U5" gate="G$1" pin="TXP"/>
<wire x1="172.72" y1="246.38" x2="116.84" y2="246.38" width="0.1524" layer="91"/>
<label x="134.62" y="246.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="REFCLK0" class="0">
<segment>
<pinref part="R85" gate="G$1" pin="1"/>
<wire x1="238.76" y1="294.64" x2="236.22" y2="294.64" width="0.1524" layer="91"/>
<label x="236.22" y="294.64" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="RXCLK/!PHYAD1"/>
<wire x1="116.84" y1="261.62" x2="119.38" y2="261.62" width="0.1524" layer="91"/>
<label x="119.38" y="261.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R101" gate="G$1" pin="2"/>
<wire x1="50.8" y1="287.02" x2="53.34" y2="287.02" width="0.1524" layer="91"/>
<label x="53.34" y="287.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="106.68" y1="302.26" x2="109.22" y2="302.26" width="0.1524" layer="91"/>
<label x="109.22" y="302.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="RXD2/RMIISEL" class="0">
<segment>
<pinref part="R86" gate="G$1" pin="1"/>
<wire x1="238.76" y1="287.02" x2="236.22" y2="287.02" width="0.1524" layer="91"/>
<label x="236.22" y="287.02" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="RXD2/!RMIISEL"/>
<pinref part="R96" gate="G$1" pin="2"/>
<wire x1="60.96" y1="246.38" x2="38.1" y2="246.38" width="0.1524" layer="91"/>
<label x="58.42" y="246.38" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="RXD3/PHYAD2" class="0">
<segment>
<pinref part="R87" gate="G$1" pin="1"/>
<wire x1="238.76" y1="279.4" x2="236.22" y2="279.4" width="0.1524" layer="91"/>
<label x="236.22" y="279.4" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R97" gate="G$1" pin="2"/>
<pinref part="U5" gate="G$1" pin="RXD3/!PHYAD2"/>
<wire x1="38.1" y1="238.76" x2="60.96" y2="238.76" width="0.1524" layer="91"/>
<label x="58.42" y="238.76" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="RXER/PHYAD0" class="0">
<segment>
<pinref part="R88" gate="G$1" pin="1"/>
<wire x1="238.76" y1="271.78" x2="236.22" y2="271.78" width="0.1524" layer="91"/>
<label x="236.22" y="271.78" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R98" gate="G$1" pin="2"/>
<pinref part="U5" gate="G$1" pin="RXER/RXD4/!PHYAD0"/>
<wire x1="38.1" y1="231.14" x2="60.96" y2="231.14" width="0.1524" layer="91"/>
<label x="58.42" y="231.14" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MODE2" class="0">
<segment>
<pinref part="R89" gate="G$1" pin="2"/>
<wire x1="190.5" y1="304.8" x2="193.04" y2="304.8" width="0.1524" layer="91"/>
<label x="193.04" y="304.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="COL/CRS_DV/!MODE2"/>
<wire x1="116.84" y1="228.6" x2="124.46" y2="228.6" width="0.1524" layer="91"/>
<wire x1="124.46" y1="228.6" x2="124.46" y2="226.06" width="0.1524" layer="91"/>
<pinref part="R104" gate="G$1" pin="1"/>
<wire x1="124.46" y1="226.06" x2="137.16" y2="226.06" width="0.1524" layer="91"/>
<label x="124.46" y="226.06" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="RXD1/MODE1" class="0">
<segment>
<pinref part="R90" gate="G$1" pin="2"/>
<wire x1="190.5" y1="297.18" x2="193.04" y2="297.18" width="0.1524" layer="91"/>
<label x="193.04" y="297.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="RXD1/!MODE1"/>
<pinref part="R95" gate="G$1" pin="2"/>
<wire x1="60.96" y1="254" x2="38.1" y2="254" width="0.1524" layer="91"/>
<label x="58.42" y="254" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="RXD0/MODE0" class="0">
<segment>
<pinref part="R91" gate="G$1" pin="2"/>
<wire x1="190.5" y1="289.56" x2="193.04" y2="289.56" width="0.1524" layer="91"/>
<label x="193.04" y="289.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="RXD0/!MODE0"/>
<pinref part="R94" gate="G$1" pin="2"/>
<wire x1="60.96" y1="261.62" x2="38.1" y2="261.62" width="0.1524" layer="91"/>
<label x="58.42" y="261.62" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="RBIAS" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="RBIAS"/>
<pinref part="R92" gate="G$1" pin="2"/>
<wire x1="68.58" y1="205.74" x2="60.96" y2="205.74" width="0.1524" layer="91"/>
<wire x1="60.96" y1="205.74" x2="60.96" y2="203.2" width="0.1524" layer="91"/>
<label x="60.96" y="205.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="PHY_VDDCR" class="0">
<segment>
<pinref part="C135" gate="G$1" pin="1"/>
<wire x1="33.02" y1="195.58" x2="33.02" y2="198.12" width="0.1524" layer="91"/>
<pinref part="C134" gate="G$1" pin="1"/>
<wire x1="33.02" y1="198.12" x2="20.32" y2="198.12" width="0.1524" layer="91"/>
<wire x1="20.32" y1="198.12" x2="20.32" y2="195.58" width="0.1524" layer="91"/>
<label x="22.86" y="198.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="VDDCR"/>
<wire x1="68.58" y1="218.44" x2="66.04" y2="218.44" width="0.1524" layer="91"/>
<label x="66.04" y="218.44" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="TXCLK" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="TXCLK"/>
<wire x1="116.84" y1="264.16" x2="119.38" y2="264.16" width="0.1524" layer="91"/>
<label x="119.38" y="264.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R102" gate="G$1" pin="2"/>
<wire x1="50.8" y1="294.64" x2="53.34" y2="294.64" width="0.1524" layer="91"/>
<label x="53.34" y="294.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="RXDV" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="RXDV"/>
<wire x1="116.84" y1="251.46" x2="119.38" y2="251.46" width="0.1524" layer="91"/>
<label x="119.38" y="251.46" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R100" gate="G$1" pin="2"/>
<wire x1="50.8" y1="279.4" x2="53.34" y2="279.4" width="0.1524" layer="91"/>
<label x="53.34" y="279.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII1_TX_EN" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="TXEN"/>
<wire x1="116.84" y1="256.54" x2="119.38" y2="256.54" width="0.1524" layer="91"/>
<label x="119.38" y="256.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETH_TXD4" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="INT/TXER/TXD4"/>
<wire x1="60.96" y1="233.68" x2="58.42" y2="233.68" width="0.1524" layer="91"/>
<label x="58.42" y="233.68" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R99" gate="G$1" pin="1"/>
<wire x1="238.76" y1="302.26" x2="236.22" y2="302.26" width="0.1524" layer="91"/>
<label x="236.22" y="302.26" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_TXCLK" class="0">
<segment>
<pinref part="R102" gate="G$1" pin="1"/>
<wire x1="40.64" y1="294.64" x2="38.1" y2="294.64" width="0.1524" layer="91"/>
<label x="38.1" y="294.64" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_RXCLK" class="0">
<segment>
<pinref part="R101" gate="G$1" pin="1"/>
<wire x1="40.64" y1="287.02" x2="38.1" y2="287.02" width="0.1524" layer="91"/>
<label x="38.1" y="287.02" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="CRS" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="CRS"/>
<pinref part="R103" gate="G$1" pin="1"/>
<wire x1="116.84" y1="231.14" x2="127" y2="231.14" width="0.1524" layer="91"/>
<label x="119.38" y="231.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="RCLKIN" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="XTAL/CLKIN"/>
<pinref part="R105" gate="G$1" pin="1"/>
<wire x1="109.22" y1="208.28" x2="134.62" y2="208.28" width="0.1524" layer="91"/>
<label x="111.76" y="208.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="91.44" y1="284.48" x2="91.44" y2="281.94" width="0.1524" layer="91"/>
<label x="91.44" y="281.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="PHY1_XTAL2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="XTAL2"/>
<pinref part="R107" gate="G$1" pin="1"/>
<wire x1="109.22" y1="205.74" x2="132.08" y2="205.74" width="0.1524" layer="91"/>
<wire x1="132.08" y1="205.74" x2="132.08" y2="203.2" width="0.1524" layer="91"/>
<label x="111.76" y="205.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="PHY1_X1" class="0">
<segment>
<pinref part="R105" gate="G$1" pin="2"/>
<pinref part="C141" gate="G$1" pin="1"/>
<wire x1="144.78" y1="208.28" x2="147.32" y2="208.28" width="0.1524" layer="91"/>
<wire x1="147.32" y1="208.28" x2="147.32" y2="190.5" width="0.1524" layer="91"/>
<pinref part="Y3" gate="G$1" pin="1"/>
<wire x1="147.32" y1="190.5" x2="147.32" y2="185.42" width="0.1524" layer="91"/>
<wire x1="142.24" y1="190.5" x2="147.32" y2="190.5" width="0.1524" layer="91"/>
<junction x="147.32" y="190.5"/>
<label x="147.32" y="190.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="PHY1_X2" class="0">
<segment>
<pinref part="C140" gate="G$1" pin="1"/>
<pinref part="R107" gate="G$1" pin="2"/>
<wire x1="132.08" y1="185.42" x2="132.08" y2="190.5" width="0.1524" layer="91"/>
<pinref part="Y3" gate="G$1" pin="2"/>
<wire x1="132.08" y1="190.5" x2="132.08" y2="193.04" width="0.1524" layer="91"/>
<wire x1="137.16" y1="190.5" x2="132.08" y2="190.5" width="0.1524" layer="91"/>
<junction x="132.08" y="190.5"/>
<label x="132.08" y="190.5" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="RMII1_REF_CLK" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="76.2" y1="302.26" x2="73.66" y2="302.26" width="0.1524" layer="91"/>
<label x="73.66" y="302.26" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII1_REFCLK" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="91.44" y1="294.64" x2="91.44" y2="302.26" width="0.1524" layer="91"/>
<wire x1="91.44" y1="302.26" x2="86.36" y2="302.26" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="91.44" y1="302.26" x2="96.52" y2="302.26" width="0.1524" layer="91"/>
<junction x="91.44" y="302.26"/>
<label x="91.44" y="297.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="+3V3_ETH2" class="0">
<segment>
<pinref part="FB7" gate="G$1" pin="2"/>
<wire x1="144.78" y1="134.62" x2="149.86" y2="134.62" width="0.1524" layer="91"/>
<pinref part="C131" gate="G$1" pin="1"/>
<wire x1="149.86" y1="129.54" x2="149.86" y2="134.62" width="0.1524" layer="91"/>
<junction x="149.86" y="134.62"/>
<wire x1="147.32" y1="149.86" x2="149.86" y2="149.86" width="0.1524" layer="91"/>
<wire x1="149.86" y1="134.62" x2="149.86" y2="142.24" width="0.1524" layer="91"/>
<label x="147.32" y="149.86" size="1.778" layer="95" rot="R180"/>
<pinref part="R120" gate="G$1" pin="1"/>
<wire x1="149.86" y1="142.24" x2="149.86" y2="149.86" width="0.1524" layer="91"/>
<wire x1="149.86" y1="149.86" x2="152.4" y2="149.86" width="0.1524" layer="91"/>
<junction x="149.86" y="149.86"/>
<pinref part="R119" gate="G$1" pin="1"/>
<wire x1="152.4" y1="142.24" x2="149.86" y2="142.24" width="0.1524" layer="91"/>
<junction x="149.86" y="142.24"/>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="152.4" y1="134.62" x2="149.86" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U16" gate="G$1" pin="VDDIO"/>
<wire x1="68.58" y1="60.96" x2="66.04" y2="60.96" width="0.1524" layer="91"/>
<label x="66.04" y="60.96" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R125" gate="G$1" pin="2"/>
<wire x1="220.98" y1="149.86" x2="226.06" y2="149.86" width="0.1524" layer="91"/>
<wire x1="226.06" y1="149.86" x2="226.06" y2="154.94" width="0.1524" layer="91"/>
<label x="226.06" y="154.94" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="ETH2_MODE2" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="162.56" y1="134.62" x2="165.1" y2="134.62" width="0.1524" layer="91"/>
<label x="165.1" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U16" gate="G$1" pin="COL/CRS_DV/!MODE2"/>
<wire x1="116.84" y1="68.58" x2="127" y2="68.58" width="0.1524" layer="91"/>
<wire x1="127" y1="68.58" x2="127" y2="63.5" width="0.1524" layer="91"/>
<pinref part="R148" gate="G$1" pin="1"/>
<label x="127" y="66.04" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="ETH2_VDD_PHY" class="0">
<segment>
<pinref part="FB7" gate="G$1" pin="1"/>
<wire x1="129.54" y1="134.62" x2="127" y2="134.62" width="0.1524" layer="91"/>
<label x="127" y="134.62" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R126" gate="G$1" pin="2"/>
<wire x1="142.24" y1="101.6" x2="142.24" y2="104.14" width="0.1524" layer="91"/>
<wire x1="142.24" y1="104.14" x2="152.4" y2="104.14" width="0.1524" layer="91"/>
<pinref part="R127" gate="G$1" pin="2"/>
<wire x1="152.4" y1="104.14" x2="162.56" y2="104.14" width="0.1524" layer="91"/>
<wire x1="162.56" y1="104.14" x2="172.72" y2="104.14" width="0.1524" layer="91"/>
<wire x1="172.72" y1="104.14" x2="185.42" y2="104.14" width="0.1524" layer="91"/>
<wire x1="185.42" y1="104.14" x2="187.96" y2="104.14" width="0.1524" layer="91"/>
<wire x1="152.4" y1="101.6" x2="152.4" y2="104.14" width="0.1524" layer="91"/>
<junction x="152.4" y="104.14"/>
<pinref part="R128" gate="G$1" pin="2"/>
<wire x1="162.56" y1="101.6" x2="162.56" y2="104.14" width="0.1524" layer="91"/>
<junction x="162.56" y="104.14"/>
<pinref part="R129" gate="G$1" pin="2"/>
<wire x1="172.72" y1="101.6" x2="172.72" y2="104.14" width="0.1524" layer="91"/>
<junction x="172.72" y="104.14"/>
<pinref part="R130" gate="G$1" pin="2"/>
<wire x1="185.42" y1="101.6" x2="185.42" y2="104.14" width="0.1524" layer="91"/>
<junction x="185.42" y="104.14"/>
<label x="187.96" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U16" gate="G$1" pin="VDD1A"/>
<wire x1="68.58" y1="53.34" x2="66.04" y2="53.34" width="0.1524" layer="91"/>
<label x="63.5" y="53.34" size="1.778" layer="95" rot="R180"/>
<wire x1="66.04" y1="53.34" x2="66.04" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U16" gate="G$1" pin="VDD2A"/>
<wire x1="66.04" y1="50.8" x2="68.58" y2="50.8" width="0.1524" layer="91"/>
<wire x1="66.04" y1="53.34" x2="63.5" y2="53.34" width="0.1524" layer="91"/>
<junction x="66.04" y="53.34"/>
</segment>
<segment>
<pinref part="C145" gate="G$1" pin="1"/>
<wire x1="7.62" y1="58.42" x2="7.62" y2="60.96" width="0.1524" layer="91"/>
<wire x1="7.62" y1="60.96" x2="20.32" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C147" gate="G$1" pin="1"/>
<wire x1="20.32" y1="60.96" x2="33.02" y2="60.96" width="0.1524" layer="91"/>
<wire x1="33.02" y1="60.96" x2="33.02" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C146" gate="G$1" pin="1"/>
<wire x1="20.32" y1="58.42" x2="20.32" y2="60.96" width="0.1524" layer="91"/>
<junction x="20.32" y="60.96"/>
<label x="7.62" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETH2_TXD4" class="0">
<segment>
<pinref part="R121" gate="G$1" pin="1"/>
<wire x1="210.82" y1="142.24" x2="208.28" y2="142.24" width="0.1524" layer="91"/>
<label x="208.28" y="142.24" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U16" gate="G$1" pin="INT/TXER/TXD4"/>
<wire x1="60.96" y1="73.66" x2="58.42" y2="73.66" width="0.1524" layer="91"/>
<label x="58.42" y="73.66" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="ETH2_REFCLK0" class="0">
<segment>
<pinref part="R122" gate="G$1" pin="1"/>
<wire x1="210.82" y1="134.62" x2="208.28" y2="134.62" width="0.1524" layer="91"/>
<label x="208.28" y="134.62" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R135" gate="G$1" pin="1"/>
<wire x1="91.44" y1="149.86" x2="93.98" y2="149.86" width="0.1524" layer="91"/>
<label x="93.98" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R140" gate="G$1" pin="2"/>
<wire x1="43.18" y1="119.38" x2="45.72" y2="119.38" width="0.1524" layer="91"/>
<label x="45.72" y="119.38" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U16" gate="G$1" pin="RXCLK/!PHYAD1"/>
<wire x1="116.84" y1="101.6" x2="119.38" y2="101.6" width="0.1524" layer="91"/>
<label x="119.38" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETH2_RXER/PHYAD0" class="0">
<segment>
<pinref part="R125" gate="G$1" pin="1"/>
<wire x1="210.82" y1="149.86" x2="208.28" y2="149.86" width="0.1524" layer="91"/>
<label x="208.28" y="149.86" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R146" gate="G$1" pin="2"/>
<pinref part="U16" gate="G$1" pin="RXER/RXD4/!PHYAD0"/>
<wire x1="45.72" y1="71.12" x2="60.96" y2="71.12" width="0.1524" layer="91"/>
<label x="60.96" y="71.12" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="ETH2_TXP" class="0">
<segment>
<pinref part="U16" gate="G$1" pin="TXP"/>
<pinref part="CON4" gate="MAGJACK_RJ45" pin="TD+"/>
<wire x1="116.84" y1="86.36" x2="172.72" y2="86.36" width="0.1524" layer="91"/>
<pinref part="R129" gate="G$1" pin="1"/>
<wire x1="172.72" y1="86.36" x2="200.66" y2="86.36" width="0.1524" layer="91"/>
<wire x1="172.72" y1="91.44" x2="172.72" y2="86.36" width="0.1524" layer="91"/>
<junction x="172.72" y="86.36"/>
<label x="121.92" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETH2_TCTRCT" class="0">
<segment>
<pinref part="R130" gate="G$1" pin="1"/>
<pinref part="C142" gate="G$1" pin="1"/>
<wire x1="185.42" y1="91.44" x2="185.42" y2="88.9" width="0.1524" layer="91"/>
<pinref part="CON4" gate="MAGJACK_RJ45" pin="TCT"/>
<wire x1="185.42" y1="88.9" x2="185.42" y2="73.66" width="0.1524" layer="91"/>
<wire x1="185.42" y1="73.66" x2="185.42" y2="71.12" width="0.1524" layer="91"/>
<wire x1="200.66" y1="88.9" x2="185.42" y2="88.9" width="0.1524" layer="91"/>
<junction x="185.42" y="88.9"/>
<pinref part="CON4" gate="MAGJACK_RJ45" pin="RCT"/>
<wire x1="200.66" y1="73.66" x2="185.42" y2="73.66" width="0.1524" layer="91"/>
<junction x="185.42" y="73.66"/>
<label x="185.42" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETH2_TXN" class="0">
<segment>
<pinref part="CON4" gate="MAGJACK_RJ45" pin="TD-"/>
<wire x1="200.66" y1="83.82" x2="162.56" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U16" gate="G$1" pin="TXN"/>
<pinref part="R128" gate="G$1" pin="1"/>
<wire x1="162.56" y1="83.82" x2="116.84" y2="83.82" width="0.1524" layer="91"/>
<wire x1="162.56" y1="91.44" x2="162.56" y2="83.82" width="0.1524" layer="91"/>
<junction x="162.56" y="83.82"/>
<label x="121.92" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETH2_RXP" class="0">
<segment>
<pinref part="U16" gate="G$1" pin="RXP"/>
<pinref part="CON4" gate="MAGJACK_RJ45" pin="RD+"/>
<wire x1="116.84" y1="78.74" x2="152.4" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R127" gate="G$1" pin="1"/>
<wire x1="152.4" y1="78.74" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<wire x1="152.4" y1="91.44" x2="152.4" y2="78.74" width="0.1524" layer="91"/>
<junction x="152.4" y="78.74"/>
<label x="121.92" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETH2_RXN" class="0">
<segment>
<pinref part="CON4" gate="MAGJACK_RJ45" pin="RD-"/>
<pinref part="U16" gate="G$1" pin="RXN"/>
<wire x1="200.66" y1="76.2" x2="142.24" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R126" gate="G$1" pin="1"/>
<wire x1="142.24" y1="76.2" x2="116.84" y2="76.2" width="0.1524" layer="91"/>
<wire x1="142.24" y1="91.44" x2="142.24" y2="76.2" width="0.1524" layer="91"/>
<junction x="142.24" y="76.2"/>
<label x="121.92" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETH2_GC" class="0">
<segment>
<pinref part="R133" gate="G$1" pin="1"/>
<pinref part="CON4" gate="MAGJACK_RJ45" pin="GLED-"/>
<wire x1="246.38" y1="66.04" x2="236.22" y2="66.04" width="0.1524" layer="91"/>
<label x="236.22" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETH2_YC" class="0">
<segment>
<pinref part="R132" gate="G$1" pin="1"/>
<pinref part="CON4" gate="MAGJACK_RJ45" pin="YLED-"/>
<wire x1="246.38" y1="81.28" x2="236.22" y2="81.28" width="0.1524" layer="91"/>
<label x="236.22" y="81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETH2_YA" class="0">
<segment>
<pinref part="R131" gate="G$1" pin="1"/>
<pinref part="CON4" gate="MAGJACK_RJ45" pin="YLED+"/>
<wire x1="246.38" y1="88.9" x2="241.3" y2="88.9" width="0.1524" layer="91"/>
<wire x1="241.3" y1="88.9" x2="236.22" y2="88.9" width="0.1524" layer="91"/>
<junction x="241.3" y="88.9"/>
<wire x1="241.3" y1="88.9" x2="241.3" y2="93.98" width="0.1524" layer="91"/>
<label x="241.3" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U16" gate="G$1" pin="LED2/!INTSEL"/>
<wire x1="109.22" y1="53.34" x2="111.76" y2="53.34" width="0.1524" layer="91"/>
<label x="111.76" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETH2_GA" class="0">
<segment>
<pinref part="CON4" gate="MAGJACK_RJ45" pin="GLED+"/>
<wire x1="236.22" y1="73.66" x2="238.76" y2="73.66" width="0.1524" layer="91"/>
<label x="238.76" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U16" gate="G$1" pin="LED1/!REGOFF"/>
<wire x1="109.22" y1="55.88" x2="111.76" y2="55.88" width="0.1524" layer="91"/>
<label x="111.76" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETH2_RBIAS" class="0">
<segment>
<pinref part="U16" gate="G$1" pin="RBIAS"/>
<wire x1="68.58" y1="45.72" x2="60.96" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R134" gate="G$1" pin="2"/>
<wire x1="60.96" y1="43.18" x2="60.96" y2="45.72" width="0.1524" layer="91"/>
<label x="60.96" y="45.72" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="ETH2_PHY_VDDCR" class="0">
<segment>
<pinref part="C143" gate="G$1" pin="1"/>
<wire x1="182.88" y1="40.64" x2="182.88" y2="43.18" width="0.1524" layer="91"/>
<wire x1="182.88" y1="43.18" x2="198.12" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C144" gate="G$1" pin="1"/>
<wire x1="198.12" y1="40.64" x2="198.12" y2="43.18" width="0.1524" layer="91"/>
<label x="182.88" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U16" gate="G$1" pin="VDDCR"/>
<wire x1="68.58" y1="58.42" x2="66.04" y2="58.42" width="0.1524" layer="91"/>
<label x="66.04" y="58.42" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII0_REFCLK" class="0">
<segment>
<pinref part="R137" gate="G$1" pin="2"/>
<pinref part="R135" gate="G$1" pin="2"/>
<wire x1="71.12" y1="149.86" x2="76.2" y2="149.86" width="0.1524" layer="91"/>
<pinref part="R136" gate="G$1" pin="2"/>
<wire x1="76.2" y1="149.86" x2="81.28" y2="149.86" width="0.1524" layer="91"/>
<wire x1="76.2" y1="142.24" x2="76.2" y2="149.86" width="0.1524" layer="91"/>
<junction x="76.2" y="149.86"/>
<label x="76.2" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="RMII2_REF_CLK" class="0">
<segment>
<pinref part="R137" gate="G$1" pin="1"/>
<wire x1="60.96" y1="149.86" x2="58.42" y2="149.86" width="0.1524" layer="91"/>
<label x="58.42" y="149.86" size="1.778" layer="95" rot="R180"/>
<pinref part="R138" gate="G$1" pin="1"/>
<wire x1="58.42" y1="147.32" x2="58.42" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ETH2_RCLKIN" class="0">
<segment>
<pinref part="R136" gate="G$1" pin="1"/>
<wire x1="76.2" y1="132.08" x2="76.2" y2="129.54" width="0.1524" layer="91"/>
<label x="76.2" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U16" gate="G$1" pin="XTAL/CLKIN"/>
<pinref part="R149" gate="G$1" pin="1"/>
<wire x1="109.22" y1="48.26" x2="139.7" y2="48.26" width="0.1524" layer="91"/>
<label x="111.76" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETH2_TXCLK" class="0">
<segment>
<pinref part="R139" gate="G$1" pin="2"/>
<wire x1="43.18" y1="127" x2="45.72" y2="127" width="0.1524" layer="91"/>
<label x="45.72" y="127" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U16" gate="G$1" pin="TXCLK"/>
<wire x1="116.84" y1="104.14" x2="119.38" y2="104.14" width="0.1524" layer="91"/>
<label x="119.38" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETH2_RXDV" class="0">
<segment>
<pinref part="R141" gate="G$1" pin="2"/>
<wire x1="43.18" y1="111.76" x2="45.72" y2="111.76" width="0.1524" layer="91"/>
<label x="45.72" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U16" gate="G$1" pin="RXDV"/>
<wire x1="116.84" y1="91.44" x2="119.38" y2="91.44" width="0.1524" layer="91"/>
<label x="119.38" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII2_TXCLK" class="0">
<segment>
<pinref part="R139" gate="G$1" pin="1"/>
<wire x1="33.02" y1="127" x2="30.48" y2="127" width="0.1524" layer="91"/>
<label x="30.48" y="127" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII2_RXCLK" class="0">
<segment>
<pinref part="R140" gate="G$1" pin="1"/>
<wire x1="33.02" y1="119.38" x2="30.48" y2="119.38" width="0.1524" layer="91"/>
<label x="30.48" y="119.38" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII2_RXDV" class="0">
<segment>
<pinref part="R141" gate="G$1" pin="1"/>
<wire x1="33.02" y1="111.76" x2="30.48" y2="111.76" width="0.1524" layer="91"/>
<label x="30.48" y="111.76" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII2_TXD0" class="0">
<segment>
<pinref part="U16" gate="G$1" pin="TXD0"/>
<wire x1="60.96" y1="104.14" x2="58.42" y2="104.14" width="0.1524" layer="91"/>
<label x="58.42" y="104.14" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII2_TXD1" class="0">
<segment>
<pinref part="U16" gate="G$1" pin="TXD1"/>
<wire x1="60.96" y1="96.52" x2="58.42" y2="96.52" width="0.1524" layer="91"/>
<label x="58.42" y="96.52" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII2_TXD2" class="0">
<segment>
<pinref part="U16" gate="G$1" pin="TXD2"/>
<wire x1="60.96" y1="88.9" x2="58.42" y2="88.9" width="0.1524" layer="91"/>
<label x="58.42" y="88.9" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII2_TXD3" class="0">
<segment>
<pinref part="U16" gate="G$1" pin="TXD3"/>
<wire x1="60.96" y1="81.28" x2="58.42" y2="81.28" width="0.1524" layer="91"/>
<label x="58.42" y="81.28" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="ETH2_RXD0" class="0">
<segment>
<pinref part="R142" gate="G$1" pin="2"/>
<pinref part="U16" gate="G$1" pin="RXD0/!MODE0"/>
<wire x1="45.72" y1="101.6" x2="60.96" y2="101.6" width="0.1524" layer="91"/>
<label x="60.96" y="101.6" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R120" gate="G$1" pin="2"/>
<wire x1="162.56" y1="149.86" x2="165.1" y2="149.86" width="0.1524" layer="91"/>
<label x="165.1" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETH2_RXD1" class="0">
<segment>
<pinref part="R143" gate="G$1" pin="2"/>
<pinref part="U16" gate="G$1" pin="RXD1/!MODE1"/>
<wire x1="45.72" y1="93.98" x2="60.96" y2="93.98" width="0.1524" layer="91"/>
<label x="60.96" y="93.98" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R119" gate="G$1" pin="2"/>
<wire x1="162.56" y1="142.24" x2="165.1" y2="142.24" width="0.1524" layer="91"/>
<label x="165.1" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETH2_RXD2" class="0">
<segment>
<pinref part="R144" gate="G$1" pin="2"/>
<pinref part="U16" gate="G$1" pin="RXD2/!RMIISEL"/>
<wire x1="45.72" y1="86.36" x2="60.96" y2="86.36" width="0.1524" layer="91"/>
<label x="60.96" y="86.36" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R123" gate="G$1" pin="1"/>
<wire x1="210.82" y1="127" x2="208.28" y2="127" width="0.1524" layer="91"/>
<label x="208.28" y="127" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="ETH2_RXD3" class="0">
<segment>
<pinref part="R145" gate="G$1" pin="2"/>
<pinref part="U16" gate="G$1" pin="RXD3/!PHYAD2"/>
<wire x1="45.72" y1="78.74" x2="60.96" y2="78.74" width="0.1524" layer="91"/>
<label x="60.96" y="78.74" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R124" gate="G$1" pin="1"/>
<wire x1="210.82" y1="119.38" x2="208.28" y2="119.38" width="0.1524" layer="91"/>
<label x="208.28" y="119.38" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII2_RXD0" class="0">
<segment>
<pinref part="R142" gate="G$1" pin="1"/>
<wire x1="35.56" y1="101.6" x2="33.02" y2="101.6" width="0.1524" layer="91"/>
<label x="33.02" y="101.6" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII2_RXD1" class="0">
<segment>
<pinref part="R143" gate="G$1" pin="1"/>
<wire x1="35.56" y1="93.98" x2="33.02" y2="93.98" width="0.1524" layer="91"/>
<label x="33.02" y="93.98" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII2_RXD2" class="0">
<segment>
<pinref part="R144" gate="G$1" pin="1"/>
<wire x1="35.56" y1="86.36" x2="33.02" y2="86.36" width="0.1524" layer="91"/>
<label x="33.02" y="86.36" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII2_RXD3" class="0">
<segment>
<pinref part="R145" gate="G$1" pin="1"/>
<wire x1="35.56" y1="78.74" x2="33.02" y2="78.74" width="0.1524" layer="91"/>
<label x="33.02" y="78.74" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII2_RXER" class="0">
<segment>
<pinref part="R146" gate="G$1" pin="1"/>
<wire x1="35.56" y1="71.12" x2="33.02" y2="71.12" width="0.1524" layer="91"/>
<label x="33.02" y="71.12" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="MII2_TXEN" class="0">
<segment>
<pinref part="U16" gate="G$1" pin="TXEN"/>
<wire x1="116.84" y1="96.52" x2="119.38" y2="96.52" width="0.1524" layer="91"/>
<label x="119.38" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETH2_CRS" class="0">
<segment>
<pinref part="U16" gate="G$1" pin="CRS"/>
<wire x1="116.84" y1="71.12" x2="127" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R147" gate="G$1" pin="1"/>
<label x="119.38" y="71.12" size="1.27" layer="95"/>
</segment>
</net>
<net name="MII2_CRS_DV" class="0">
<segment>
<pinref part="R147" gate="G$1" pin="2"/>
<wire x1="137.16" y1="71.12" x2="139.7" y2="71.12" width="0.1524" layer="91"/>
<label x="139.7" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII2_COL" class="0">
<segment>
<pinref part="R148" gate="G$1" pin="2"/>
<wire x1="137.16" y1="63.5" x2="139.7" y2="63.5" width="0.1524" layer="91"/>
<label x="139.7" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="PHY2_X2" class="0">
<segment>
<pinref part="C148" gate="G$1" pin="1"/>
<pinref part="R150" gate="G$1" pin="2"/>
<wire x1="134.62" y1="30.48" x2="134.62" y2="33.02" width="0.1524" layer="91"/>
<pinref part="Y4" gate="G$1" pin="2"/>
<wire x1="134.62" y1="33.02" x2="134.62" y2="35.56" width="0.1524" layer="91"/>
<wire x1="142.24" y1="33.02" x2="134.62" y2="33.02" width="0.1524" layer="91"/>
<junction x="134.62" y="33.02"/>
<label x="134.62" y="33.02" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="PHY2_X1" class="0">
<segment>
<pinref part="Y4" gate="G$1" pin="1"/>
<pinref part="C149" gate="G$1" pin="1"/>
<wire x1="147.32" y1="33.02" x2="152.4" y2="33.02" width="0.1524" layer="91"/>
<wire x1="152.4" y1="33.02" x2="152.4" y2="30.48" width="0.1524" layer="91"/>
<wire x1="152.4" y1="33.02" x2="152.4" y2="48.26" width="0.1524" layer="91"/>
<junction x="152.4" y="33.02"/>
<pinref part="R149" gate="G$1" pin="2"/>
<wire x1="152.4" y1="48.26" x2="149.86" y2="48.26" width="0.1524" layer="91"/>
<label x="152.4" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="PHY2_XTAL2" class="0">
<segment>
<pinref part="U16" gate="G$1" pin="XTAL2"/>
<wire x1="109.22" y1="45.72" x2="134.62" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R150" gate="G$1" pin="1"/>
<label x="111.76" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="3.302" y="2.286" size="10.16" layer="97">PERIPHERAL MODULES</text>
<text x="58.674" y="93.98" size="3.048" layer="97">USB-A CIRCUIT</text>
<text x="149.352" y="230.124" size="3.048" layer="97">HEARTBEAT LED</text>
<text x="45.212" y="225.044" size="3.048" layer="97">COOLING FAN</text>
<text x="151.892" y="174.244" size="3.048" layer="97">STATUS LEDs</text>
<text x="19.812" y="146.304" size="3.048" layer="97">315/433MHz RF TRANSMITTER</text>
<text x="3.048" y="17.272" size="2.54" layer="97">Linux Micro-PC
Rev 1.0
SBS 2015</text>
</plain>
<instances>
<instance part="U7" gate="G$1" x="45.72" y="66.04"/>
<instance part="CON5" gate="G$1" x="119.38" y="58.42"/>
<instance part="U13" gate="G$1" x="86.36" y="35.56" rot="MR270"/>
<instance part="FB4" gate="G$1" x="88.9" y="66.04"/>
<instance part="FB5" gate="G$1" x="76.2" y="78.74" rot="R270"/>
<instance part="SUPPLY59" gate="GND" x="106.68" y="35.56"/>
<instance part="SUPPLY60" gate="GND" x="30.48" y="48.26"/>
<instance part="C132" gate="G$1" x="7.62" y="60.96"/>
<instance part="R75" gate="G$1" x="22.86" y="58.42" rot="R90"/>
<instance part="R76" gate="G$1" x="27.94" y="81.28" rot="R90"/>
<instance part="D1" gate="G$1" x="53.34" y="215.9"/>
<instance part="FAN" gate="G$1" x="71.12" y="213.36"/>
<instance part="R1" gate="G$1" x="53.34" y="195.58" rot="R90"/>
<instance part="R2" gate="G$1" x="63.5" y="195.58" rot="R90"/>
<instance part="Q1" gate="G$1" x="55.88" y="180.34"/>
<instance part="SUPPLY17" gate="GND" x="58.42" y="172.72"/>
<instance part="TP1" gate="G$1" x="48.26" y="180.34"/>
<instance part="R69" gate="G$1" x="48.26" y="172.72" rot="R90"/>
<instance part="SUPPLY26" gate="GND" x="48.26" y="165.1"/>
<instance part="LD2" gate="G$1" x="182.88" y="220.98" rot="R90"/>
<instance part="R67" gate="G$1" x="165.1" y="220.98"/>
<instance part="SUPPLY55" gate="GND" x="187.96" y="205.74"/>
<instance part="R111" gate="G$1" x="177.8" y="165.1"/>
<instance part="R112" gate="G$1" x="177.8" y="127"/>
<instance part="R114" gate="G$1" x="177.8" y="50.8"/>
<instance part="Q2" gate="BJT" x="185.42" y="213.36"/>
<instance part="R68" gate="G$1" x="175.26" y="213.36"/>
<instance part="Q3" gate="BJT" x="182.88" y="157.48"/>
<instance part="Q4" gate="BJT" x="182.88" y="119.38"/>
<instance part="Q5" gate="BJT" x="182.88" y="81.28"/>
<instance part="Q6" gate="BJT" x="182.88" y="43.18"/>
<instance part="R70" gate="G$1" x="172.72" y="157.48"/>
<instance part="R71" gate="G$1" x="172.72" y="119.38"/>
<instance part="R72" gate="G$1" x="172.72" y="81.28"/>
<instance part="R73" gate="G$1" x="172.72" y="43.18"/>
<instance part="U18" gate="G$1" x="45.72" y="134.62"/>
<instance part="SUPPLY81" gate="GND" x="48.26" y="127"/>
<instance part="U12" gate="G$1" x="96.52" y="129.54" rot="MR0"/>
<instance part="SUPPLY82" gate="GND" x="116.84" y="114.3"/>
<instance part="C164" gate="G$1" x="121.92" y="124.46"/>
<instance part="SUPPLY83" gate="GND" x="78.74" y="116.84"/>
<instance part="SUPPLY52" gate="GND" x="185.42" y="35.56"/>
<instance part="SUPPLY85" gate="GND" x="185.42" y="73.66"/>
<instance part="SUPPLY86" gate="GND" x="185.42" y="111.76"/>
<instance part="SUPPLY87" gate="GND" x="185.42" y="149.86"/>
<instance part="LD3" gate="G$1" x="167.64" y="165.1" rot="R90"/>
<instance part="LD4" gate="G$1" x="167.64" y="127" rot="R90"/>
<instance part="LD5" gate="G$1" x="167.64" y="88.9" rot="R90"/>
<instance part="LD6" gate="G$1" x="167.64" y="50.8" rot="R90"/>
<instance part="R74" gate="G$1" x="177.8" y="88.9"/>
<instance part="R108" gate="G$1" x="165.1" y="73.66" rot="R90"/>
<instance part="R109" gate="G$1" x="165.1" y="111.76" rot="R90"/>
<instance part="R110" gate="G$1" x="165.1" y="35.56" rot="R90"/>
<instance part="SUPPLY89" gate="GND" x="165.1" y="104.14"/>
<instance part="SUPPLY90" gate="GND" x="165.1" y="66.04"/>
<instance part="SUPPLY91" gate="GND" x="165.1" y="27.94"/>
<instance part="R113" gate="G$1" x="165.1" y="149.86" rot="R90"/>
<instance part="SUPPLY88" gate="GND" x="165.1" y="142.24"/>
<instance part="R115" gate="G$1" x="167.64" y="205.74" rot="R90"/>
<instance part="SUPPLY92" gate="GND" x="167.64" y="198.12"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U13" gate="G$1" pin="GND"/>
<wire x1="96.52" y1="48.26" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<pinref part="CON5" gate="G$1" pin="GND"/>
<wire x1="96.52" y1="50.8" x2="106.68" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SUPPLY59" gate="GND" pin="GND"/>
<wire x1="106.68" y1="50.8" x2="109.22" y2="50.8" width="0.1524" layer="91"/>
<wire x1="106.68" y1="38.1" x2="106.68" y2="50.8" width="0.1524" layer="91"/>
<junction x="106.68" y="50.8"/>
<pinref part="CON5" gate="G$1" pin="SHIELD"/>
<wire x1="124.46" y1="43.18" x2="124.46" y2="38.1" width="0.1524" layer="91"/>
<wire x1="124.46" y1="38.1" x2="106.68" y2="38.1" width="0.1524" layer="91"/>
<junction x="106.68" y="38.1"/>
</segment>
<segment>
<pinref part="SUPPLY60" gate="GND" pin="GND"/>
<wire x1="30.48" y1="50.8" x2="30.48" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="GND"/>
<wire x1="30.48" y1="53.34" x2="30.48" y2="58.42" width="0.1524" layer="91"/>
<wire x1="30.48" y1="58.42" x2="33.02" y2="58.42" width="0.1524" layer="91"/>
<pinref part="R75" gate="G$1" pin="1"/>
<wire x1="22.86" y1="53.34" x2="30.48" y2="53.34" width="0.1524" layer="91"/>
<junction x="30.48" y="53.34"/>
<pinref part="C132" gate="G$1" pin="-"/>
<wire x1="22.86" y1="53.34" x2="7.62" y2="53.34" width="0.1524" layer="91"/>
<wire x1="7.62" y1="53.34" x2="7.62" y2="55.88" width="0.1524" layer="91"/>
<junction x="22.86" y="53.34"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="S"/>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R69" gate="G$1" pin="1"/>
<pinref part="SUPPLY26" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="Q2" gate="BJT" pin="E"/>
<pinref part="SUPPLY55" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U18" gate="G$1" pin="GND"/>
<pinref part="SUPPLY81" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U12" gate="G$1" pin="!OE"/>
<wire x1="111.76" y1="127" x2="114.3" y2="127" width="0.1524" layer="91"/>
<wire x1="114.3" y1="127" x2="114.3" y2="121.92" width="0.1524" layer="91"/>
<pinref part="SUPPLY82" gate="GND" pin="GND"/>
<wire x1="114.3" y1="121.92" x2="114.3" y2="116.84" width="0.1524" layer="91"/>
<wire x1="114.3" y1="116.84" x2="116.84" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C164" gate="G$1" pin="2"/>
<wire x1="116.84" y1="116.84" x2="121.92" y2="116.84" width="0.1524" layer="91"/>
<wire x1="121.92" y1="116.84" x2="121.92" y2="119.38" width="0.1524" layer="91"/>
<junction x="116.84" y="116.84"/>
<pinref part="U12" gate="G$1" pin="GND"/>
<wire x1="111.76" y1="121.92" x2="114.3" y2="121.92" width="0.1524" layer="91"/>
<junction x="114.3" y="121.92"/>
</segment>
<segment>
<pinref part="U12" gate="G$1" pin="2Y"/>
<pinref part="SUPPLY83" gate="GND" pin="GND"/>
<wire x1="81.28" y1="121.92" x2="78.74" y2="121.92" width="0.1524" layer="91"/>
<wire x1="78.74" y1="121.92" x2="78.74" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q6" gate="BJT" pin="E"/>
<pinref part="SUPPLY52" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="Q5" gate="BJT" pin="E"/>
<pinref part="SUPPLY85" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="Q4" gate="BJT" pin="E"/>
<pinref part="SUPPLY86" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="Q3" gate="BJT" pin="E"/>
<pinref part="SUPPLY87" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R109" gate="G$1" pin="1"/>
<pinref part="SUPPLY89" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R108" gate="G$1" pin="1"/>
<pinref part="SUPPLY90" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R110" gate="G$1" pin="1"/>
<pinref part="SUPPLY91" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R113" gate="G$1" pin="1"/>
<pinref part="SUPPLY88" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R115" gate="G$1" pin="1"/>
<pinref part="SUPPLY92" gate="GND" pin="GND"/>
</segment>
</net>
<net name="SYS_5V" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="IN"/>
<pinref part="C132" gate="G$1" pin="+"/>
<wire x1="33.02" y1="73.66" x2="7.62" y2="73.66" width="0.1524" layer="91"/>
<wire x1="7.62" y1="73.66" x2="7.62" y2="63.5" width="0.1524" layer="91"/>
<label x="7.62" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R67" gate="G$1" pin="1"/>
<wire x1="160.02" y1="220.98" x2="154.94" y2="220.98" width="0.1524" layer="91"/>
<label x="154.94" y="220.98" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="VDD_3V3A" class="0">
<segment>
<pinref part="R76" gate="G$1" pin="2"/>
<wire x1="27.94" y1="86.36" x2="27.94" y2="88.9" width="0.1524" layer="91"/>
<label x="27.94" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="USB1_OCN" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="!OC"/>
<pinref part="R76" gate="G$1" pin="1"/>
<wire x1="33.02" y1="68.58" x2="27.94" y2="68.58" width="0.1524" layer="91"/>
<wire x1="27.94" y1="68.58" x2="27.94" y2="76.2" width="0.1524" layer="91"/>
<label x="27.94" y="68.58" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="USB1_DP" class="0">
<segment>
<pinref part="CON5" gate="G$1" pin="D+"/>
<pinref part="U13" gate="G$1" pin="D+"/>
<wire x1="109.22" y1="55.88" x2="86.36" y2="55.88" width="0.1524" layer="91"/>
<wire x1="86.36" y1="55.88" x2="86.36" y2="48.26" width="0.1524" layer="91"/>
<label x="104.14" y="55.88" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="USB1_DN" class="0">
<segment>
<pinref part="CON5" gate="G$1" pin="D-"/>
<pinref part="U13" gate="G$1" pin="D-"/>
<wire x1="109.22" y1="60.96" x2="81.28" y2="60.96" width="0.1524" layer="91"/>
<wire x1="81.28" y1="60.96" x2="81.28" y2="48.26" width="0.1524" layer="91"/>
<label x="104.14" y="60.96" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="USB1_DRVVBUS" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="!EN"/>
<pinref part="R75" gate="G$1" pin="2"/>
<wire x1="33.02" y1="63.5" x2="22.86" y2="63.5" width="0.1524" layer="91"/>
<label x="12.7" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="USB1_PWR" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="OUT"/>
<pinref part="FB4" gate="G$1" pin="1"/>
<wire x1="58.42" y1="66.04" x2="76.2" y2="66.04" width="0.1524" layer="91"/>
<pinref part="FB5" gate="G$1" pin="2"/>
<wire x1="76.2" y1="66.04" x2="81.28" y2="66.04" width="0.1524" layer="91"/>
<wire x1="76.2" y1="71.12" x2="76.2" y2="66.04" width="0.1524" layer="91"/>
<junction x="76.2" y="66.04"/>
<pinref part="U13" gate="G$1" pin="VBUS"/>
<wire x1="76.2" y1="48.26" x2="76.2" y2="66.04" width="0.1524" layer="91"/>
<junction x="76.2" y="66.04"/>
<label x="60.96" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="USB_VBUS" class="0">
<segment>
<pinref part="FB4" gate="G$1" pin="2"/>
<pinref part="CON5" gate="G$1" pin="VBUS"/>
<wire x1="96.52" y1="66.04" x2="109.22" y2="66.04" width="0.1524" layer="91"/>
<label x="96.52" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="USB1_VBUS" class="0">
<segment>
<pinref part="FB5" gate="G$1" pin="1"/>
<wire x1="76.2" y1="86.36" x2="76.2" y2="88.9" width="0.1524" layer="91"/>
<label x="76.2" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="48.26" y1="215.9" x2="50.8" y2="215.9" width="0.1524" layer="91"/>
<label x="48.26" y="215.9" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="U18" gate="G$1" pin="VCC"/>
<wire x1="48.26" y1="134.62" x2="50.8" y2="134.62" width="0.1524" layer="91"/>
<label x="50.8" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U12" gate="G$1" pin="VCC"/>
<wire x1="111.76" y1="137.16" x2="114.3" y2="137.16" width="0.1524" layer="91"/>
<label x="121.92" y="132.08" size="1.778" layer="95"/>
<pinref part="U12" gate="G$1" pin="2OE"/>
<wire x1="111.76" y1="132.08" x2="114.3" y2="132.08" width="0.1524" layer="91"/>
<wire x1="114.3" y1="132.08" x2="114.3" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C164" gate="G$1" pin="1"/>
<wire x1="114.3" y1="132.08" x2="121.92" y2="132.08" width="0.1524" layer="91"/>
<wire x1="121.92" y1="132.08" x2="121.92" y2="127" width="0.1524" layer="91"/>
<junction x="114.3" y="132.08"/>
</segment>
</net>
<net name="PWM_FAN" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="G"/>
<pinref part="TP1" gate="G$1" pin="1"/>
<wire x1="53.34" y1="177.8" x2="48.26" y2="177.8" width="0.1524" layer="91"/>
<wire x1="48.26" y1="177.8" x2="45.72" y2="177.8" width="0.1524" layer="91"/>
<junction x="48.26" y="177.8"/>
<label x="45.72" y="177.8" size="1.778" layer="95" rot="R180"/>
<pinref part="R69" gate="G$1" pin="2"/>
</segment>
</net>
<net name="FAN_IN" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="FAN" gate="G$1" pin="PWR"/>
<wire x1="55.88" y1="215.9" x2="60.96" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FAN_OUT" class="0">
<segment>
<pinref part="FAN" gate="G$1" pin="GND"/>
<wire x1="60.96" y1="208.28" x2="58.42" y2="208.28" width="0.1524" layer="91"/>
<wire x1="58.42" y1="208.28" x2="58.42" y2="203.2" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="58.42" y1="203.2" x2="53.34" y2="203.2" width="0.1524" layer="91"/>
<wire x1="53.34" y1="203.2" x2="53.34" y2="200.66" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="58.42" y1="203.2" x2="63.5" y2="203.2" width="0.1524" layer="91"/>
<wire x1="63.5" y1="203.2" x2="63.5" y2="200.66" width="0.1524" layer="91"/>
<junction x="58.42" y="203.2"/>
<label x="58.42" y="208.28" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="FAN_FET_D" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="63.5" y1="190.5" x2="63.5" y2="187.96" width="0.1524" layer="91"/>
<wire x1="63.5" y1="187.96" x2="58.42" y2="187.96" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="58.42" y1="187.96" x2="58.42" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="53.34" y1="190.5" x2="53.34" y2="187.96" width="0.1524" layer="91"/>
<wire x1="53.34" y1="187.96" x2="58.42" y2="187.96" width="0.1524" layer="91"/>
<junction x="58.42" y="187.96"/>
</segment>
</net>
<net name="LD2_HB" class="0">
<segment>
<label x="165.1" y="213.36" size="1.778" layer="95" rot="R180"/>
<pinref part="R68" gate="G$1" pin="1"/>
<wire x1="170.18" y1="213.36" x2="167.64" y2="213.36" width="0.1524" layer="91"/>
<pinref part="R115" gate="G$1" pin="2"/>
<wire x1="167.64" y1="213.36" x2="165.1" y2="213.36" width="0.1524" layer="91"/>
<wire x1="167.64" y1="210.82" x2="167.64" y2="213.36" width="0.1524" layer="91"/>
<junction x="167.64" y="213.36"/>
</segment>
</net>
<net name="LD2_A" class="0">
<segment>
<pinref part="R67" gate="G$1" pin="2"/>
<pinref part="LD2" gate="G$1" pin="A"/>
<wire x1="170.18" y1="220.98" x2="177.8" y2="220.98" width="0.1524" layer="91"/>
<label x="170.18" y="220.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="Q1_C" class="0">
<segment>
<pinref part="LD2" gate="G$1" pin="C"/>
<wire x1="185.42" y1="220.98" x2="187.96" y2="220.98" width="0.1524" layer="91"/>
<label x="187.96" y="220.98" size="1.778" layer="95"/>
<pinref part="Q2" gate="BJT" pin="C"/>
<wire x1="187.96" y1="218.44" x2="187.96" y2="220.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3_PER" class="0">
<segment>
<pinref part="LD5" gate="G$1" pin="A"/>
<wire x1="162.56" y1="88.9" x2="160.02" y2="88.9" width="0.1524" layer="91"/>
<label x="160.02" y="88.9" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="LD3" gate="G$1" pin="A"/>
<wire x1="162.56" y1="165.1" x2="160.02" y2="165.1" width="0.1524" layer="91"/>
<label x="160.02" y="165.1" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="LD4" gate="G$1" pin="A"/>
<wire x1="162.56" y1="127" x2="160.02" y2="127" width="0.1524" layer="91"/>
<label x="160.02" y="127" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="LD6" gate="G$1" pin="A"/>
<wire x1="160.02" y1="50.8" x2="162.56" y2="50.8" width="0.1524" layer="91"/>
<label x="160.02" y="50.8" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="Q3_C" class="0">
<segment>
<pinref part="R111" gate="G$1" pin="2"/>
<pinref part="Q3" gate="BJT" pin="C"/>
<wire x1="185.42" y1="162.56" x2="185.42" y2="165.1" width="0.1524" layer="91"/>
<label x="185.42" y="165.1" size="1.778" layer="95"/>
<wire x1="182.88" y1="165.1" x2="185.42" y2="165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="Q4_C" class="0">
<segment>
<pinref part="Q4" gate="BJT" pin="C"/>
<wire x1="185.42" y1="124.46" x2="185.42" y2="127" width="0.1524" layer="91"/>
<pinref part="R112" gate="G$1" pin="2"/>
<wire x1="185.42" y1="127" x2="182.88" y2="127" width="0.1524" layer="91"/>
<label x="185.42" y="127" size="1.778" layer="95"/>
</segment>
</net>
<net name="Q5_C" class="0">
<segment>
<pinref part="Q5" gate="BJT" pin="C"/>
<label x="185.42" y="88.9" size="1.778" layer="95"/>
<wire x1="185.42" y1="86.36" x2="185.42" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R74" gate="G$1" pin="2"/>
<wire x1="185.42" y1="88.9" x2="182.88" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="Q6_C" class="0">
<segment>
<pinref part="Q6" gate="BJT" pin="C"/>
<wire x1="185.42" y1="48.26" x2="185.42" y2="50.8" width="0.1524" layer="91"/>
<label x="185.42" y="50.8" size="1.778" layer="95"/>
<pinref part="R114" gate="G$1" pin="2"/>
<wire x1="185.42" y1="50.8" x2="182.88" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="Q2_B" class="0">
<segment>
<pinref part="R68" gate="G$1" pin="2"/>
<pinref part="Q2" gate="BJT" pin="B"/>
<wire x1="180.34" y1="213.36" x2="182.88" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="Q3_B" class="0">
<segment>
<pinref part="R70" gate="G$1" pin="2"/>
<pinref part="Q3" gate="BJT" pin="B"/>
<wire x1="177.8" y1="157.48" x2="180.34" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="Q4_B" class="0">
<segment>
<pinref part="Q4" gate="BJT" pin="B"/>
<pinref part="R71" gate="G$1" pin="2"/>
<wire x1="180.34" y1="119.38" x2="177.8" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="Q5_B" class="0">
<segment>
<pinref part="R72" gate="G$1" pin="2"/>
<pinref part="Q5" gate="BJT" pin="B"/>
<wire x1="177.8" y1="81.28" x2="180.34" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="Q6_B" class="0">
<segment>
<pinref part="Q6" gate="BJT" pin="B"/>
<pinref part="R73" gate="G$1" pin="2"/>
<wire x1="180.34" y1="43.18" x2="177.8" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LD3_USR1" class="0">
<segment>
<pinref part="R70" gate="G$1" pin="1"/>
<label x="162.56" y="157.48" size="1.778" layer="95" rot="R180"/>
<wire x1="167.64" y1="157.48" x2="165.1" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R113" gate="G$1" pin="2"/>
<wire x1="165.1" y1="157.48" x2="162.56" y2="157.48" width="0.1524" layer="91"/>
<wire x1="165.1" y1="154.94" x2="165.1" y2="157.48" width="0.1524" layer="91"/>
<junction x="165.1" y="157.48"/>
</segment>
</net>
<net name="LD4_USR2" class="0">
<segment>
<pinref part="R71" gate="G$1" pin="1"/>
<wire x1="167.64" y1="119.38" x2="165.1" y2="119.38" width="0.1524" layer="91"/>
<label x="162.56" y="119.38" size="1.778" layer="95" rot="R180"/>
<pinref part="R109" gate="G$1" pin="2"/>
<wire x1="165.1" y1="119.38" x2="162.56" y2="119.38" width="0.1524" layer="91"/>
<wire x1="165.1" y1="116.84" x2="165.1" y2="119.38" width="0.1524" layer="91"/>
<junction x="165.1" y="119.38"/>
</segment>
</net>
<net name="LD5_USR3" class="0">
<segment>
<pinref part="R72" gate="G$1" pin="1"/>
<wire x1="167.64" y1="81.28" x2="165.1" y2="81.28" width="0.1524" layer="91"/>
<label x="162.56" y="81.28" size="1.778" layer="95" rot="R180"/>
<pinref part="R108" gate="G$1" pin="2"/>
<wire x1="165.1" y1="81.28" x2="162.56" y2="81.28" width="0.1524" layer="91"/>
<wire x1="165.1" y1="78.74" x2="165.1" y2="81.28" width="0.1524" layer="91"/>
<junction x="165.1" y="81.28"/>
</segment>
</net>
<net name="LD6_USR4" class="0">
<segment>
<pinref part="R73" gate="G$1" pin="1"/>
<wire x1="167.64" y1="43.18" x2="165.1" y2="43.18" width="0.1524" layer="91"/>
<label x="162.56" y="43.18" size="1.778" layer="95" rot="R180"/>
<pinref part="R110" gate="G$1" pin="2"/>
<wire x1="165.1" y1="43.18" x2="162.56" y2="43.18" width="0.1524" layer="91"/>
<wire x1="165.1" y1="40.64" x2="165.1" y2="43.18" width="0.1524" layer="91"/>
<junction x="165.1" y="43.18"/>
</segment>
</net>
<net name="H2_UART1_TX" class="0">
<segment>
<pinref part="U18" gate="G$1" pin="DATA"/>
<wire x1="48.26" y1="139.7" x2="58.42" y2="139.7" width="0.1524" layer="91"/>
<wire x1="58.42" y1="139.7" x2="58.42" y2="132.08" width="0.1524" layer="91"/>
<pinref part="U12" gate="G$1" pin="1Y"/>
<wire x1="58.42" y1="132.08" x2="81.28" y2="132.08" width="0.1524" layer="91"/>
<label x="58.42" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART1_TX" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="1A"/>
<wire x1="81.28" y1="137.16" x2="78.74" y2="137.16" width="0.1524" layer="91"/>
<label x="78.74" y="137.16" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="LD3_C" class="0">
<segment>
<pinref part="R111" gate="G$1" pin="1"/>
<pinref part="LD3" gate="G$1" pin="C"/>
<wire x1="172.72" y1="165.1" x2="170.18" y2="165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LD4_C" class="0">
<segment>
<pinref part="R112" gate="G$1" pin="1"/>
<pinref part="LD4" gate="G$1" pin="C"/>
<wire x1="172.72" y1="127" x2="170.18" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LD5_C" class="0">
<segment>
<pinref part="LD5" gate="G$1" pin="C"/>
<pinref part="R74" gate="G$1" pin="1"/>
<wire x1="172.72" y1="88.9" x2="170.18" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LD6_C" class="0">
<segment>
<pinref part="R114" gate="G$1" pin="1"/>
<pinref part="LD6" gate="G$1" pin="C"/>
<wire x1="172.72" y1="50.8" x2="170.18" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,2,71.12,264.16,U1A,VSS_2,GND,,,"/>
<approved hash="104,2,71.12,254,U1A,VSS_RTC,GND,,,"/>
<approved hash="104,2,71.12,243.84,U1A,VREFN,GNDA_ADC,,,"/>
<approved hash="104,2,71.12,220.98,U1A,VREFP,VREFP_ADC,,,"/>
<approved hash="104,2,71.12,180.34,U1A,VDDS_RTC,VRTC,,,"/>
<approved hash="104,2,71.12,177.8,U1A,VDDA_ADC,VDD_ADC,,,"/>
<approved hash="104,2,134.62,162.56,U1A,VDDS_DDR_2,VDDS_DDR,,,"/>
<approved hash="104,2,134.62,165.1,U1A,VDDS_2,VDDS,,,"/>
<approved hash="104,2,134.62,167.64,U1A,VDDS_PLL_DDR,VDD_PLL,,,"/>
<approved hash="104,2,134.62,170.18,U1A,VSSA_ADC,GNDA_ADC,,,"/>
<approved hash="104,2,134.62,172.72,U1A,VDDS_SRAM_CORE_BG,VDD_1V8,,,"/>
<approved hash="104,2,134.62,185.42,U1A,VDDS_DDR_3,VDDS_DDR,,,"/>
<approved hash="104,2,134.62,187.96,U1A,VDD_CORE_2,VDD_CORE,,,"/>
<approved hash="104,2,134.62,190.5,U1A,VDD_CORE_3,VDD_CORE,,,"/>
<approved hash="104,2,134.62,193.04,U1A,VSS_3,GND,,,"/>
<approved hash="104,2,134.62,208.28,U1A,VDDS_DDR_4,VDDS_DDR,,,"/>
<approved hash="104,2,134.62,210.82,U1A,VDD_CORE_4,VDD_CORE,,,"/>
<approved hash="104,2,134.62,215.9,U1A,VSS_4,GND,,,"/>
<approved hash="104,2,134.62,218.44,U1A,VSS_5,GND,,,"/>
<approved hash="104,2,134.62,231.14,U1A,VDDS_DDR_5,VDDS_DDR,,,"/>
<approved hash="104,2,134.62,233.68,U1A,VSS_6,GND,,,"/>
<approved hash="104,2,134.62,236.22,U1A,VSS_7,GND,,,"/>
<approved hash="104,2,134.62,238.76,U1A,VSS_8,GND,,,"/>
<approved hash="104,2,134.62,241.3,U1A,VSS_9,GND,,,"/>
<approved hash="104,2,134.62,256.54,U1A,VSS_10,GND,,,"/>
<approved hash="104,2,134.62,259.08,U1A,VSS_11,GND,,,"/>
<approved hash="104,2,134.62,261.62,U1A,VSS_12,GND,,,"/>
<approved hash="104,2,134.62,264.16,U1A,VSS,GND,,,"/>
<approved hash="104,2,185.42,254,U1B,VDDS_DDR_2,VDDS_DDR,,,"/>
<approved hash="104,2,185.42,251.46,U1B,VDD_CORE_2,VDD_CORE,,,"/>
<approved hash="104,2,185.42,248.92,U1B,VSS_2,GND,,,"/>
<approved hash="104,2,185.42,246.38,U1B,VDD_CORE_3,VDD_CORE,,,"/>
<approved hash="104,2,185.42,243.84,U1B,VSS_3,GND,,,"/>
<approved hash="104,2,185.42,228.6,U1B,VDD_CORE_4,VDD_CORE,,,"/>
<approved hash="104,2,185.42,226.06,U1B,VDD_CORE_5,VDD_CORE,,,"/>
<approved hash="104,2,185.42,223.52,U1B,VDD_CORE_6,VDD_CORE,,,"/>
<approved hash="104,2,185.42,220.98,U1B,VDD_CORE_7,VDD_CORE,,,"/>
<approved hash="104,2,185.42,205.74,U1B,VSS_4,GND,,,"/>
<approved hash="104,2,185.42,203.2,U1B,VSS_5,GND,,,"/>
<approved hash="104,2,185.42,200.66,U1B,VSS_6,GND,,,"/>
<approved hash="104,2,185.42,198.12,U1B,VSS_7,GND,,,"/>
<approved hash="104,2,185.42,185.42,U1B,VDDSHV6_2,VDD_3V3A,,,"/>
<approved hash="104,2,185.42,182.88,U1B,VDDS_2,VDDS,,,"/>
<approved hash="104,2,185.42,180.34,U1B,VSS_8,GND,,,"/>
<approved hash="104,2,185.42,177.8,U1B,VDD_CORE_8,VDD_CORE,,,"/>
<approved hash="104,2,248.92,162.56,U1B,VDDSHV6_3,VDD_3V3A,,,"/>
<approved hash="104,2,248.92,165.1,U1B,VDDSHV6,VDD_3V3A,,,"/>
<approved hash="104,2,248.92,167.64,U1B,VDDSHV1_2,VDD_3V3A,,,"/>
<approved hash="104,2,248.92,170.18,U1B,VDDSHV1,VDD_3V3A,,,"/>
<approved hash="104,2,248.92,243.84,U1B,VSS,GND,,,"/>
<approved hash="104,2,71.12,116.84,U1C,VSS_2,GND,,,"/>
<approved hash="104,2,71.12,45.72,U1C,VDDSHV6_2,VDD_3V3A,,,"/>
<approved hash="104,2,71.12,43.18,U1C,VDDSHV6_3,VDD_3V3A,,,"/>
<approved hash="104,2,71.12,40.64,U1C,VDDSHV6_4,VDD_3V3A,,,"/>
<approved hash="104,2,71.12,38.1,U1C,VDDSHV6_5,VDD_3V3A,,,"/>
<approved hash="104,2,134.62,48.26,U1C,VDD_MPU_2,VDD_MPU,,,"/>
<approved hash="104,2,134.62,50.8,U1C,VDD_MPU_3,VDD_MPU,,,"/>
<approved hash="104,2,134.62,53.34,U1C,VDD_MPU_4,VDD_MPU,,,"/>
<approved hash="104,2,134.62,55.88,U1C,VDD_MPU_5,VDD_MPU,,,"/>
<approved hash="104,2,134.62,58.42,U1C,VDDSHV6_6,VDD_3V3A,,,"/>
<approved hash="104,2,134.62,71.12,U1C,VDD_CORE_2,VDD_CORE,,,"/>
<approved hash="104,2,134.62,73.66,U1C,VSS_3,GND,,,"/>
<approved hash="104,2,134.62,76.2,U1C,VSS_4,GND,,,"/>
<approved hash="104,2,134.62,78.74,U1C,VDD_MPU_6,VDD_MPU,,,"/>
<approved hash="104,2,134.62,81.28,U1C,VDDSHV6,VDD_3V3A,,,"/>
<approved hash="104,2,134.62,93.98,U1C,VSS_5,GND,,,"/>
<approved hash="104,2,134.62,96.52,U1C,VDD_CORE_3,VDD_CORE,,,"/>
<approved hash="104,2,134.62,99.06,U1C,VSS_6,GND,,,"/>
<approved hash="104,2,134.62,101.6,U1C,VDD_MPU_7,VDD_MPU,,,"/>
<approved hash="104,2,134.62,104.14,U1C,VDDSHV4_2,VDD_3V3A,,,"/>
<approved hash="104,2,134.62,106.68,U1C,VDDS_PLL_MPU,VDD_PLL,,,"/>
<approved hash="104,2,134.62,116.84,U1C,VSS_7,GND,,,"/>
<approved hash="104,2,134.62,119.38,U1C,VSS,GND,,,"/>
<approved hash="104,2,134.62,127,U1C,VDDSHV4,VDD_3V3A,,,"/>
<approved hash="104,2,190.5,137.16,U1D,VSS_2,GND,,,"/>
<approved hash="104,2,190.5,134.62,U1D,VSS_3,GND,,,"/>
<approved hash="104,2,190.5,132.08,U1D,VDD_CORE_2,VDD_CORE,,,"/>
<approved hash="104,2,190.5,129.54,U1D,VDDS_2,VDDS,,,"/>
<approved hash="104,2,190.5,127,U1D,VDDSHV5_2,VDD_3V3A,,,"/>
<approved hash="104,2,190.5,114.3,U1D,VSS_4,GND,,,"/>
<approved hash="104,2,190.5,111.76,U1D,VSS_5,GND,,,"/>
<approved hash="104,2,190.5,109.22,U1D,VSS_6,GND,,,"/>
<approved hash="104,2,190.5,106.68,U1D,VSS_7,GND,,,"/>
<approved hash="104,2,190.5,104.14,U1D,VDDSHV5,VDD_3V3A,,,"/>
<approved hash="104,2,190.5,91.44,U1D,VSS_8,GND,,,"/>
<approved hash="104,2,190.5,88.9,U1D,VDD_CORE_3,VDD_CORE,,,"/>
<approved hash="104,2,190.5,86.36,U1D,VSS_9,GND,,,"/>
<approved hash="104,2,190.5,83.82,U1D,VDD_CORE_4,VDD_CORE,,,"/>
<approved hash="104,2,190.5,81.28,U1D,VSSA_USB_2,GND,,,"/>
<approved hash="104,2,190.5,68.58,U1D,VSS_10,GND,,,"/>
<approved hash="104,2,190.5,66.04,U1D,VSS_11,GND,,,"/>
<approved hash="104,2,190.5,63.5,U1D,VDD_CORE_5,VDD_CORE,,,"/>
<approved hash="104,2,190.5,58.42,U1D,VSSA_USB,GND,,,"/>
<approved hash="104,2,190.5,55.88,U1D,VDDA3P3V_USB0,VDD_3V3A,,,"/>
<approved hash="104,2,190.5,53.34,U1D,VDDA1P8V_USB0,VDD_1V8,,,"/>
<approved hash="104,2,190.5,45.72,U1D,VDDSHV2_2,VDD_3V3A,,,"/>
<approved hash="104,2,190.5,43.18,U1D,VDDSHV2,VDD_3V3A,,,"/>
<approved hash="104,2,190.5,40.64,U1D,VDDSHV3_2,VDD_3V3A,,,"/>
<approved hash="104,2,190.5,38.1,U1D,VDDSHV3,VDD_3V3A,,,"/>
<approved hash="104,2,248.92,48.26,U1D,VDDS_PLL_CORE_LCD,VDD_PLL,,,"/>
<approved hash="104,2,248.92,50.8,U1D,VDDS_OSC,VDD_PLL,,,"/>
<approved hash="104,2,248.92,60.96,U1D,VDDA3P3V_USB1,VDD_3V3A,,,"/>
<approved hash="104,2,248.92,63.5,U1D,VDDA1P8V_USB1,VDD_1V8,,,"/>
<approved hash="104,2,248.92,119.38,U1D,VSS_OSC,GND,,,"/>
<approved hash="104,2,248.92,137.16,U1D,VSS,GND,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
